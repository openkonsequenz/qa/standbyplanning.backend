////
******************************************************************************
* Copyright © 2018 Mettenmeier GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*
*     http://www.eclipse.org/legal/epl-v10.html
*
******************************************************************************
////
= openKonsequenz "StandbyPlanning" - documentation for administration
:Date: 2019-01-10
:Revision: 1

What is needed?
    
    * KeyCloak
    * Postgres
    * oK-Auth-Application
        ** portalFE
        ** portal
        

=== KeyCloak[[KeyCloak]]

You need to have a running KeyCloak. All the user of the application standbyplanning have to be well-kept in KeyCloak. 
For minimal access use the role 
*BP_Leseberechtigte*.

Ensure you have configured a client. 


.Client
[source]
	Client ID:                      standbyplanning
	Name :                          Bereitschaftsplanung
	Enabled:                        true
	Consent Required:               false
	Client Protocol:                openid-connect
	Access Type :                   public
    Standard Flow Enabled:          true
    Implicit Flow Enabled:          false
    Direct Access Grants Enabled:   true
    Authorization Enabled:          false
    Root URL:               
    Valid Redirect URIs:            http://localhost:8080/*
    Base URL:                       http://localhost:8080/spfe/
    Admin URL:   
    Web Origins:                    *

Ensure you have configured the roles.
	
.Roles
[source]
    Role Name:              BP_Admin
    Scope Param Required:   false
    Composite Roles:        false
----
	Role Name:              BP_Gruppenleiter
	Scope Param Required:   false
	Composite Roles:        false
----	
	Role Name:              BP_Leseberechtigte
	Scope Param Required:   false
	Composite Roles:        false
----	
	Role Name:              BP_Sachbearbeiter
	Scope Param Required:   false
	Composite Roles:        false
	
	
=== Postgres

A postgres database and a user have to be created. After that all the needed tables and sequences will be genreated bei the "Standby Planning" backend (spbe). For more details see <<../howto/config#Postgres, Postgres config>>.


=== oK-Auth-Application

The auth&auth application is available from the oK Eclipse Git. Refere to https://git.eclipse.org for getting the code and more installation details.  
For more details how to configure the Standby Planning Application in the Auth-Application see <<../howto/config#AuthAuth, Auth&Auth>>
