////
******************************************************************************
* Copyright © 2018 Mettenmeier GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*
*     http://www.eclipse.org/legal/epl-v10.html
*
******************************************************************************
////
= openKonsequenz "StandbyPlanning" - documentation for user
:Date: 2018-06-29
:Revision: 1

The user documentation will be found online in the application StandbyPlanning. 

