////
*****************************************************************************
* Copyright © 2018 Mettenmeier GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*
*     http://www.eclipse.org/legal/epl-v10.html
*
*****************************************************************************
////
= openKonsequenz "StandbyPlanning" - How to run the module
:Date: 2018-06-07
:Revision: 1

== WAR deployment
Put the two war files on the server. 

* *spfe.war* "StandbyPlanning" frontend binaries.
* *spbe.war* "StandbyPlanning" backend binaries.

Restart the server. 

== define Keycloak URL
Define the URL to your openkonsequenz auth2auth modul. For your local development you can use http instead of https. 

.change url in spfe/main*.js
[source]
	loginPage:"https://localhost:8080/portalFE/#/login"
	
Define the base URL of the frontend application.

.change url in spfe/main*.js
[source]
	basePath:"https://localhost:8080/spbe/webapi"
	
== Login and Run [[login]]
After your deployment and your configuration you will start with this URL: 
"server":"port"/portalFE/#/login

