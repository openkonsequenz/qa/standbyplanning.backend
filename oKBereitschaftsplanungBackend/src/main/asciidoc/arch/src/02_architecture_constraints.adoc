[[section-architecture-constraints]]
== Architecture Constraints
TODO: LINK zum Eclipse Repository anpassen

The main architecture constraints are:

* *Public License* The module must be available under the "Eclipse Public License 2.0".
* *Availability* The source code of the module must be accessible to any interested person/company.
Therefore the project is published at https://projects.eclipse.org/projects/technology.openk-usermodules/developer


=== Technical Constraints

The following technical constraints are given:

.Technical Contraints
[options="header"]
|========================================================
|Component|Constraints
|Basis components of the reference platform
a|* Application Server Tomcat
* Database PostgreSQL

|Programming Language Backend
a|* Java 1.8
* REST/JSON Interfaces
* Eclipse Link as ORM
* JUnit + Mockito

|Programming Language Frontend
a|* Angular 6.1.0 (Javascript, Typescript, HTML5, CSS3)
* Bootstrap
* According to oK-GUI-Styleguide

|Java QA environment
a| * Sonarqube 6.4

|IDE
a|* Not restricted (Eclipse, Microsoft Visual Code ...)

|Build system
a|* Backend: Maven
* Frontend: NodeJS + Angular/cli

|Libraries, Frameworks,Components
a|* Used Libraries/Frameworks have to be compatible to the Eclipse Public License

|Architecture Documentation
a|* According ARC42-Template
|========================================================


=== Technical Dependencies

==== Modules
The following modules are required to use the 'Standby Planning':

.Modules
[options="header"]
|=========================================================
|Name of the module|Purpose|Status of the module
|'Auth&Auth'|Authentification and Authorization|available
|=========================================================

==== Libraries

*Frontend*
[width="100%",options="header,footer"]
|====================
|Dependency  |Reason for Usage
|@angular/* libraries  |Comes with Angular
|classlist.js, core-js, rxjs, web-animations-js, zone.js  |Comes with Angular
|@ng-bootstrap/ng-bootstrap  |This dependency provides a load of different bootstrap/angular components. Mostly we use the datepicker/timepicker but some other components found it's way into the application as well.
|ag-grid, ag-grid-angular  |This library is used to display tables. It provides functionality like filtering out of the box. This library was preferred by the Product Owner and therefore we used it.
|bootstrap  |As we want a responsive application in general there is no way around bootstrap in these days. Therefore we put it into our application.
|font-awesome  |To make our app more appealing to the users we are using font-awesome to get range of nice icons into our application.
|primeng/primeicons|PrimeNG is a huge library which offers different angular components like multiselect boxes, context menu or toast messages. These components are part of our application.

|====================

TODO: Pfad zu den Datein (Bibliotheken) ergaenzen.

The used libraries can be found here:

* Backend  
** pom.xml of the module
* Frontend
** package.json

 