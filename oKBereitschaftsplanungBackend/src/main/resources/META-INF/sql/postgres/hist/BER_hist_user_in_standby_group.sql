/*User_in_standby_group History*/
CREATE TABLE IF NOT EXISTS USER_IN_STANDBY_GROUP_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE USER_IN_STANDBY_GROUP EXCLUDING ALL);
--
-- Create a row in {0}.user_in_standby_group_hist_audit to reflect the operation performed on user_in_standby_group,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_user_in_standby_group_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.user_in_standby_group_hist SELECT nextval('user_in_standby_group_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.user_in_standby_group_hist SELECT nextval('user_in_standby_group_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.user_in_standby_group_hist SELECT nextval('user_in_standby_group_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS user_in_standby_group_audit on {0}.user_in_standby_group;
CREATE TRIGGER user_in_standby_group_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.user_in_standby_group FOR EACH ROW EXECUTE PROCEDURE process_user_in_standby_group_audit();