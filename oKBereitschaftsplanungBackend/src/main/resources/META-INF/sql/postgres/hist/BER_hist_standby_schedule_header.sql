/*Standby_schedule_header History*/
CREATE TABLE IF NOT EXISTS STANDBY_SCHEDULE_HEADER_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE STANDBY_SCHEDULE_HEADER EXCLUDING ALL);
--
-- Create a row in {0}.standby_schedule_header_hist_audit to reflect the operation performed on standby_schedule_header,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_standby_schedule_header_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.standby_schedule_header_hist SELECT nextval('standby_schedule_header_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.standby_schedule_header_hist SELECT nextval('standby_schedule_header_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.standby_schedule_header_hist SELECT nextval('standby_schedule_header_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS standby_schedule_header_audit on {0}.standby_schedule_header;
CREATE TRIGGER standby_schedule_header_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.standby_schedule_header FOR EACH ROW EXECUTE PROCEDURE process_standby_schedule_header_audit();