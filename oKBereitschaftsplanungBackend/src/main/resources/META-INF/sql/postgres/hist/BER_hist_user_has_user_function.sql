/*User_has_user_function History*/
CREATE TABLE IF NOT EXISTS USER_HAS_USER_FUNCTION_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE USER_HAS_USER_FUNCTION EXCLUDING ALL);
--
-- Create a row in {0}.user_has_user_function_hist_audit to reflect the operation performed on user_has_user_function,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_user_has_user_function_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.user_has_user_function_hist SELECT nextval('user_has_user_function_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.user_has_user_function_hist SELECT nextval('user_has_user_function_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.user_has_user_function_hist SELECT nextval('user_has_user_function_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS user_has_user_function_audit on {0}.user_has_user_function;
CREATE TRIGGER user_has_user_function_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.user_has_user_function FOR EACH ROW EXECUTE PROCEDURE process_user_has_user_function_audit();