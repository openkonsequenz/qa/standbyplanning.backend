/*Address History*/
CREATE TABLE IF NOT EXISTS ADDRESS_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE ADDRESS EXCLUDING ALL);
--
-- Create a row in {0}.address_hist_audit to reflect the operation performed on address,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_address_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.address_hist SELECT nextval('address_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.address_hist SELECT nextval('address_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.address_hist SELECT nextval('address_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS address_audit on {0}.address;
CREATE TRIGGER address_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.address FOR EACH ROW EXECUTE PROCEDURE process_address_audit();