/*Int_int_location_has_postcode History*/
CREATE TABLE IF NOT EXISTS INT_LOCATION_HAS_POSTCODE_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE INT_LOCATION_HAS_POSTCODE EXCLUDING ALL);
--
-- Create a row in {0}.int_location_has_postcode_hist_audit to reflect the operation performed on int_location_has_postcode,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_int_location_has_postcode_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.int_location_has_postcode_hist SELECT nextval('int_location_has_postcode_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.int_location_has_postcode_hist SELECT nextval('int_location_has_postcode_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.int_location_has_postcode_hist SELECT nextval('int_location_has_postcode_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS int_location_has_postcode_audit on {0}.int_location_has_postcode;
CREATE TRIGGER int_location_has_postcode_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.int_location_has_postcode FOR EACH ROW EXECUTE PROCEDURE process_int_location_has_postcode_audit();