/* {0}.contact_data */
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (1, '0123 456789', 'test@test.de', 'Y', '0123 456789', '0123 456789', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (2, '0123 987654', 'test@test.de', 'N', '0123 987654', '0123 111111', '01234656789');
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (3, '0123 456789', 'test@test.de', 'Y', '0123 456789', '0123 456789', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (4, '0123 987654', 'test@test.de', 'N', '0123 987654', '0123 111111', '01234656789');
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (5, '+NN 0123 987654', 'nn@test.de', 'N', '+NN 0123 987654', '+NN 0123 111111', '+NN 01234656789');
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (6, '+NN 0123 987654', 'nn@test.de', 'N', '+NN 0123 987654', '+NN 0123 111111', '+NN 01234656789');
/* Erweiterungen der E-Netz Südhessen - Anfang */
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (7, '(300) 435-6080', 'tes4t@test.de', 'N', '(975) 548-7621', '(219) 417-3285', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (8, '(994) 383-6544', 'te54st@test.de', 'N', '(587) 873-9757', '(433) 257-8877', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (9, '(250) 510-2504', 'tedfst@test.de', 'N', '(397) 744-7544', '(284) 522-7937', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (10, '(995) 537-8184', 'teszzt@test.de', 'N', '(949) 536-2798', '(520) 426-7025', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (11, '(982) 380-2632', 'teswet@test.de', 'N', '(376) 946-5895', '(748) 503-8563', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (12, '(407) 496-3663', 'tewsdst@test.de', 'N', '(557) 717-8956', '(617) 483-7693', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (13, '(245) 484-7550', 'tes97t@test.de', 'N', '(317) 995-1246', '(908) 909-7791', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (14, '(650) 677-6566', 'tes8t@test.de', 'N', '(356) 315-3457', '(965) 264-1810', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (15, '(378) 782-0711', 'te123st@test.de', 'N', '(377) 893-6911', '(861) 811-6658', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (16, '(686) 296-2695', 'te7t@test.de', 'N', '(434) 509-8384', '(435) 606-8637', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (17, '(881) 590-8507', 'te8jst@test.de', 'N', '(628) 730-1660', '(533) 278-9871', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (18, '(488) 286-7079', 'tesb4t@test.de', 'N', '(825) 643-6912', '(836) 591-6795', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (19, '(552) 867-8236', 'tesc8t@test.de', 'N', '(513) 474-8959', '(487) 393-7557', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (20, '(761) 979-7649', 'teswst@test.de', 'N', '(509) 634-5259', '(351) 451-2439', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (21, '(313) 815-2191', 'tiiest@test.de', 'N', '(325) 758-5887', '(446) 635-3725', '01234656789');	
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (22, '(789) 546-6892', 'tesoot@test.de', 'N', '(316) 615-1805', '(748) 235-7444', '01234656789');	
/* Erweiterungen der E-Netz Südhessen - Ende */	
/* Erweiterungen der N-ERGIE Anfang  */
INSERT INTO {0}.contact_data(id, cellphone, email, is_private, pager, phone, radiocomm) VALUES (23, '(789) 546-6892', 'tesoot@test.de', 'N', '(316) 615-1805', '(748) 235-7444', '01234656789');
/* Erweiterungen der N-ERGIE Ende*/
ALTER SEQUENCE {0}.CONTACT_DATA_ID_SEQ INCREMENT BY 23