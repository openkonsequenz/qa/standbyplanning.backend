-- {0}.user_function
INSERT INTO {0}.user_function (id, function_name) VALUES (1, 'Meister Betrieb Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (2, 'Fachkraft Betrieb Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (3, 'Ingenieure Strom/Gas/Wasser')
INSERT INTO {0}.user_function (id, function_name) VALUES (4, 'Meister Betrieb Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (5, 'Meister Betrieb Wasser')
INSERT INTO {0}.user_function (id, function_name) VALUES (6, 'Fachkraft Betrieb Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (7, 'Mitarbeiter ohne Bereitschaft')
INSERT INTO {0}.user_function (id, function_name) VALUES (8, 'STÖRUNGSVERANTWORTLICHE')
INSERT INTO {0}.user_function (id, function_name) VALUES (9, 'Fuhrpark')
-- Erweiterung der N-ERGIE
INSERT INTO {0}.user_function (id, function_name) VALUES (10, 'Arbeitsvorbereiter Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (11, 'Blaulichtfahrzeug 16:00 - 7:00')
INSERT INTO {0}.user_function (id, function_name) VALUES (12, 'Blaulichtfahrzeug Besetzung arbeitstäglich 7:00 - 16:00')
INSERT INTO {0}.user_function (id, function_name) VALUES (13, 'Einsatzleiter Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (14, 'Einsatzleiter Fernwärme')
INSERT INTO {0}.user_function (id, function_name) VALUES (15, 'Einsatzleiter Gas Region ROT, GDRM-Anlagen')
INSERT INTO {0}.user_function (id, function_name) VALUES (16, 'Einsatzleiter Gas/Wasser  Nürnberg Stadt')
INSERT INTO {0}.user_function (id, function_name) VALUES (17, 'Einsatzleiter Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (18, 'Einsatzleiter Strom / Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (19, 'Einsatzleiter Strom Nürnberg Stadt')
INSERT INTO {0}.user_function (id, function_name) VALUES (20, 'Elektriker Gas/Wasser (Mess- und Regelwesen)')
INSERT INTO {0}.user_function (id, function_name) VALUES (21, 'Fachfirma 1')
INSERT INTO {0}.user_function (id, function_name) VALUES (22, 'Fachfirma 2')
INSERT INTO {0}.user_function (id, function_name) VALUES (23, 'Fahrer')
INSERT INTO {0}.user_function (id, function_name) VALUES (24, 'Fahrer GW')
INSERT INTO {0}.user_function (id, function_name) VALUES (25, 'Fremdfirma  Nord')
INSERT INTO {0}.user_function (id, function_name) VALUES (26, 'Fremdfirma  Süd')
INSERT INTO {0}.user_function (id, function_name) VALUES (27, 'FW-Monteur Leitungen Dampf/Heißwasser')
INSERT INTO {0}.user_function (id, function_name) VALUES (28, 'FW-Monteur Netzstation Dampf/Heißwasser')
INSERT INTO {0}.user_function (id, function_name) VALUES (29, 'FW-Monteur Übergabestation Dampf')
INSERT INTO {0}.user_function (id, function_name) VALUES (30, 'FW-Monteur Übergabestation Heißwasser')
INSERT INTO {0}.user_function (id, function_name) VALUES (31, 'FW-Station elektrischer  Teil MSR')
INSERT INTO {0}.user_function (id, function_name) VALUES (32, 'Lecksuche G/W')
INSERT INTO {0}.user_function (id, function_name) VALUES (33, 'LKW und Sonderfahrzeuge')
INSERT INTO {0}.user_function (id, function_name) VALUES (34, 'Logistik')
INSERT INTO {0}.user_function (id, function_name) VALUES (35, 'Messtechnik 1')
INSERT INTO {0}.user_function (id, function_name) VALUES (36, 'Messtechnik 2')
INSERT INTO {0}.user_function (id, function_name) VALUES (37, 'Monteur  Gas  Sulzbach -Rosenberg')
INSERT INTO {0}.user_function (id, function_name) VALUES (38, 'Monteur GW1')
INSERT INTO {0}.user_function (id, function_name) VALUES (39, 'Monteur GW2')
INSERT INTO {0}.user_function (id, function_name) VALUES (40, 'Monteur GW3')
INSERT INTO {0}.user_function (id, function_name) VALUES (41, 'Monteur Kommunikationsnetz')
INSERT INTO {0}.user_function (id, function_name) VALUES (42, 'Monteur Schaltanlagen/Umspannwerke Wochenenddienst')
INSERT INTO {0}.user_function (id, function_name) VALUES (43, 'Monteur Schaltanlagen/Umspannwerke')
INSERT INTO {0}.user_function (id, function_name) VALUES (44, 'Monteur Strom Stadt')
INSERT INTO {0}.user_function (id, function_name) VALUES (45, 'Monteur Strom Stadt1')
INSERT INTO {0}.user_function (id, function_name) VALUES (46, 'Monteur Strom Stadt2')
INSERT INTO {0}.user_function (id, function_name) VALUES (47, 'Monteur Strom Stadt3')
INSERT INTO {0}.user_function (id, function_name) VALUES (48, 'Netzmonteur NSG-NW-NN')
INSERT INTO {0}.user_function (id, function_name) VALUES (49, 'Netzmonteur NSG-NW-NO')
INSERT INTO {0}.user_function (id, function_name) VALUES (50, 'Netzmonteur NSG-NW-NS')
INSERT INTO {0}.user_function (id, function_name) VALUES (51, 'Netzmonteur NSG-NW-NW')
INSERT INTO {0}.user_function (id, function_name) VALUES (52, 'NM Monteur')
INSERT INTO {0}.user_function (id, function_name) VALUES (53, 'NM Monteur "Zusatz"')
INSERT INTO {0}.user_function (id, function_name) VALUES (54, 'NM Monteur Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (55, 'NM Monteur Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (56, 'NN Monteur "Zusatz"')
INSERT INTO {0}.user_function (id, function_name) VALUES (57, 'NN Monteur Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (58, 'NN Monteur Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (59, 'NO Monteur "Zusatz"')
INSERT INTO {0}.user_function (id, function_name) VALUES (60, 'NO Monteur Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (61, 'NO Monteur Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (62, 'NS Monteur "Zusatz"')
INSERT INTO {0}.user_function (id, function_name) VALUES (63, 'NS Monteur Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (64, 'NS Monteur Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (65, 'NW Monteur "Zusatz"')
INSERT INTO {0}.user_function (id, function_name) VALUES (66, 'NW Monteur Gas')
INSERT INTO {0}.user_function (id, function_name) VALUES (67, 'NW Monteur Strom')
INSERT INTO {0}.user_function (id, function_name) VALUES (68, 'Reglermonteur 1')
INSERT INTO {0}.user_function (id, function_name) VALUES (69, 'Reglermonteur 2')
INSERT INTO {0}.user_function (id, function_name) VALUES (70, 'Schweißer')
INSERT INTO {0}.user_function (id, function_name) VALUES (71, 'Sonderfuhrpark')
INSERT INTO {0}.user_function (id, function_name) VALUES (72, 'Strom 1  NSG-NW-NW')
INSERT INTO {0}.user_function (id, function_name) VALUES (73, 'Strom 1 NSG-NW-NN')
INSERT INTO {0}.user_function (id, function_name) VALUES (74, 'Strom 1 NSG-NW-NO')
INSERT INTO {0}.user_function (id, function_name) VALUES (75, 'Strom 1 NSG-NW-NS')
INSERT INTO {0}.user_function (id, function_name) VALUES (76, 'Tiefbau 1')
INSERT INTO {0}.user_function (id, function_name) VALUES (77, 'Tiefbau 2')
INSERT INTO {0}.user_function (id, function_name) VALUES (78, 'Zusatz Gas  NSG-NW-NW')
INSERT INTO {0}.user_function (id, function_name) VALUES (79, 'Zusatz Strom NSG-NW-NN')
INSERT INTO {0}.user_function (id, function_name) VALUES (80, 'Zusatz Strom NSG-NW-NS')
INSERT INTO {0}.user_function (id, function_name) VALUES (81, 'Zusatz Strom NSG-NW-NW')
INSERT INTO {0}.user_function (id, function_name) VALUES (82, 'Zusatz Strom NSG-NW-NO')
ALTER SEQUENCE {0}.USER_FUNCTION_ID_SEQ INCREMENT BY 83