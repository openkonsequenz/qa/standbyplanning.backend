/* public.standby_group */
INSERT INTO {0}.standby_group(id, extend_standby_time, modification_date, next_user_in_next_cycle, note,  title) VALUES (1, 'Y', '2018-08-06 02:00:00', 'N', 'Test-Notiz',  'group1');
INSERT INTO {0}.standby_group(id, extend_standby_time, modification_date, next_user_in_next_cycle, note,  title) VALUES (2, 'N', '2018-08-06 02:00:00', 'N', 'Test-Notiz',  'group2');
INSERT INTO {0}.standby_group(id, extend_standby_time, modification_date, next_user_in_next_cycle, note,  title) VALUES (3, 'N', '2018-08-06 02:00:00', 'Y', 'Test-Notiz',  'Sondergruppe');
INSERT INTO {0}.standby_group(id, extend_standby_time, modification_date, next_user_in_next_cycle, note,  title) VALUES (4, 'N', '2018-08-06 02:00:00', 'Y', 'Test-Notiz',  'group4');
INSERT INTO {0}.standby_group(id, extend_standby_time, modification_date, next_user_in_next_cycle, note,  title) VALUES (5, 'N', '2018-08-06 02:00:00', 'Y', 'Test-Notiz',  'group5');
SELECT setval('{0}.standby_group_id_seq', 5, true);