/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.resources;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.controller.BaseWebService;
import org.eclipse.openk.core.controller.TokenManager;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.controller.external.AuthController;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseResource extends BaseWebService {

	private boolean developMode;
	private static final String LET_ME_IN = "LET_ME_IN";

	@Autowired
	AuthController authController;

	public BaseResource(Logger logger, FileHelper fileHelper) {
		super(logger, fileHelper);

		init(Globals.APP_PROP_FILE_NAME, Globals.PROP_VERSION);

	}

	protected void isDevelopMode(boolean devMode) {
		this.developMode = devMode;
	}

	@Override
	protected void assertAndRefreshToken(String token, String[] secureType) throws HttpStatusException {
		if (isBackdoor(token)) {
			return;
		}

		String baseURLPortal = "";
		String useHttps = "";

		try {
			baseURLPortal = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_BASE_URL);
			useHttps = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_USE_HTTPS);

		} catch (Exception e) {
			super.logger.error(e);
		}

		checkAuth(useHttps, baseURLPortal, token, secureType);
	}

	protected void checkAuth(String useHttps, String baseURL, String token, String[] secureType)
			throws HttpStatusException {

		try {

			boolean https = Boolean.parseBoolean(useHttps);

			HttpResponse response = authController.performGetRequest(https, baseURL, "checkAuth", token);
			authController.validateResponse(response);

			TokenManager.getInstance().checkAutLevel(token, secureType);

		} catch (Exception e) {
			super.logger.error("authentication failed on external service! ", e);
			throw new HttpStatusException(HttpStatus.SC_UNAUTHORIZED);
		}
	}

	protected boolean isBackdoor(String token) {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"
		return developMode && LET_ME_IN.equals(token);
	}

	protected String init(String appProp, String propertie) {
		String beversion = "";
		try {
			beversion = fileHelper.getProperty(appProp, propertie);
		} catch (Exception e) {
			super.logger.error(e, e);
			return null;
		}
		if (beversion.contains("DEVELOP") || beversion.contains("SNAPSHOT")) {
			developMode = true;
		} else {
			developMode = false;
		}

		return beversion;
	}
}
