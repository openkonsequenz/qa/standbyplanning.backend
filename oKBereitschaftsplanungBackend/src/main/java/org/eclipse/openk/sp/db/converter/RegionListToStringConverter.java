/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.CustomConverter;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.UserInRegion;

public class RegionListToStringConverter implements CustomConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
		if (source instanceof List) {
			return listUserInRegionToString((List) source);
		} else if (source instanceof String) {
			return stringToRegionList((String) source);
		}
		return null;
	}

	public String listUserInRegionToString(List source) {
		StringBuilder resultBuf = new StringBuilder("");
		for (Object object : source) {
			UserInRegion userInRegion = (UserInRegion) object;
			if (!resultBuf.toString().equals("")) {
				resultBuf.append(",");
			}
			resultBuf.append(userInRegion.getRegion().getRegionName());
		}

		return resultBuf.toString();
	}

	public List<Region> stringToRegionList(String source) {
		List<Region> lsRegion = new ArrayList<>();

		if (source.contains(",")) {
			String[] arr = source.split(",");
			for (String str : arr) {
				addStandbyGroup(lsRegion, str);
			}
		} else {
			addStandbyGroup(lsRegion, source);
		}
		return lsRegion;
	}

	public List<Region> addStandbyGroup(List<Region> lsRegion, String str) {
		Region r = new Region();
		r.setRegionName(str);
		lsRegion.add(r);

		return lsRegion;
	}
}
