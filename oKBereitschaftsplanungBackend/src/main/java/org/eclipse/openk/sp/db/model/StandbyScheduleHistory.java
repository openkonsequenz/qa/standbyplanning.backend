/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "STANDBY_SCHEDULE_HISTORY" database table.
 */
@Entity
@Table(name = "STANDBY_SCHEDULE_HISTORY")
public class StandbyScheduleHistory extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_SCHEDULE_HIST_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_SCHEDULE_HIST_ID_SEQ", sequenceName = "STANDBY_SCHEDULE_HIST_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@Column(name = "changed_by", nullable = false)
	private String changedBy;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "schedule_header", nullable = false)
	private StandbyScheduleHeader standbyScheduleHeader;

	@Column(name = "file_hash_value", nullable = false)
	private String fileHashValue;

	@Lob
	@Column(name = "file_data", nullable = false)
	private byte[] file;

	public StandbyScheduleHistory() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the changedBy
	 */
	public String getChangedBy() {
		return changedBy;
	}

	/**
	 * @param changedBy
	 *            the changedBy to set
	 */
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	/**
	 * @return the standbyScheduleHeader
	 */
	public StandbyScheduleHeader getStandbyScheduleHeader() {
		return standbyScheduleHeader;
	}

	/**
	 * @param standbyScheduleHeader
	 *            the standbyScheduleHeader to set
	 */
	public void setStandbyScheduleHeader(StandbyScheduleHeader standbyScheduleHeader) {
		this.standbyScheduleHeader = standbyScheduleHeader;
	}

	/**
	 * @return the fileHashValue
	 */
	public String getFileHashValue() {
		return fileHashValue;
	}

	/**
	 * @param fileHashValue
	 *            the fileHashValue to set
	 */
	public void setFileHashValue(String fileHashValue) {
		this.fileHashValue = fileHashValue;
	}

	/**
	 * @return the file
	 */
	public byte[] getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(byte[] file) {
		this.file = file;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}
