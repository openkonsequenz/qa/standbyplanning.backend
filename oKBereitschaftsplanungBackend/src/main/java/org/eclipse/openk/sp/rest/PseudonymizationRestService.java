/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.rest;



import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.controller.ResponseBuilderWrapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.PseudonymizationController;
import org.eclipse.openk.sp.dto.NoopResultDto;
import org.eclipse.openk.sp.dto.PseudonymizationResultDto;
import org.eclipse.openk.sp.exceptions.SpExceptionMapper;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;



@Path("pseudonymize")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PseudonymizationRestService extends BaseResource {

	// ================================================================
	// == Constants
	// ================================================================

	// ================================================================
	// == Variables
	// ================================================================

	@Autowired
	private PseudonymizationController pseudonymizationController;

	// ================================================================
	// == Constructors
	// ================================================================

	public PseudonymizationRestService() {
		super(Logger.getLogger(PseudonymizationRestService.class.getName()), new FileHelper());
	}

	// ================================================================
	// == Helper Methods
	// ================================================================

	public Response responseFromException(Exception e) {
		int errcode;
		String retJson;

		if (e instanceof HttpStatusException) {
			logger.error("Caught BackendException", e);
			errcode = ((HttpStatusException) e).getHttpStatus();
			retJson = SpExceptionMapper.toJson((HttpStatusException) e);
			return Response.status(errcode).entity(retJson).build();
		} else {
			logger.error("Unexpected exception", e);
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(SpExceptionMapper.getGeneralErrorJson())
					.build();
		}
	}

	// ================================================================
	// == REST Endpoints
	// ================================================================

	@ApiOperation(value = "PseudonymizationResult", notes = "This services pseudonymizes user data and returns some status information about the performed pseudonymization.")
	@GET
	@Path("/user/{id}")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response pseudonymize1(
			@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt
	) {

		final boolean bDoModify = false;
		ModifyingInvokable<PseudonymizationResultDto> invokable = pseudonymizeUser -> pseudonymizationController.pseudonymize(id, bDoModify);

		String[] securityRoles = Globals.getAllRolls();
		// String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@ApiOperation(value = "PseudonymizationResult", notes = "This services pseudonymizes user data and returns some status information about the performed pseudonymization.")
	@GET
	@Path("/user/{id}/{doModify}")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response pseudonymize2(
			@PathParam("id") Long id,
			@PathParam("doModify") Boolean paramDoModify,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt
	) {

		final boolean bDoModify = (paramDoModify == null) ? false : (boolean)paramDoModify;
		ModifyingInvokable<PseudonymizationResultDto> invokable = pseudonymizeUser -> pseudonymizationController.pseudonymize(id, bDoModify);

		String[] securityRoles = Globals.getAllRolls();
		// String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@ApiOperation(value = "NoopResult", notes = "No operation.")
	@GET
	@Path("/noop")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response noop() {
		try {
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse("{}");
		} catch (Exception ee) {
			return responseFromException(ee);
		}
	}

	@ApiOperation(value = "NoopResult", notes = "No operation.")
	@GET
	@Path("/noopWithJWT")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response noopWithJWT(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt
	) {

		ModifyingInvokable<NoopResultDto> invokable = pseudonymizeUser -> pseudonymizationController.noop();

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@ApiOperation(value = "NoopResult", notes = "No operation.")
	@GET
	@Path("/noopWithJWTAll")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response noopWithJWTAll(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt
	) {

		ModifyingInvokable<NoopResultDto> invokable = pseudonymizeUser -> pseudonymizationController.noop();

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
