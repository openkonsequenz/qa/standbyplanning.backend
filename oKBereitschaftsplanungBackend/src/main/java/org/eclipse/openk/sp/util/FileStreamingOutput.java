/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

public class FileStreamingOutput implements StreamingOutput {

	private File file;

	public FileStreamingOutput(File file) {
		this.file = file;
	}

	@Override
	public void write(OutputStream output) throws IOException {

		try (FileInputStream input = new FileInputStream(file)) {
			int bytes;
			while ((bytes = input.read()) != -1) {
				output.write(bytes);
			}
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}

}
