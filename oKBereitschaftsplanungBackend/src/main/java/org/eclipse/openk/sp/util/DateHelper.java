/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.joda.time.MutableInterval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateHelper {

	private static final int CONST_WEEK = 7;

	/**
	 * The one and only locale this application supports.
	 */
	private static final Locale LOCALE = Locale.GERMANY;

	/**
	 * Patterns for date parsing.
	 * 
	 * @see #parseDate()
	 */
	private static final SimpleDateFormat[] DATE_FORMATS = new SimpleDateFormat[] {
			new SimpleDateFormat("yyyy-M-d", LOCALE), new SimpleDateFormat("yyyy-M-dd", LOCALE),
			new SimpleDateFormat("yyyy-MM-d", LOCALE), new SimpleDateFormat("yyyy-MM-dd", LOCALE) };

	/** default private constructor. */
	private DateHelper() {

	}

	/**
	 * This method parses dates specified as strings and returns dates with German
	 * time zone. This method is to be preferred as it fixes the time zone to
	 * Germany.
	 * 
	 * @param sDate Input values such as "2022-09-31", "2022-9-31", "2022-09-1",
	 *              "2022-9-1"
	 * @return Returns a corresponding date object with German time zone.
	 * @throws ParseException If the date can not be parsed a ParseException is
	 *                        raised.
	 */
	public static Date parseDate(String sDate) throws ParseException {
		sDate = sDate.trim();
		if (sDate.length() == 0) {
			throw new ParseException("Failed to parse date!", 0);
		}

		for (SimpleDateFormat f : DATE_FORMATS) {
			try {
				return f.parse(sDate);
			} catch (ParseException ee) {
			}
		}

		throw new ParseException("Failed to parse date!", 0);
	}

	/**
	 * function to calculate with Dates:
	 * 
	 * @author weber
	 */
	public static Date addDaysToDate(Date date, Integer days) {
		DateTime dateTime = new DateTime(date);
		DateTime result = dateTime.plusDays(days);
		return result.toDate();
	}

	public static Date addYearsToDate(Date date, int years) {
		DateTime dateTime = new DateTime(date);
		DateTime result = dateTime.plusYears(years);
		return result.toDate();
	}

	public static Date addHoursToDate(Date date, Integer hours) {
		DateTime dateTime = new DateTime(date);
		DateTime result = dateTime.plusHours(hours);
		return result.toDate();
	}

	/**
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 * @author mx
	 */
	public static String convertToString(Date date, String pattern) {

		DateTime dateTime = new DateTime(date);

		if (pattern == null) {
			pattern = "dd.MM.yyyy";
		}
		DateTimeFormatter jodaDTF = DateTimeFormat.forPattern(pattern);

		return dateTime.toString(jodaDTF);
	}

	/**
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 * @author mx
	 */
	public static String convertToLocalString(Date date, String pattern) {

		GregorianCalendar calendar = new GregorianCalendar(Locale.GERMANY);
		calendar.setTime(date);
		LocalDateTime localDate = LocalDateTime.fromDateFields(calendar.getTime());

		if (pattern == null) {
			pattern = "EE. dd.MM.yy HH:mm";
		}

		DateTimeFormatter jodaDTF = DateTimeFormat.forPattern(pattern);

		return localDate.toString(jodaDTF);
	}

	/**
	 * Method to return the day of week.
	 * 
	 * @param date
	 * @return
	 */
	public static int getDayOfWeek(Date date) {
		DateTime dateTime = new DateTime(date);
		dateTime.dayOfWeek().getAsText();
		return dateTime.getDayOfWeek();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static String getDayOfWeekText(Date date) {
		return getDayOfWeekText(date, null);
	}

	/**
	 * 
	 * @param date
	 * @param locale
	 * @return
	 */
	public static String getDayOfWeekText(Date date, Locale locale) {
		DateTime dateTime = new DateTime(date);

		if (locale == null) {
			locale = Locale.GERMAN;
		}
		return dateTime.dayOfWeek().getAsText(locale);
	}

	/**
	 * Method to calculate the day between to week days. startDay - endDay. If
	 * endDay int value is smaller add a week (7) for calculating.
	 * 
	 * @param startWeekDayInt
	 * @param endWeekDayInt
	 * @return
	 */
	public static int calculateDifferenceOfDays(int startWeekDayInt, int endWeekDayInt, Date dayFrom, Date dayTo) {
		if (startWeekDayInt == endWeekDayInt) {
			if (DateHelper.isDateBefore(dayFrom, dayTo)) {
				return 0;
			} else {
				return 7;
			}
		} else if (startWeekDayInt < endWeekDayInt) {
			return endWeekDayInt - startWeekDayInt;
		} else {
			return endWeekDayInt + CONST_WEEK - startWeekDayInt;
		}
	}

	/**
	 * Method to set the time of the date to '00:00:00' of next day.
	 * 
	 * @param date
	 * @return
	 */
	public static Date getEndOfDay(Date date) {
		date = DateHelper.addDaysToDate(date, 1);
		return getDateAt(date, 0, 0, 0);
	}

	public static Date getEndOfThisDay(Date date) {
		DateTime dateTime = new DateTime(date);
		return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), 0, 0, 0).toDate();
	}

	/**
	 * Method to set the time of the date to '00:00:00'.
	 * 
	 * @param date
	 * @return
	 */
	public static Date getStartOfDay(Date date) {
		return getDateAt(date, 0, 0, 0);
	}

	public static Date getDateAt(Date date, Integer... hms) {
		int h = 0;
		int m = 0;
		int s = 0;

		if (hms != null && (hms instanceof Integer[])) {
			Integer[] int_hms = (Integer[]) hms;

			if (int_hms.length >= 1 && hms[0] != null) {
				h = hms[0];
			}

			if (int_hms.length >= 2 && hms[1] != null) {
				m = hms[1];
			}

			if (int_hms.length >= 3 && hms[2] != null) {
				s = hms[2];
			}

		}

		DateTime dateTime = new DateTime(date);
		return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), h, m, s).toDate();
	}

	/**
	 * Method to set the time of the date to '00:00:01'.
	 * 
	 * @param date
	 * @return
	 */
	public static Date getStartOfDayPlus1(Date date) {
		DateTime dateTime = new DateTime(date);
		int sec = 5;
		int min = 0;
		return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), 0, min, sec)
				.toDate();
	}

	/**
	 * Method to set the time of timeOfDate as time for the date.
	 * 
	 * @param date
	 * @param timeOfDate
	 * @return
	 */
	public static Date getDateWithTime(Date date, Date timeOfDate) {
		DateTime dateTime = new DateTime(date);
		DateTime time = new DateTime(timeOfDate);
		return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(),
				time.getHourOfDay(), time.getMinuteOfHour(), time.getSecondOfMinute()).toDate();
	}

	/**
	 * Method to set the time of timeOfDate as time for the date.
	 * 
	 * @param date
	 * @param timeOfDate
	 * @return
	 */
	public static Date getDateWithTime(Date date, int hour, int minute) {
		DateTime dateTime = new DateTime(date);

		return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), hour, minute, 0)
				.toDate();
	}

	/**
	 * Method to check if the given date is after the comparativeDate.
	 * 
	 * @param checkDate
	 * @param comparativeDate
	 * @return
	 */
	public static Boolean isDateAfter(Date checkDate, Date comparativeDate) {
		DateTime checkObj = new DateTime(checkDate);
		DateTime compareObj = new DateTime(comparativeDate);
		return checkObj.isAfter(compareObj.getMillis());
	}

	public static boolean isSameDate(Date checkDate, Date comparativeDate) {
		boolean equalDay = true;
		DateTime checkObj = new DateTime(checkDate);
		DateTime compareObj = new DateTime(comparativeDate);
		if (checkObj.getYear() == compareObj.getYear() && checkObj.getDayOfYear() == compareObj.getDayOfYear()) {
			equalDay = true;
		} else {
			equalDay = false;
		}
		return equalDay;
	}

	/**
	 * Method to compare Dates bei ms.
	 * 
	 * @param checkDate
	 * @param comparativeDate
	 * @return -1 : if checkDate is earlier then comparativeDate </br>
	 *         0 : if the dates are equal </br>
	 *         1 : if checkDate is after comparativeDate
	 */
	public static int isSameDateTime(Date checkDate, Date comparativeDate) {
		DateTime checkObj = new DateTime(checkDate);
		DateTime compareObj = new DateTime(comparativeDate);
		if (checkObj.getMillis() < compareObj.getMillis()) {
			return -1;
		} else if (checkObj.getMillis() == compareObj.getMillis()) {
			return 0;
		} else {
			return 1;
		}
	}

	/**
	 * Method to check if the given date is before the comparativeDate.
	 * 
	 * @param checkDate
	 * @param comparativeDate
	 * @return
	 */
	public static Boolean isDateBefore(Date checkDate, Date comparativeDate) {
		DateTime checkObj = new DateTime(checkDate);
		DateTime compareObj = new DateTime(comparativeDate);
		return checkObj.isBefore(compareObj.getMillis());
	}

	public static Date getDate(int year, int month, int day, int hour, int minute, int second) {
		DateTime dateObj = new DateTime(year, month, day, hour, minute, second);

		return dateObj.toDate();
	}

	public static Date getDate(int year, int month, int day) {
		return getDate(year, month, day, 0, 0, 0);
	}

	public static Date getDateFromLabel(String label) {
		String[] arr = label.split(",");
		return DateHelper.convertStringToDate(arr[0]);

	}

	private static Date convertStringToDate(String string) {
		return convertStringToDate(string, null);
	}

	private static Date convertStringToDate(String string, String format) {
		if (format == null) {
			format = "dd.MM.yyyy";
		}
		DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
		DateTime result = DateTime.parse(string, formatter);
		return result.toDate();
	}

	public static String getLabelFormDate(Date date) {
		return DateHelper.convertToString(date, null) + ", " + DateHelper.getDayOfWeekText(date);
	}

	public static Date getDateFromString(String dateString, String inputPattern) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(inputPattern);
		return sdf.parse(dateString);
	}

	/**
	 * Method to check if the coparedInterval is fully covered by the list of
	 * intervals.
	 * 
	 * @param comparedInterval
	 * @param lsIntervals
	 * @return
	 */
	public static boolean isCoveredBy(Interval comparedInterval, List<Interval> lsIntervals) {

		final MutableInterval mutableInterval = new MutableInterval(comparedInterval);
		for (final Interval interval : lsIntervals) {
			if (interval.getStartMillis() <= mutableInterval.getStartMillis()
					&& interval.getEndMillis() > mutableInterval.getStartMillis()) {
				if (interval.getEndMillis() > mutableInterval.getEndMillis()) {
					return true;
				}
				mutableInterval.setStartMillis(interval.getEndMillis());
			}
		}
		return mutableInterval.getStartMillis() == mutableInterval.getEndMillis();
	}
}
