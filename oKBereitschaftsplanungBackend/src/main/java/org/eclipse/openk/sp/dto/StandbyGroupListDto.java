/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.db.model.StandbyList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBYGROUP" Data Transfer Object (DTO)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "StandbyGroupDto")
@JsonInclude(Include.NON_NULL)
public class StandbyGroupListDto extends AbstractDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * default serial id.
	 */

	private Long id;
	private String title;
	private String note;
	private Date modificationDate;
	private Boolean nextUserInNextCycle;
	private List<StandbyList> lsStandbyLists = new ArrayList<>();

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the nextUserInNextCycle
	 */
	public Boolean getNextUserInNextCycle() {
		return nextUserInNextCycle;
	}

	/**
	 * @param nextUserInNextCycle
	 *            the nextUserInNextCycle to set
	 */
	public void setNextUserInNextCycle(Boolean nextUserInNextCycle) {
		this.nextUserInNextCycle = nextUserInNextCycle;
	}

	/**
	 * @return the lsStandbyLists
	 */
	public List<StandbyList> getLsStandbyLists() {
		return lsStandbyLists;
	}

	/**
	 * @param lsStandbyLists the lsStandbyLists to set
	 */
	public void setLsStandbyLists(List<StandbyList> lsStandbyLists) {
		this.lsStandbyLists = lsStandbyLists;
	}

}