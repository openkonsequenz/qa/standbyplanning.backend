/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.annotations.Compress;
import org.eclipse.openk.sp.controller.planning.ArchiveController;
import org.eclipse.openk.sp.dto.ArchiveStandbyScheduleHeaderDto;
import org.eclipse.openk.sp.dto.ArchiveStandbyScheduleHeaderSelectionDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("archive")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ArchiveStandbyScheduleRestService extends BaseResource {

	@Autowired
	private ArchiveController aCo;

	public ArchiveStandbyScheduleRestService() {
		super(Logger.getLogger(ArchiveStandbyScheduleRestService.class.getName()), new FileHelper());
	}

	@PUT
	@Path("/standbylist/{statusId}")
	public Response createByList(@PathParam("statusId") Long statusId,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleFilterDto standbyScheduleFilterDto) {

		ModifyingInvokable<ArchiveStandbyScheduleHeaderDto> invokable = modusr -> aCo
				.manualCreateByStandbyList(standbyScheduleFilterDto, statusId, modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/standbygroup/{statusId}")
	public Response createByGroup(@PathParam("statusId") Long statusId,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleFilterDto standbyScheduleFilterDto) {

		ModifyingInvokable<ArchiveStandbyScheduleHeaderDto> invokable = modusr -> aCo
				.createByStandbyGroup(standbyScheduleFilterDto, statusId, modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/list")
	public Response getAllArchiveHeader(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<ArchiveStandbyScheduleHeaderSelectionDto>> invokable = modusr -> aCo.getAll();

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Compress
	@Path("/{id}")
	public Response getOneArchiveHeader(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<ArchiveStandbyScheduleHeaderDto> invokable = modusr -> aCo.getOneArchiveHeader(id);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/search")
	public Response getSearchArchiveHeader(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleSearchDto searchDto) {

		ModifyingInvokable<List<ArchiveStandbyScheduleHeaderSelectionDto>> invokable = modusr -> aCo.search(searchDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN, Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
