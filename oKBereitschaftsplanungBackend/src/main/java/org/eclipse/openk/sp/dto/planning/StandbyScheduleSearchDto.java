/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.TimeDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBY_SCHEDULE_SEARCH" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleSearchDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleSearchDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Date dateIndex;
	private TimeDto timeIndex;
	private String city;
	private Long locationId;
	private List<BranchDto> lsBranch;
	private List<BranchDto> lsBranchSelected;
	private Long listId;

	/**
	 * @return the dateIndex
	 */
	public Date getDateIndex() {
		return dateIndex;
	}

	/**
	 * @param dateIndex the dateIndex to set
	 */
	public void setDateIndex(Date dateIndex) {
		this.dateIndex = dateIndex;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public List<BranchDto> getLsBranch() {
		return lsBranch;
	}

	public void setLsBranch(List<BranchDto> lsBranch) {
		this.lsBranch = lsBranch;
	}

	public TimeDto getTimeIndex() {
		return timeIndex;
	}

	public void setTimeIndex(TimeDto timeIndex) {
		this.timeIndex = timeIndex;
	}

	public List<BranchDto> getLsBranchSelected() {
		return lsBranchSelected;
	}

	public void setLsBranchSelected(List<BranchDto> lsBranchSelected) {
		this.lsBranchSelected = lsBranchSelected;
	}

	public Long getListId() {
		return listId;
	}

	public void setListId(Long listId) {
		this.listId = listId;
	}

}
