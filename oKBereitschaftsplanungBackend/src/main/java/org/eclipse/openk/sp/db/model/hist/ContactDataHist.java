/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.db.model.hist;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "CONTACT_DATA_HIST" database table.
 *
 * @author Jürgen Knauth
 */
@Entity
@Table(name = "CONTACT_DATA_HIST")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class ContactDataHist extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------

	@Id
	@Column(name = "hist_id", updatable = false)
	private Long histId;

	@Column(name = "operation", nullable = true, updatable = false)
	private String operation;

	@Column(name = "stamp", nullable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date stamp;

	// ----------------------------------------------------------------

	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "is_private", nullable = false)
	@Convert("booleanConverter")
	private Boolean isPrivate;

	@Column(name = "phone", length = 256, nullable = false)
	private String phone;

	@Column(name = "cellphone", length = 256, nullable = true)
	private String cellphone;

	@Column(name = "radiocomm", length = 256, nullable = true)
	private String radiocomm;

	@Column(name = "email", length = 256, nullable = true)
	private String email;

	@Column(name = "pager", length = 256, nullable = true)
	private String pager;

	// ----------------------------------------------------------------

	public Long getHistId() {
		return histId;
	}

	public void setHistId(Long histId) {
		this.histId = histId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getStamp() {
		return stamp;
	}

	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}

	// ----------------------------------------------------------------

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the isPrivate
	 */
	public Boolean getIsPrivate() {
		return isPrivate;
	}

	/**
	 * @param isPrivate
	 *            the isPrivate to set
	 */
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the cellphone
	 */
	public String getCellphone() {
		return cellphone;
	}

	/**
	 * @param cellphone
	 *            the cellphone to set
	 */
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	/**
	 * @return the radiocomm
	 */
	public String getRadiocomm() {
		return radiocomm;
	}

	/**
	 * @param radiocomm
	 *            the radiocomm to set
	 */
	public void setRadiocomm(String radiocomm) {
		this.radiocomm = radiocomm;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the pager
	 */
	public String getPager() {
		return pager;
	}

	/**
	 * @param pager
	 *            the pager to set
	 */
	public void setPager(String pager) {
		this.pager = pager;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}
