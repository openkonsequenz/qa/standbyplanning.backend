/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.DateHelper;
import org.mozilla.javascript.tools.shell.Global;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBY_SCHEDULE_FILTER" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleActionDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleActionDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private String validFrom;
	private String validTo;
	private Date validFromDate;
	private Date validToDate;
	private Long statusId;
	private Long standbyGroupId;
	private Long currentUserId;
	private Long newUserId;
	private Long scheduleBodyId;

	public StandbyScheduleActionDto() {
		/** default constructor. */
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFromDate() {
		return validFromDate;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFromDate(Date validFrom) {
		this.validFromDate = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidToDate() {
		return validToDate;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidToDate(Date validTo) {
		this.validToDate = validTo;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the standbyGroupId
	 */
	public Long getStandbyGroupId() {
		return standbyGroupId;
	}

	/**
	 * @param standbyGroupId the standbyGroupId to set
	 */
	public void setStandbyGroupId(Long standbyGroupId) {
		this.standbyGroupId = standbyGroupId;
	}

	/**
	 * @return the currentUserId
	 */
	public Long getCurrentUserId() {
		return currentUserId;
	}

	/**
	 * @param currentUserId the currentUserId to set
	 */
	public void setCurrentUserId(Long currentUserId) {
		this.currentUserId = currentUserId;
	}

	/**
	 * @return the newUserId
	 */
	public Long getNewUserId() {
		return newUserId;
	}

	/**
	 * @param newUserId the newUserId to set
	 */
	public void setNewUserId(Long newUserId) {
		this.newUserId = newUserId;
	}

	/**
	 * @return the scheduleBodyId
	 */
	public Long getScheduleBodyId() {
		return scheduleBodyId;
	}

	/**
	 * @param scheduleBodyId the scheduleBodyId to set
	 */
	public void setScheduleBodyId(Long scheduleBodyId) {
		this.scheduleBodyId = scheduleBodyId;
	}

	/**
	 * @return the validFrom
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public void createDatesFromStrings() throws SpException {
		try {
			if (this.getValidFrom() != null) {
				this.setValidFromDate(DateHelper.getDateFromString(this.getValidFrom(), Globals.DATE_TIME_FORMAT));
			}
			if (this.getValidTo() != null) {
				this.setValidToDate(DateHelper.getDateFromString(this.getValidTo(), Globals.DATE_TIME_FORMAT));
			}
		} catch (Exception e) {
			Logger.getLogger(StandbyScheduleActionDto.class).error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}
	}
}
