/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "ArchiveStandbyScheduleHeader" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "ArchiveStandbyScheduleHeaderDto")
@JsonInclude(Include.NON_NULL)
public class ArchiveStandbyScheduleHeaderDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String title;
	private String comment;
	private String jsonPlan;
	private Date modificationDate;
	private String modifiedBy;
	private String modifiedCause;
	private Date validFrom;
	private Date validTo;
	private Long statusId;
	private String statusName;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the jsonPlan
	 */
	public String getJsonPlan() {
		return jsonPlan;
	}

	/**
	 * @param jsonPlan the jsonPlan to set
	 */
	public void setJsonPlan(String jsonPlan) {
		this.jsonPlan = jsonPlan;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedCause
	 */
	public String getModifiedCause() {
		return modifiedCause;
	}

	/**
	 * @param modifiedCause the modifiedCause to set
	 */
	public void setModifiedCause(String modifiedCause) {
		this.modifiedCause = modifiedCause;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

}
