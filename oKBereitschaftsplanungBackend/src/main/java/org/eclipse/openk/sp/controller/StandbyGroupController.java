/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.controller.msg.ValidationMsgController;
import org.eclipse.openk.sp.controller.planning.BodyDataCopyController;
import org.eclipse.openk.sp.controller.validation.ValidationController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.BranchRepository;
import org.eclipse.openk.sp.db.dao.CalendarRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.dao.StandbyDurationRepository;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.UserFunctionRepository;
import org.eclipse.openk.sp.db.dao.UserInStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.CalendarDay;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.dto.CopyDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.dto.StandbyDurationDto;
import org.eclipse.openk.sp.dto.StandbyDurationResponseDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupResponseDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.UserSmallSelectionDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle StandbyGroup operations. */
public class StandbyGroupController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(StandbyGroupController.class);

	@Autowired
	private CopyController copyController;

	@Autowired
	private StandbyGroupRepository standbyGroupRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private UserFunctionRepository userFunctionRepository;

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private UserInStandbyGroupRepository userInStandbyGroupRepository;

	@Autowired
	private StandbyDurationRepository standbyDurationRepository;

	@Autowired
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Autowired
	private CalendarRepository calendarRepository;

	@Autowired
	private EntityConverter entityConverter;

	@Autowired
	private BodyDataCopyController bodyDataCopyController;

	@Autowired
	private ValidationController validationController;

	/**
	 * Method to query all standbyGroups.
	 * 
	 * @return
	 */
	public List<StandbyGroupDto> getStandbyGroups() {
		ArrayList<StandbyGroupDto> listStandbyGroup = new ArrayList<>();
		Iterable<StandbyGroup> itStandbyGroupList = standbyGroupRepository.findAllByOrderByTitleAsc();

		for (StandbyGroup standbyGroup : itStandbyGroupList) {
			StandbyGroupDto standbyGroupDto = (StandbyGroupDto) entityConverter.convertEntityToDto(standbyGroup,
					new StandbyGroupDto());
			listStandbyGroup.add(standbyGroupDto);

			convertListsToDto(standbyGroup, standbyGroupDto);
		}
		return listStandbyGroup;
	}

	@SuppressWarnings("unchecked")
	private void convertListsToDto(StandbyGroup standbyGroup, StandbyGroupDto standbyGroupDto) {

		standbyGroupDto.setLsRegions(entityConverter.convertEntityToDtoList(standbyGroup.getLsRegions(),
				standbyGroupDto.getLsRegions(), RegionSelectionDto.class));

		List<UserInStandbyGroupDto> lsUserInStandbyGroupDtos = entityConverter.convertEntityToDtoList(
				standbyGroup.getLsUserInStandbyGroups(), standbyGroupDto.getLsUserInStandbyGroups(),
				UserInStandbyGroupDto.class);
		Collections.sort(lsUserInStandbyGroupDtos, (o1, o2) -> o1.getPosition().compareTo(o2.getPosition()));
		standbyGroupDto.setLsUserInStandbyGroups(lsUserInStandbyGroupDtos);

		standbyGroupDto
				.setLsStandbyDurations(entityConverter.convertEntityToDtoList(standbyGroup.getLsStandbyDurations(),
						standbyGroupDto.getLsStandbyDurations(), StandbyDurationDto.class));
	}

	/**
	 * 
	 * @return
	 */
	public List<StandbyGroupSelectionDto> getStandbyGroupsSelection() {
		ArrayList<StandbyGroupSelectionDto> listStandbyGroup = new ArrayList<>();
		Iterable<StandbyGroup> itStandbyGroupList = standbyGroupRepository.findAllByOrderByTitleAsc();

		for (StandbyGroup standbyGroup : itStandbyGroupList) {
			listStandbyGroup.add((StandbyGroupSelectionDto) entityConverter.convertEntityToDto(standbyGroup,
					new StandbyGroupSelectionDto()));
		}
		return listStandbyGroup;
	}

	/**
	 * Method to select a standbyGroup by its id.
	 * 
	 * @param userid
	 * @return
	 */
	public StandbyGroupDto getStandbyGroup(Long standbyGroupid) {
		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupid);

		Collections.sort(standbyGroup.getLsStandbyDurations(),
				(o1, o2) -> o1.getValidDayFrom().compareTo(o2.getValidDayFrom()));

		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) entityConverter.convertEntityToDto(standbyGroup,
				new StandbyGroupDto());

		convertListsToDto(standbyGroup, standbyGroupDto);
		return standbyGroupDto;
	}

	/**
	 * Method to persist a standbyGroup.
	 * 
	 * @param userid
	 * @return
	 * @throws HttpStatusException
	 */
	public StandbyGroupDto saveStandbyGroup(StandbyGroupDto standbyGroupDto) {
		StandbyGroup standbyGroup = new StandbyGroup();

		List<Region> lsRegions = new ArrayList<>();
		List<CalendarDay> lsCalendar = new ArrayList<>();

		if (standbyGroupDto.getId() != null && standbyGroupRepository.exists(standbyGroupDto.getId())) {
			standbyGroup = standbyGroupRepository.findOne(standbyGroupDto.getId());

			lsRegions = standbyGroup.getLsRegions();
			lsCalendar = standbyGroup.getLsIgnoredCalendarDays();

		}

		standbyGroup = (StandbyGroup) entityConverter.convertDtoToEntity(standbyGroupDto, standbyGroup);

		standbyGroup.setModificationDate(new Date());
		standbyGroup.setLsRegions(lsRegions);
		standbyGroup.setLsIgnoredCalendarDays(lsCalendar);

		standbyGroup = standbyGroupRepository.save(standbyGroup);
		standbyGroupDto = (StandbyGroupDto) entityConverter.convertEntityToDto(standbyGroup, new StandbyGroupDto());

		return standbyGroupDto;
	}

	/**
	 * Method to persist a standbyGroup.
	 * 
	 * @param userid
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<BranchDto> saveBranchesForStandbyGroup(Long standbyGroupId, List<BranchDto> lsBranchDtos) {
		StandbyGroup sbg = standbyGroupRepository.findOne(standbyGroupId);

		for (BranchDto dto : lsBranchDtos) {
			Branch b = branchRepository.findOne(dto.getId());
			b.getLsStandbyGroups().add(sbg);
			branchRepository.save(b);
			sbg.getLsBranches().add(b);
		}

		standbyGroupRepository.save(sbg);

		lsBranchDtos = new ArrayList<>();
		lsBranchDtos = entityConverter.convertEntityToDtoList(sbg.getLsBranches(), lsBranchDtos, BranchDto.class);

		return lsBranchDtos;
	}

	/**
	 * Method to persist a standbyGroup.
	 * 
	 * @param userid
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<BranchDto> deleteBranchesForStandbyGroup(Long standbyGroupId, List<BranchDto> lsBranchDtos) {
		StandbyGroup sbg = standbyGroupRepository.findOne(standbyGroupId);

		for (BranchDto dto : lsBranchDtos) {
			Branch b = branchRepository.findOne(dto.getId());
			b.getLsStandbyGroups().remove(sbg);
			branchRepository.save(b);
			sbg.getLsBranches().remove(b);
		}

		standbyGroupRepository.save(sbg);

		lsBranchDtos = new ArrayList<>();
		lsBranchDtos = entityConverter.convertEntityToDtoList(sbg.getLsBranches(), lsBranchDtos, BranchDto.class);

		return lsBranchDtos;
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listUserInStandbyGroupDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public UserInStandbyGroupResponseDto saveUserList(Long standbyGroupId,
			List<UserInStandbyGroupDto> listUserInStandbyGroupDto) throws SpException {

		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

		for (UserInStandbyGroupDto dto : listUserInStandbyGroupDto) {
			UserInStandbyGroup userInStandbyGroup = (UserInStandbyGroup) entityConverter.convertDtoToEntity(dto,
					new UserInStandbyGroup());

			if (dto.getId() != null && userInStandbyGroupRepository.exists(dto.getId())) {
				// update existing
				UserInStandbyGroup savedUisg = userInStandbyGroupRepository.findOne(dto.getId());
				if (savedUisg.getPosition().intValue() != dto.getPosition().intValue()) {
					this.pushPositionOfUser(savedUisg.getStandbyGroup().getId(), dto.getPosition());
					userInStandbyGroup.setPosition(dto.getPosition());
				}

				List<StandbyScheduleBody> lsScheduleBodies = new ArrayList<>();
				if (DateHelper.isDateAfter(dto.getValidFrom(), savedUisg.getValidFrom())
						|| DateHelper.isSameDate(dto.getValidFrom(), savedUisg.getValidFrom())) {
					// check if user was planned in difference of time
					lsScheduleBodies.addAll(standbyScheduleBodyRepository.findByUserAndGroupAndDateAndStatus(
							savedUisg.getUser().getId(), standbyGroup.getId(),
							DateHelper.getStartOfDay(savedUisg.getValidFrom()), dto.getValidFrom(), 1l));
					lsScheduleBodies.addAll(standbyScheduleBodyRepository.findByUserAndGroupAndDateAndStatus(
							savedUisg.getUser().getId(), standbyGroup.getId(),
							DateHelper.getStartOfDay(savedUisg.getValidFrom()), dto.getValidFrom(), 2l));
				}

				if (DateHelper.isDateBefore(dto.getValidTo(), savedUisg.getValidTo())
						|| DateHelper.isSameDate(dto.getValidTo(), savedUisg.getValidTo())) {
					lsScheduleBodies.addAll(standbyScheduleBodyRepository.findByUserAndGroupAndDateAndStatus(
							savedUisg.getUser().getId(), standbyGroup.getId(), dto.getValidTo(), savedUisg.getValidTo(),
							1l));
					lsScheduleBodies.addAll(standbyScheduleBodyRepository.findByUserAndGroupAndDateAndStatus(
							savedUisg.getUser().getId(), standbyGroup.getId(), dto.getValidTo(), savedUisg.getValidTo(),
							2l));
				}

				if (lsScheduleBodies != null && !lsScheduleBodies.isEmpty()) {
					for (StandbyScheduleBody body : lsScheduleBodies) {
						lsMsg.addAll(ValidationMsgController.createMsgUserWasPlanned(body));
					}
				}
				userInStandbyGroupRepository.save(userInStandbyGroup);

			} else if (standbyGroupRepository.exists(standbyGroupId) && userRepository.exists(dto.getUserId())) {
				// new
				User user = userRepository.findOne(dto.getUserId());
				userInStandbyGroup.setId(null);
				userInStandbyGroup.setUser(user);
				userInStandbyGroup.setStandbyGroup(standbyGroup);
				Integer nextPosition = getNextPositionForGroupMember(standbyGroup);
				userInStandbyGroup.setPosition(nextPosition);
				standbyGroup.getLsUserInStandbyGroups().add(userInStandbyGroup);

				userInStandbyGroupRepository.save(userInStandbyGroup);
				standbyGroupRepository.save(standbyGroup);

			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_UNKNOWN_SBG_OR_UISBG, standbyGroupId, dto.getId());
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}
		}

		UserInStandbyGroupResponseDto userInStandbyGroupResponseDto = new UserInStandbyGroupResponseDto();
		listUserInStandbyGroupDto = new ArrayList<>();
		listUserInStandbyGroupDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsUserInStandbyGroups(),
				listUserInStandbyGroupDto, UserInStandbyGroupDto.class);
		Collections.sort(listUserInStandbyGroupDto, (o1, o2) -> o1.getPosition().compareTo(o2.getPosition()));
		userInStandbyGroupResponseDto.setData(listUserInStandbyGroupDto);

		userInStandbyGroupResponseDto.setLsMsg(lsMsg);

		return userInStandbyGroupResponseDto;
	}

	private Integer getNextPositionForGroupMember(StandbyGroup standbyGroup) {
		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		int maxPos = 0;

		for (UserInStandbyGroup userInStandbyGroup : list) {
			if (userInStandbyGroup.getPosition() > maxPos) {
				maxPos = userInStandbyGroup.getPosition();
			}
		}

		return ++maxPos;
	}

	/**
	 * Method to select all {@link UserInStandbyGroup} entities for the group which
	 * position is equal or higher then the given to push all of them to the next
	 * position.
	 * 
	 * @param groupId
	 * @param pos
	 */
	public void pushPositionOfUser(Long groupId, Integer pos) {
		List<UserInStandbyGroup> lsUserInStandbyGroup = userInStandbyGroupRepository
				.findGroupMemberWithMinPosition(groupId, pos);
		for (UserInStandbyGroup uisg : lsUserInStandbyGroup) {
			uisg.setPosition(uisg.getPosition() + 1);
			userInStandbyGroupRepository.save(uisg);
		}
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listUserInStandbyGroupDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public UserInStandbyGroupResponseDto deleteUserList(Long standbyGroupId,
			List<UserInStandbyGroupDto> listUserInStandbyGroupDto) throws SpException {

		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		for (UserInStandbyGroupDto dto : listUserInStandbyGroupDto) {
			if (standbyGroupRepository.exists(standbyGroupId) && userInStandbyGroupRepository.exists(dto.getId())) {

				UserInStandbyGroup userInStandbyGroup = userInStandbyGroupRepository.findOne(dto.getId());
				long userId = userInStandbyGroup.getUser().getId();

				// validate group for deleted user
				Date from = new Date();
				Date nextYear = DateHelper.addYearsToDate(from, 1);
				DateTime dt = new DateTime(nextYear);
				Date till = DateHelper.getDate(dt.getYear(), 12, 31, 23, 59, 59);

				List<StandbyScheduleBody> lsScheduleBodies = standbyScheduleBodyRepository
						.findByUserAndGroupAndDateAndStatus(userId, standbyGroup.getId(), from, till, 1l);
				if (lsScheduleBodies != null && !lsScheduleBodies.isEmpty()) {
					for (StandbyScheduleBody body : lsScheduleBodies) {
						lsMsg.addAll(ValidationMsgController.createMsgUserWasPlanned(body));
					}
				}

				// remove
				standbyGroup.getLsUserInStandbyGroups().remove(userInStandbyGroup);

				standbyGroupRepository.save(standbyGroup);
				userInStandbyGroupRepository.delete(userInStandbyGroup);

			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_UNKNOWN_SBG_OR_UISBG, standbyGroupId, dto.getId());
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}
		}

		listUserInStandbyGroupDto = new ArrayList<>();
		listUserInStandbyGroupDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsUserInStandbyGroups(),
				listUserInStandbyGroupDto, UserInStandbyGroupDto.class);

		Collections.sort(listUserInStandbyGroupDto, (o1, o2) -> o1.getPosition().compareTo(o2.getPosition()));

		// response dto
		UserInStandbyGroupResponseDto responseDto = new UserInStandbyGroupResponseDto();
		responseDto.setData(listUserInStandbyGroupDto);
		responseDto.setLsMsg(lsMsg);

		return responseDto;
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listStandbyDurationDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public StandbyDurationResponseDto saveDurationList(Long standbyGroupId,
			List<StandbyDurationDto> listStandbyDurationDto) throws SpException {

		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

		for (StandbyDurationDto dto : listStandbyDurationDto) {
			if (standbyGroupRepository.exists(standbyGroupId)) {

				StandbyDuration standbyDuration = (StandbyDuration) entityConverter.convertDtoToEntity(dto,
						new StandbyDuration());
				standbyDuration.setStandbyGroup(standbyGroup);
				standbyGroup.getLsStandbyDurations().add(standbyDuration);
				standbyDuration.setModificationDate(new Date());

				standbyDurationRepository.save(standbyDuration);
				standbyGroupRepository.save(standbyGroup);

			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_UNKNOWN_SBG_OR_DURATION, standbyGroupId,
						dto.getStandbyDurationId());
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}
		}

		listStandbyDurationDto = new ArrayList<>();
		listStandbyDurationDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsStandbyDurations(),
				listStandbyDurationDto, StandbyDurationDto.class);

		Collections.sort(listStandbyDurationDto, (o1, o2) -> o1.getValidDayFrom().compareTo(o2.getValidDayFrom()));

		StandbyDurationResponseDto spResponseDto = new StandbyDurationResponseDto();
		spResponseDto.setData(listStandbyDurationDto);
		spResponseDto.setLsMsg(new ArrayList<PlanningMsgDto>());

		return spResponseDto;
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listStandbyDurationDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public List<StandbyDurationDto> deleteDurationList(Long standbyGroupId,
			List<StandbyDurationDto> listStandbyDurationDto) throws SpException {

		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

		for (StandbyDurationDto dto : listStandbyDurationDto) {
			if (standbyGroupRepository.exists(standbyGroupId)
					&& standbyDurationRepository.exists(dto.getStandbyDurationId())) {

				StandbyDuration standbyDuration = standbyDurationRepository.findOne(dto.getStandbyDurationId());
				standbyGroup.getLsStandbyDurations().remove(standbyDuration);

				standbyGroupRepository.save(standbyGroup);
				standbyDurationRepository.delete(standbyDuration);

			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_UNKNOWN_SBG_OR_DURATION, standbyGroupId,
						dto.getStandbyDurationId());
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}
		}

		listStandbyDurationDto = new ArrayList<>();
		listStandbyDurationDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsStandbyDurations(),
				listStandbyDurationDto, StandbyDurationDto.class);

		return listStandbyDurationDto;
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param lsCalendarDayDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public List<CalendarDayDto> saveIgnoredCalendarDayList(Long standbyGroupId, List<CalendarDayDto> lsCalendarDayDto)
			throws SpException {

		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);
		for (CalendarDayDto dto : lsCalendarDayDto) {
			if (standbyGroupRepository.exists(standbyGroupId)) {
				if (calendarRepository.exists(dto.getId())) {
					CalendarDay day = calendarRepository.findOne(dto.getId());
					standbyGroup.getLsIgnoredCalendarDays().add(day);
					day.getLsStandbyGroups().add(standbyGroup);
					standbyGroupRepository.save(standbyGroup);
					calendarRepository.save(day);
				} else {
					SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
					String msgParam = SpMsg.getLbl(SpMsg.TXT_CALENDAR_WITH_ID, dto.getId());
					SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
					LOGGER.error(spE, spE);
					throw spE;
				}
			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId);
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}
		}

		standbyDurationRepository.flush();
		lsCalendarDayDto = new ArrayList<>();
		lsCalendarDayDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsIgnoredCalendarDays(),
				lsCalendarDayDto, CalendarDayDto.class);

		return lsCalendarDayDto;
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listStandbyDurationDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public List<CalendarDayDto> deleteIgnoredCalendarDayList(Long standbyGroupId, List<CalendarDayDto> lsCalendarDayDto)
			throws SpException {

		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);
		for (CalendarDayDto dto : lsCalendarDayDto) {
			if (standbyGroupRepository.exists(standbyGroupId)) {
				if (calendarRepository.exists(dto.getId())) {
					CalendarDay day = calendarRepository.findOne(dto.getId());
					standbyGroup.getLsIgnoredCalendarDays().remove(day);
					day.getLsStandbyGroups().remove(standbyGroup);
					standbyGroupRepository.save(standbyGroup);
					calendarRepository.save(day);
				} else {
					SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
					String msgParam = SpMsg.getLbl(SpMsg.TXT_CALENDAR_WITH_ID, dto.getId());
					SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
					LOGGER.error(spE, spE);
					throw spE;
				}
			} else {
				SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
				String msgParam = SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId);
				SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
				LOGGER.error(spE, spE);
				throw spE;
			}
		}

		standbyDurationRepository.flush();
		lsCalendarDayDto = new ArrayList<>();
		lsCalendarDayDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsIgnoredCalendarDays(),
				lsCalendarDayDto, CalendarDayDto.class);

		return lsCalendarDayDto;
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listStandbyDurationDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public List<UserFunctionSelectionDto> saveUserFunctionList(Long standbyGroupId,
			List<UserFunctionSelectionDto> listUserFunctionSelectionDto) throws SpException {

		try {
			StandbyGroup standbyGroup = null;
			if (standbyGroupId != null && standbyGroupRepository.exists(standbyGroupId)) {
				standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

				for (UserFunctionSelectionDto dto : listUserFunctionSelectionDto) {
					if (dto.getFunctionId() != null && userFunctionRepository.exists(dto.getFunctionId())) {
						UserFunction uf = userFunctionRepository.findOne(dto.getFunctionId());
						uf.getLsStandbyGroups().add(standbyGroup);
						userFunctionRepository.save(uf);

						standbyGroup.getLsUserFunction().add(uf);
						standbyGroupRepository.save(standbyGroup);

						standbyGroupRepository.flush();
					} else {
						SpException e = new SpException(100,
								SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION_WITH_ID, dto.getFunctionId())
										+ SpMsg.TXT_IS_UNKNOWEN,
								new Exception(SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION_WITH_ID, standbyGroupId)
										+ SpMsg.TXT_IS_UNKNOWEN));
						LOGGER.error(e, e);
						throw e;
					}
				}
			} else {
				SpException e = new SpException(100,
						SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId) + SpMsg.TXT_IS_UNKNOWEN,
						new Exception(
								SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId) + SpMsg.TXT_IS_UNKNOWEN));
				LOGGER.error(e, e);
				throw e;
			}

			listUserFunctionSelectionDto = new ArrayList<>();
			listUserFunctionSelectionDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsUserFunction(),
					listUserFunctionSelectionDto, UserFunctionSelectionDto.class);

			return listUserFunctionSelectionDto;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listUserFunctionSelectionDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public List<UserFunctionSelectionDto> deleteUserFunctionList(Long standbyGroupId,
			List<UserFunctionSelectionDto> listUserFunctionSelectionDto) throws SpException {
		Long tmpUfId = null;
		try {
			StandbyGroup standbyGroup = null;
			if (standbyGroupId != null && standbyGroupRepository.exists(standbyGroupId)) {
				standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

				for (UserFunctionSelectionDto dto : listUserFunctionSelectionDto) {
					if (dto.getFunctionId() != null && userFunctionRepository.exists(dto.getFunctionId())) {
						tmpUfId = dto.getFunctionId();
						UserFunction uf = userFunctionRepository.findOne(dto.getFunctionId());
						uf.getLsStandbyGroups().remove(standbyGroup);
						userFunctionRepository.save(uf);

						standbyGroup.getLsUserFunction().remove(uf);
						standbyGroupRepository.save(standbyGroup);

						standbyGroupRepository.flush();
					} else {
						tmpUfId = dto.getFunctionId();
						SpException e = new SpException(100,
								SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION_WITH_ID, dto.getFunctionId())
										+ SpMsg.TXT_IS_UNKNOWEN,
								new Exception(SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION_WITH_ID, tmpUfId)
										+ SpMsg.TXT_IS_UNKNOWEN));
						LOGGER.error(e, e);
						throw e;
					}
				}
			} else {
				SpException e = new SpException(100,
						SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId) + " is unknown.", new Exception(
								SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId) + SpMsg.TXT_IS_UNKNOWEN));
				LOGGER.error(e, e);
				throw e;
			}

			listUserFunctionSelectionDto = new ArrayList<>();
			listUserFunctionSelectionDto = entityConverter.convertEntityToDtoList(standbyGroup.getLsUserFunction(),
					listUserFunctionSelectionDto, UserFunctionSelectionDto.class);

			return listUserFunctionSelectionDto;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION_WITH_ID, tmpUfId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listStandbyDurationDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public List<RegionSelectionDto> saveRegionsForStandbyGroup(Long standbyGroupId,
			List<RegionSelectionDto> lsRegionSelectionDtos) throws SpException {

		Long tmpRegionId = null;
		try {
			StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

			for (RegionSelectionDto dto : lsRegionSelectionDtos) {
				if (standbyGroupRepository.exists(standbyGroupId)) {
					tmpRegionId = dto.getId();

					Region region = regionRepository.findOne(dto.getId());

					region.getLsStandbyGroups().add(standbyGroup);
					regionRepository.save(region);

					standbyGroup.getLsRegions().add(region);
					standbyGroupRepository.save(standbyGroup);
					standbyGroupRepository.flush();

				} else {
					SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
					StringBuilder msgParamBuilder = new StringBuilder();
					msgParamBuilder.append(SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpRegionId));
					msgParamBuilder.append(SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId));
					SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParamBuilder.toString());
					LOGGER.error(spE, spE);
					throw spE;
				}
			}

			lsRegionSelectionDtos = new ArrayList<>();
			lsRegionSelectionDtos = entityConverter.convertEntityToDtoList(standbyGroup.getLsRegions(),
					lsRegionSelectionDtos, RegionSelectionDto.class);

			return lsRegionSelectionDtos;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpRegionId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * 
	 * @param standbyGroupId
	 * @param listStandbyDurationDto
	 * @return
	 * @throws SpException
	 */
	@SuppressWarnings("unchecked")
	public List<RegionSelectionDto> deleteRegionsForStandbyGroup(Long standbyGroupId,
			List<RegionSelectionDto> lsRegionSelectionDtos) throws SpException {

		Long tmpRegionId = null;
		try {
			StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

			for (RegionSelectionDto dto : lsRegionSelectionDtos) {
				if (standbyGroupRepository.exists(standbyGroupId)) {
					tmpRegionId = dto.getId();

					Region region = regionRepository.findOne(dto.getId());
					region.getLsStandbyGroups().remove(standbyGroup);
					regionRepository.save(region);

					standbyGroup.getLsRegions().remove(region);
					standbyGroupRepository.save(standbyGroup);
					standbyGroupRepository.flush();

				} else {
					SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
					StringBuilder msgParamBuilder = new StringBuilder();
					msgParamBuilder.append(SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpRegionId));
					msgParamBuilder.append(SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId));
					SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParamBuilder.toString());
					LOGGER.error(spE, spE);
					throw spE;
				}
			}

			lsRegionSelectionDtos = new ArrayList<>();
			lsRegionSelectionDtos = entityConverter.convertEntityToDtoList(standbyGroup.getLsRegions(),
					lsRegionSelectionDtos, RegionSelectionDto.class);

			return lsRegionSelectionDtos;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpRegionId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	public List<UserSmallSelectionDto> getUserListUnique(Long standbyGroupId) throws SpException {
		List<UserSmallSelectionDto> listDto = new ArrayList<>();

		try {
			// List<User> listEntity =
			// userInStandbyGroupRepository.findUniqueById(standbyGroupId);
			List<Object[]> listEntity = userInStandbyGroupRepository.findUniqueGroupUser(standbyGroupId);

			for (Object[] obj : listEntity) {
				UserSmallSelectionDto dto = new UserSmallSelectionDto();
				listDto.add(dto);

				dto.setId(((Number) obj[0]).longValue());
				dto.setLastname((String) obj[1]);
				dto.setFirstname((String) obj[2]);
			}

		} catch (Exception e) {
			SpException sp = new SpException(100,
					SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId) + " is unknown.", new Exception(
							SpMsg.getLbl(SpMsg.TXT_STANDBY_GROUP_WITH_ID, standbyGroupId) + SpMsg.TXT_IS_UNKNOWEN));
			LOGGER.error(sp, sp);
			throw sp;
		}

		return listDto;
	}

	public List<TransferGroupDto> getStandbyGroupsAndLists() throws SpException {
		return bodyDataCopyController.getGroupsAndLists();
	}

	@SuppressWarnings("unchecked")
	public List<UserInStandbyGroupSelectionDto> getUserInGroupList(Long standbyGroupId) {

		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);
		List<UserInStandbyGroup> listUserInStandbyGroup = standbyGroup.getLsUserInStandbyGroups();
		List<UserInStandbyGroupSelectionDto> listUserInStandbyGroupDto = new ArrayList<>();

		listUserInStandbyGroupDto = entityConverter.convertEntityToDtoList(listUserInStandbyGroup,
				listUserInStandbyGroupDto, UserInStandbyGroupSelectionDto.class);

		return listUserInStandbyGroupDto;
	}

	@SuppressWarnings("unchecked")
	public Map<Long, List<UserInStandbyGroupSelectionDto>> getUserInGroupList(Long[] standbyGroupIds) {

		Map<Long, List<UserInStandbyGroupSelectionDto>> mapUserInGroup = new HashMap<>();

		List<StandbyGroup> lsStandbyGroups = standbyGroupRepository.findGroups(standbyGroupIds);

		for (StandbyGroup standbyGroup : lsStandbyGroups) {
			List<UserInStandbyGroup> listUserInStandbyGroup = standbyGroup.getLsUserInStandbyGroups();
			List<UserInStandbyGroupSelectionDto> listUserInStandbyGroupDto = new ArrayList<>();

			listUserInStandbyGroupDto = entityConverter.convertEntityToDtoList(listUserInStandbyGroup,
					listUserInStandbyGroupDto, UserInStandbyGroupSelectionDto.class);

			mapUserInGroup.put(standbyGroup.getId(), listUserInStandbyGroupDto);
		}

		return mapUserInGroup;
	}

	public StandbyGroupDto copy(Long standbyGroupId, CopyDto copyDto) throws SpException {
		return copyController.copy(standbyGroupId, copyDto);
	}
}
