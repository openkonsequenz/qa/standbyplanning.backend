/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.util.CustomDateYMDSerializer;
import org.eclipse.openk.sp.util.CustomDateYMDDeserializer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The "STANDBYPLANNINGSTATUS" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleBlueprintDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleBlueprintDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@JsonSerialize(using = CustomDateYMDSerializer.class)
	@JsonDeserialize(using = CustomDateYMDDeserializer.class) // enforces correct time zone in Date objects
	private Date validFrom;

	@JsonSerialize(using = CustomDateYMDSerializer.class)
	@JsonDeserialize(using = CustomDateYMDDeserializer.class) // enforces correct time zone in Date objects
	private Date validTo;

	private Long standbyGroupId;

	private Long statusId;

	private Long standbyListId;

	private Long startIdOfUser;

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the startIdOfUser
	 */
	public Long getStartIdOfUser() {
		return startIdOfUser;
	}

	/**
	 * @param startIdOfUser the startIdOfUser to set
	 */
	public void setStartIdOfUser(Long startIdOfUser) {
		this.startIdOfUser = startIdOfUser;
	}

	/**
	 * @return the standbyGroupId
	 */
	public Long getStandbyGroupId() {
		return standbyGroupId;
	}

	/**
	 * @param standbyGroupId the standbyGroupId to set
	 */
	public void setStandbyGroupId(Long standbyGroupId) {
		this.standbyGroupId = standbyGroupId;
	}

	/**
	 * @return the standbyListId
	 */
	public Long getStandbyListId() {
		return standbyListId;
	}

	/**
	 * @param standbyListId the standbyListId to set
	 */
	public void setStandbyListId(Long standbyListId) {
		this.standbyListId = standbyListId;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
}