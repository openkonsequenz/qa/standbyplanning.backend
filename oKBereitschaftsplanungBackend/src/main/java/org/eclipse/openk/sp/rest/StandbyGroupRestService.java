/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.controller.StandbyGroupController;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.dto.CopyDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.dto.StandbyDurationDto;
import org.eclipse.openk.sp.dto.StandbyDurationResponseDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupResponseDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.UserSmallSelectionDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("standbygroup")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StandbyGroupRestService extends BaseResource {

	@Autowired
	private StandbyGroupController sGCo;

	public StandbyGroupRestService() {
		super(Logger.getLogger(StandbyGroupRestService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/list")
	public Response getStandbyGroups(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<StandbyGroupSelectionDto>> invokable = modusr -> sGCo.getStandbyGroupsSelection();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/selectionlist")
	public Response getStandbyGroupsSelection(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		return getStandbyGroups(jwt);
	}

	@GET
	@Path("/{id}")
	public Response getStandbyGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<StandbyGroupDto> invokable = modusr -> sGCo.getStandbyGroup(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);

	}

	@PUT
	@Path("/{id}/branch/save/list")
	public Response saveBranchesForStandbyGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<BranchDto> listBranchDto) {

		ModifyingInvokable<List<BranchDto>> invokable = modusr -> sGCo.saveBranchesForStandbyGroup(id, listBranchDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/branch/delete/list")
	public Response deleteBranchesOfStandbyGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<BranchDto> listBranchDto) {

		ModifyingInvokable<List<BranchDto>> invokable = modusr -> sGCo.deleteBranchesForStandbyGroup(id, listBranchDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/save")
	public Response saveStandbyGroup(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid StandbyGroupDto standbyGroupDto) {

		ModifyingInvokable<AbstractDto> invokable = modusr -> sGCo.saveStandbyGroup(standbyGroupDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/region/save/list")
	public Response saveRegionForGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<RegionSelectionDto> lsRegionSelectionDtos) {
		ModifyingInvokable<List<RegionSelectionDto>> invokable = modusr -> sGCo.saveRegionsForStandbyGroup(id,
				lsRegionSelectionDtos);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/region/delete/list")
	public Response deleteRegionForGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<RegionSelectionDto> lsRegionSelectionDtos) {
		ModifyingInvokable<List<RegionSelectionDto>> invokable = modusr -> sGCo.deleteRegionsForStandbyGroup(id,
				lsRegionSelectionDtos);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/user/save/list")
	public Response saveStandbyGroupUserList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<UserInStandbyGroupDto> listStandbyGroupUserDto) {

		ModifyingInvokable<UserInStandbyGroupResponseDto> invokable = modusr -> sGCo.saveUserList(id,
				listStandbyGroupUserDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@GET
	@Path("/{id}/user/list")
	public Response getStandbyGroupUserList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<UserInStandbyGroupSelectionDto>> invokable = modusr -> sGCo.getUserInGroupList(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@GET
	@Path("/{id}/user/list/unique")
	public Response getStandbyGroupUserListUnique(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<UserSmallSelectionDto>> invokable = modusr -> sGCo.getUserListUnique(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/{id}/user/delete/list")
	public Response deleteStandbyGroupUserList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<UserInStandbyGroupDto> listStandbyGroupUserDto) {

		ModifyingInvokable<UserInStandbyGroupResponseDto> invokable = modusr -> sGCo.deleteUserList(id,
				listStandbyGroupUserDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/{id}/duration/save/list")
	public Response saveStandbyGroupDurationList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<StandbyDurationDto> listStandbyDurationDto) {

		ModifyingInvokable<StandbyDurationResponseDto> invokable = modusr -> sGCo.saveDurationList(id,
				listStandbyDurationDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/{id}/duration/delete/list")
	public Response deleteStandbyGroupDurationList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<StandbyDurationDto> listStandbyDurationDto) {

		ModifyingInvokable<List<StandbyDurationDto>> invokable = modusr -> sGCo.deleteDurationList(id,
				listStandbyDurationDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/{id}/userfunction/save/list")
	public Response saveFunctionsForGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<UserFunctionSelectionDto> lsUserFunctionSelectionDtos) {

		ModifyingInvokable<List<UserFunctionSelectionDto>> invokable = modusr -> sGCo.saveUserFunctionList(id,
				lsUserFunctionSelectionDtos);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/userfunction/delete/list")
	public Response deleteFunctionsFromGroup(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<UserFunctionSelectionDto> lsUserFunctionSelectionDtos) {

		ModifyingInvokable<List<UserFunctionSelectionDto>> invokable = modusr -> sGCo.deleteUserFunctionList(id,
				lsUserFunctionSelectionDtos);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/calendar/ignore/save/list")
	public Response saveIgnoreCalendarDayList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<CalendarDayDto> listCalendarDaysDto) {

		ModifyingInvokable<List<CalendarDayDto>> invokable = modusr -> sGCo.saveIgnoredCalendarDayList(id,
				listCalendarDaysDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/{id}/calendar/ignore/delete/list")
	public Response deleteIgnoreCalendarDayList(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<CalendarDayDto> listCalendarDaysDto) {

		ModifyingInvokable<List<CalendarDayDto>> invokable = modusr -> sGCo.deleteIgnoredCalendarDayList(id,
				listCalendarDaysDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@GET
	@Path("/transfer/list")
	public Response getLists(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<TransferGroupDto>> invokable = modusr -> sGCo.getStandbyGroupsAndLists();

		String[] securityRoles = Globals.getAllRolls();

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/{id}/copy")
	public Response copyDeep(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			CopyDto copyDto) {

		ModifyingInvokable<StandbyGroupDto> invokable = modusr -> sGCo.copy(id, copyDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}
}
