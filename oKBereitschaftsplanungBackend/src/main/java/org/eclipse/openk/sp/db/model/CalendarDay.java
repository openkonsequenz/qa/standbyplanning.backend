/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "CALENDAR" database table.
 */
@Entity
@Table(name = "CALENDAR")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class CalendarDay extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CALENDAR_ID_SEQ")
	@SequenceGenerator(name = "CALENDAR_ID_SEQ", sequenceName = "CALENDAR_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 256, nullable = false)
	private String name;

	@Column(name = "shorttext", length = 8, nullable = true)
	private String shorttext;

	@Convert("booleanConverter")
	private Boolean repeat;

	@Column(name = "valid_from", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "valid_to", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;

	@Column(name = "non_business_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dateIndex;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@ManyToMany(mappedBy = "lsIgnoredCalendarDays", fetch = FetchType.EAGER)
	private List<StandbyGroup> lsStandbyGroups = new ArrayList<>();

	public CalendarDay() {
		/** default constructor. */
	}

	public CalendarDay(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setName(String title) {
		this.name = title;
	}

	/**
	 * @return the shorttext
	 */
	public String getShorttext() {
		return shorttext;
	}

	/**
	 * @param shorttext
	 *            the shorttext to set
	 */
	public void setShorttext(String shorttext) {
		this.shorttext = shorttext;
	}

	/**
	 * @return the repeat
	 */
	public Boolean getRepeat() {
		return repeat;
	}

	/**
	 * @param repeat
	 *            the repeat to set
	 */
	public void setRepeat(Boolean repeat) {
		this.repeat = repeat;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * 
	 * @return
	 */
	public Date getDateIndex() {
		return dateIndex;
	}

	/**
	 * 
	 * @param date
	 */
	public void setDateIndex(Date date) {
		this.dateIndex = date;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the lsStandbyGroups
	 */
	public List<StandbyGroup> getLsStandbyGroups() {
		return lsStandbyGroups;
	}

	/**
	 * @param lsStandbyGroups
	 *            the lsStandbyGroups to set
	 */
	public void setLsStandbyGroups(List<StandbyGroup> lsStandbyGroups) {
		this.lsStandbyGroups = lsStandbyGroups;
	}

	@Override
	public int compareTo(AbstractEntity arg0) {
		return (getId() == arg0.getId()) ? 1 : 0;
	}

}