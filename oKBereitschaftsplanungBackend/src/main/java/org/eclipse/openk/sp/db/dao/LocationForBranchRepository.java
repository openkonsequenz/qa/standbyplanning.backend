/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.List;

import org.eclipse.openk.sp.db.model.LocationForBranch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationForBranchRepository extends JpaRepository<LocationForBranch, Long> {

	List<LocationForBranch> findAllByOrderByValidFromAsc();
}