/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.List;

import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StandbyGroupRepository extends JpaRepository<StandbyGroup, Long> {

	@Query(value = "SELECT g.title, g.id , l.title, l.id  FROM standby_list_has_standby_group i "
			+ "	inner join standby_group g on (i.standby_group_id = g.id) "
			+ "	inner join standby_list l on (i.standby_list_id = l.id) ORDER BY g.title ASC", nativeQuery = true)
	List<Object[]> findGroupsWithLists();

	@Query(value = "SELECT g FROM StandbyGroup g WHERE (g.id in :groupIds)")
	List<StandbyGroup> findGroups(@Param("groupIds") Long[] groupIds);

	List<StandbyGroup> findAllByOrderByTitleAsc();
}