/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.controller.ResponseBuilderWrapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.exceptions.SpExceptionMapper;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle authentication operations. */
public class AuthController extends RestService {

	@Autowired
	FileHelper fileHelper;

	private static final Logger LOGGER = Logger.getLogger(AuthController.class.getName());

	public AuthController() {
		super.setPrintExceptionsOnlyToLog(false);
	}

	public Response logout(String token) throws IOException, HttpStatusException {

		String baseURLPortal = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_BASE_URL);
		String useHttps = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_USE_HTTPS);
		boolean https = Boolean.parseBoolean(useHttps);

		performGetRequest(https, baseURLPortal, "logout", token);

		return ResponseBuilderWrapper.INSTANCE.buildOKResponse(SpExceptionMapper.getGeneralOKJson());

	}

	public Response login(String token) throws IOException, HttpStatusException {

		String baseURLPortal = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_BASE_URL);
		String useHttps = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_USE_HTTPS);
		boolean https = Boolean.parseBoolean(useHttps);

		LOGGER.debug("baseURL: " + baseURLPortal);
		LOGGER.debug("https: " + useHttps);

		performGetRequest(https, baseURLPortal, "checkAuth", token);

		LOGGER.info("login - end");
		return ResponseBuilderWrapper.INSTANCE.buildOKResponse(SpExceptionMapper.getGeneralOKJson());
	}

}
