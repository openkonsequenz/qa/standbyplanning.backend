/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.external.ExternalInterfaceMapper;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySearchDto;
import org.eclipse.openk.sp.dto.TimeDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
/** Class to handle SEARCH operations. */
public class SearchController {
	protected static final Logger LOGGER = Logger.getLogger(SearchController.class);

	@Autowired
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Autowired
	private StandbyListRepository standbyListRepository;

	@Autowired
	private StandbyGroupController standbyGroupController;

	@Autowired
	private EntityConverter entityConverter;

	@Autowired
	private ExternalInterfaceMapper externalInterfaces;

	public List<StandbyScheduleBodySearchDto> searchNow(StandbyScheduleSearchDto standbyScheduleSearchDto,
			String token) {

		Date date = standbyScheduleSearchDto.getDateIndex();
		if (date == null) {
			date = new Date();
		}

		Date validFrom = date;
		Date validTo = DateHelper.getEndOfDay(validFrom);

		TimeDto time = standbyScheduleSearchDto.getTimeIndex();

		if (time != null) {
			validFrom = DateHelper.getDateWithTime(date, time.getHour(), time.getMinute());
		}

		List<StandbyScheduleBody> listBodies = standbyScheduleBodyRepository.findByDateAndStatus(validFrom, validTo,
				SpMsg.STATUS_CLOSED_PLANNING);

		List<StandbyScheduleBody> listFilteredBodies = filterBodiesByLocation(standbyScheduleSearchDto, listBodies);
		listFilteredBodies = filterBodiesByBranches(standbyScheduleSearchDto, listFilteredBodies);
		listFilteredBodies = filterBodiesByList(standbyScheduleSearchDto, listFilteredBodies);

		// external interface for calculating the distance
		listFilteredBodies = externalInterfaces.calcDistance(listFilteredBodies, standbyScheduleSearchDto, token);

		// sorting
		return sortBodies(standbyScheduleSearchDto, listFilteredBodies);
	}

	public List<StandbyScheduleBody> filterBodiesByList(StandbyScheduleSearchDto standbyScheduleSearchDto,
			List<StandbyScheduleBody> listFilteredBodies) {

		List<StandbyScheduleBody> filteredBodies = listFilteredBodies;
		List<String> groupNameList = new ArrayList();

		if (standbyScheduleSearchDto.getListId() == null) {
			return filteredBodies;
		}

		Long listId = standbyScheduleSearchDto.getListId();

		if (listId != null && standbyListRepository.exists(listId)) {

			StandbyList standbyList = standbyListRepository.findOne(listId);

			for (StandbyGroup sbg : standbyList.getLsStandbyGroups()) {
				groupNameList.add(sbg.getTitle());
			}

			filteredBodies = new ArrayList();

			for (StandbyScheduleBody body : listFilteredBodies) {

				if (groupNameList.contains(body.getStandbyGroup().getTitle())) {
					filteredBodies.add(body);
				}
			}
		}

		return filteredBodies;
	}

	// Sorting strategy https://openkonsequenz.atlassian.net/browse/BP-664
	public List<StandbyScheduleBodySearchDto> sortBodies(StandbyScheduleSearchDto standbyScheduleSearchDto,
			List<StandbyScheduleBody> listFilteredBodies) {

		List<StandbyScheduleBodySearchDto> listBodyDtoBranchDirectHit = new ArrayList<>();
		List<StandbyScheduleBodySearchDto> listBodyDtoIndirectHit = new ArrayList<>();
		Map<Long, List<StandbyScheduleBodySearchDto>> mapGroupBranch = new HashMap<>();

		if (standbyScheduleSearchDto.getLsBranchSelected() == null) {
			standbyScheduleSearchDto.setLsBranchSelected(new ArrayList<>());
		}
		List<Long> searchBranchIdList = new ArrayList<>();
		for (BranchDto branchDto : standbyScheduleSearchDto.getLsBranchSelected()) {
			searchBranchIdList.add(branchDto.getId());
		}

		List<Long> lsGroupIds = new ArrayList<>();
		for (StandbyScheduleBody standbyScheduleBody : listFilteredBodies) {
			lsGroupIds.add(standbyScheduleBody.getStandbyGroup().getId());
		}

		Map<Long, List<UserInStandbyGroupSelectionDto>> mapUserInGroup = new HashMap<>();
		Long[] groupIds = lsGroupIds.toArray(new Long[lsGroupIds.size()]);

		if (!lsGroupIds.isEmpty()) {
			mapUserInGroup = standbyGroupController.getUserInGroupList(groupIds);
		}

		for (StandbyScheduleBody standbyScheduleBody : listFilteredBodies) {

			List<StandbyScheduleBodySearchDto> listBodyDto = calcBranchHit(mapGroupBranch, searchBranchIdList,
					standbyScheduleBody, listBodyDtoBranchDirectHit, listBodyDtoIndirectHit);

			StandbyScheduleBodySearchDto bodyDto = (StandbyScheduleBodySearchDto) entityConverter
					.convertEntityToDto(standbyScheduleBody, new StandbyScheduleBodySearchDto());

			listBodyDto.add(bodyDto);

			List<UserInStandbyGroupSelectionDto> listUserInStandbyGroupDto = mapUserInGroup
					.get(standbyScheduleBody.getStandbyGroup().getId());
			bodyDto.setLsUserInStandbyGroup(listUserInStandbyGroupDto);

		}

		// sorting by groupname
		Collections.sort(listBodyDtoBranchDirectHit,
				(o1, o2) -> o1.getStandbyGroup().getTitle().compareTo(o2.getStandbyGroup().getTitle()));
		Collections.sort(listBodyDtoIndirectHit,
				(o1, o2) -> o1.getStandbyGroup().getTitle().compareTo(o2.getStandbyGroup().getTitle()));
		// concat
		listBodyDtoBranchDirectHit.addAll(listBodyDtoIndirectHit);

		return listBodyDtoBranchDirectHit;
	}

	public List<StandbyScheduleBodySearchDto> calcBranchHit(
			Map<Long, List<StandbyScheduleBodySearchDto>> mapGroupBranch, List<Long> searchBranchIdList,
			StandbyScheduleBody standbyScheduleBody, List<StandbyScheduleBodySearchDto> listBodyDto1,
			List<StandbyScheduleBodySearchDto> listBodyDto2) {

		if (searchBranchIdList == null || searchBranchIdList.isEmpty()) {
			return listBodyDto1;
		}

		if (mapGroupBranch.containsKey(standbyScheduleBody.getStandbyGroup().getId())) {
			return mapGroupBranch.get(standbyScheduleBody.getStandbyGroup().getId());
		}

		List<Branch> lsGroupBranches = standbyScheduleBody.getStandbyGroup().getLsBranches();

		if (lsGroupBranches.size() != searchBranchIdList.size()) {
			mapGroupBranch.put(standbyScheduleBody.getStandbyGroup().getId(), listBodyDto2);
			return mapGroupBranch.get(standbyScheduleBody.getStandbyGroup().getId());
		}

		boolean containsAll = true;

		for (Branch branch : lsGroupBranches) {

			if (!searchBranchIdList.contains(branch.getId())) {
				mapGroupBranch.put(standbyScheduleBody.getStandbyGroup().getId(), listBodyDto2);
				containsAll = false;
				break;
			}
		}

		if (containsAll) {
			mapGroupBranch.put(standbyScheduleBody.getStandbyGroup().getId(), listBodyDto1);
		}

		return mapGroupBranch.get(standbyScheduleBody.getStandbyGroup().getId());
	}

	public List<StandbyScheduleBody> filterBodiesByBranches(StandbyScheduleSearchDto standbyScheduleSearchDto,
			List<StandbyScheduleBody> listBodies) {

		List<StandbyScheduleBody> listFilteredBodies = new ArrayList<>();
		List<BranchDto> listBranches = standbyScheduleSearchDto.getLsBranchSelected();
		int branchHit = 0;

		if (listBranches != null && !listBranches.isEmpty()) {

			for (StandbyScheduleBody standbyScheduleBody : listBodies) {
				branchHit = 0;
				for (BranchDto branchDto : listBranches) {

					if (hasBodyThisBranch(standbyScheduleBody, branchDto.getId())) {
						branchHit++;
						break;
					}
				}

				if (branchHit > 0) {
					listFilteredBodies.add(standbyScheduleBody);
				}

			}
		} else {
			listFilteredBodies = listBodies;
		}

		return listFilteredBodies;
	}

	public boolean hasBodyThisBranch(StandbyScheduleBody standbyScheduleBody, Long id) {

		List<Branch> lsSbgBranches = standbyScheduleBody.getStandbyGroup().getLsBranches();

		// if no branches in group definition, then show every body
		if (lsSbgBranches == null || lsSbgBranches.isEmpty()) {
			return true;
		}

		for (Branch branch : lsSbgBranches) {
			if (id.longValue() == branch.getId().longValue()) {
				return true;
			}
		}
		return false;
	}

	public List<StandbyScheduleBody> filterBodiesByLocation(StandbyScheduleSearchDto standbyScheduleSearchDto,
			List<StandbyScheduleBody> listBodies) {

		List<StandbyScheduleBody> filteredBodies = new ArrayList<>();

		if (standbyScheduleSearchDto.getLocationId() != null) {

			for (StandbyScheduleBody standbyScheduleBody : listBodies) {
				List<Region> lsRegions = standbyScheduleBody.getStandbyGroup().getLsRegions();

				for (Region region : lsRegions) {
					List<Location> lsLocations = region.getLsLocations();
					for (Location location : lsLocations) {

						if (0 == location.getId().compareTo(standbyScheduleSearchDto.getLocationId())) {
							if (!filteredBodies.contains(standbyScheduleBody)) {
								filteredBodies.add(standbyScheduleBody);
							}
							break;
						}
					}

					if (filteredBodies.contains(standbyScheduleBody)) {
						break;
					}
				}
			}

		} else {
			filteredBodies = listBodies;
		}

		return filteredBodies;
	}

	public boolean matchEmptySearch(StandbyScheduleSearchDto standbyScheduleSearchDto) {
		boolean match = false;

		if (standbyScheduleSearchDto.getCity() == null || standbyScheduleSearchDto.getCity().equals("")) {
			match = true;
		}
		return match;
	}

}
