/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.db.model.CalendarDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CalendarRepository extends JpaRepository<CalendarDay, Long> {

	@Query(value = "SELECT cal FROM CalendarDay as cal "
			+ "WHERE (cal.dateIndex >= :startDate AND cal.dateIndex <= :endDate) ORDER BY cal.dateIndex ASC")
	public List<CalendarDay> findValidByDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	public List<CalendarDay> findAllByOrderByDateIndexAsc();

	public List<CalendarDay> findAllByOrderByDateIndexDesc();

}