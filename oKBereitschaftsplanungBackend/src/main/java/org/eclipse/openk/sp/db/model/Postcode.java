/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "POSTCODE" database table.
 */
@Entity
@Table(name = "Postcode")
public class Postcode extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "POSTCODE_ID_SEQ")
	@SequenceGenerator(name = "POSTCODE_ID_SEQ", sequenceName = "POSTCODE_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "postcode", length = 256, nullable = false)
	private String pcode;

	@ManyToMany(mappedBy = "lsPostcode")
	private List<Location> lsLocation = new ArrayList<>();

	public Postcode() {
	}

	public Postcode(Long id) {
		super();
		this.id = id;
	}

	public Postcode(String string) {
		super();
		this.pcode = string;
	}

	/**
	 * @return the pcode
	 */
	public String getPcode() {
		return pcode;
	}

	/**
	 * @param pcode
	 *            the pcode to set
	 */
	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	/**
	 * @return the lsLocation
	 */
	public List<Location> getLsLocation() {
		return lsLocation;
	}

	/**
	 * @param lsLocation
	 *            the lsLocation to set
	 */
	public void setLsLocation(List<Location> lsLocation) {
		this.lsLocation = lsLocation;
	}

	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}
