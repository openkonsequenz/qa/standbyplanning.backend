/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "SpResponseDto" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyDurationResponseDto")
@JsonInclude(Include.NON_NULL)
public class StandbyDurationResponseDto extends AbstractDto {
	/***/
	private static final long serialVersionUID = 1L;
	private List<PlanningMsgDto> lsMsg = new ArrayList<>();
	private List<StandbyDurationDto> data = new ArrayList<>();

	/**
	 * @return the data
	 */
	public List<StandbyDurationDto> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<StandbyDurationDto> data) {
		this.data = data;
	}

	/**
	 * @return the lsMsg
	 */
	public List<PlanningMsgDto> getLsMsg() {
		return lsMsg;
	}

	/**
	 * @param lsMsg the lsMsg to set
	 */
	public void setLsMsg(List<PlanningMsgDto> lsMsg) {
		this.lsMsg = lsMsg;
	}
}
