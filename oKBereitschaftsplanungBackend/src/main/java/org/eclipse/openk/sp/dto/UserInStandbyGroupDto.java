/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "USERINSTANDBYGROUP" Data Transfer Object (DTO)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "UserInStandbyGroupDto")
@JsonInclude(Include.NON_NULL)
public class UserInStandbyGroupDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Long standbyGroupId;
	@NotNull(message = "Mitarbeiter ist nicht gesetzt")
	private Long userId;
	private String firstname;
	private String lastname;

	@NotNull(message = "Gültig von ist nicht gesetzt")
	private Date validFrom;
	@NotNull(message = "Gültig bis ist nicht gesetzt")
	private Date validTo;

	private String userRegionStr;
	private String userHasUserFunctionStr;
	private String organisationName;
	private Integer position;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the standbyGroupId
	 */
	public Long getStandbyGroupId() {
		return standbyGroupId;
	}

	/**
	 * @param standbyGroupId the standbyGroupId to set
	 */
	public void setStandbyGroupId(Long standbyGroupId) {
		this.standbyGroupId = standbyGroupId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the userRegionStr
	 */
	public String getUserRegionStr() {
		return userRegionStr;
	}

	/**
	 * @param userRegionStr the userRegionStr to set
	 */
	public void setUserRegionStr(String userRegionStr) {
		this.userRegionStr = userRegionStr;
	}

	/**
	 * @return the userHasUserFunctionStr
	 */
	public String getUserHasUserFunctionStr() {
		return userHasUserFunctionStr;
	}

	/**
	 * @param userHasUserFunctionStr the userHasUserFunctionStr to set
	 */
	public void setUserHasUserFunctionStr(String userHasUserFunctionStr) {
		this.userHasUserFunctionStr = userHasUserFunctionStr;
	}

	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	/**
	 * @param organisationName the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	/**
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Integer pos) {
		this.position = pos;
	}
}
