/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.UserController;
import org.eclipse.openk.sp.controller.msg.ValidationMsgController;
import org.eclipse.openk.sp.controller.validation.IValidator;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to validate if all time slots are set for a group. */
public class UserAvailableValidator extends AbstractController implements IValidator {
	protected static final Logger LOGGER = Logger.getLogger(UserAvailableValidator.class);

	@Autowired
	private UserController userController;

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<PlanningMsgDto> execute(Date from, Date till, StandbyGroup currentGroup,
			List<StandbyGroup> lsStandbyGroups, Long statusId, Long userId) throws SpException {
		List<PlanningMsgDto> planningMsgDtos = new ArrayList<>();

		Boolean isValid = userController.isUserValid(userId, from, till);
		if (!isValid) {
			planningMsgDtos.addAll(ValidationMsgController.createMsgInvalidTimeForUser(userRepository.findOne(userId)));
		}
		return planningMsgDtos;
	}
}
