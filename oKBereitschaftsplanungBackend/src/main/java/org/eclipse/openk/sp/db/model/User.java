/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "USER" database table.
 */
@Entity
@Table(name = "STANDBY_USER")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class User extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_USER_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_USER_ID_SEQ", sequenceName = "STANDBY_USER_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "hr_number", length = 256, nullable = true)
	private String hrNumber;

	@Column(name = "user_key", length = 256, nullable = true)
	private String userKey;

	@Column(name = "firstname", length = 256, nullable = false)
	private String firstname;

	@Column(name = "lastname", length = 256, nullable = false)
	private String lastname;

	@Column(name = "is_company", nullable = false)
	@Convert("booleanConverter")
	private Boolean isCompany;

	@Column(name = "notes", length = 2048, nullable = true)
	private String notes;

	@Column(name = "valid_from", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "valid_to", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "private_address_id", nullable = true)
	private Address privateAddress;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "business_contact_id", nullable = true)
	private ContactData businessContactData;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "private_contact_id", nullable = true)
	private ContactData privateContactData;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "organisation_id", nullable = true)
	private Organisation organisation;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", nullable = true)
	private List<UserInRegion> lsUserInRegions = new ArrayList<>();

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", nullable = true)
	private List<UserHasUserFunction> lsUserHasUserFunction = new ArrayList<>();

	public User() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname
	 *            the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname
	 *            the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the isCompany
	 */
	public Boolean getIsCompany() {
		return isCompany;
	}

	/**
	 * @param isCompany
	 *            the isCompany to set
	 */
	public void setIsCompany(Boolean isCompany) {
		this.isCompany = isCompany;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the privateAddress
	 */
	public Address getPrivateAddress() {
		return privateAddress;
	}

	/**
	 * @param privateAddress
	 *            the privateAddress to set
	 */
	public void setPrivateAddress(Address privateAddress) {
		this.privateAddress = privateAddress;
	}

	/**
	 * @return the businessContactData
	 */
	public ContactData getBusinessContactData() {
		return businessContactData;
	}

	/**
	 * @param businessContactData
	 *            the businessContactData to set
	 */
	public void setBusinessContactData(ContactData businessContactData) {
		this.businessContactData = businessContactData;
	}

	/**
	 * @return the privateContactData
	 */
	public ContactData getPrivateContactData() {
		return privateContactData;
	}

	/**
	 * @param privateContactData
	 *            the privateContactData to set
	 */
	public void setPrivateContactData(ContactData privateContactData) {
		this.privateContactData = privateContactData;
	}

	/**
	 * @return the organisation
	 */
	public Organisation getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation
	 *            the organisation to set
	 */
	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return the lsUserInRegions
	 */
	public List<UserInRegion> getLsUserInRegions() {
		return lsUserInRegions;
	}

	/**
	 * @param lsUserInRegions
	 *            the lsUserInRegions to set
	 */
	public void setLsUserInRegions(List<UserInRegion> lsUserInRegions) {
		this.lsUserInRegions = lsUserInRegions;
	}

	/**
	 * @return the lsUserHasUserFunction
	 */
	public List<UserHasUserFunction> getLsUserHasUserFunction() {
		return lsUserHasUserFunction;
	}

	/**
	 * @param lsUserHasUserFunction
	 *            the lsUserHasUserFunction to set
	 */
	public void setLsUserHasUserFunction(List<UserHasUserFunction> lsUserHasUserFunction) {
		this.lsUserHasUserFunction = lsUserHasUserFunction;
	}

	/**
	 * @return the hrNumber
	 */
	public String getHrNumber() {
		return hrNumber;
	}

	/**
	 * @param hrNumber
	 *            the hrNumber to set
	 */
	public void setHrNumber(String hrNumber) {
		this.hrNumber = hrNumber;
	}

	/**
	 * @return the userKey
	 */
	public String getUserKey() {
		return userKey;
	}

	/**
	 * @param userKey
	 *            the userKey to set
	 */
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}
}
