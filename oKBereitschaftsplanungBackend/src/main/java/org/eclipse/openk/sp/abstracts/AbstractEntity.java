/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.abstracts;

import java.io.Serializable;

public abstract class AbstractEntity implements Serializable, Comparable<AbstractEntity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract Long getId();

	public abstract void setId(Long id);

	public AbstractEntity copy() {
		return null;
	};

}
