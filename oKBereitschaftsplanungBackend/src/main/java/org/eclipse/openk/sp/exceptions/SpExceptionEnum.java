/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.exceptions;

public enum SpExceptionEnum {

	/** 401 **/
	HTTP_UNAUTHERIZED_EXCEPTION(new SpErrorEntry(401, "Authentifizierung ist fehlgeschlagen.")),
	/** 404 **/
	HTTP_NOT_FOUND_EXCEPTION(new SpErrorEntry(404, "Der Service ist nicht erreichbar.")),
	/** 500 **/
	HTTP_INTERNAL_SERVER_EXCEPTION(new SpErrorEntry(500, "Interner Fehler aufgetreten.")),
	DB_TRANSACTIONAL_EXCEPTION(new SpErrorEntry(500, "Es kann nicht gespeichert werden.")),
	/** 1000 - Default Exception (unknown or uncaught). */
	DEFAULT_EXCEPTION(new SpErrorEntry(1000, "Es ist ein unbekannter Fehler aufgetreten.")),
	// UNKNOWN_ENTITY_EXCEPTION if no entity will be found for the given id.
	UNKNOWN_ENTITY_EXCEPTION(new SpErrorEntry(1001, "Eines der folgenden Entities kann nicht gefunden werden: {0}")),
	// LOADING_LIST_EXCEPTION - list of entities could not get loaded.
	LOADING_LIST_EXCEPTION(new SpErrorEntry(1002, "Liste für folgenden Entities kann nicht geladen werden: {0}")),
	// COULD_NOT_SAVE_ENTITY_EXCEPTION
	COULD_NOT_SAVE_ENTITY_EXCEPTION(new SpErrorEntry(1003, "Folgende Entities konnten nicht gespeichert werden: {0}")),
	// COULD_NOT_DELETE_ENTITY_EXCEPTION
	COULD_NOT_DELETE_ENTITY_EXCEPTION(new SpErrorEntry(1004,
			"Folgende Entities konnten nicht gelöscht werden: {0}, da folgender Fehler aufgetreten ist. ( {1} )")),
	// MISSING_PARAMETER_EXCEPTION - at least one parameter is missing
	MISSING_PARAMETER_EXCEPTION(new SpErrorEntry(1005, "Es fehlt der folgendende Einagebeparameter : {0}")),
	// MISSING_PARAMETER_EXCEPTION - at least one parameter is missing
	MISSING_PARAMETERS_EXCEPTION(new SpErrorEntry(1006, "Die Eingabeparameter sind nicht vollständig")),
	// USER_NOT_AVAILABLE_EXCEPTION - not valid for time
	USER_NOT_AVAILABLE_EXCEPTION(new SpErrorEntry(1007,
			"Der Benutzer ist für den angegebenen Zeitraum nicht valide und steht somit nicht zur Verfügung.")),
	// USER_NOT_AVAILABLE_EXCEPTION - not valid for time
	USER_NOT_IN_GROUP_EXCEPTION(new SpErrorEntry(1008,
			"Der Benutzer steht der gewählten Gruppe für den angegebenen Zeitraum nicht zur Verfügung.")),
	// THE BEAN CONTAINER FOR THE REPORT IS EMPTY
	NO_DATA_FOR_REPORT_FOUND(new SpErrorEntry(1009, "Es wurden keine Daten gefunden. Report ohne Inhalt. ")),
	/** Bodies found and no overwite permission **/
	NO_OVERWRITE_PERMISSION(new SpErrorEntry(2001,
			"Es wurden im Zeitintervall ({0}, {1}) verplante Bereitschaften gefunden. Das Überschreiben der Bereitschaften ist verboten!")),
	NO_STANDBY_GROUP_IN_LIST(new SpErrorEntry(2002, "Es ist keine Bereitschaftsgruppe der Liste zugeordnet! {0}")),
	ERROR_ON_LIST(new SpErrorEntry(2000, "Es ist ein Fehler bei der Verarbeitung der Liste aufgetreten! {0}"));

	private final SpErrorEntry entry;

	/**
	 * MMException enum constructor.
	 * 
	 * @param entry the errorEntry
	 */
	SpExceptionEnum(SpErrorEntry entry) {
		this.entry = entry;
	}

	/**
	 * getter.
	 * 
	 * @return {@link ErrorEntry} the errorEntry for the exception
	 */
	public SpErrorEntry getEntry() {
		return new SpErrorEntry(this.entry.getCode(), this.entry.getMessage(), null);
	}
}
