/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * This class enables HTTP JSON (de)serialization to have deterministic date
 * conversion.
 *
 * @author knauth
 */
public class CustomDateYMDDeserializer extends StdDeserializer<Date> {

	private static final long serialVersionUID = 1L;

	public CustomDateYMDDeserializer() {
		this(null);
	}

	public CustomDateYMDDeserializer(Class<Date> t) {
		super(t);
	}

	@Override
	public Date deserialize(JsonParser jsonParser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		String sDate = jsonParser.getText();
		try {
			Date ret = DateHelper.parseDate(sDate);
			return ret;
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}

	}
}
