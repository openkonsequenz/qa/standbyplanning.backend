/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.db.model.ArchiveStandbyScheduleHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ArchiveRepository extends JpaRepository<ArchiveStandbyScheduleHeader, Long> {

	List<ArchiveStandbyScheduleHeader> findAllByOrderByModificationDateDesc();

	@Query(value = "SELECT archive FROM ArchiveStandbyScheduleHeader archive " + "WHERE ("
			+ "archive.validFrom <= :validFrom AND archive.validTo >= :validTo AND archive.statusId = :statusId) "
			+ "ORDER BY archive.validFrom ASC")
	List<ArchiveStandbyScheduleHeader> findByDateAndStatus(@Param("validFrom") Date validFrom,
			@Param("validTo") Date validTo, @Param("statusId") Long status);

	@Query(value = "SELECT archive FROM ArchiveStandbyScheduleHeader archive " + "WHERE ("
			+ "archive.validFrom <= :validFrom AND archive.validTo >= :validTo) " + "ORDER BY archive.validFrom ASC")
	List<ArchiveStandbyScheduleHeader> findByDate(@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

}