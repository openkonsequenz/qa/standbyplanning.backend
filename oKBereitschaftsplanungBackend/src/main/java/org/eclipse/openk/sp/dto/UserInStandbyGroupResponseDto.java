/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "USERINSTANDBYGROUP" Data Transfer Object (DTO)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "UserInStandbyGroupResponseDto")
@JsonInclude(Include.NON_NULL)
public class UserInStandbyGroupResponseDto extends AbstractDto {

	private static final long serialVersionUID = 1L;
	private List<PlanningMsgDto> lsMsg = new ArrayList<>();
	private List<UserInStandbyGroupDto> data = new ArrayList<>();

	/**
	 * @return the lsMsg
	 */
	public List<PlanningMsgDto> getLsMsg() {
		return lsMsg;
	}

	/**
	 * @param lsMsg
	 *            the lsMsg to set
	 */
	public void setLsMsg(List<PlanningMsgDto> lsMsg) {
		this.lsMsg = lsMsg;
	}

	/**
	 * @return the data
	 */
	public List<UserInStandbyGroupDto> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<UserInStandbyGroupDto> data) {
		this.data = data;
	}
}
