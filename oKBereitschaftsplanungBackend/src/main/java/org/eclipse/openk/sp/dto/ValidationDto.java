/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.dto.planning.StandbyScheduleActionDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "ValidationDto")
@JsonInclude(Include.NON_NULL)
public class ValidationDto extends StandbyScheduleActionDto {
	/***/
	private static final long serialVersionUID = 1L;
	private Long standbyListId;

	/**
	 * @return the standbyListId
	 */
	public Long getStandbyListId() {
		return standbyListId;
	}

	/**
	 * @param standbyListId the standbyListId to set
	 */
	public void setStandbyListId(Long standbyListId) {
		this.standbyListId = standbyListId;
	}

}
