/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.lang.reflect.Type;
import java.util.ArrayList;

import org.eclipse.openk.sp.controller.external.dto.DistanceServiceDto;
import org.eclipse.openk.sp.db.model.CalendarDay;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.Organisation;
import org.eclipse.openk.sp.db.model.Postcode;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.OrganisationDto;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.dto.RegionDto;
import org.eclipse.openk.sp.dto.StandbyDurationDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyListDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodyDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.dto.UserFunctionDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupDto;
import org.eclipse.openk.sp.dto.report.ReportDto;

import com.google.gson.reflect.TypeToken;

public class GsonTypeHelper {

	protected GsonTypeHelper() {
	}

	/**
	 * User
	 */
	public static final Type JSON_TYPE_USER_ARRAY_LIST = new TypeToken<ArrayList<User>>() {
	}.getType();

	public static final Type JSON_TYPE_USER_DTO_ARRAY_LIST = new TypeToken<ArrayList<UserDto>>() {
	}.getType();

	/**
	 * Location
	 */
	public static final Type JSON_TYPE_LOCATION_ARRAY_LIST = new TypeToken<ArrayList<Location>>() {
	}.getType();

	public static final Type JSON_TYPE_LOCATION_DTO_ARRAY_LIST = new TypeToken<ArrayList<LocationDto>>() {
	}.getType();

	/**
	 * StandbyGroup
	 */
	public static final Type JSON_TYPE_STANDBYGROUP_ARRAY_LIST = new TypeToken<ArrayList<StandbyGroup>>() {
	}.getType();

	public static final Type JSON_TYPE_STANDBYGROUP_DTO_ARRAY_LIST = new TypeToken<ArrayList<StandbyGroupDto>>() {
	}.getType();

	public static final Type JSON_TYPE_STANDBYGROUP_SELECTIOM_DTO_ARRAY_LIST = new TypeToken<ArrayList<StandbyGroupSelectionDto>>() {
	}.getType();

	/**
	 * Region
	 */
	public static final Type JSON_TYPE_REGION_ARRAY_LIST = new TypeToken<ArrayList<Region>>() {
	}.getType();

	public static final Type JSON_TYPE_REGION_DTO_ARRAY_LIST = new TypeToken<ArrayList<RegionDto>>() {
	}.getType();

	/**
	 * UserFunction
	 */
	public static final Type JSON_TYPE_USERFUNCTION_ARRAY_LIST = new TypeToken<ArrayList<UserFunction>>() {
	}.getType();

	public static final Type JSON_TYPE_USERFUNCTION_DTO_ARRAY_LIST = new TypeToken<ArrayList<UserFunctionDto>>() {
	}.getType();

	/**
	 * Postcode
	 */
	public static final Type JSON_TYPE_POSTCODE_ARRAY_LIST = new TypeToken<ArrayList<Postcode>>() {
	}.getType();

	public static final Type JSON_TYPE_POSTCODE_DTO_ARRAY_LIST = new TypeToken<ArrayList<PostcodeDto>>() {
	}.getType();

	/**
	 * Organisation
	 */
	public static final Type JSON_TYPE_ORGANISATION_ARRAY_LIST = new TypeToken<ArrayList<Organisation>>() {
	}.getType();

	public static final Type JSON_TYPE_ORGANISATION_DTO_ARRAY_LIST = new TypeToken<ArrayList<OrganisationDto>>() {
	}.getType();

	/**
	 * UserInStandbyGroup
	 */
	public static final Type JSON_TYPE_USERINSTANDBYGROUP_ARRAY_LIST = new TypeToken<ArrayList<UserInStandbyGroup>>() {
	}.getType();

	public static final Type JSON_TYPE_USERINSTANDBYGROUP_DTO_ARRAY_LIST = new TypeToken<ArrayList<UserInStandbyGroupDto>>() {
	}.getType();

	/**
	 * StandbyList
	 */
	public static final Type JSON_TYPE_STANDBYLIST_ARRAY_LIST = new TypeToken<ArrayList<StandbyList>>() {
	}.getType();

	public static final Type JSON_TYPE_STANDBYLIST_DTO_ARRAY_LIST = new TypeToken<ArrayList<StandbyListDto>>() {
	}.getType();

	/**
	 * StandbyBody
	 */
	public static final Type JSON_TYPE_STANDBYBODY_ARRAY_LIST = new TypeToken<ArrayList<StandbyScheduleBody>>() {
	}.getType();

	public static final Type JSON_TYPE_STANDBYBODY_DTO_ARRAY_LIST = new TypeToken<ArrayList<StandbyScheduleBodyDto>>() {
	}.getType();

	public static final Type JSON_TYPE_STANDBYBODY_SELECTION_DTO_ARRAY_LIST = new TypeToken<ArrayList<StandbyScheduleBodySelectionDto>>() {
	}.getType();

	/**
	 * StandbyDuration
	 */
	public static final Type JSON_TYPE_STANDBYDURATION_ARRAY_LIST = new TypeToken<ArrayList<StandbyDuration>>() {
	}.getType();

	public static final Type JSON_TYPE_STANDBYDURATION_DTO_ARRAY_LIST = new TypeToken<ArrayList<StandbyDurationDto>>() {
	}.getType();

	/**
	 * CALENDARDAY
	 */
	public static final Type JSON_TYPE_CALENDARDAY_ARRAY_LIST = new TypeToken<ArrayList<CalendarDay>>() {
	}.getType();

	public static final Type JSON_TYPE_CALENDARDAY_DTO_ARRAY_LIST = new TypeToken<ArrayList<CalendarDayDto>>() {
	}.getType();

	/**
	 * REPORTS
	 */
	public static final Type JSON_TYPE_REPORT_DTO_ARRAY_LIST = new TypeToken<ArrayList<ReportDto>>() {
	}.getType();

	/**
	 * DistanceServiceDto
	 */
	public static final Type JSON_TYPE_DISTANCESERVICE_DTO_ARRAY_LIST = new TypeToken<ArrayList<DistanceServiceDto>>() {
	}.getType();

}
