/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.msg.ValidationMsgController;
import org.eclipse.openk.sp.controller.validation.IValidator;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to validate if all time slots are set for a group. */
public class DoublePlannedValidator extends AbstractController implements IValidator {
	protected static final Logger LOGGER = Logger.getLogger(DoublePlannedValidator.class);
	protected static final long DIVISOR_MS_TO_MIN = 60000;

	@Autowired
	private StandbyScheduleBodyRepository scheduleBodyRepository;

	@Override
	public List<PlanningMsgDto> execute(Date from, Date till, StandbyGroup currentGroup,
			List<StandbyGroup> lsStandbyGroups, Long statusId, Long userId) throws SpException {
		List<PlanningMsgDto> planningMsgDtos = new ArrayList<>();

		// fetch all active schedule bodies
		List<StandbyScheduleBody> lsBodies = scheduleBodyRepository.findByGroupAndDateAndStatus(currentGroup.getId(),
				from, till, statusId);

		// loop over every single scheduleBody and check if the user is planned in other
		// groups at the same time
		for (StandbyScheduleBody body : lsBodies) {
			List<StandbyScheduleBody> lsAlternativeBodies = scheduleBodyRepository
					.findByUserHittingDateInterval(body.getUser().getId(), body.getValidFrom(), body.getValidTo());
			if (lsAlternativeBodies != null) {
				for (StandbyScheduleBody alternativeBody : lsAlternativeBodies) {
					// no msg if its the same schedule body entry or same group in status 1 or
					// status 2
					if (alternativeBody.getId().longValue() != body.getId().longValue() && alternativeBody
							.getStandbyGroup().getId().longValue() != body.getStandbyGroup().getId().longValue()) {
						planningMsgDtos
								.addAll(ValidationMsgController.createMsgInvalidGroupForUser(body, alternativeBody));
					}
				}
			}
		}
		return planningMsgDtos;
	}
}
