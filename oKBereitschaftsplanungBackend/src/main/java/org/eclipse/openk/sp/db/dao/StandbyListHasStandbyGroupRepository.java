/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.List;

import org.eclipse.openk.sp.db.model.StandbyListHasStandbyGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StandbyListHasStandbyGroupRepository extends JpaRepository<StandbyListHasStandbyGroup, Long> {

	@Query(value = "SELECT m FROM StandbyListHasStandbyGroup m WHERE m.standbyGroup.id = :id")
	public List<StandbyListHasStandbyGroup> findById(@Param("id") Long id);

	@Query(value = "SELECT COUNT (uisg.position) FROM StandbyListHasStandbyGroup uisg WHERE (uisg.standbyGroup.id = :groupId)")
	public Integer getNextPositionForGroupMember(@Param("groupId") Long groupId);

	@Query(value = "SELECT slhg FROM StandbyListHasStandbyGroup slhg WHERE (slhg.standbyList.id = :listId AND slhg.position >= :pos)")
	public List<StandbyListHasStandbyGroup> findListMemberWithMinPosition(@Param("listId") Long listId,
			@Param("pos") Integer pos);

}