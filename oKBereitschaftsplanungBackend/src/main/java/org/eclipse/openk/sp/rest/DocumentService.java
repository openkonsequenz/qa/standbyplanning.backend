/********************************************************************************
 * Copyright (c) 2019 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.io.File;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.StreamingOutput;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.DocumentController;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import io.swagger.annotations.ApiParam;

@Path("documents")
@Transactional
public class DocumentService extends BaseResource {

	@Autowired
	private DocumentController documentController;

	public DocumentService() {
		super(Logger.getLogger(DocumentService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/user/documentation")
	@Produces({ "application/pdf" })
	public StreamingOutput getUserDocumentation(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<File> invokable = modusr -> documentController.getUserDocumentation();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnableStream(jwt, securityRoles, invokable);
	}
}
