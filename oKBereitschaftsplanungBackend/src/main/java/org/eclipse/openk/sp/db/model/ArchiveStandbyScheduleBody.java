/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "STANDBY_SCHEDULE_BODY" database table.
 */
@Entity
@Table(name = "ARCHIVE_BODY")
public class ArchiveStandbyScheduleBody extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARCHIVE_BODY_ID_SEQ")
	@SequenceGenerator(name = "ARCHIVE_BODY_ID_SEQ", sequenceName = "ARCHIVE_BODY_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "valid_from", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "valid_to", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@Column(name = "modification_by", nullable = false)
	private String modifiedBy;

	@Column(name = "modification_cause", nullable = true)
	private String modifiedCause;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status_id", nullable = false)
	private StandbyStatus status;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "standby_group_id", nullable = false)
	private StandbyGroup standbyGroup;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "archive_header_id", nullable = false)
	private ArchiveStandbyScheduleHeader standbyHeader;

	public ArchiveStandbyScheduleBody(ArchiveStandbyScheduleHeader header, StandbyScheduleBody body) {

		setStandbyGroup(body.getStandbyGroup());
		setStatus(body.getStatus());
		setUser(body.getUser());
		setValidFrom(body.getValidFrom());
		setValidTo(body.getValidTo());
		setModificationDate(body.getModificationDate());
		setModifiedBy(body.getModifiedBy());
		setModifiedCause(body.getModifiedCause());
		setStandbyHeader(header);
	}

	public ArchiveStandbyScheduleBody() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the status
	 */
	public StandbyStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(StandbyStatus status) {
		this.status = status;
	}

	/**
	 * @return the standbyGroup
	 */
	public StandbyGroup getStandbyGroup() {
		return standbyGroup;
	}

	/**
	 * @param standbyGroup
	 *            the standbyGroup to set
	 */
	public void setStandbyGroup(StandbyGroup standbyGroup) {
		this.standbyGroup = standbyGroup;
	}

	/**
	 * @return the modifiedCause
	 */
	public String getModifiedCause() {
		return modifiedCause;
	}

	/**
	 * @param modifiedCause
	 *            the modifiedCause to set
	 */
	public void setModifiedCause(String modifiedCause) {
		this.modifiedCause = modifiedCause;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

	/**
	 * @return the standbyHeader
	 */
	public ArchiveStandbyScheduleHeader getStandbyHeader() {
		return standbyHeader;
	}

	/**
	 * @param standbyHeader
	 *            the standbyHeader to set
	 */
	public void setStandbyHeader(ArchiveStandbyScheduleHeader standbyHeader) {
		this.standbyHeader = standbyHeader;
	}

}
