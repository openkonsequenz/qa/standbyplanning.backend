/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.BranchRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle BRANCH operations. */
public class BranchController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(BranchController.class);

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private EntityConverter entityConverter;

	/**
	 * 
	 * @return
	 */
	public List<BranchDto> getBranchSelection() throws SpException {
		try {
			ArrayList<BranchDto> listLocation = new ArrayList<>();
			Iterable<Branch> itBranchList = branchRepository.findAllByOrderByTitleAsc();
			for (Branch branch : itBranchList) {
				listLocation.add((BranchDto) entityConverter.convertEntityToDto(branch, new BranchDto()));
			}
			return listLocation;
		} catch (Exception e) {
			SpException spE = new SpException(SpExceptionEnum.LOADING_LIST_EXCEPTION, SpMsg.TXT_BRANCHES);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}
}
