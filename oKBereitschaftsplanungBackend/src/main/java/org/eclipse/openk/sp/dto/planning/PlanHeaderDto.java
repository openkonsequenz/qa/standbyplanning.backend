/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "PlanHeaderDto")
@JsonInclude(Include.NON_NULL)
public class PlanHeaderDto  extends AbstractDto{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String label; 
	
	private ArrayList<StandbyGroupSelectionDto> listGroups = new ArrayList<>();

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param lable the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the listGroups
	 */
	public List<StandbyGroupSelectionDto> getListGroups() {
		return listGroups;
	}

	/**
	 * @param listGroups the listGroups to set
	 */
	public void setListGroups(List<StandbyGroupSelectionDto> listGroups) {
		this.listGroups = (ArrayList<StandbyGroupSelectionDto>) listGroups;
	} 
	
}
