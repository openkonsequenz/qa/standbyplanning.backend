/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.impl.pseudonymization.NewPseudonymizationData;
import org.eclipse.openk.sp.controller.impl.pseudonymization.PseudonymizationCtx;
import org.eclipse.openk.sp.controller.impl.pseudonymization.PseudonymizationHelperController;
import org.eclipse.openk.sp.controller.impl.pseudonymization.PseudonymizeArchiveHelperController;
import org.eclipse.openk.sp.db.dao.ArchiveRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.ArchiveStandbyScheduleHeader;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.NoopResultDto;
import org.eclipse.openk.sp.dto.PseudonymizationResultDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * High level API that performes pseudonymization. (Implementation details are
 * implemented in other classes.)
 *
 * @author Jürgen Knauth
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PseudonymizationController extends AbstractController {

	// ================================================================
	// == Constants
	// ================================================================

	// ================================================================
	// == Variables
	// ================================================================

	protected static final Logger LOGGER = Logger.getLogger(PseudonymizationController.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ArchiveRepository archiveRepository;

	@Autowired
	private PseudonymizeArchiveHelperController pseudonymizeArchiveHelper;

	@Autowired
	private PseudonymizationHelperController pseudonymizationHelper;

	// ================================================================
	// == Constructors
	// ================================================================

	// ================================================================
	// == Helper Methods
	// ================================================================

	/**
	 * Retrieve a fully initialized user object
	 *
	 * @param userId The ID of the user to load
	 * @return Returns the user object
	 * @throws SpException An exception is thrown if the user ist not found
	 */
	private User x_getUserE(long userId) throws SpException {
		User user = null;
		try {
			user = userRepository.findOne(userId);
		} catch (Exception ee) {
			throw new SpException(400, "No such user.", ee);
		}
		if (user == null) {
			throw new SpException(400, "No such user.");
		}

		user.getLsUserInRegions(); // required to force loading (and compensate for lazy loading)

		return user;
	}

	// ================================================================
	// == Public Methods
	// ================================================================

	/**
	 * Pseudonymize a user.
	 *
	 * @param userId    (required) The ID of the user to pseudonymize
	 * @param bDoModify (required) Specify <code>false</code> here for a test run
	 *                  without changes to simulate pseudonymization. Specify
	 *                  <code>true</code> here to actually perform pseudonymization.
	 * @return Returns a DTO that provides information about the planned/actually
	 *         performed changes.
	 * @throws SpException
	 */
	public PseudonymizationResultDto pseudonymize(long userId, final boolean bDoModify) throws SpException {
		try {
			final User user = x_getUserE(userId);
			// Produce an object that holds new pseudonymization data to fill in on
			// performing pseudonymization.
			final NewPseudonymizationData pd = new NewPseudonymizationData(userId);
			final PseudonymizationCtx ctx = new PseudonymizationCtx(bDoModify, user, pd);

			// ----------------------------------------------------------------

			ctx.logInfo("----Replacing: " + user.getId());
			ctx.logInfo("----Actually modifying: " + bDoModify);

			// ----------------------------------------------------------------
			// ==>> modify user

			ctx.logInfo("----BEGIN MODIFYING USER");
			pseudonymizationHelper.modifyUser(ctx, user);
			ctx.logInfo("----END MODIFYING USER");

			// ----------------------------------------------------------------
			// ==>> modify archive

			ctx.logInfo("----BEGIN MODIFYING ARCHIVE");
			pseudonymizeArchiveHelper.modifyAll_ArchiveStandbyScheduleHeader(ctx);
			ctx.logInfo("----END MODIFYING ARCHIVE");

			// ----------------------------------------------------------------
			// ==>> scan history tables

			ctx.logInfo("----BEGIN MODIFYING HISTORY TABLES");
			pseudonymizationHelper.modifyUserHistByUserId(ctx, userId);
			pseudonymizationHelper.modifyAll_StandbyScheduleBody(ctx);
			pseudonymizationHelper.modifyAll_StandbyScheduleBodyHist(ctx);
			pseudonymizationHelper.modifyAll_StandbyScheduleHistory(ctx);
			pseudonymizationHelper.modifyAll_StandbyScheduleHistoryHist(ctx);
			ctx.logInfo("----END MODIFYING HISTORY TABLES");

			// ----------------------------------------------------------------

			return ctx.result;
		} catch (Exception ee) {
			System.out.println("----");
			ee.printStackTrace();
			System.out.println("----");

			SpErrorEntry errorEntry = SpExceptionEnum.HTTP_INTERNAL_SERVER_EXCEPTION.getEntry();
			SpException spException = new SpException(errorEntry);
			spException.setStackTrace(ee.getStackTrace());
			throw spException;
		}
	}

	public NoopResultDto noop() {
		return new NoopResultDto();
	}

}
