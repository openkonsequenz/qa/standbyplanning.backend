/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.db.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findAllByOrderByLastnameAsc();

	@Query(value = "SELECT DISTINCT u " + "FROM User as u " + "INNER JOIN UserHasUserFunction as uhf "
			+ "ON u.id = uhf.user.id " + "INNER JOIN UserInRegion as uir " + "ON u.id = uir.user.id "
			+ "WHERE (uhf.userFunction.id IN :strFIDs " + "AND uir.region.id IN :strRIDs "
			+ "AND u.validTo >= :validDate " + "AND u.validFrom <= :validDate) " + "ORDER BY u.lastname ASC")
	public List<User> findByStandbyGroupFIDRID(@Param("strFIDs") List<Long> strFIDs,
			@Param("strRIDs") List<Long> strRIDs, @Param("validDate") Date validDate);

	@Query(value = "SELECT DISTINCT u " + "FROM User as u " + "INNER JOIN UserHasUserFunction as uhf "
			+ "ON u.id = uhf.user.id " + "WHERE (uhf.userFunction.id IN :strFIDs " + "AND u.validTo >= :validDate "
			+ "AND u.validFrom <= :validDate) " + "ORDER BY u.lastname ASC")
	public List<User> findByStandbyGroupFID(@Param("strFIDs") List<Long> strFIDs, @Param("validDate") Date validDate);

	@Query(value = "SELECT DISTINCT u " + "FROM User as u " + "INNER JOIN UserInRegion as uir "
			+ "ON u.id = uir.user.id " + "WHERE (uir.region.id IN :strRIDs " + "AND u.validTo >= :validDate "
			+ "AND u.validFrom <= :validDate) " + "ORDER BY u.lastname ASC")
	public List<User> findByStandbyGroupRID(@Param("strRIDs") List<Long> strRIDs, @Param("validDate") Date validDate);

	@Query(value = "SELECT DISTINCT u " + "FROM User as u " + "WHERE (u.validTo >= :validDate "
			+ "AND u.validFrom <= :validDate) " + "ORDER BY u.lastname ASC")
	public List<User> findAllValid(@Param("validDate") Date validDate);
}