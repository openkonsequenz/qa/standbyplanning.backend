/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "PSEUDONYMIZATION_RESULT" Data Transfer Object (DTO).
 * This DTO returns detailed information about the pseudonymization process.
 *
 * @author Jürgen Knauth
 */
@XmlRootElement(name = "PseudonymizationResultDto")
@JsonInclude(Include.NON_NULL)
public class PseudonymizationResultDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;
	private Long userId;
	private Boolean success;
	private ArrayList<String> logMsgs;

	/**
	 * Convenience constructor.
	 */
	public PseudonymizationResultDto(Long userId, Boolean success, ArrayList<String> logMsgs) {
		this.userId = userId;
		this.success = success;
		this.logMsgs = logMsgs;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId		the id to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the result
	 */
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * @param success          the result to set
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	/**
	 * Direct access to the list that holds log messages.
	 *
	 * @return the result
	 */
	public ArrayList<String> getLogMsgs() {
		return logMsgs;
	}

	/**
	 * @param success          the result to set
	 */
	public void setLogMsgs(ArrayList<String> logMsgs) {
		this.logMsgs = logMsgs;
	}

}
