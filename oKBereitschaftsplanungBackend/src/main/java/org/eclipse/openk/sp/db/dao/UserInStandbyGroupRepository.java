/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserInStandbyGroupRepository extends JpaRepository<UserInStandbyGroup, Long> {

	@Query(value = "SELECT m FROM UserInStandbyGroup m WHERE m.standbyGroup.id = :id")
	public List<UserInStandbyGroup> findById(@Param("id") Long id);

	@Query(value = "SELECT DISTINCT m.user FROM UserInStandbyGroup m WHERE m.standbyGroup.id = :id")
	public List<User> findUniqueById(@Param("id") Long id);

	@Query(value = "SELECT DISTINCT sbu.id, sbu.lastname, sbu.firstname FROM user_in_standby_group uig "
			+ "INNER JOIN standby_user sbu ON uig.user_id = sbu.id "
			+ "WHERE uig.standby_group_id = ? ORDER BY sbu.lastname", nativeQuery = true)
	public List<Object[]> findUniqueGroupUser(@Param("id") Long id);

	@Query(value = "SELECT uisg FROM UserInStandbyGroup uisg WHERE (uisg.standbyGroup.id = :groupId AND uisg.validFrom <= :validFrom AND uisg.validTo >= :validTo)")
	public List<UserInStandbyGroup> findUserForInterval(@Param("groupId") Long groupId,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	@Query(value = "SELECT uisg FROM UserInStandbyGroup uisg WHERE (uisg.user.id = :userId AND uisg.standbyGroup.id = :groupId AND uisg.validFrom <= :validFrom AND uisg.validTo >= :validTo)")
	public List<UserInStandbyGroup> findUserForInterval(@Param("userId") Long userId, @Param("groupId") Long groupId,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	@Query(value = "SELECT COUNT (uisg.position) FROM UserInStandbyGroup uisg WHERE (uisg.standbyGroup.id = :groupId)")
	public Integer getNextPositionForGroupMember(@Param("groupId") Long groupId);

	@Query(value = "SELECT uisg FROM UserInStandbyGroup uisg WHERE (uisg.standbyGroup.id = :groupId AND uisg.position >= :pos)")
	public List<UserInStandbyGroup> findGroupMemberWithMinPosition(@Param("groupId") Long groupId,
			@Param("pos") Integer pos);

}