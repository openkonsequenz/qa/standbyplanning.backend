/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.db.model.hist;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "ADDRESS" database table.
 *
 * @author Jürgen Knauth
 */
@Entity
@Table(name = "ADDRESS_HIST")
public class AddressHist extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------

	@Id
	@Column(name = "hist_id", updatable = false)
	private Long histId;

	@Column(name = "operation", nullable = true, updatable = false)
	private String operation;

	@Column(name = "stamp", nullable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date stamp;

	// ----------------------------------------------------------------

	@Id
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "postcode", length = 8, nullable = false)
	private String postcode;

	@Column(name = "community", length = 256, nullable = false)
	private String community;

	@Column(name = "community_suffix", length = 256, nullable = true)
	private String communitySuffix;

	@Column(name = "street", length = 256, nullable = false)
	private String street;

	@Column(name = "housenumber", length = 32, nullable = false)
	private String housenumber;

	@Column(name = "wgs_84_zone", length = 256, nullable = true)
	private String wgs84zone;

	@Column(name = "latitude", length = 32, nullable = true)
	private String latitude;

	@Column(name = "longitude", length = 32, nullable = true)
	private String longitude;

	@Column(name = "url_map", length = 256, nullable = true)
	private String urlMap;

	// ----------------------------------------------------------------

	public AddressHist() {
		/** default constructor. */
	}

	// ----------------------------------------------------------------

	public Long getHistId() {
		return histId;
	}

	public void setHistId(Long histId) {
		this.histId = histId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getStamp() {
		return stamp;
	}

	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}

	// ----------------------------------------------------------------

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * @param postcode
	 *            the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	/**
	 * @return the community
	 */
	public String getCommunity() {
		return community;
	}

	/**
	 * @param community
	 *            the community to set
	 */
	public void setCommunity(String community) {
		this.community = community;
	}

	/**
	 * @return the communitySuffix
	 */
	public String getCommunitySuffix() {
		return communitySuffix;
	}

	/**
	 * @param communitySuffix
	 *            the communitySuffix to set
	 */
	public void setCommunitySuffix(String communitySuffix) {
		this.communitySuffix = communitySuffix;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the housenumber
	 */
	public String getHousenumber() {
		return housenumber;
	}

	/**
	 * @param housenumber
	 *            the housenumber to set
	 */
	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}

	/**
	 * @return the wgs84zone
	 */
	public String getWgs84zone() {
		return wgs84zone;
	}

	/**
	 * @param wgs84zone
	 *            the wgs84zone to set
	 */
	public void setWgs84zone(String wgs84zone) {
		this.wgs84zone = wgs84zone;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the urlMap
	 */
	public String getUrlMap() {
		return urlMap;
	}

	/**
	 * @param urlMap
	 *            the urlMap to set
	 */
	public void setUrlMap(String urlMap) {
		this.urlMap = urlMap;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}