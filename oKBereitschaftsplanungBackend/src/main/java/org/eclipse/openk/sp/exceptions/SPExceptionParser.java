/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.exceptions;

public class SPExceptionParser {
	public static final String MSG_FOREIGN_KEY_CONSTRAINT = "violates foreign key constraint";
	public static final String MSG_NOT_NULL_CONSTRAINT = "violates not-null constraint";
	public static final String MSG_VALUE_TO_LONG = "Wert zu lang";
	public static final String MSG_NULL_VALUE = "NULL-Wert in Spalte";

	private SPExceptionParser() {

	}

	public static String parseSQLExceptions(String errorMsg) {
		String result = "";
		int startCut = 0;
		int endCut = 0;
		if (errorMsg.contains(MSG_FOREIGN_KEY_CONSTRAINT)) {
			startCut = errorMsg.indexOf("ERROR") + 6;
			endCut = errorMsg.indexOf("Error Code:") - 1;
			result = errorMsg.substring(startCut, endCut);
		} else if (errorMsg.contains(MSG_VALUE_TO_LONG) || errorMsg.contains(MSG_NULL_VALUE)) {
			startCut = errorMsg.indexOf("FEHLER") + 7;
			endCut = errorMsg.indexOf("Error Code:") - 1;
			result = errorMsg.substring(startCut, endCut);
		} else if (errorMsg.contains(MSG_NOT_NULL_CONSTRAINT)) {
			startCut = errorMsg.indexOf("ERROR") + 6;
			endCut = errorMsg.indexOf("Detail:") - 1;
			result = errorMsg.substring(startCut, endCut);
		} else {
			// default
			result = "Es ist ein Fehler bei einer Interaktion mit der Datenbank aufgetreten.";
		}

		return result.trim();
	}

}
