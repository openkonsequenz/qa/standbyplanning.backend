/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "STANDBY_SCHEDULE_HEADER" database table.
 */
@Entity
@Table(name = "STANDBY_SCHEDULE_HEADER")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class StandbyScheduleHeader extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_SCHEDULE_HEADER_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_SCHEDULE_HEADER_ID_SEQ", sequenceName = "STANDBY_SCHEDULE_HEADER_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 256, nullable = false)
	private String title;

	@Column(name = "valid_from", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "valid_to", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "status_id", nullable = true)
	private StandbyStatus standbyStatus;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "int_standby_ls_schedule_header", joinColumns = {
			@JoinColumn(name = "schedule_header_id") }, inverseJoinColumns = { @JoinColumn(name = "standby_group_id") })
	private List<StandbyList> lsStandbyLists = new ArrayList<>();

	public StandbyScheduleHeader() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the standbyStatus
	 */
	public StandbyStatus getStandbyStatus() {
		return standbyStatus;
	}

	/**
	 * @param standbyStatus
	 *            the standbyStatus to set
	 */
	public void setStandbyStatus(StandbyStatus standbyStatus) {
		this.standbyStatus = standbyStatus;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}
