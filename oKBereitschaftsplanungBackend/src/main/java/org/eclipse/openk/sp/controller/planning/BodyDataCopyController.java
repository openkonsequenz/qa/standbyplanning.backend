/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.planning;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.validation.ValidationController;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.StandbyStatusRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyListHasStandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleCopyDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.FileHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.eclipse.openk.sp.util.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class BodyDataCopyController extends AbstractController {

	private static final String TXT_ACTION_COPY = "kopiert von Bereitschaft {0}";

	protected static final Logger LOGGER = Logger.getLogger(BodyDataCopyController.class);

	@Autowired
	private StandbyStatusRepository standbyStatusRepository;

	@Autowired
	StandbyGroupRepository standbyGroupRepository;

	@Autowired
	StandbyListRepository standbyListRepository;

	@Autowired
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Autowired
	private ArchiveController archiveController;

	@Autowired
	private ValidationController validationController;

	@Autowired
	PlannedDataController plannedDataController;

	@Autowired
	FileHelper fileHelper;

	public PlanningMsgResponseDto copyByGroups(StandbyScheduleCopyDto standbyScheduleCopyDto, String modUserName)
			throws SpException {

		PlanningMsgResponseDto responseDto = new PlanningMsgResponseDto();
		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		responseDto.setLsMsg(lsMsg);

		try {

			Set<Long> setGoupIds = new HashSet<>();
			validateCopyDto(standbyScheduleCopyDto, lsMsg);

			Long sourceStatusId = standbyScheduleCopyDto.getStatusId();
			StandbyStatus targetStatus = calcTargetStatus(sourceStatusId, lsMsg);

			LOGGER.info("ArchiveGroups before copyByGroups - start " + new Date());

			// history
			archiveController.createArchiveForIntervallAndGroups(standbyScheduleCopyDto.getLsTransferGroup(),
					standbyScheduleCopyDto.getValidFrom(), standbyScheduleCopyDto.getValidTo(),
					SpMsg.STATUS_CLOSED_PLANNING, modUserName, SpMsg.ACT_STANDBY_MOVED_TO_IST_EBENE, "");

			LOGGER.info("ArchiveGroups before copyByGroups - end " + new Date());
			LOGGER.info("copyByGroups - start " + new Date());

			for (TransferGroupDto transferGroupDto : standbyScheduleCopyDto.getLsTransferGroup()) {
				setGoupIds.add(transferGroupDto.getGroupId());
			}

			List<StandbyScheduleBody> newSbsbList = new ArrayList<StandbyScheduleBody>();

			// list with standby groups that should get validated afterwards.
			List<StandbyGroup> lsGroupsForValidation = new ArrayList<>();
			for (Long groupId : setGoupIds) {

				LOGGER.info("delete and copy on group " + groupId + " " + new Date());

				StandbyGroup standbyGroup = standbyGroupRepository.findOne(groupId);
				lsGroupsForValidation.add(standbyGroup);

				String info = "Verarbeitung der Gruppe " + standbyGroup.getTitle() + " für den Zeitraum: ("
						+ standbyScheduleCopyDto.getValidFrom() + ") bis zum (" + standbyScheduleCopyDto.getValidTo()
						+ ") ";
				PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO_ML3);
				lsMsg.add(dto);

				// get target bodies
				List<StandbyScheduleBody> listBodyTarget = deleteTargetBodies(standbyScheduleCopyDto, groupId,
						targetStatus, lsMsg);

				// get source bodies
				List<StandbyScheduleBody> listBodySource = validateSourceBodies(standbyScheduleCopyDto, groupId,
						sourceStatusId, lsMsg);

				// check existing
				if (listBodyTarget.isEmpty()) {

					// copy all bodies
					for (StandbyScheduleBody standbyScheduleBody : listBodySource) {

						// copy and set attributes
						StandbyScheduleBody newSbsb = standbyScheduleBody.copy();
						newSbsb.setStatus(targetStatus);
						newSbsb.setModificationDate(new Date());
						newSbsb.setModifiedCause(MessageFormat.format(TXT_ACTION_COPY, standbyScheduleBody.getId()));
						newSbsb.setModifiedBy(modUserName);

						// save
						// standbyScheduleBodyRepository.save(newSbsb);
						newSbsbList.add(newSbsb);
						lsMsg.add(createMsgSavingBody(newSbsb));
					}

				}
			}
			// save
			if (!newSbsbList.isEmpty()) {
				standbyScheduleBodyRepository.save(newSbsbList);
			}

			LOGGER.info("copyByGroups - end " + new Date());

			LOGGER.info("validation after copyByGroups - start " + new Date());

			if (doValidate(Globals.VALIDATE_AFTER_TRANSFER)) {

				// // start validation
				List<String> validationBeanNames = new ArrayList<>();
				validationBeanNames.add(ValidationController.BEAN_GROUP_COVERAGE_VALIDATOR);
				validationBeanNames.add(ValidationController.BEAN_DOUBLE_PLANNED_VALIDATOR);
				validationBeanNames.add(ValidationController.BEAN_PLACEHOLDER_STANDBY_USER_VALIDATOR);

				responseDto.getLsMsg()
						.addAll(validationController.startValidation(standbyScheduleCopyDto.getValidFrom(),
								standbyScheduleCopyDto.getValidTo(), lsGroupsForValidation, validationBeanNames,
								targetStatus.getId(), null));
			} else {
				LOGGER.info("validation is off ");
			}

			LOGGER.info("validation after copyByGroups - end " + new Date());
		} catch (SpException e) {
			LOGGER.error(e, e);
			throw e;
		} catch (Exception e) {
			LOGGER.error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}

		return responseDto;

	}

	private boolean doValidate(String validateAfterTransfer) {
		boolean doValidate = false;
		try {
			Properties property = fileHelper.loadPropertiesFromResource(Globals.APP_PROP_FILE_NAME);
			String validate = property.getProperty(validateAfterTransfer);
			doValidate = Boolean.parseBoolean(validate);
		} catch (Exception e) {
			LOGGER.info("can't read propertie " + validateAfterTransfer + " => no validation!");
		}
		return doValidate;
	}

	public List<StandbyScheduleBody> validateSourceBodies(StandbyScheduleCopyDto standbyScheduleCopyDto, Long groupId,
			Long sourceStatusId, List<PlanningMsgDto> lsMsg) {

		List<StandbyScheduleBody> listBodySource = plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(
				groupId, standbyScheduleCopyDto.getValidFrom(), standbyScheduleCopyDto.getValidTo(), sourceStatusId);

		if (listBodySource.isEmpty()) {
			String info = "Es wurden keine Bereitschaften zur Übertragung gefunden!";
			PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO_ML5);
			lsMsg.add(dto);
		}

		return listBodySource;
	}

	public StandbyStatus calcTargetStatus(Long sourceStatusId, List<PlanningMsgDto> lsMsg) {

		Long targetStatusId = (sourceStatusId == SpMsg.STATUS_PLANNING) ? SpMsg.STATUS_CLOSED_PLANNING
				: SpMsg.STATUS_PLANNING;

		StandbyStatus sourceStatus = standbyStatusRepository.findOne(sourceStatusId);
		StandbyStatus targetStatus = standbyStatusRepository.findOne(targetStatusId);

		String info = "Übernahme von Daten: " + sourceStatus.getTitle() + " => " + targetStatus.getTitle();
		PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO_ML1);
		lsMsg.add(dto);

		return targetStatus;
	}

	public List<StandbyScheduleBody> deleteTargetBodies(StandbyScheduleCopyDto standbyScheduleCopyDto, Long groupId,
			StandbyStatus targetStatus, List<PlanningMsgDto> lsMsg) throws SpException {

		Boolean overwrite = standbyScheduleCopyDto.getOverwrite();

		List<StandbyScheduleBody> listBodyTarget = plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(
				groupId, standbyScheduleCopyDto.getValidFrom(), standbyScheduleCopyDto.getValidTo(),
				targetStatus.getId());

		// check overwrite

		if (listBodyTarget != null && !listBodyTarget.isEmpty()) {

			if (!overwrite) {
				String info = "Das Überschreiben der Bereitschaften ist abgeschaltet. Die in der Zielebene "
						+ targetStatus.getTitle() + " gefunden Bereitschaften können nicht gelöscht werden!";
				PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_WARN);
				lsMsg.add(dto);
			} else {
				String info = "Das Überschreiben der Bereitschaft_en ist eingeschaltet. Die in der Zielebene "
						+ targetStatus.getTitle() + " gefunden Bereitschaften werden gelöscht!";
				PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_WARN);
				lsMsg.add(dto);

				// delete
				listBodyTarget = deleteExistingBodysInIntervall(listBodyTarget, lsMsg);

			}
		}

		return listBodyTarget;
	}

	public boolean validateCopyDto(StandbyScheduleCopyDto standbyScheduleCopyDto, List<PlanningMsgDto> lsMsg)
			throws SpException {

		List<Object> lsValues = new ArrayList<>();
		lsValues.add(standbyScheduleCopyDto.getStatusId());
		lsValues.add(standbyScheduleCopyDto.getValidFrom());
		lsValues.add(standbyScheduleCopyDto.getValidTo());
		lsValues.add(standbyScheduleCopyDto.getOverwrite());
		lsValues.add(standbyScheduleCopyDto.getLsTransferGroup());

		try {
			ValidationHelper.isNoNullValueInList(lsValues);
		} catch (SpException e) {
			PlanningMsgDto dto = new PlanningMsgDto(e.getMessage(), PlanningMsgDto.CSS_DANGER,
					PlanningMsgDto.CSS_DANGER);
			lsMsg.add(dto);
			throw e;
		}

		if (standbyScheduleCopyDto.getLsTransferGroup().isEmpty()) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), ee.getMessage(), null);
			LOGGER.error(spE, spE);

			String info = "Die Eingabeparameter konnten nicht ausgewertet werden! ";
			PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.CSS_DANGER, PlanningMsgDto.CSS_DANGER);
			lsMsg.add(dto);

			throw spE;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<TransferGroupDto> getGroupsAndLists() {
		List<TransferGroupDto> lsDto = new ArrayList<>();
		List<Object[]> list = standbyGroupRepository.findGroupsWithLists();
		for (Object[] obj : list) {
			TransferGroupDto newDto = new TransferGroupDto(obj);
			lsDto.add(newDto);
		}
		return lsDto;
	}

	public List<TransferGroupDto> getGroupsInList(Long listId) {
		StandbyList sbList = standbyListRepository.findOne(listId);

		List<TransferGroupDto> lsDto = new ArrayList<>();
		for (StandbyListHasStandbyGroup standbyListHasStandbyGroup : sbList.getLsStandbyListHasStandbyGroup()) {
			TransferGroupDto newDto = new TransferGroupDto(standbyListHasStandbyGroup.getStandbyGroup().getTitle(),
					standbyListHasStandbyGroup.getStandbyGroup().getId());
			lsDto.add(newDto);
		}
		return lsDto;
	}

	/**
	 * Method to create a success message for saving an {@link StandbyScheduleBody}
	 * object.
	 * 
	 * @param stbyBody
	 * @return
	 */
	public PlanningMsgDto createMsgSavingBody(StandbyScheduleBody stbyBody) {
		String info = "[" + stbyBody.getStandbyGroup().getTitle() + "] " + stbyBody.getUser().getFirstname() + " "
				+ stbyBody.getUser().getLastname() + " wurde für den Zeitraum vom (" + stbyBody.getValidFrom()
				+ ") bis zum (" + stbyBody.getValidTo() + ") gespeichert.";
		PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO_ML5);
		return dto;
	}

	/**
	 * Method to create a failure message for saving an {@link StandbyScheduleBody}
	 * object.
	 * 
	 * @param stbyBody
	 * @return
	 */
	public PlanningMsgDto createMsgFailSavingBody(StandbyScheduleBodySelectionDto stbyBody) {
		String info = "[" + stbyBody.getStandbyGroup().getTitle() + " (" + stbyBody.getStandbyGroup().getId() + ")] "
				+ stbyBody.getUser().getFirstname() + " " + stbyBody.getUser().getLastname() + " ("
				+ stbyBody.getUser().getId() + ") wurde für den Zeitraum vom (" + stbyBody.getValidFrom()
				+ ") bis zum (" + stbyBody.getValidTo()
				+ ") nicht gespeichert. Bei der Wiederherstellung ist ein Fehler aufgetreten. "
				+ "Die Bereitschaftsgruppe oder der Mitarbeiter mit der ID konnten nicht gefunden werden!";
		PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.WARN, PlanningMsgDto.CSS_DANGER);
		return dto;
	}

	/**
	 * Method to delete a success message for saving an {@link StandbyScheduleBody}
	 * object.
	 * 
	 * @param stbyBody
	 * @return
	 */
	public PlanningMsgDto deleteMsgSavingBody(StandbyScheduleBody stbyBody) {
		String info = "[" + stbyBody.getStandbyGroup().getTitle() + "] " + stbyBody.getUser().getFirstname() + " "
				+ stbyBody.getUser().getLastname() + " wurde für den Zeitraum vom (" + stbyBody.getValidFrom()
				+ ") bis zum (" + stbyBody.getValidTo() + ") wurde gelöscht.";
		PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_WARN);
		return dto;
	}

	public List<StandbyScheduleBody> deleteExistingBodysInIntervall(List<StandbyScheduleBody> listBodyTarget,
			List<PlanningMsgDto> lsMsg) {

		// LOGGER.info("deleteExistingBodysInIntervall " + groupId + " before
		// copyByGroups - start " + new Date());
		if (listBodyTarget != null && !listBodyTarget.isEmpty()) {

			// clear existing
			for (StandbyScheduleBody standbyScheduleBody : listBodyTarget) {
				lsMsg.add(deleteMsgSavingBody(standbyScheduleBody));
			}

			standbyScheduleBodyRepository.delete(listBodyTarget);
			listBodyTarget = new ArrayList<>();
		}
		// LOGGER.info("deleteExistingBodysInIntervall " + groupId + " before
		// copyByGroups - end " + new Date());

		return listBodyTarget;
	}

	public PlanningMsgResponseDto deleteByGroups(StandbyScheduleCopyDto standbyScheduleCopyDto, String modUserName)
			throws SpException {

		PlanningMsgResponseDto responseDto = new PlanningMsgResponseDto();
		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		responseDto.setLsMsg(lsMsg);

		try {

			validateDeleteDto(standbyScheduleCopyDto, lsMsg);

			standbyScheduleCopyDto.setLsTransferGroup(getGroupsInList(standbyScheduleCopyDto.getStandbyListId()));
			standbyScheduleCopyDto.setOverwrite(true);

			StandbyStatus status = standbyStatusRepository.findOne(standbyScheduleCopyDto.getStatusId());

			// history
			if (standbyScheduleCopyDto.getStatusId().longValue() == 2) {
				archiveController.createArchiveForIntervallAndGroups(standbyScheduleCopyDto.getLsTransferGroup(),
						standbyScheduleCopyDto.getValidFrom(), standbyScheduleCopyDto.getValidTo(), status.getId(),
						modUserName, SpMsg.ACT_STANDBY_DELETE, "");
			}
			List<StandbyGroup> lsGroupsForValidation = new ArrayList<>();
			for (TransferGroupDto group : standbyScheduleCopyDto.getLsTransferGroup()) {

				StandbyGroup standbyGroup = standbyGroupRepository.findOne(group.getGroupId());
				lsGroupsForValidation.add(standbyGroup);

				String info = "Löschung der Bereitschaften in der Gruppe " + standbyGroup.getTitle()
						+ " für den Zeitraum: (" + standbyScheduleCopyDto.getValidFrom() + ") bis zum ("
						+ standbyScheduleCopyDto.getValidTo() + ") ";
				PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO_ML3);
				lsMsg.add(dto);

				// delete bodies
				deleteTargetBodies(standbyScheduleCopyDto, standbyGroup.getId(), status, lsMsg);

			}

			if (doValidate(Globals.VALIDATE_AFTER_DELETE)) {
				// start validation
				List<String> validationBeanNames = new ArrayList<>();
				validationBeanNames.add(ValidationController.BEAN_GROUP_COVERAGE_VALIDATOR);
				validationBeanNames.add(ValidationController.BEAN_DOUBLE_PLANNED_VALIDATOR);
				validationBeanNames.add(ValidationController.BEAN_PLACEHOLDER_STANDBY_USER_VALIDATOR);

				responseDto.getLsMsg()
						.addAll(validationController.startValidation(standbyScheduleCopyDto.getValidFrom(),
								standbyScheduleCopyDto.getValidTo(), lsGroupsForValidation, validationBeanNames,
								status.getId(), null));
			} else {

				LOGGER.info("validation after delete is off ");

			}
		} catch (SpException e) {
			LOGGER.error(e, e);
			throw e;
		}

		return responseDto;

	}

	public boolean validateDeleteDto(StandbyScheduleCopyDto standbyScheduleCopyDto, List<PlanningMsgDto> lsMsg)
			throws SpException {

		List<Object> lsValues = new ArrayList<>();
		lsValues.add(standbyScheduleCopyDto.getStatusId());
		lsValues.add(standbyScheduleCopyDto.getValidFrom());
		lsValues.add(standbyScheduleCopyDto.getValidTo());
		lsValues.add(standbyScheduleCopyDto.getStandbyListId());

		try {
			ValidationHelper.isNoNullValueInList(lsValues);
		} catch (SpException e) {
			PlanningMsgDto dto = new PlanningMsgDto(e.getMessage(), PlanningMsgDto.CSS_DANGER,
					PlanningMsgDto.CSS_DANGER);
			lsMsg.add(dto);
			throw e;
		}

		Long listId = standbyScheduleCopyDto.getStandbyListId();

		if (!standbyListRepository.exists(listId)) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), ee.getMessage(), null);
			LOGGER.error(spE, spE);

			String info = "Die Eingabeparameter konnten nicht ausgewertet werden! ";
			PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.CSS_DANGER, PlanningMsgDto.CSS_DANGER);
			lsMsg.add(dto);

			throw spE;
		}
		return true;
	}
}
