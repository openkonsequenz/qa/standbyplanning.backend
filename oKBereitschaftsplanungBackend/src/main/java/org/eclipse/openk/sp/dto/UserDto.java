/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "USER" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "UserDto")
@JsonInclude(Include.NON_NULL)
public class UserDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@Size(max = 256, message = "Vorname mit max. 256 Zeichen")
	private String firstname;
	@Size(max = 256, message = "Nachname mit max. 256 Zeichen")
	private String lastname;
	@Size(max = 256, message = "HR-Nummer mit max. 256 Zeichen")
	private String hrNumber;
	@Size(max = 256, message = "Pers-Nr. mit max. 256 Zeichen")
	private String userKey;
	@NotNull(message = "Privat oder Firma kennzeichnen")
	private Boolean isCompany;
	@Size(max = 2048, message = "Vorname mit max. 2048 Zeichen")
	private String notes;

	private Date validFrom;

	private Date validTo;
	private Date modificationDate;
	@Valid
	private OrganisationDto organisation;
	@Valid
	private AddressDto privateAddress;
	@Valid
	private ContactDataDto businessContactData;
	@Valid
	private ContactDataDto privateContactData;

	private List<UserInRegionDto> lsUserInRegions = new ArrayList<>();
	private List<UserHasUserFunctionDto> lsUserFunctions = new ArrayList<>();

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the isCompany
	 */
	public Boolean getIsCompany() {
		return isCompany;
	}

	/**
	 * @param isCompany the isCompany to set
	 */
	public void setIsCompany(Boolean isCompany) {
		this.isCompany = isCompany;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the organisation
	 */
	public OrganisationDto getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation the organisation to set
	 */
	public void setOrganisation(OrganisationDto organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return the privateAddress
	 */
	public AddressDto getPrivateAddress() {
		return privateAddress;
	}

	/**
	 * @param privateAddress the privateAddress to set
	 */
	public void setPrivateAddress(AddressDto privateAddress) {
		this.privateAddress = privateAddress;
	}

	/**
	 * @return the businessContactData
	 */
	public ContactDataDto getBusinessContactData() {
		return businessContactData;
	}

	/**
	 * @param businessContactData the businessContactData to set
	 */
	public void setBusinessContactData(ContactDataDto businessContactData) {
		this.businessContactData = businessContactData;
	}

	/**
	 * @return the privateContactData
	 */
	public ContactDataDto getPrivateContactData() {
		return privateContactData;
	}

	/**
	 * @param privateContactData the privateContactData to set
	 */
	public void setPrivateContactData(ContactDataDto privateContactData) {
		this.privateContactData = privateContactData;
	}

	/**
	 * @return the lsUserInRegions
	 */
	public List<UserInRegionDto> getLsUserInRegions() {
		return lsUserInRegions;
	}

	/**
	 * @param lsUserInRegions the lsUserInRegions to set
	 */
	public void setLsUserInRegions(List<UserInRegionDto> lsUserInRegions) {
		this.lsUserInRegions = lsUserInRegions;
	}

	/**
	 * @return the lsUserFunctions
	 */
	public List<UserHasUserFunctionDto> getLsUserFunctions() {
		return lsUserFunctions;
	}

	/**
	 * @param lsUserFunctions the lsUserFunctions to set
	 */
	public void setLsUserFunctions(List<UserHasUserFunctionDto> lsUserFunctions) {
		this.lsUserFunctions = lsUserFunctions;
	}

	public String getHrNumber() {
		return hrNumber;
	}

	public void setHrNumber(String hrNumber) {
		this.hrNumber = hrNumber;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
}
