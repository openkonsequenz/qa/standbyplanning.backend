/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "USER" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "UserDto")
@JsonInclude(Include.NON_NULL)
public class UserSmallSelectionDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String firstname;
	private String lastname;
	private ContactDataDto businessContactData;
	private ContactDataDto privateContactData;

	public UserSmallSelectionDto() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public ContactDataDto getBusinessContactData() {
		return businessContactData;
	}

	public void setBusinessContactData(ContactDataDto businessContactData) {
		this.businessContactData = businessContactData;
	}

	/**
	 * @return the privateContactData
	 */
	public ContactDataDto getPrivateContactData() {
		return privateContactData;
	}

	/**
	 * @param privateContactData the privateContactData to set
	 */
	public void setPrivateContactData(ContactDataDto privateContactData) {
		this.privateContactData = privateContactData;
	}

}
