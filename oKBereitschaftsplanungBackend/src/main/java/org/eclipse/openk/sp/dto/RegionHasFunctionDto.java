/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "REGION" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "RegionHasFunctionDto")
@JsonInclude(Include.NON_NULL)
public class RegionHasFunctionDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long regionHasFunctionId;

	private RegionDto region;

	private UserFunctionDto userFunction;

	private String regionName;
	private Long regionId;

	private String functionName;
	private Long functionId;

	private Date validFrom;

	private Date validTo;

	public RegionHasFunctionDto() {
		/** default constructor. */
	}



	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the region
	 */
	public RegionDto getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(RegionDto region) {
		this.region = region;
	}

	/**
	 * @return the userFunction
	 */
	public UserFunctionDto getUserFunction() {
		return userFunction;
	}

	/**
	 * @param userFunction
	 *            the userFunction to set
	 */
	public void setUserFunction(UserFunctionDto userFunction) {
		this.userFunction = userFunction;
	}

	/**
	 * @return the regionName
	 */
	public String getRegionName() {
		return regionName;
	}

	/**
	 * @param regionName
	 *            the regionName to set
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return the regionId
	 */
	public Long getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId
	 *            the regionId to set
	 */
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the functionName
	 */
	public String getFunctionName() {
		return functionName;
	}

	/**
	 * @param functionName the functionName to set
	 */
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	/**
	 * @return the functionId
	 */
	public Long getFunctionId() {
		return functionId;
	}

	/**
	 * @param functionId the functionId to set
	 */
	public void setFunctionId(Long functionId) {
		this.functionId = functionId;
	}



	/**
	 * @return the regionHasFunctionId
	 */
	public Long getRegionHasFunctionId() {
		return regionHasFunctionId;
	}



	/**
	 * @param regionHasFunctionId the regionHasFunctionId to set
	 */
	public void setRegionHasFunctionId(Long regionHasFunctionId) {
		this.regionHasFunctionId = regionHasFunctionId;
	}

}
