/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.CalendarRepository;
import org.eclipse.openk.sp.db.model.CalendarDay;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle CALENDAR operations. */
public class CalendarController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(CalendarController.class);

	@Autowired
	private CalendarRepository calendarRepository;

	@Autowired
	private EntityConverter entityConverter;

	/**
	 * 
	 * @return
	 */
	public List<CalendarDayDto> getCalendarDays() throws SpException {
		try {
			ArrayList<CalendarDayDto> list = new ArrayList<>();
			Iterable<CalendarDay> itList = calendarRepository.findAllByOrderByDateIndexDesc();
			for (CalendarDay entry : itList) {
				list.add((CalendarDayDto) entityConverter.convertEntityToDto(entry, new CalendarDayDto()));
			}
			return list;

		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.LOADING_LIST_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_CALENDAR);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	public boolean containsDate(Date date) throws SpException {
		List<CalendarDayDto> list = getCalendarDays();
		for (CalendarDayDto calendarDayDto : list) {
			Date calendarDate = calendarDayDto.getDateIndex();
			if (DateHelper.isSameDate(date, calendarDate)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	public List<CalendarDayDto> getCalendarDaysSelection() throws SpException {
		return getCalendarDays();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public CalendarDayDto getCalendarDay(Long id) throws SpException {
		try {
			CalendarDay entry = calendarRepository.findOne(id);
			return (CalendarDayDto) entityConverter.convertEntityToDto(entry, new CalendarDayDto());
		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_CALENDAR_WITH_ID, id);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a CalendarDay.
	 * 
	 * @param calendarDayDto
	 * @return
	 * @throws SpException
	 */
	public CalendarDayDto saveCalendarDay(CalendarDayDto calendarDayDto) throws SpException {
		try {
			CalendarDay entry = (CalendarDay) entityConverter.convertDtoToEntity(calendarDayDto, new CalendarDay());

			entry.setModificationDate(new Date());
			entry = calendarRepository.save(entry);

			return (CalendarDayDto) entityConverter.convertEntityToDto(entry, new CalendarDayDto());
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_CALENDAR_WITH_ID, new Gson().toJson(calendarDayDto));
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	public CalendarDayDto deleteCalendarDay(CalendarDayDto calendarDayDto) throws SpException {
		try {
			calendarRepository.delete(calendarDayDto.getId());
			return null;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_DELETE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_CALENDAR_WITH_ID, new Gson().toJson(calendarDayDto));
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}
}
