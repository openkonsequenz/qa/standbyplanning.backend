package org.eclipse.openk.sp.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * This class enables HTTP JSON (de)serialization to have deterministic date
 * conversion.
 *
 * @author knauth
 */
public class CustomDateYMDSerializer extends StdSerializer<Date> {

	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

	private static final long serialVersionUID = 1L;

	public CustomDateYMDSerializer() {
		this(null);
	}

	public CustomDateYMDSerializer(Class<Date> t) {
		super(t);
	}

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeString(FORMATTER.format(value));
	}
}
