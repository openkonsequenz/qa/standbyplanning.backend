/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.eclipse.openk.auth2.util.JwtHelper;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.eclipse.openk.sp.exceptions.SpNestedException;

/** Class to handle requests to external interfaces operations. */
public class RestService {

	private boolean printExceptionsOnlyToLog = true;

	protected static final Logger LOGGER = Logger.getLogger(RestService.class);

	public HttpResponse performGetRequest(boolean useHttps, String baseURL, String restFunctionWithParams, String token)
			throws SpNestedException {
		HttpResponse response = null;

		String completeRequest = baseURL + "/" + restFunctionWithParams;
		LOGGER.debug("CompleteUrl: " + completeRequest);

		// create HTTP Client
		SSLContextBuilder builder = new SSLContextBuilder();
		CloseableHttpClient httpClient = createHttpsClient(useHttps, builder);

		// create new Request with given URL
		HttpGet getRequest = new HttpGet(completeRequest);
		getRequest.addHeader("accept", Globals.HEADER_JSON_UTF8);

		if (token != null && !token.isEmpty()) {
			String accesstoken = JwtHelper.formatToken(token);
			getRequest.addHeader(Globals.KEYCLOAK_AUTH_TAG, "Bearer " + accesstoken);

		} else {

			LOGGER.debug("UNAUTHORIZED 401");
			ErrorReturn errorReturn = new ErrorReturn(HttpStatus.SC_UNAUTHORIZED,
					"Token is Unauthorized, maybe to old!");

			if (!printExceptionsOnlyToLog) {
				throw new SpNestedException(errorReturn);
			}
		}

		// Execute request an catch response
		try {
			response = httpClient.execute(getRequest);

		} catch (IOException e) {
			String errtext = "Communication to <" + completeRequest + "> failed!";
			LOGGER.warn(errtext, e);
			ErrorReturn errorReturn = new ErrorReturn(HttpStatus.SC_SERVICE_UNAVAILABLE,
					errtext + " No service, server maybe down!");

			if (!printExceptionsOnlyToLog) {
				throw new SpNestedException(errorReturn);
			}

		}
		return response;
	}

	public HttpResponse performPutRequest(boolean useHttps, String baseURL, String restFunctionWithParams, String token,
			String body) throws SpNestedException {
		HttpResponse response = null;

		String completeRequest = baseURL + "/" + restFunctionWithParams;
		LOGGER.debug("CompleteUrl: " + completeRequest);

		// create HTTP Client
		SSLContextBuilder builder = new SSLContextBuilder();
		CloseableHttpClient httpClient = createHttpsClient(useHttps, builder);

		// create new Request with given URL
		HttpPut putRequest = new HttpPut(completeRequest);
		putRequest.addHeader("Content-type", Globals.HEADER_JSON_UTF8);

		if (token != null && !token.isEmpty()) {
			String accesstoken = JwtHelper.formatToken(token);
			putRequest.addHeader(Globals.KEYCLOAK_AUTH_TAG, "Bearer " + accesstoken);

		} else {

			LOGGER.debug("UNAUTHORIZED 401");
			ErrorReturn errorReturn = new ErrorReturn(HttpStatus.SC_UNAUTHORIZED, "Token is unvalid, maybe to old!");

			if (!printExceptionsOnlyToLog) {
				throw new SpNestedException(errorReturn);
			}
		}

		if (StringUtils.isNotBlank(body)) {
			putRequest.setEntity(new StringEntity(body, "utf-8"));
		}

		// Execute request an catch response
		try {
			response = httpClient.execute(putRequest);

		} catch (IOException e) {
			String errtext = "Communication to <" + completeRequest + "> failed!";
			LOGGER.warn(errtext, e);
			ErrorReturn errorReturn = new ErrorReturn(HttpStatus.SC_SERVICE_UNAVAILABLE,
					errtext + " No service, server maybe down!");

			if (!printExceptionsOnlyToLog) {
				throw new SpNestedException(errorReturn);
			}

		}
		return response;
	}

	public CloseableHttpClient createHttpsClient(boolean useHttps, SSLContextBuilder builder) throws SpNestedException {

		if (useHttps) {
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());

				return HttpClients.custom().setSSLSocketFactory(sslsf).build();
			} catch (Exception e) {
				String errtext = "Error on establish an SSL connection. Fallback to http: SSLContextBuilderException";
				LOGGER.warn(errtext, e);
				ErrorReturn errorReturn = new ErrorReturn(HttpStatus.SC_SERVICE_UNAVAILABLE, errtext);

				if (!printExceptionsOnlyToLog) {
					throw new SpNestedException(errorReturn);
				}

			}
		}
		return HttpClientBuilder.create().build();
	}

	public boolean validateResponse(HttpResponse response) throws SpNestedException {

		if (response == null) {
			LOGGER.info("Error: no response found!");

			ErrorReturn errorReturn = new ErrorReturn(HttpStatus.SC_NOT_FOUND, "Get no response on this URL!");

			if (!printExceptionsOnlyToLog) {
				throw new SpNestedException(errorReturn);
			}
			return false;
		}

		StatusLine statusLine = response.getStatusLine();

		if (statusLine == null || statusLine.getStatusCode() != HttpStatus.SC_OK) {

			int code = response.getStatusLine().getStatusCode();
			String msg = response.getStatusLine().getReasonPhrase();

			if (msg == null || msg.equals(""))
				switch (code) {
				case HttpStatus.SC_UNAUTHORIZED:
					msg = "Token is Unauthorized, maybe to old!";
					break;

				default:
					msg = "Please contact your administrator for more informations!";
					break;
				}

			LOGGER.info("Error: " + code + " - " + msg);

			ErrorReturn errorReturn = new ErrorReturn(code, msg);

			if (!printExceptionsOnlyToLog) {
				throw new SpNestedException(errorReturn);
			}
			return false;
		}
		return true;
	}

	public String responseToJson(HttpResponse response, Charset utf8) throws SpNestedException {
		String retJson = null;
		try {
			retJson = EntityUtils.toString(response.getEntity(), utf8);
		} catch (Exception e) {
			LOGGER.info(e, e);
			ErrorReturn errorReturn = new ErrorReturn(HttpStatus.SC_INTERNAL_SERVER_ERROR,
					"Error on getting JSON from response");

			if (!printExceptionsOnlyToLog) {
				throw new SpNestedException(errorReturn);
			}
		}

		return retJson;
	}

	/**
	 * @return the printExceptionsOnlyToLog
	 */
	public boolean isPrintExceptionsOnlyToLog() {
		return printExceptionsOnlyToLog;
	}

	/**
	 * @param printExceptionsOnlyToLog the printExceptionsOnlyToLog to set
	 */
	public void setPrintExceptionsOnlyToLog(boolean printExceptionsOnlyToLog) {
		this.printExceptionsOnlyToLog = printExceptionsOnlyToLog;
	}

}
