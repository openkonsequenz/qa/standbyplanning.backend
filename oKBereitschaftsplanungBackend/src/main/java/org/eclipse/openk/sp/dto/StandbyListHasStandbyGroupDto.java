/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBYLISTHASSTANDBYGROUPDTO" Data Transfer Object (DTO)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "StandbyListHasStandbyGroupDto")
@JsonInclude(Include.NON_NULL)
public class StandbyListHasStandbyGroupDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "Gruppe muss gesetzt sein")
	private Long standbyGroupId;
	private String standbyGroupName;
	@NotNull(message = "Liste muss gesetzt sein")
	private Long standbyListId;
	private Integer position;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the standbyGroupId
	 */
	public Long getStandbyGroupId() {
		return standbyGroupId;
	}

	/**
	 * @param standbyGroupId the standbyGroupId to set
	 */
	public void setStandbyGroupId(Long standbyGroupId) {
		this.standbyGroupId = standbyGroupId;
	}

	/**
	 * @return the standbyGroupName
	 */
	public String getStandbyGroupName() {
		return standbyGroupName;
	}

	/**
	 * @param standbyGroupName the standbyGroupName to set
	 */
	public void setStandbyGroupName(String standbyGroupName) {
		this.standbyGroupName = standbyGroupName;
	}

	/**
	 * @return the standbyListId
	 */
	public Long getStandbyListId() {
		return standbyListId;
	}

	/**
	 * @param standbyListId the standbyListId to set
	 */
	public void setStandbyListId(Long standbyListId) {
		this.standbyListId = standbyListId;
	}

	/**
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}

}
