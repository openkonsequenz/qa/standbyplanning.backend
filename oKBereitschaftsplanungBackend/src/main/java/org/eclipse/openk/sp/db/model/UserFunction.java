/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "USER_FUNCTION" database table.
 */
@Entity
@Table(name = "USER_FUNCTION")
public class UserFunction extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_FUNCTION_ID_SEQ")
	@SequenceGenerator(name = "USER_FUNCTION_ID_SEQ", sequenceName = "USER_FUNCTION_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "function_name", length = 256, nullable = false)
	private String functionName;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_function_id", nullable = true)
	private List<UserHasUserFunction> lsUserHasUserFunction = new ArrayList<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "int_standbygroup_has_function", joinColumns = @JoinColumn(name = "user_function_id"), inverseJoinColumns = @JoinColumn(name = "group_id"))
	private List<StandbyGroup> lsStandbyGroups = new ArrayList<>();

	public UserFunction() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the functionName
	 */
	public String getFunctionName() {
		return functionName;
	}

	/**
	 * @param functionName
	 *            the functionName to set
	 */
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	/**
	 * @return the lsUserHasUserFunction
	 */
	public List<UserHasUserFunction> getLsUserHasUserFunction() {
		return lsUserHasUserFunction;
	}

	/**
	 * @param lsUserHasUserFunction
	 *            the lsUserHasUserFunction to set
	 */
	public void setLsUserHasUserFunction(List<UserHasUserFunction> lsUserHasUserFunction) {
		this.lsUserHasUserFunction = lsUserHasUserFunction;
	}

	/**
	 * @return the lsStandbyGroups
	 */
	public List<StandbyGroup> getLsStandbyGroups() {
		return lsStandbyGroups;
	}

	/**
	 * @param lsStandbyGroups
	 *            the lsStandbyGroups to set
	 */
	public void setLsStandbyGroups(List<StandbyGroup> lsStandbyGroups) {
		this.lsStandbyGroups = lsStandbyGroups;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

	@Override
	public UserFunction copy() {

		UserFunction newUserFunction = new UserFunction();
		newUserFunction.setFunctionName(this.getFunctionName());
		newUserFunction.setId(null);
		newUserFunction.setLsStandbyGroups(this.getLsStandbyGroups());
		newUserFunction.setLsUserHasUserFunction(this.lsUserHasUserFunction);

		return newUserFunction;

	}
}
