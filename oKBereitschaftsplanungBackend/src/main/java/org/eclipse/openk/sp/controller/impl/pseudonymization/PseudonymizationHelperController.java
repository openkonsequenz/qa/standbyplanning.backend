/********************************************************************************
 * Copyright (c) 2022 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.impl.pseudonymization;



import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleHistoryRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.dao.hist.AddressHistRepository;
import org.eclipse.openk.sp.db.dao.hist.ContactDataHistRepository;
import org.eclipse.openk.sp.db.dao.hist.StandbyScheduleBodyHistRepository;
import org.eclipse.openk.sp.db.dao.hist.StandbyScheduleHistoryHistRepository;
import org.eclipse.openk.sp.db.dao.hist.UserHistRepository;
import org.eclipse.openk.sp.db.model.Address;
import org.eclipse.openk.sp.db.model.ContactData;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyScheduleHistory;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.hist.AddressHist;
import org.eclipse.openk.sp.db.model.hist.ContactDataHist;
import org.eclipse.openk.sp.db.model.hist.StandbyScheduleBodyHist;
import org.eclipse.openk.sp.db.model.hist.StandbyScheduleHistoryHist;
import org.eclipse.openk.sp.db.model.hist.UserHist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



/**
 * Helper controller that assists in pseudonymization.
 *
 * @author		Jürgen Knauth
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PseudonymizationHelperController {

	// ================================================================
	// == Constants
	// ================================================================

	// ================================================================
	// == Variables
	// ================================================================

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserHistRepository userHistRepository;
	@Autowired
	private AddressHistRepository addressHistRepository;
	@Autowired
	private ContactDataHistRepository contactDataHistRepository;

	@Autowired
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Autowired
	private StandbyScheduleBodyHistRepository standbyScheduleBodyHistRepository;

	@Autowired
	private StandbyScheduleHistoryRepository standbyScheduleHistoryRepository;

	@Autowired
	private StandbyScheduleHistoryHistRepository standbyScheduleHistoryHistRepository;

	// ================================================================
	// == Constructors
	// ================================================================

	// ================================================================
	// == Helper Methods
	// ================================================================

	// TESTED.
	/**
	 * Pseudonymize data (non-recursively).
	 *
	 * @param ctx		(required) The pseudonymization context
	 * @param userToPseudonymize		(required) Data
	 * @return			Returns <code>true</code> if the data has been modified and could now be
	 *					saved.
	 */
	private boolean x_modify(PseudonymizationCtx ctx, ContactData data)
	{
		ctx.logDoModify("Private contact data: all fields");

		data.setCellphone(ctx.theNewData.getCellPhoneNumber());
		data.setEmail(ctx.theNewData.getEMail());
		data.setPager(ctx.theNewData.getPager());
		data.setPhone(ctx.theNewData.getPhoneNumber());
		data.setRadiocomm(ctx.theNewData.getRadioComm());

		return true;
	}

	// TESTED.
	/**
	 * Pseudonymize data (non-recursively).
	 *
	 * @param ctx		(required) The pseudonymization context
	 * @param userToPseudonymize		(required) Data
	 * @return			Returns <code>true</code> if the data has been modified and could now be
	 *					saved.
	 */
	private boolean x_modifyHist(PseudonymizationCtx ctx, ContactDataHist data)
	{
		ctx.logDoModify("Private contact data history: all fields");

		data.setCellphone(ctx.theNewData.getCellPhoneNumber());
		data.setEmail(ctx.theNewData.getEMail());
		data.setPager(ctx.theNewData.getPager());
		data.setPhone(ctx.theNewData.getPhoneNumber());
		data.setRadiocomm(ctx.theNewData.getRadioComm());

		return true;
	}

	// TESTED.
	/**
	 * Pseudonymize data (non-recursively).
	 *
	 * @param ctx		(required) The pseudonymization context
	 * @param userToPseudonymize		(required) Data
	 * @return			Returns <code>true</code> if the data has been modified and could now be
	 *					saved.
	 */
	private boolean x_modify(PseudonymizationCtx ctx, Address data)
	{
		ctx.logDoModify("Private user address: all fields");

		data.setPostcode(ctx.theNewData.getPostcode());
		data.setCommunity(ctx.theNewData.getCommunity());
		data.setCommunitySuffix(ctx.theNewData.getCommunitySuffix());
		data.setStreet(ctx.theNewData.getStreet());
		data.setHousenumber(ctx.theNewData.getHousenumber());
		data.setWgs84zone(ctx.theNewData.getWgs84Zone());
		data.setLatitude(ctx.theNewData.getLatitude());
		data.setLongitude(ctx.theNewData.getLongitude());
		data.setUrlMap(ctx.theNewData.getUrlMap());

		return true;
	}

	// TESTED.
	/**
	 * Pseudonymize data (non-recursively).
	 *
	 * @param ctx		(required) The pseudonymization context
	 * @param userToPseudonymize		(required) Data
	 * @return			Returns <code>true</code> if the data has been modified and could now be
	 *					saved.
	 */
	private boolean x_modifyHist(PseudonymizationCtx ctx, AddressHist data)
	{
		ctx.logDoModify("Private user address history: all fields");

		data.setPostcode(ctx.theNewData.getPostcode());
		data.setCommunity(ctx.theNewData.getCommunity());
		data.setCommunitySuffix(ctx.theNewData.getCommunitySuffix());
		data.setStreet(ctx.theNewData.getStreet());
		data.setHousenumber(ctx.theNewData.getHousenumber());
		data.setWgs84zone(ctx.theNewData.getWgs84Zone());
		data.setLatitude(ctx.theNewData.getLatitude());
		data.setLongitude(ctx.theNewData.getLongitude());
		data.setUrlMap(ctx.theNewData.getUrlMap());

		return true;
	}

	/**
	 * Pseudonymize data (non-recursively).
	 *
	 * @param ctx		(required) The pseudonymization context
	 * @param data		(required) Data
	 * @return			Returns <code>true</code> if the data has been modified and could now be
	 *					saved.
	 */
	private boolean x_modify(PseudonymizationCtx ctx, User data)
	{
		ctx.logDoModify("User: firstname, lastname, hrnumber, userkey, validto");

		data.setFirstname(ctx.theNewData.getFirstName());
		data.setLastname(ctx.theNewData.getLastName());
		data.setHrNumber(ctx.theNewData.getHrNumber());
		data.setUserKey(ctx.theNewData.getUserKey());
		data.setValidTo(ctx.theNewData.getValidTo());

		return true;
	}

	// TESTED.
	/**
	 * Pseudonymize data (non-recursively).
	 *
	 * @param ctx		(required) The pseudonymization context
	 * @param data		(required) Data
	 * @return			Returns <code>true</code> if the data has been modified and could now be
	 *					saved.
	 */
	private boolean x_modifyHist(PseudonymizationCtx ctx, UserHist data)
	{
		ctx.logDoModify("User history: firstname, lastname, hrnumber, userkey");

		data.setFirstname(ctx.theNewData.getFirstName());
		data.setLastname(ctx.theNewData.getLastName());
		data.setHrNumber(ctx.theNewData.getHrNumber());
		data.setUserKey(ctx.theNewData.getUserKey());
		// data.setValidTo(ctx.theNewData.getValidTo());

		return true;
	}

	// TESTED.
	private boolean x_modifySingle(PseudonymizationCtx ctx, StandbyScheduleBody obj)
	{
		if (StringUtils.equals(obj.getModifiedBy(), ctx.orgUserKey)) {
			ctx.logDoModify("modification_by");
			obj.setModifiedBy(ctx.theNewData.getUserKey());
			return true;
		}
		return false;
	}

	private boolean x_modifySingle(PseudonymizationCtx ctx, StandbyScheduleBodyHist obj)
	{
		if (StringUtils.equals(obj.getModifiedBy(), ctx.orgUserKey)) {
			ctx.logDoModify("modification_by");
			obj.setModifiedBy(ctx.theNewData.getUserKey());
			return true;
		}
		return false;
	}

	private boolean x_modifySingle(PseudonymizationCtx ctx, StandbyScheduleHistory obj)
	{
		if (StringUtils.equals(obj.getChangedBy(), ctx.orgUserKey)) {
			ctx.logDoModify("changed_by");
			obj.setChangedBy(ctx.theNewData.getUserKey());
			return true;
		}
		return false;
	}

	private boolean x_modifySingle(PseudonymizationCtx ctx, StandbyScheduleHistoryHist obj)
	{
		if (StringUtils.equals(obj.getChangedBy(), ctx.orgUserKey)) {
			ctx.logDoModify("changed_by");
			obj.setChangedBy(ctx.theNewData.getUserKey());
			return true;
		}
		return false;
	}

	// ================================================================
	// == Public Methods
	// ================================================================

	// TESTED.
	/**
	 * Pseudonymize User objects.
	 */
	public void modifyUser(PseudonymizationCtx ctx, User user)
	{
		boolean bModified = false;
		bModified |= x_modify(ctx, user);
		if (user.getPrivateAddress() != null) {
			bModified |= x_modify(ctx, user.getPrivateAddress());
		}
		if (user.getPrivateContactData() != null) {
			bModified |= x_modify(ctx, user.getPrivateContactData());
		}

		if (bModified && ctx.bDoModify) {
			userRepository.save(user);
		}
	}

	// TESTED.
	/**
	 * Pseudonymize User-History objects.
	 *
	 */
	public void modifyUserHistByUserId(PseudonymizationCtx ctx, Long userId)
	{
		//if (userRepository == null) {			// NOTE: Not used
		//	throw new RuntimeException("userRepository is null!");
		//}

		if (userHistRepository == null) {
			throw new RuntimeException("userHistRepository is null!");
		}
		if (addressHistRepository == null) {
			throw new RuntimeException("addressHistRepository is null!");
		}
		if (contactDataHistRepository == null) {
			throw new RuntimeException("contactDataHistRepository is null!");
		}

		// ----
		// ----
		// ----

		for (UserHist data: userHistRepository.findById(userId)) {
			if (data.getPrivateAddressId() != null) {
				for (AddressHist dataAddr: addressHistRepository.findById(data.getPrivateAddressId())) {
					if (x_modifyHist(ctx, dataAddr) && ctx.bDoModify) {
						addressHistRepository.save(dataAddr);
					}
				}
			}
			if (data.getPrivateContactDataId() != null) {
				for (ContactDataHist dataContact: contactDataHistRepository.findById(data.getPrivateContactDataId())) {
					if (x_modifyHist(ctx, dataContact) && ctx.bDoModify) {
						contactDataHistRepository.save(dataContact);
					}
				}
			}
			if (x_modifyHist(ctx, data) && ctx.bDoModify) {
				userHistRepository.save(data);
			}
		}
	}

	// TESTED.
	public void modifyAll_StandbyScheduleBody(PseudonymizationCtx ctx)
	{
		for (StandbyScheduleBody x: standbyScheduleBodyRepository.findAll()) {
			if (x_modifySingle(ctx, x) && ctx.bDoModify) {
				standbyScheduleBodyRepository.save(x);
			}
		}
	}

	public void modifyAll_StandbyScheduleBodyHist(PseudonymizationCtx ctx)
	{
		for (StandbyScheduleBodyHist x: standbyScheduleBodyHistRepository.findAll()) {
			if (x_modifySingle(ctx, x) && ctx.bDoModify) {
				standbyScheduleBodyHistRepository.save(x);
			}
		}
	}

	public void modifyAll_StandbyScheduleHistory(PseudonymizationCtx ctx)
	{
		for (StandbyScheduleHistory x: standbyScheduleHistoryRepository.findAll()) {
			if (x_modifySingle(ctx, x) && ctx.bDoModify) {
				standbyScheduleHistoryRepository.save(x);
			}
		}
	}

	public void modifyAll_StandbyScheduleHistoryHist(PseudonymizationCtx ctx)
	{
		for (StandbyScheduleHistoryHist x: standbyScheduleHistoryHistRepository.findAll()) {
			if (x_modifySingle(ctx, x) && ctx.bDoModify) {
				standbyScheduleHistoryHistRepository.save(x);
			}
		}
	}

}
