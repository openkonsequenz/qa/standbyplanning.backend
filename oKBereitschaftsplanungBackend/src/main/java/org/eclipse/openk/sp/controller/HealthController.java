/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.dao.PostcodeRepository;
import org.eclipse.openk.sp.exceptions.SpExceptionMapper;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle health operations. */
public class HealthController {

	@Autowired
	PostcodeRepository postcodeRepository;
	
	@Autowired
	FileHelper fileHelper; 

	private static final Logger LOGGER = Logger.getLogger(HealthController.class.getName());

	public String getVersion() {

		try {
			return fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_VERSION);
		} catch (IOException e) {
			LOGGER.error(e, e);
			return e.getMessage();
		}
	}

	public Response responseFromException(Exception e) {
		int errcode;
		String retJson;

		if (e instanceof HttpStatusException) {
			LOGGER.error("Caught BackendException", e);
			errcode = ((HttpStatusException) e).getHttpStatus();
			retJson = SpExceptionMapper.toJson((HttpStatusException) e);
			return Response.status(errcode).entity(retJson).build();
		} else {
			LOGGER.error("Unexpected exception", e);
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(SpExceptionMapper.getGeneralErrorJson())
					.build();
		}
	}

	public boolean checkDB() throws HttpStatusException {

		try {
			postcodeRepository.count();
		} catch (Exception e) {
			throw new HttpStatusException(HttpStatus.SC_SERVICE_UNAVAILABLE);
		}

		return true;
	}

}
