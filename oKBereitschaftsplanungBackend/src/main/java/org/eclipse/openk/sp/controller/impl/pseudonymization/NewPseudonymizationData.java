/********************************************************************************
 * Copyright (c) 2022-2023 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.impl.pseudonymization;

import java.util.Date;

/**
 * This class holds the data to fill in later during pseudonymization.
 *
 * @author Jürgen Knauth
 */
public class NewPseudonymizationData
{

	private static final long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;

	private String firstName;
	private String lastName;
	private String hrNumber;
	private String userKey;
	private Date validTo;
	private String community;
	private String communitySuffix;
	private String postcode;
	private String street;
	private String houseNumber;
	private String urlMap;
	private String latitude;
	private String longitude;
	private String wgs84Zone;

	private String phoneNumber;
	private String cellPhoneNumber;
	private String radioComm;
	private String email;
	private String pager;

	public NewPseudonymizationData(long number)
	{
		firstName = "anonymous" + number;
		lastName = "anonymous" + number;
		hrNumber = null;
		userKey = "anonymous" + number;
		validTo = new Date((new Date()).getTime() - MILLIS_IN_A_DAY);	// yesterday

		community = "Musterstadt";
		communitySuffix = null;
		postcode = "00000";
		street = "Foobarweg";
		houseNumber = "0";
		urlMap = null;
		latitude = "0.0000";
		longitude = "0.0000";
		wgs84Zone = "EPSG-xy";

		phoneNumber = "0123 45678";
		cellPhoneNumber = "0123 45678";
		radioComm = "0123 45678";
		email = "anonymous" + number + "@example.org";
		pager = "0123 45678";
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public String getHrNumber()
	{
		return hrNumber;
	}

	public String getUserKey()
	{
		return userKey;
	}

	public Date getValidTo()
	{
		return validTo;
	}

	public String getCommunity()
	{
		return community;
	}

	public String getCommunitySuffix()
	{
		return communitySuffix;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public String getStreet()
	{
		return street;
	}

	public String getHousenumber()
	{
		return houseNumber;
	}

	public String getLatitude()
	{
		return latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public String getWgs84Zone()
	{
		return wgs84Zone;
	}

	public String getUrlMap()
	{
		return urlMap;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public String getCellPhoneNumber()
	{
		return cellPhoneNumber;
	}

	public String getRadioComm()
	{
		return radioComm;
	}

	public String getEMail()
	{
		return email;
	}

	public String getPager()
	{
		return pager;
	}

}
