/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "STANDBY_PLANNING_STATUS" database table.
 */
@Entity
@Table(name = "STANDBY_PLANNING_STATUS")
public class StandbyPlanningStatus extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_PLANNING_STATUS_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_PLANNING_STATUS_ID_SEQ", sequenceName = "STANDBY_PLANNING_STATUS_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 256, nullable = false)
	private String title;

	public StandbyPlanningStatus() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}
}
