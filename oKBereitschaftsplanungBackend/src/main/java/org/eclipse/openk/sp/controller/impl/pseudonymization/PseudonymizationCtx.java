/********************************************************************************
 * Copyright (c) 2022 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.impl.pseudonymization;



import java.util.ArrayList;

import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.PseudonymizationResultDto;



/**
 * An instance of this class is used throughout the process of pseudonymization.
 * It holds essential data for the process and receives information about individual pseudonymization activities.
 *
 * @author Jürgen Knauth
 */
public class PseudonymizationCtx {

	// ================================================================
	// == Constants
	// ================================================================

	// ================================================================
	// == Variables
	// ================================================================

	/**
	 * If set to <code>true</code> actually perform pseudonymization.
	 * If <code>false</code> just tell what would be done.
	 */
	public final boolean bDoModify;

	/**
	 * The user ID to perform pseudonymization for
	 */
	public final long userId;

	public final String orgUserFirstName;
	public final String orgUserLastName;
	public final String orgUserKey;

	/**
	 * The user object to perform pseudonymization for
	 */
	// public final User userToPseudonymize;

	/**
	 * The pseudonymization data to use during pseudonymization
	 */
	public final NewPseudonymizationData theNewData;

	public final PseudonymizationResultDto result;

	// ================================================================
	// == Constructors
	// ================================================================

	public PseudonymizationCtx(boolean bDoModify, User user, NewPseudonymizationData theNewData)
	{
		this.bDoModify = bDoModify;
		this.userId = user.getId();
		//this.userToPseudonymize = (User)user.copy();
		this.orgUserFirstName = user.getFirstname();
		this.orgUserLastName = user.getLastname();
		this.orgUserKey = user.getUserKey();
		this.theNewData = theNewData;
		this.result = new PseudonymizationResultDto(userId, true, new ArrayList<String>());
	}

	// ================================================================
	// == Helper Methods
	// ================================================================

	// ================================================================
	// == Public Methods
	// ================================================================

	public void logDoModify(String msg)
	{
		if (msg == null) {
			msg = "";
		}
		if (bDoModify) {
			result.getLogMsgs().add("Modifying: " + msg);
		} else {
			result.getLogMsgs().add("Would modify: " + msg);
		}
	}

	public void logDoDelete(String msg)
	{
		if (msg == null) {
			msg = "";
		}
		if (bDoModify) {
			result.getLogMsgs().add("Deleting: " + msg);
		} else {
			result.getLogMsgs().add("Would delete: " + msg);
		}
	}

	public void logInfo(String msg)
	{
		if (msg == null) {
			msg = "";
		}
		result.getLogMsgs().add("Info: " + msg);
	}

	public void logDebug(String msg)
	{
		if (msg == null) {
			msg = "";
		}
		result.getLogMsgs().add("Debug-Info: " + msg);
	}

	public void logError(String errMsg)
	{
		if (errMsg == null) {
			errMsg = "";
		}
		result.getLogMsgs().add("Error: " + errMsg);
		result.setSuccess(false);
	}

}
