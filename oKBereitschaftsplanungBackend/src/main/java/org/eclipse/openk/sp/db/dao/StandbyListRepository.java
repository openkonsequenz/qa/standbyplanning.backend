/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.List;

import org.eclipse.openk.sp.db.model.StandbyList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StandbyListRepository extends JpaRepository<StandbyList, Long> {

	List<StandbyList> findAllByOrderByTitleAsc();

	@Query(value = "SELECT l.title, l.id FROM standby_list l ORDER BY l.title ASC", nativeQuery = true)
	List<Object[]> findAllLists();

}