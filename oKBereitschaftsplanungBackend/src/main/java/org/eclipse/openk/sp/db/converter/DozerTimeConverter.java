/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import java.util.Date;

import org.dozer.CustomConverter;
import org.eclipse.openk.sp.dto.TimeDto;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DozerTimeConverter implements CustomConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
		if (source instanceof Date) {
			Date date = (Date) source;
			DateTime dateTime = new DateTime(date);
			TimeDto dto = new TimeDto();
			dto.setHour(dateTime.getHourOfDay());
			dto.setMinute(dateTime.getMinuteOfHour());
			dto.setSecond(dateTime.getSecondOfMinute());
			return dto;
		} else if (source instanceof TimeDto) {
			TimeDto dto = (TimeDto) source;
			String timeString = "" + dto.getHour() + ":" + dto.getMinute() + ":" + dto.getSecond();
			DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");
			return formatter.parseDateTime(timeString).toDate();
		}
		return null;
	}
}
