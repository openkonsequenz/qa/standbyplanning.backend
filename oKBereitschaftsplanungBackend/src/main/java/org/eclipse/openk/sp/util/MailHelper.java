/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MailHelper {

	@Autowired
	private FileHelper fileHelper;

	@Value("${report.mail.propertiesPath}")
	private String propertiesPath;

	private Session session;

	private String fromAddress;

	@PostConstruct
	private void init() throws IOException {
		// load mail properties file
		File propertiesFile = fileHelper.loadFileFromFileSystemOrResource(propertiesPath);
		Properties properties = new Properties();
		properties.load(new FileInputStream(propertiesFile));
		fromAddress = properties.getProperty("mail.smtp.from");
		session = Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(properties.getProperty("mail.imap.user"),
						properties.getProperty("mail.imap.password"));
			}
		});
	}

	public MimeMessage newMultipartMail() throws MessagingException {
		Multipart mp = new MimeMultipart();
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(fromAddress);
		msg.setContent(mp);
		return msg;
	}

	public static void setToRecipients(MimeMessage mail, List<String> to) throws MessagingException {
		InternetAddress[] addresses = InternetAddress.parse(String.join(", ", to));
		mail.addRecipients(RecipientType.TO, addresses);
	}

	public static void setSubject(MimeMessage mail, String subject) throws MessagingException {
		mail.setSubject(subject);
	}
	
	public static void addText(MimeMessage mail, String text) throws IOException, MessagingException {
		if (!(mail.getContent() instanceof MimeMultipart)) {
			throw new MessagingException("Not a Multipart Message");
		}
		MimeMultipart multiPart = (MimeMultipart) mail.getContent();
		MimeBodyPart textPart = new MimeBodyPart();
		textPart.setText(text);
		multiPart.addBodyPart(textPart);
	}

	public static void addAttachment(MimeMessage mail, File reportFile, String fileName) throws IOException, MessagingException {
		if (!(mail.getContent() instanceof MimeMultipart)) {
			throw new MessagingException("Not a Multipart Message");
		}
		MimeMultipart multiPart = (MimeMultipart) mail.getContent();
		MimeBodyPart attachmentPart = new MimeBodyPart();
		attachmentPart.setFileName(MimeUtility.encodeText(fileName));
		attachmentPart.setDisposition(MimeBodyPart.ATTACHMENT);
		DataSource ds = new FileDataSource(reportFile);
		attachmentPart.setDataHandler(new DataHandler(ds));
		multiPart.addBodyPart(attachmentPart);
	}

	public void send(MimeMessage msg) throws MessagingException {
		System.out.println("send");
		Transport.send(msg);
	}


}
