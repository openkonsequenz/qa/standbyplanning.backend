/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.springframework.stereotype.Service;

@Service
public class FileHelper {

	public static final Logger LOGGER = Logger.getLogger(FileHelper.class.getName());

	public Properties loadPropertiesFromResource(String filename) throws IOException {
		Properties props = new Properties();

		try {
			props.load(this.loadFileFromResource(filename));
		} catch (IOException | NullPointerException e) {
			LOGGER.warn("Exception while reading the properties file", e);
			throw e;
		}

		return props;
	}

	public InputStream loadFileFromResource(String filename) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		try {
			return classLoader.getResourceAsStream(filename);
		} catch (Exception e) {
			LOGGER.warn("Exception while reading the file: " + filename, e);
			return null;
		}
	}

	public File loadFolderFromFileSystemOrResource(String folder, boolean exists) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			LOGGER.info(classLoader.getResource(folder).getPath());
			return new File(classLoader.getResource(folder).getPath());
		} catch (Exception e) {
			LOGGER.info("File not found in resource. Searching in file system..");
			File file = new File(folder);

			try {
				if (!file.exists() && exists) {
					LOGGER.error("The System couldnt find the folder or has no access rights to it: [" + folder + "]");
					throw new FileNotFoundException();
				}
			} catch (FileNotFoundException e1) {
				LOGGER.error("The System couldnt find the folder or has no access rights to it: [" + folder + "]");
				LOGGER.warn("Exception while reading the file", e1);
				return null;
			}

			return file;
		}
	}
	
	public File loadFileFromFileSystemOrResource(String folder) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		try {
			// The name of a resource is a '/'-separated path name that identifies the
			// resource.
			return new File(classLoader.getResource(folder).getPath());
		} catch (Exception e) {
			LOGGER.info("Folder: " + folder + " not found in resource. Searching in file system.");
		}
		
		try {
			// The name of a resource is a '/'-separated path name that identifies the
			// resource.
			return new File(folder);
		} catch (Exception e) {
			LOGGER.info("Folder: " + folder + " not found in file system.");
		}
		
		
		return null;
	}

	public File loadFileFromFileSystemOrResource(String folder, String filename, boolean exists) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		File file = null;

		try {

			File dirFile = loadFileFromFileSystemOrResource(folder);

			if (dirFile != null && dirFile.exists()) {
				file = new File(dirFile, filename);

				if (exists && !file.exists()) {
					LOGGER.error("The System couldnt find the file or has no access rights to it: [" + folder + "/"
							+ filename + "]");

					throw new FileNotFoundException();

				}

			}

		} catch (FileNotFoundException e) {
			LOGGER.info("File: " + filename + " not found in resource.");
		}

		return file;
	}

	public String getProperty(String filename, String property) throws IOException {
		String propValue = null;
		Properties props = loadPropertiesFromResource(filename);
		propValue = props.getProperty(property);

		if (propValue == null) {
			LOGGER.error("Exception reading the property");
			throw new RuntimeException("Exception on property '" + property + "'"); // NOSONAR
		}

		return propValue;
	}

	/**
	 * Method to read the InputStream as UTF-8 String.
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String getTextFromInputStream(InputStream is, Charset charset) throws IOException {
		StringBuilder textBuilder = new StringBuilder();
		try (Reader reader = new BufferedReader(new InputStreamReader(is, Charset.forName(charset.name())))) {
			int c = 0;
			while ((c = reader.read()) != -1) {
				textBuilder.append((char) c);
			}
			return textBuilder.toString();
		}
	}

	/**
	 * Method to read the file extension from given file.
	 * 
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public static String getFileExtension(File f) throws SpException {
		try {
			String[] arrString = f.getName().split("\\.");
			return arrString[arrString.length - 1];
		} catch (Exception e) {
			LOGGER.error(e, e);
			throw new SpException(SpExceptionEnum.DEFAULT_EXCEPTION.getEntry());
		}
	}
}
