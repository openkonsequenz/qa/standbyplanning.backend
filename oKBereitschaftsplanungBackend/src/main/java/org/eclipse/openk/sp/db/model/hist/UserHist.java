/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.db.model.hist;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "STANDBY_USER_HIST" database table.
 *
 * @author Jürgen Knauth
 */
@Entity
@Table(name = "STANDBY_USER_HIST")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class UserHist extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------

	@Id
	@Column(name = "hist_id", updatable = false)
	private Long histId;

	@Column(name = "operation", nullable = true, updatable = false)
	private String operation;

	@Column(name = "stamp", nullable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date stamp;

	// ----------------------------------------------------------------

	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "hr_number", length = 256, nullable = true)
	private String hrNumber;

	@Column(name = "user_key", length = 256, nullable = true)
	private String userKey;

	@Column(name = "firstname", length = 256, nullable = false)
	private String firstname;

	@Column(name = "lastname", length = 256, nullable = false)
	private String lastname;

	@Column(name = "is_company", nullable = false)
	@Convert("booleanConverter")
	private Boolean isCompany;

	@Column(name = "notes", length = 2048, nullable = true)
	private String notes;

	@Column(name = "valid_from", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "valid_to", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@Column(name = "private_address_id", nullable = true)
	private Long privateAddressId;

	@Column(name = "business_contact_id", nullable = true)
	private Long businessContactDataId;

	@Column(name = "private_contact_id", nullable = true)
	private Long privateContactDataId;

	@Column(name = "organisation_id", nullable = true)
	private Long organisationId;

	// ----------------------------------------------------------------

	public UserHist() {
		/** default constructor. */
	}

	// ----------------------------------------------------------------

	public Long getHistId() {
		return histId;
	}

	public void setHistId(Long histId) {
		this.histId = histId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getStamp() {
		return stamp;
	}

	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}

	// ----------------------------------------------------------------

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname
	 *            the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname
	 *            the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the isCompany
	 */
	public Boolean getIsCompany() {
		return isCompany;
	}

	/**
	 * @param isCompany
	 *            the isCompany to set
	 */
	public void setIsCompany(Boolean isCompany) {
		this.isCompany = isCompany;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the privateAddress
	 */
	public Long getPrivateAddressId() {
		return privateAddressId;
	}

	/**
	 * @param privateAddress
	 *            the privateAddress to set
	 */
	public void setPrivateAddressId(Long privateAddressId) {
		this.privateAddressId = privateAddressId;
	}

	/**
	 * @return the businessContactData
	 */
	public Long getBusinessContactDataId() {
		return businessContactDataId;
	}

	/**
	 * @param businessContactData
	 *            the businessContactData to set
	 */
	public void setBusinessContactDataId(Long businessContactDataId) {
		this.businessContactDataId = businessContactDataId;
	}

	/**
	 * @return the privateContactData
	 */
	public Long getPrivateContactDataId() {
		return privateContactDataId;
	}

	/**
	 * @param privateContactData
	 *            the privateContactData to set
	 */
	public void setPrivateContactDataId(Long privateContactDataId) {
		this.privateContactDataId = privateContactDataId;
	}

	/**
	 * @return the organisation
	 */
	public Long getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisation
	 *            the organisation to set
	 */
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * @return the hrNumber
	 */
	public String getHrNumber() {
		return hrNumber;
	}

	/**
	 * @param hrNumber
	 *            the hrNumber to set
	 */
	public void setHrNumber(String hrNumber) {
		this.hrNumber = hrNumber;
	}

	/**
	 * @return the userKey
	 */
	public String getUserKey() {
		return userKey;
	}

	/**
	 * @param userKey
	 *            the userKey to set
	 */
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}
}
