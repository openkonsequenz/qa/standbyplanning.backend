/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.planning.BodyDataCopyController;
import org.eclipse.openk.sp.controller.planning.PlannedDataController;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.dto.StandbyScheduleBlueprintDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleCopyDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle StandbyPlan operations. */
public class StandbyScheduleController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(StandbyScheduleController.class);

	@Autowired
	private PlannedDataController plannedDataController;

	@Autowired
	private PlanningController planningController;

	@Autowired
	private BodyDataCopyController bodyDataCopyController;

	public PlanningMsgResponseDto copyBodyData(StandbyScheduleCopyDto standbyScheduleCopyDto, String modUserName)
			throws SpException {
		return bodyDataCopyController.copyByGroups(standbyScheduleCopyDto, modUserName);
	}

	public StandbyScheduleDto getFilteredPlanPlanning(StandbyScheduleFilterDto standbyScheduleFilterDto)
			throws SpException {
		return plannedDataController.getFilteredPlanByStatus(standbyScheduleFilterDto, SpMsg.STATUS_PLANNING, true);
	}

	public StandbyScheduleDto getFilteredPlanClosedPlanning(StandbyScheduleFilterDto standbyScheduleFilterDto)
			throws SpException {
		return plannedDataController.getFilteredPlanByStatus(standbyScheduleFilterDto, SpMsg.STATUS_CLOSED_PLANNING,
				true);
	}

	public StandbyScheduleDto getFilteredPlanByStatusId(StandbyScheduleFilterDto standbyScheduleFilterDto,
			Long statusId) throws SpException {
		LOGGER.debug(new Date());
		StandbyScheduleDto plan = plannedDataController.getFilteredPlanByStatus(standbyScheduleFilterDto, statusId,
				true);
		LOGGER.debug(new Date() + " for count day's " + plan.getListPlanRows().size());

		return plan;
	}

	public PlanningMsgResponseDto calculatePlan(StandbyScheduleBlueprintDto standbyBlueprintDto, String username)
			throws SpException {
		PlanningMsgResponseDto planningMsgResponseDto = new PlanningMsgResponseDto();
		PlanningMsgResponseDto tempResponse = planningController.startPlanning(standbyBlueprintDto, username);
		planningMsgResponseDto.getLsMsg().addAll(tempResponse.getLsMsg());
		return planningMsgResponseDto;
	}

	public PlanningMsgResponseDto deleteBodyData(StandbyScheduleCopyDto standbyScheduleCopyDto, String modUserName)
			throws SpException {
		planningController.startWithNewList();
		return bodyDataCopyController.deleteByGroups(standbyScheduleCopyDto, modUserName);
	}
}
