/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.PostcodeRepository;
import org.eclipse.openk.sp.db.model.Postcode;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle POSTCODE operations. */
public class PostcodeController {
	protected static final Logger LOGGER = Logger.getLogger(PostcodeController.class);

	@Autowired
	private PostcodeRepository postcodeRepository;

	@Autowired
	private EntityConverter entityConverter;

	/**
	 * Method to query all postcodes.
	 * 
	 * @return
	 */
	public List<PostcodeDto> getPostcodes() {
		ArrayList<PostcodeDto> listPostcode = new ArrayList<>();
		Iterable<Postcode> itPostcodeList = postcodeRepository.findAllByOrderByPcodeAsc();
		for (Postcode postcode : itPostcodeList) {
			listPostcode.add(
					(PostcodeDto) entityConverter.convertEntityToDto(postcode, new PostcodeDto(postcode.getPcode())));
		}
		return listPostcode;
	}

	/**
	 * Method to select a postcode by its id.
	 * 
	 * @param postcodeid
	 * @return
	 */
	public PostcodeDto getPostcode(Long postcodeid) {

		if (postcodeRepository.exists(postcodeid)) {
			Postcode postcode = postcodeRepository.findOne(postcodeid);
			return (PostcodeDto) entityConverter.convertEntityToDto(postcode, new PostcodeDto(postcode.getPcode()));
		} else {
			return null;
		}

	}

	/**
	 * Method to persist a postcode.
	 * 
	 * @param postcodeDto
	 * @return PostcodeDto
	 */
	public PostcodeDto savePostcode(PostcodeDto postcodeDto) {

		Postcode postcode = (Postcode) entityConverter.convertDtoToEntity(postcodeDto, new Postcode());

		postcodeRepository.save(setLocations(postcode));

		return (PostcodeDto) entityConverter.convertEntityToDto(postcode, new PostcodeDto(postcode.getPcode()));

	}

	private Postcode setLocations(Postcode postcode) {

		Postcode savedLocation = getSavedPostcode(postcode);

		if (savedLocation != null) {
			// locations
			postcode.setLsLocation(savedLocation.getLsLocation());

		}

		return postcode;
	}

	private Postcode getSavedPostcode(Postcode postcode) {
		Postcode savedPostcode = null;
		if (postcode.getId() != null) {
			savedPostcode = postcodeRepository.findOne(postcode.getId());
		}
		return savedPostcode;
	}

	protected Postcode getSavedPostcode(Long id) {
		return getSavedPostcode(new Postcode(id));
	}

}
