/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.UserController;
import org.eclipse.openk.sp.dto.LabelValueDto;
import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.dto.UserHasUserFunctionDto;
import org.eclipse.openk.sp.dto.UserInRegionDto;
import org.eclipse.openk.sp.dto.UserSelectionDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import io.swagger.annotations.ApiParam;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Transactional
public class UserRestService extends BaseResource {

	@Autowired
	private UserController userController;

	public UserRestService() {
		super(Logger.getLogger(UserRestService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/list")
	public Response getUsers(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<UserDto>> invokable = modusr -> userController.getUsers();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/selectionlist")
	public Response getUsersSelection(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<UserSelectionDto>> invokable = modusr -> userController.getUsersSelection();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/dropdownlist")
	public Response getUsersDropDownList(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<LabelValueDto>> invokable = modusr -> userController.getUsersDropDown();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/filter/standbygroup/{sbgId}")
	public Response getFilteredUsersSelection(@PathParam("sbgId") Long sbgId,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<UserSelectionDto>> invokable = modusr -> userController
				.getFilteredUsersSelection(sbgId);

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/{id}")
	public Response getUser(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<UserDto> invokable = modusr -> userController.getUser(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);

	}

	@PUT
	@Path("/{id}/region/save/list")
	public Response saveRegionsForUser(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<UserInRegionDto> lsUserInRegionsDto) {

		ModifyingInvokable<List<UserInRegionDto>> invokable = modusr -> userController.saveRegionsForUser(id,
				lsUserInRegionsDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/region/delete/list")
	public Response deleteRegionsForUser(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<UserInRegionDto> lsUserInRegionsDto) {

		ModifyingInvokable<List<UserInRegionDto>> invokable = modusr -> userController.deleteRegionsForUser(id,
				lsUserInRegionsDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/userfunction/save/list")
	public Response saveFunctionsForUser(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<UserHasUserFunctionDto> lsFunctionDto) {

		ModifyingInvokable<List<UserHasUserFunctionDto>> invokable = modusr -> userController.saveFunctionsForUser(id,
				lsFunctionDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/userfunction/delete/list")
	public Response deleteFunctionsForUser(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<UserHasUserFunctionDto> lsFunctionDto) {

		ModifyingInvokable<List<UserHasUserFunctionDto>> invokable = modusr -> userController.deleteFunctionsForUser(id,
				lsFunctionDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/save")
	public Response saveUser(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid UserDto userDto) {

		ModifyingInvokable<UserDto> invokable = modusr -> userController.saveUser(userDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/anonymize")
	public Response anonymizeUser(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			UserDto userDto) {

		ModifyingInvokable<UserDto> invokable = modusr -> userController.anonymizeUser(userDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
