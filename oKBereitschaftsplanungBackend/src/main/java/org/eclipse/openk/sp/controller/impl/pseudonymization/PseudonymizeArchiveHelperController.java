/********************************************************************************
 * Copyright (c) 2022-2023 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.impl.pseudonymization;



import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.db.dao.ArchiveRepository;
import org.eclipse.openk.sp.db.model.ArchiveStandbyScheduleHeader;
import org.eclipse.openk.sp.dto.ContactDataDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleArchiveDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.UserSmallSelectionDto;
import org.eclipse.openk.sp.dto.planning.PlanHeaderDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsArchiveDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;



/**
 * Helper controller that assists in pseudonymization.
 *
 * @author		Jürgen Knauth
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PseudonymizeArchiveHelperController
{

	public static class ModifiedFlag
	{
		public boolean bModified;
	}

	public static class DebuggingInfo_ArchiveStandbyScheduleHeader
	{
		public boolean bModified;
		public StandbyScheduleArchiveDto storedPlan;

		public DebuggingInfo_ArchiveStandbyScheduleHeader(boolean bModified, StandbyScheduleArchiveDto storedPlan)
		{
			this.bModified = bModified;
			this.storedPlan = storedPlan;
		}
	}

	private static class StringResult
	{
		public final String text;
		public final boolean bModified;

		public StringResult(String text, boolean bModified)
		{
			this.text = text;
			this.bModified = bModified;
		}
	}

	// ================================================================================================================================
	// == Constants
	// ================================================================================================================================

	protected static final Logger LOGGER = Logger.getLogger(PseudonymizeArchiveHelperController.class);

	// ================================================================================================================================
	// == Variables
	// ================================================================================================================================

	@Autowired
	private ArchiveRepository archiveRepository;

	// ================================================================================================================================
	// == Constructors
	// ================================================================================================================================

	// ================================================================================================================================
	// == Helper Methods
	// ================================================================================================================================

	private void x_tryModifyRecursively(PseudonymizationCtx ctx, StandbyScheduleFilterDto filter, ModifiedFlag bModified)
	{
		filter.setTitle(x_replaceFirstnameLastnameInTextN(ctx, filter.getTitle(), "filter.title", bModified));

		if (filter.getLsTransferGroup() != null) {
			for (TransferGroupDto grp: filter.getLsTransferGroup()) {
				if (grp != null) {
					x_tryModifyRecursively(ctx, grp, bModified);
				}
			}
		}
	}

	private void x_tryModifyRecursively(PseudonymizationCtx ctx, TransferGroupDto x, ModifiedFlag bModified)
	{
		// nothing to do
	}

	private void x_tryModifyRecursively(PseudonymizationCtx ctx, PlanHeaderDto planHeader, ModifiedFlag bModified)
	{
		if (planHeader.getListGroups() != null) {
			for (StandbyGroupSelectionDto x: planHeader.getListGroups()) {
				x_modifyRecursively(ctx, x, bModified);
			}
		}
	}

	private void x_tryModifyRecursively(PseudonymizationCtx ctx, PlanRowsArchiveDto row, ModifiedFlag bModified)
	{
		if (row.getListGroupBodys() != null) {
			for (List<StandbyScheduleBodySelectionDto> x1: row.getListGroupBodys()) {
				if (x1 != null) {
					for (StandbyScheduleBodySelectionDto x2: x1) {
						if (x2 != null) {
							x_tryModifyRecursively(ctx, x2, bModified);
						}
					}
				}
			}
		}
	}

	private void x_modifyRecursively(PseudonymizationCtx ctx, StandbyGroupSelectionDto grpSel, ModifiedFlag bModified)
	{
		// nothing to do
	}

	private void x_tryModifyRecursively(PseudonymizationCtx ctx, StandbyScheduleBodySelectionDto schedBody, ModifiedFlag bModified)
	{
		if (schedBody.getUser() != null) {
			x_tryModifyRecursively(ctx, schedBody.getUser(), bModified);
		}
	}

	private void x_tryModifyRecursively(PseudonymizationCtx ctx, UserSmallSelectionDto smallSel, ModifiedFlag bModified)
	{
		if (smallSel.getId() == ctx.userId) {
			ctx.logDoModify("UserSmallSelectionDto: first name, last name");
			smallSel.setFirstname(ctx.theNewData.getFirstName());
			smallSel.setLastname(ctx.theNewData.getLastName());
			if (smallSel.getPrivateContactData() != null) {
				x_doModify(ctx, smallSel.getPrivateContactData());
			}
			bModified.bModified = true;
		}
	}

	private void x_doModify(PseudonymizationCtx ctx, ContactDataDto contactData)
	{
		ctx.logDoModify("ContactDataDto: phone, cellphone, radiocomm, email, pager");
		contactData.setPhone(ctx.theNewData.getPhoneNumber());
		contactData.setCellphone(ctx.theNewData.getCellPhoneNumber());
		contactData.setRadiocomm(ctx.theNewData.getRadioComm());
		contactData.setEmail(ctx.theNewData.getEMail());
		contactData.setPager(ctx.theNewData.getRadioComm());
	}

	private void x_tryModifyRecursively(PseudonymizationCtx ctx, StandbyScheduleArchiveDto plan, ModifiedFlag bModified)
	{
		if (plan.getListPlanRows() != null) {
			for (PlanRowsArchiveDto row: plan.getListPlanRows()) {
				x_tryModifyRecursively(ctx, row, bModified);
			}
		}
		if (plan.getFilter() != null) {
			x_tryModifyRecursively(ctx, plan.getFilter(), bModified);
		}
		if (plan.getPlanHeader() != null) {
			x_tryModifyRecursively(ctx, plan.getPlanHeader(), bModified);
		}
	}

	/**
	 * Replace first und last name of user to pseudonymize in the specified text.
	 * This method is tolerant against null in <code>text</code>.
	 *
	 * This method is public so that it can be tested.
	 *
	 * @param		ctx			(required) The pseudonymization context object
	 * @param		text		(optional) The text to perform pseudonymization on
	 */
	public static StringResult x_replaceFirstnameLastnameInTextN(PseudonymizationCtx ctx, String text, String logMsg)
	{
		if (text == null) {
			return new StringResult(null, false);
		}

		boolean bModified = false;

		String s1 = ctx.orgUserFirstName + " " + ctx.orgUserLastName;
		String s1repl = ctx.theNewData.getFirstName() + " " + ctx.theNewData.getLastName();
		String s2 = ctx.orgUserLastName + " " + ctx.orgUserFirstName;
		String s2repl = ctx.theNewData.getLastName() + " " + ctx.theNewData.getFirstName();
		if (text.contains(s1)) {
			ctx.logDoModify(logMsg);
			text = text.replace(s1, s1repl);
			bModified = true;
		}
		if (text.contains(s2)) {
			ctx.logDoModify(logMsg);
			text = text.replace(s2, s2repl);
			bModified = true;
		}

		return new StringResult(text, bModified);
	}

	/**
	 * This method is public so that it can be tested.
	 */
	public static String x_replaceFirstnameLastnameInTextN(PseudonymizationCtx ctx, String text, String logMsg, ModifiedFlag bModified)
	{
		StringResult result = x_replaceFirstnameLastnameInTextN(ctx, text, logMsg);
		if (result.bModified) {
			bModified.bModified = true;
		}
		return result.text;
	}

	// ================================================================================================================================

	/**
	 * Do pseudonymization.
	 *
	 * @param ctx		(required) The pseudonymization context
	 * @param header	(required) Data
	 * @return			Returns <code>true</code> if the data has been modified and should now be
	 *					saved. Note: <code>false</code> is always returned if writing is disabled.
	 */
	private DebuggingInfo_ArchiveStandbyScheduleHeader modifyRecursively(PseudonymizationCtx ctx, ArchiveStandbyScheduleHeader header)
	{
		if (archiveRepository == null) {
			throw new RuntimeException("archiveRepository is null!");
		}

		// ----
		// ----
		// ----

		ModifiedFlag bModified = new ModifiedFlag();

		// load data
		StandbyScheduleArchiveDto storedPlan = null;
		try {
			String sStoredJson = header.getJsonPlan();
			ctx.logDebug("Trying to parse JSON from StandbyScheduleArchive.ID=" + header.getId());
			if (sStoredJson != null) {
				storedPlan = new Gson().fromJson(sStoredJson, StandbyScheduleArchiveDto.class);
				ctx.logDebug("Parsing succeeded, result contains data.");
			} else {
				// no data
				storedPlan = null;
				ctx.logDebug("Parsing succeeded, result is null.");
			}
		} catch (Exception ee) {
			// ERROR
			ctx.logError("Failed to parse plan! => We're going to delete it.");
			storedPlan = null;
		}

		// modify data (if we succeeded earlier)
		if (storedPlan != null) {
			// loading did succeed
			x_tryModifyRecursively(ctx, storedPlan, bModified);
		}

		// ----------------------------------------------------------------

		ctx.logDoModify("ArchiveStandbyScheduleHeader: title, annotation/comment, modifiedCause, modifiedBy");
		header.setTitle(x_replaceFirstnameLastnameInTextN(ctx, header.getTitle(), "title", bModified));
		header.setComment(x_replaceFirstnameLastnameInTextN(ctx, header.getComment(), "comment", bModified));
		header.setModifiedCause(x_replaceFirstnameLastnameInTextN(ctx, header.getModifiedCause(), "modifiedCause", bModified));
		if (StringUtils.equals(header.getModifiedBy(), ctx.orgUserKey)) {
			header.setModifiedBy(ctx.theNewData.getUserKey());
			bModified.bModified = true;
		}

		// ----------------------------------------------------------------

		String sNewJson;
		if (storedPlan != null) {
			// loading did succeed
			ctx.logDoModify("plan");
			sNewJson = new Gson().toJson(storedPlan);
		} else {
			ctx.logDoDelete("plan");
			sNewJson = null;
		}
		header.setJsonPlan(sNewJson);

		// ----------------------------------------------------------------

		// save data
		if (ctx.bDoModify) {
			ctx.logInfo("Saving changes to data ...");
			archiveRepository.save(header);
		}

		return new DebuggingInfo_ArchiveStandbyScheduleHeader(bModified.bModified, storedPlan);
	}

	// ================================================================================================================================
	// == Public Methods
	// ================================================================================================================================

	public List<DebuggingInfo_ArchiveStandbyScheduleHeader> modifyAll_ArchiveStandbyScheduleHeader(PseudonymizationCtx ctx)
	{
		ArrayList<DebuggingInfo_ArchiveStandbyScheduleHeader> ret = new ArrayList<>();
		for (ArchiveStandbyScheduleHeader x: archiveRepository.findAll()) {
			ret.add(modifyRecursively(ctx, x));
		}
		return ret;
	}

}
