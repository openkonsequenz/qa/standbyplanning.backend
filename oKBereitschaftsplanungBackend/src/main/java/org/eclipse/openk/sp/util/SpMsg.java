/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.text.MessageFormat;

public class SpMsg {

	/** private constructor. */
	private SpMsg() {
	}

	/**
	 * entities
	 */
	public static final String TXT_ENTITY_WITH_ID = "{0} ({1})";
	public static final String TXT_BRANCHES = "Branches";
	public static final String TXT_BRANCHES_WITH_ID = "Branches ({0})";
	public static final String TXT_CALENDAR = "Calendar";
	public static final String TXT_CALENDAR_WITH_ID = "Calendar ({0})";
	public static final String TXT_LOCATION = "Location";
	public static final String TXT_LOCATION_WITH_ID = "Location ({0})";
	public static final String TXT_REGION_WITH_ID = "Region ({0})";
	public static final String TXT_REGION_HAS_FUNCTION_WITH_ID = "RegionHasFunction ({0})";
	public static final String TXT_STANDBY_GROUP = "StandbyGroup";
	public static final String TXT_STANDBY_GROUP_WITH_ID = "StandbyGroup ({0})";
	public static final String TXT_STANDBY_LIST = "StandbyList";
	public static final String TXT_STANDBY_LIST_WITH_ID = "List mit der ID ({0})";
	public static final String TXT_STANDBY_LIST_WITH_ID_NO_GROUPS = "Die List mit der ID ({0}) enthält keine Gruppen!";
	public static final String TXT_USERS = "Users";
	public static final String TXT_USER_WITH_ID = "User ({0})";
	public static final String TXT_USER_FUNCTION = "UserFunction";
	public static final String TXT_USER_FUNCTION_WITH_ID = "UserFunction ({0})";
	public static final String TXT_USER_HAS_FUNCTION_WITH_ID = "UserHasFunction ({0})";
	public static final String TXT_POSTCODE_WITH_ID = "Postcode ({0})";
	public static final String TXT_IS_UNKNOWEN = " is unknown.";
	public static final String TXT_UNKNOWN_SBG_OR_UISBG = "StandbyGroup ({0}) or User/InStandbyGroup ({1}) are unknown.";
	public static final String TXT_UNKNOWN_SBL_OR_SBG = "StandbyList ({0}) or StandbyGroup ({1}) are unknown.";
	public static final String TXT_UNKNOWN_SBG_OR_DURATION = "StandbyGroup ({0}) or StandbyDuration ({1}) are unknown.";

	/**
	 * Date
	 */
	public static final String TXT_WORKING_DAY = "workday";
	public static final String TXT_NOT_WORKING_DAY = "offduty";

	/**
	 * Status
	 */
	public static final Long STATUS_PLANNING = 1L;
	public static final Long STATUS_CLOSED_PLANNING = 2L;

	/**
	 * Actions on 'IST-Ebene'
	 */
	public static final String ACT_STANDBY_MANUAL_ARCHIVING_LIST = "100: Listenarchivierung manuell";
	public static final String ACT_STANDBY_MANUAL_ARCHIVING_GROUP = "101: Gruppenarchivierung manuell";
	public static final String ACT_STANDBY_MOVE = "103: Bereitschaft verschoben";
	public static final String ACT_STANDBY_REPLACE = "104: Bereitschaft (-ten) ersetzt";
	public static final String ACT_STANDBY_DELETE = "105: Bereitschaft (-ten) gelöscht";
	public static final String ACT_STANDBY_FOR_APPROVE_BY_WORKERS_COUNCIL = "106: Vorlage beim Betriebsrat zur Genehmigung";
	public static final String ACT_STANDBY_APPROVED_BY_WORKERS_COUNCIL = "107: Genehmigt durch Betriebsrat";
	public static final String ACT_STANDBY_MOVED_TO_IST_EBENE = "108: Gruppen von Plan- in Ist-Ebene übernommen";

	/**
	 * 
	 * @param msg
	 * @param params
	 * @return
	 */
	public static final String getLbl(String msg, Object... params) {
		return MessageFormat.format(msg, params);
	}

}
