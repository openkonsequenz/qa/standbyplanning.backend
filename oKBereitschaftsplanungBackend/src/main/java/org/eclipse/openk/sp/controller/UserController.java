/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.OrganisationRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.dao.UserFunctionRepository;
import org.eclipse.openk.sp.db.dao.UserHasUserFunctionRepository;
import org.eclipse.openk.sp.db.dao.UserInRegionRepository;
import org.eclipse.openk.sp.db.dao.UserInStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.Organisation;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserHasUserFunction;
import org.eclipse.openk.sp.db.model.UserInRegion;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.LabelValueDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.dto.UserFunctionDto;
import org.eclipse.openk.sp.dto.UserHasUserFunctionDto;
import org.eclipse.openk.sp.dto.UserInRegionDto;
import org.eclipse.openk.sp.dto.UserSelectionDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle USER operations. */
public class UserController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(UserController.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private StandbyGroupController standbyGroupController;

	@Autowired
	private UserInRegionRepository userInRegionRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private UserFunctionRepository userFunctionRepository;

	@Autowired
	private OrganisationRepository organisationRepository;

	@Autowired
	private UserHasUserFunctionRepository userHasUserFunctionRepository;

	@Autowired
	private UserInStandbyGroupRepository userInStandbyGroupRepository;

	@Autowired
	private EntityConverter entityConverter;

	/**
	 * Method to query all users.
	 * 
	 * @param jwt
	 * @return
	 */

	public List<UserDto> getUsers() throws SpException {
		try {
			LOGGER.info("getUsers()");
			ArrayList<UserDto> listUser = new ArrayList<>();
			Iterable<User> itUserList = userRepository.findAllByOrderByLastnameAsc();
			for (User user : itUserList) {
				listUser.add((UserDto) entityConverter.convertEntityToDto(user, new UserDto()));
			}
			return listUser;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.LOADING_LIST_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USERS);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to query all regions.
	 * 
	 * @return
	 */

	public List<UserSelectionDto> getUsersSelection() throws SpException {
		try {
			ArrayList<UserSelectionDto> listUser = new ArrayList<>();
			Iterable<User> itUserList = userRepository.findAllByOrderByLastnameAsc();
			for (User user : itUserList) {
				listUser.add((UserSelectionDto) entityConverter.convertEntityToDto(user, new UserSelectionDto()));
			}
			return listUser;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.LOADING_LIST_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USERS);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to select a user by its id.
	 * 
	 * @param jwt
	 * @param userid
	 * @return
	 */

	public UserDto getUser(Long userId) throws SpException {
		try {
			if (userRepository.exists(userId)) {

				User user = userRepository.findOne(userId);
				user.getLsUserInRegions();

				return (UserDto) entityConverter.convertEntityToDto(user, new UserDto());
			} else {
				throw new SpException();
			}
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USER_WITH_ID, userId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a user.
	 * 
	 * @param jwt
	 * @param userid
	 * @return
	 * @throws SpException
	 */

	public UserDto saveUser(UserDto userDto) throws SpException {
		User user = new User();
		User tempUser = null;
		Organisation organisation = null;
		Long orgaId = null;

		try {
			if (userDto.getId() != null) {
				user = userRepository.findOne(userDto.getId());
				user.getLsUserInRegions();
			}

			if (userDto.getOrganisation() != null) {
				orgaId = userDto.getOrganisation().getId();
			}

			if (orgaId != null && organisationRepository.exists(orgaId)) {
				organisation = organisationRepository.findOne(orgaId);
			}

			tempUser = user;
			user = (User) entityConverter.convertDtoToEntity(userDto, user);
			user.setModificationDate(new Date());
			user.setLsUserHasUserFunction(tempUser.getLsUserHasUserFunction());
			user.setLsUserInRegions(tempUser.getLsUserInRegions());
			user.setOrganisation(organisation);

			user = userRepository.save(user);
			return (UserDto) entityConverter.convertEntityToDto(user, new UserDto());

		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USER_WITH_ID, new Gson().toJson(user));
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link UserInRegion}
	 * 
	 * @param userId
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<UserInRegionDto> saveRegionsForUser(Long userId, List<UserInRegionDto> lsUserInRegionDtos)
			throws SpException {
		Long tmpRegionId = 0l;
		try {
			User user = userRepository.findOne(userId);

			for (UserInRegionDto dto : lsUserInRegionDtos) {
				tmpRegionId = dto.getRegionId();

				if (userRepository.exists(userId) && regionRepository.exists(dto.getRegionId())) {
					Region region = regionRepository.findOne(dto.getRegionId());
					UserInRegion userInRegion = (UserInRegion) entityConverter.convertDtoToEntity(dto,
							new UserInRegion());
					userInRegion.setRegion(region);
					userInRegion.setUser(user);

					user.getLsUserInRegions().add(userInRegion);

					userInRegionRepository.save(userInRegion);
					userRepository.save(user);
				} else {
					throw new IllegalArgumentException();
				}
			}

			lsUserInRegionDtos = new ArrayList<>();
			lsUserInRegionDtos = entityConverter.convertEntityToDtoList(user.getLsUserInRegions(), lsUserInRegionDtos,
					UserInRegionDto.class);

			return lsUserInRegionDtos;
		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USER_WITH_ID, userId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpRegionId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link UserInRegion}
	 * 
	 * @param userId
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<UserInRegionDto> deleteRegionsForUser(Long userId, List<UserInRegionDto> lsUserInRegionDtos)
			throws SpException {

		Long tmpUserInRegionId = 0l;
		try {
			User user = userRepository.findOne(userId);

			for (UserInRegionDto dto : lsUserInRegionDtos) {

				tmpUserInRegionId = dto.getId();

				if (userRepository.exists(userId) && regionRepository.exists(dto.getRegionId())
						&& userInRegionRepository.exists(tmpUserInRegionId)) {

					UserInRegion userInRegion = userInRegionRepository.findOne(tmpUserInRegionId);

					userInRegionRepository.delete(dto.getId());
					userInRegionRepository.flush();

					user.getLsUserInRegions().remove(userInRegion);

				} else {
					throw new IllegalArgumentException();
				}
			}

			lsUserInRegionDtos = new ArrayList<>();
			lsUserInRegionDtos = entityConverter.convertEntityToDtoList(user.getLsUserInRegions(), lsUserInRegionDtos,
					UserInRegionDto.class);

			return lsUserInRegionDtos;
		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_DELETE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpUserInRegionId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link UserInRegion}
	 * 
	 * @param userId
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<UserHasUserFunctionDto> saveFunctionsForUser(Long userId, List<UserHasUserFunctionDto> lsFunctionDtos)
			throws SpException {

		Long tmpUhufId = null;
		Long tmpFunctionId = null;

		try {
			User user = userRepository.findOne(userId);

			for (UserHasUserFunctionDto dto : lsFunctionDtos) {
				tmpUhufId = dto.getId();
				tmpFunctionId = dto.getFunctionId();

				if (userRepository.exists(userId) && userFunctionRepository.exists(dto.getFunctionId())) {
					UserHasUserFunction uhuf = new UserHasUserFunction();

					UserFunction function = userFunctionRepository.findOne(dto.getFunctionId());
					function.getLsUserHasUserFunction().add(uhuf);

					uhuf.setUserFunction(function);
					uhuf.setUser(user);

					user.getLsUserHasUserFunction().add(uhuf);

					userHasUserFunctionRepository.save(uhuf);
					userFunctionRepository.save(function);
					userRepository.save(user);

				} else {
					throw new IllegalArgumentException();
				}
			}

			lsFunctionDtos = new ArrayList<>();
			lsFunctionDtos = entityConverter.convertEntityToDtoList(user.getLsUserHasUserFunction(), lsFunctionDtos,
					UserHasUserFunctionDto.class);

			return lsFunctionDtos;
		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USER_WITH_ID, userId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION_WITH_ID, tmpFunctionId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_USER_HAS_FUNCTION_WITH_ID, tmpUhufId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link UserInRegion}
	 *
	 * @param userId
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<UserHasUserFunctionDto> deleteFunctionsForUser(Long userId, List<UserHasUserFunctionDto> lsFunctionDtos)
			throws SpException {
		LOGGER.info("deleteFunctionsForUser() - start");
		Long tmpFunctionId = null;
		Long tmpHasFunctionId = null;
		try {

			User user = userRepository.findOne(userId);

			for (UserHasUserFunctionDto dto : lsFunctionDtos) {
				tmpFunctionId = dto.getFunctionId();
				tmpHasFunctionId = dto.getId();

				if (userHasUserFunctionRepository.exists(tmpHasFunctionId) && userRepository.exists(userId)
						&& userFunctionRepository.exists(tmpFunctionId)) {

					UserHasUserFunction uhuf = userHasUserFunctionRepository.findOne(tmpHasFunctionId);

					userHasUserFunctionRepository.delete(tmpHasFunctionId);
					LOGGER.info("userHasUserFunctionRepository.delete()");

					userHasUserFunctionRepository.flush();
					LOGGER.info("userHasUserFunctionRepository.flush()");

					user.getLsUserHasUserFunction().remove(uhuf);

				} else {
					throw new IllegalArgumentException();
				}
			}

			lsFunctionDtos = new ArrayList<>();
			lsFunctionDtos = entityConverter.convertEntityToDtoList(user.getLsUserHasUserFunction(), lsFunctionDtos,
					UserHasUserFunctionDto.class);

			LOGGER.info("deleteFunctionsForUser() - end");

			return lsFunctionDtos;
		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_USER_WITH_ID, userId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_USER_FUNCTION_WITH_ID, tmpFunctionId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_USER_HAS_FUNCTION_WITH_ID, tmpHasFunctionId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * get filtered users by function and region of a standbygroup
	 * 
	 * @param sbgId
	 * @return
	 * @throws SpException
	 */
	public List<UserSelectionDto> getFilteredUsersSelection(Long standbyGroupid) {

		StandbyGroupDto standbyGroupDto = standbyGroupController.getStandbyGroup(standbyGroupid);
		// functions
		List<UserFunctionDto> lsUserFunctionDto = standbyGroupDto.getLsUserFunction();
		ArrayList<Long> listFunctionId = new ArrayList<>();
		if (lsUserFunctionDto != null) {
			for (UserFunctionDto userFunctionDto : lsUserFunctionDto) {
				listFunctionId.add(userFunctionDto.getFunctionId());
			}
		}
		// regions
		List<RegionSelectionDto> lsRegionDto = standbyGroupDto.getLsRegions();
		ArrayList<Long> listRegionId = new ArrayList<>();
		if (lsRegionDto != null) {
			for (RegionSelectionDto regionDto : lsRegionDto) {
				listRegionId.add(regionDto.getId());
			}
		}

		Date validDate = LocalDate.now().toDate();
		Iterable<User> itUserList = null;

		if (listFunctionId.isEmpty() && listRegionId.isEmpty()) {
			itUserList = userRepository.findAllValid(validDate);
		} else if (listFunctionId.isEmpty()) {
			itUserList = userRepository.findByStandbyGroupRID(listRegionId, validDate);
		} else if (listRegionId.isEmpty()) {
			itUserList = userRepository.findByStandbyGroupFID(listFunctionId, validDate);
		} else {
			itUserList = userRepository.findByStandbyGroupFIDRID(listFunctionId, listRegionId, validDate);
		}

		ArrayList<UserSelectionDto> listUser = new ArrayList<>();
		for (User user : itUserList) {
			listUser.add((UserSelectionDto) entityConverter.convertEntityToDto(user, new UserSelectionDto()));
		}
		return listUser;
	}

	/**
	 * Method to check if user is available for time slot.
	 * 
	 * @param userId
	 * @param validFrom
	 * @param validTo
	 * @return
	 * @throws SpException
	 */
	public boolean isUserValid(Long userId, Date validFrom, Date validTo) throws SpException {
		Boolean result = false;
		try {
			User user = userRepository.findOne(userId);
			if (DateHelper.isDateBefore(user.getValidFrom(), validFrom)
					&& DateHelper.isDateAfter(user.getValidTo(), validTo)) {
				result = true;
			} else {
				SpErrorEntry ee = SpExceptionEnum.USER_NOT_AVAILABLE_EXCEPTION.getEntry();
				LOGGER.warn(ee);
				return false;
			}
			return result;
		} catch (IllegalArgumentException e) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			SpException spE = new SpException(ee, userId);
			LOGGER.warn(spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			LOGGER.warn(ee);
			throw new SpException(ee);
		}
	}

	/**
	 * Method to check if user is available for time slot.
	 * 
	 * @param userId
	 * @param validFrom
	 * @param validTo
	 * @return
	 * @throws SpException
	 */
	public boolean isUserInGroup(Long userId, Long groupId, Date validFrom, Date validTo) {
		List<UserInStandbyGroup> lsUisg = userInStandbyGroupRepository.findUserForInterval(userId, groupId, validFrom,
				validTo);
		boolean isUserAvailable = false;
		if (lsUisg != null && !lsUisg.isEmpty()) {
			isUserAvailable = true;
		}
		return isUserAvailable;
	}

	public List<LabelValueDto> getUsersDropDown() throws SpException {
		List<LabelValueDto> list = new ArrayList<>();

		List<UserDto> usersList = getUsers();

		for (UserDto userDto : usersList) {

			Long value = userDto.getId();
			String label = getUserString(userDto);
			LabelValueDto dto = new LabelValueDto();

			dto.setLabel(label);
			dto.setValue(value);
			list.add(dto);
		}

		Collections.sort(list, (o1, o2) -> o1.getLabel().compareTo(o2.getLabel()));

		return list;
	}

	public String getUserString(UserDto userDto) {

		StringBuffer sb = new StringBuffer();

		String label = userDto.getLastname();
		if (label != null && !label.equals("")) {
			sb.append(label);
			sb.append(", ");
		}

		label = userDto.getFirstname();
		if (label != null && !label.equals("")) {
			sb.append(label);
			sb.append("; ");
		}

		if (userDto.getOrganisation() != null) {
			label = userDto.getOrganisation().getOrgaName();
			if (label != null && !label.equals("")) {
				sb.append(label);
				sb.append(" ");
			}
		}

		sb.append("(");
		List<UserHasUserFunctionDto> lsFunction = userDto.getLsUserFunctions();
		for (int i = 0; i < lsFunction.size(); i++) {
			UserHasUserFunctionDto uhf = lsFunction.get(i);
			sb.append(uhf.getFunctionName());
			if (i < lsFunction.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append(")");
		return sb.toString();
	}

	public UserDto anonymizeUser(UserDto userDto) {
		LOGGER.info("anonymizeUser");
		LOGGER.warn("anonymizeUser");
		LOGGER.error("anonymizeUser");
		return userDto;
	}
}
