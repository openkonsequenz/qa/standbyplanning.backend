/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHelper {

	public static String getFullStackTrace(Throwable someThrowable) {
		StringWriter sw = new StringWriter();
		someThrowable.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

}
