/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "LabelValueDto")
@JsonInclude(Include.NON_NULL)
public class LabelValueDto extends AbstractDto {

	/** default serial id. */
	private static final long serialVersionUID = 1L;
	private Long value;
	private String label;
	private List<LocationForBranchDto> lsLocationForBranches;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public List<LocationForBranchDto> getLsLocationForBranches() {
		return lsLocationForBranches;
	}

	public void setLsLocationForBranches(List<LocationForBranchDto> lsLocationForBranches) {
		this.lsLocationForBranches = lsLocationForBranches;
	}

}