/********************************************************************************
 * Copyright (c) 2022-2023 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.impl.pseudonymization;



/**
 * Utility class for working with strings.
 *
 * @author		Jürgen Knauth
 */
public class StringUtils
{

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Getters/Setters
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Compares two strings. This method is tolerant to arguments being <code>null</code>.
	 *
	 * @param		strA		(optional) A string
	 * @param		strB		(optional) Another string
	 * @return					Returns <code>true</code> if either both Strings are <code>null</code> or contain the same content.
	 */
	public static boolean equals(String strA, String strB)
	{
		if (strA == null) {
			if (strB == null) {
				return true;
			} else {
				return false;
			}
		} else {
			if (strB == null) {
				return false;
			} else {
				return strA.equals(strB);
			}
		}
	}

}




