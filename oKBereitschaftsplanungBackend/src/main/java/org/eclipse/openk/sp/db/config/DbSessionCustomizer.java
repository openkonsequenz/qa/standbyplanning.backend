/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.config;

import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.sessions.Session;
import org.springframework.stereotype.Service;

@Service
public class DbSessionCustomizer implements SessionCustomizer {

	public void customize(Session session) {
		String schema = (String) session.getProperty("eclipselink.db.schema");
		session.getLogin().setTableQualifier(schema);
		session.getLogin().setUsesStreamsForBinding(true);
	}
}
