/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation;

import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Interface for different validation beans. */
public interface IValidator {

	List<PlanningMsgDto> execute(Date from, Date till, StandbyGroup currentGroup, List<StandbyGroup> lsStandbyGroups,
			Long statusId, Long userId) throws SpException;
}
