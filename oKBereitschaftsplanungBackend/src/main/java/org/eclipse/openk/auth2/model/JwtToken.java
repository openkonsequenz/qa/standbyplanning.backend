/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.auth2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JwtToken {

    //No Sonar because variables are wrong due to jwt definition

    private String access_token; //NOSONAR
    private String refresh_token; //NOSONAR
    private String token_type; //NOSONAR
    private String session_state; //NOSONAR
    private Integer expires_in; //NOSONAR
    private Integer refresh_expires_in; //NOSONAR
    @JsonProperty("not-before-policy")
    private Integer not_before_policy; //NOSONAR

    public String getAccessToken() { return access_token; }
    public void setAccessToken(String access_token) { this.access_token = access_token; } //NOSONAR
    public String getRefreshToken() { return refresh_token; }
    public void setRefreshToken(String refresh_token) { this.refresh_token = refresh_token; } //NOSONAR
    public String getTokenType() { return token_type; }
    public void setTokenType(String token_type) { this.token_type = token_type; } //NOSONAR
    public String getSessionState() { return session_state; }
    public void setSessionState(String session_state) { this.session_state = session_state; } //NOSONAR
    public Integer getExpiresIn() { return expires_in; }
    public void setExpiresIn(Integer expires_in) { this.expires_in = expires_in; } //NOSONAR
    public Integer getRefreshExpiresIn() { return refresh_expires_in; }
    public void setRefreshExpiresIn(Integer refresh_expires_in) { this.refresh_expires_in = refresh_expires_in; } //NOSONAR
    public Integer getNotBeforePolicy() { return not_before_policy; }
    public void setNotBeforePolicy(Integer not_before_policy) { this.not_before_policy = not_before_policy; } //NOSONAR
}