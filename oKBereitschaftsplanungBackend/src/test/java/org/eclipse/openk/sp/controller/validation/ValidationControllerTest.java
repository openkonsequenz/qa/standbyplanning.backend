/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.UserController;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.controller.validation.validator.GroupCoverageValidator;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyListHasStandbyGroup;
import org.eclipse.openk.sp.dto.ValidationDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.context.ApplicationContext;

/** Class to validate if all time slots are set for a group. */
@RunWith(MockitoJUnitRunner.class)
public class ValidationControllerTest {
	protected static final Logger LOGGER = Logger.getLogger(ValidationControllerTest.class);

	@Mock
	private ApplicationContext appContext;

	@Mock
	private GroupCoverageValidator groupCoverageValidator;

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private StandbyGroupRepository standbyGroupRepository;

	@Mock
	private UserController userController;

	@Mock
	private UserRepository userRepository;

	@Mock
	private PlanningController planningController;

	@InjectMocks
	ValidationController validatonController;

	@Test
	public void startValidationTest() throws SpException {
		Date from = new Date();
		Date till = new Date();
		List<StandbyGroup> lsStandbyGroups = new ArrayList<>();
		StandbyGroup group = new StandbyGroup();
		group.setId(1L);
		group.setTitle("Gruppe1");
		lsStandbyGroups.add(group);
		group = new StandbyGroup();
		group.setTitle("Gruppe2");
		group.setId(2L);
		lsStandbyGroups.add(group);

		List<String> lsValidationBeanNames = new ArrayList<>();
		lsValidationBeanNames.add("testBeanName");
		Long statusId = 1L;

		Mockito.when(appContext.containsBean(Mockito.anyString())).thenReturn(true);
		Mockito.when(appContext.getBean(Mockito.anyString())).thenReturn(groupCoverageValidator);
		Mockito.when(groupCoverageValidator.execute(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		try {
			// positive test with existing bean
			List<PlanningMsgDto> lsResult = validatonController.startValidation(from, till, lsStandbyGroups,
					lsValidationBeanNames, statusId, null);
			assertNotNull(lsResult);

			// positive test without existing bean
			Mockito.when(appContext.containsBean(Mockito.anyString())).thenReturn(false);
			lsResult = validatonController.startValidation(from, till, lsStandbyGroups, lsValidationBeanNames, statusId,
					null);
			assertNotNull(lsResult);
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}

		try {
			// negative test with exception
			Mockito.when(appContext.containsBean(Mockito.anyString())).thenReturn(true);
			Mockito.when(appContext.getBean(Mockito.anyString())).thenThrow(new BeanDefinitionStoreException("test"));
			List<PlanningMsgDto> lsResult = validatonController.startValidation(from, till, lsStandbyGroups,
					lsValidationBeanNames, statusId, null);
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNotNull(e);
		}

	}

	@Test
	public void startValidationWithDtoTest() {
		StandbyList sbList = new StandbyList();
		// List<StandbyGroup> lsStandbyGroups = new ArrayList<>();
		List<StandbyListHasStandbyGroup> lsStandbyListHasStandbyGroup = new ArrayList<>();
		StandbyGroup group = new StandbyGroup();

		sbList.setId(1L);

		sbList.setLsStandbyListHasStandbyGroup(lsStandbyListHasStandbyGroup);

		StandbyListHasStandbyGroup standbyListHasStandbyGroup = new StandbyListHasStandbyGroup();
		standbyListHasStandbyGroup.setId(1L);
		standbyListHasStandbyGroup.setPosition(1);
		standbyListHasStandbyGroup.setStandbyGroup(group);
		standbyListHasStandbyGroup.setStandbyList(sbList);
		lsStandbyListHasStandbyGroup.add(standbyListHasStandbyGroup);

		ValidationDto validationDto = new ValidationDto();
		validationDto.setStandbyGroupId(1L);
		validationDto.setStandbyListId(1L);
		validationDto.setStatusId(1L);
		validationDto.setValidFromDate(DateHelper.getDate(2018, 10, 01));
		validationDto.setValidToDate(DateHelper.getDate(2018, 10, 03));

		List<PlanningMsgDto> lsPlanningList = new ArrayList<>();

		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(sbList);
		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(group);
		PlanningMsgResponseDto dto = null;
		try {
			// success test 1 - with standbyListId
			dto = validatonController.startValidation(validationDto);
			assertNotNull(dto);

			// success test 2 - with standbyGroupId
			dto = null;
			validationDto.setStandbyListId(null);
			dto = validatonController.startValidation(validationDto);
			assertNotNull(dto);
		} catch (Exception e) {
			assertNull(e);
		}

		// failure test 1 - without standbyListId and without standbyGroupId
		try {
			dto = null;
			validationDto.setStandbyListId(null);
			validationDto.setStandbyGroupId(null);
			dto = validatonController.startValidation(validationDto);
			assertNull(dto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// failure test 2 - null pointer exception
		try {
			dto = null;
			dto = validatonController.startValidation(null);
			assertNull(dto);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}
}
