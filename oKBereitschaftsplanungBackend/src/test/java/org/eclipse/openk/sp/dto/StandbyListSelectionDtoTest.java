/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class StandbyListSelectionDtoTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		StandbyListSelectionDto sl = new StandbyListSelectionDto();
		sl.setId(1l);
		assertEquals(1l, sl.getId().longValue());

		sl.setModificationDate(d);
		assertEquals(d, sl.getModificationDate());

		sl.setTitle("Title");
		assertEquals("Title", sl.getTitle());

		sl.setLsStandbyGroupsStr("Text");
		assertEquals("Text", sl.getLsStandbyGroupsStr());

	}

}
