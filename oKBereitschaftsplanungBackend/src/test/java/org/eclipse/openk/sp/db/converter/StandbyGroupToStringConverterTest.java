/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.junit.Test;

public class StandbyGroupToStringConverterTest {

	@Test
	public void convertTest() {
		Object destination = null;
		Class destClass = null;
		Class sourceClass = null;

		StandbyGroup elem1 = new StandbyGroup();
		elem1.setTitle("fkt1");
		StandbyGroup elem2 = new StandbyGroup();
		elem2.setTitle("fkt2");

		List<StandbyGroup> source = new ArrayList<StandbyGroup>();
		source.add(elem1);
		source.add(elem2);

		StandbyGroupToStringConverter conv = new StandbyGroupToStringConverter();
		Object obj = conv.convert(destination, source, destClass, sourceClass);

		assertNotNull(obj);

		String source2 = "Gruppe1, Gruppe2, Gruppe3";
		obj = conv.convert(destination, source2, destClass, sourceClass);

		assertNotNull(obj);

		Long source3 = new Long(2);
		obj = conv.convert(destination, source3, destClass, sourceClass);

		assertNull(obj);
	}

	@Test
	public void UhfListToString() {

		StandbyGroupToStringConverter conv = new StandbyGroupToStringConverter();

		StandbyGroup elem1 = new StandbyGroup();
		elem1.setTitle("fkt1");
		StandbyGroup elem2 = new StandbyGroup();
		elem2.setTitle("fkt2");

		List<StandbyGroup> source = new ArrayList<StandbyGroup>();
		source.add(elem1);
		source.add(elem2);

		String str = conv.listStandbyGroupToString(source);
		assertNotNull(str);

	}

	@Test
	public void StringToUHF() {

		StandbyGroupToStringConverter conv = new StandbyGroupToStringConverter();

		String source = "Gruppe1, Gruppe2, Gruppe3";

		List<StandbyGroup> lsUserHasUserFunctions = conv.stringToStandbyGroup(source);

		assertNotNull(lsUserHasUserFunctions);
		assertEquals(3, lsUserHasUserFunctions.size());

		String source2 = "Gruppe1";
		lsUserHasUserFunctions = conv.stringToStandbyGroup(source2);

		assertNotNull(lsUserHasUserFunctions);
		assertEquals(1, lsUserHasUserFunctions.size());
	}
}
