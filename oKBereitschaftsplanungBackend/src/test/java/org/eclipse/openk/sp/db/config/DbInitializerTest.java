/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.config;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class DbInitializerTest extends DbInitializer {
	public static final Logger LOGGER = Logger.getLogger(DbInitializerTest.class);
	private String dbUrl = "jdbc:h2:mem:test";
	private String dbDriver = "org.h2.Driver";
	private String dbUsername = "sa";
	private String dbPass = "sa";
	private Properties p = new Properties();

	private DbInitializer dbInitSpy = Mockito.spy(new DbInitializer());

	@Mock
	FileHelper fileHelper;

	@Mock
	Connection connection;

	@InjectMocks
	DbInitializer dbInitializer;

	@Before
	public void init() {

		ReflectionTestUtils.setField(dbInitializer, "dbUrl", dbUrl);
		ReflectionTestUtils.setField(dbInitializer, "dbDriver", dbDriver);
		ReflectionTestUtils.setField(dbInitializer, "dbUsername", dbUsername);
		ReflectionTestUtils.setField(dbInitializer, "dbPass", dbPass);
		ReflectionTestUtils.setField(dbInitializer, "dbSchema", "public");
		ReflectionTestUtils.setField(dbInitializer, "dbRedeploy", false);

		p = new Properties();
		p.setProperty("sql.scripts.delete", "BER_init,BER_init2");
		p.setProperty("sql.scripts.init", "BER_init,BER_init2");
		p.setProperty("sql.scripts.hist", "BER_init,BER_init2");

		dbInitSpy = Mockito.spy(dbInitializer);
	}

	@Test
	public void configureDatabaseTest()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException {

		when(fileHelper.loadPropertiesFromResource(any())).thenReturn(p);
		Mockito.doNothing().when(dbInitSpy).initializeDatabase();
		try {
			dbInitializer.configureDatabase();
		} catch (Exception e) {
			assertNull(e);
		}

		// change property to true for if condition
		p.setProperty("db.redeploy", "true");
		when(fileHelper.loadPropertiesFromResource(any())).thenReturn(p);
		try {
			dbInitSpy.configureDatabase();
		} catch (Exception e) {
			assertNull(e);
		}
	}

	@Test
	public void initializeDatabase() {
		try {
			when(fileHelper.loadPropertiesFromResource(any())).thenReturn(p);
			Mockito.doNothing().when(dbInitSpy).loopOverSqlFiles(any());
			try {
				dbInitSpy.initializeDatabase();
			} catch (Exception e) {
				assertNull(e);
			}
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}

	@Test
	public void loopOverSqlFiles() {
		try {
			Mockito.doNothing().when(dbInitSpy).runSQLStatementByFile(any());
			String str = "test,test2,test3";
			dbInitSpy.loopOverSqlFiles(str.split(","));
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}

	@Test
	public void runSQLStatementByFileTest() {
		try {
			Statement stmt = null;

			when(fileHelper.loadPropertiesFromResource(any())).thenReturn(p);
			dbInitializer.configureDatabase();
			Connection conn = dbInitializer.getJdbcConnection();

//			when(dbInitializer.readSqlFromFile(any())).thenReturn("SELECT 1;");
			dbInitializer.runSQLStatementByFile("test");
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNotNull(e);
		}

	}

	@Test
	public void readSqlFromFileTest() {
		try {
			dbInitializer.readSqlFromFile("logging.properties");
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNotNull(e);
		}
	}

	@Test
	public void getJdbcConnectionPositiveTest() {
		Statement stmt = null;
		try {
			when(fileHelper.loadPropertiesFromResource(any())).thenReturn(p);
			dbInitializer.configureDatabase();
			Connection conn = dbInitializer.getJdbcConnection();
			// null try
			dbInitializer.closeStatement(stmt);

			// set try
			stmt = conn.createStatement();
			dbInitializer.closeStatement(stmt);

			// closed try
			stmt = conn.createStatement();
			stmt.close();
			dbInitializer.closeStatement(stmt);

			dbInitializer.closeConnection(conn);
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}

	@Test
	public void getJdbcConnectionNegativeTest() {
		Statement stmt = null;
		try {
			when(fileHelper.loadPropertiesFromResource(any())).thenReturn(p);
			dbInitializer.configureDatabase();
			Connection conn = dbInitializer.getJdbcConnection();

			// ------------
			// Test statements
			// ------------
			// null try
			dbInitializer.closeStatement(stmt);

			// set try
			stmt = conn.createStatement();
			dbInitializer.closeStatement(stmt);

			// closed try
			stmt = conn.createStatement();
			stmt.close();
			dbInitializer.closeStatement(stmt);

			try {
				conn.close();
				stmt.executeQuery("SELECT WRONG.Test.Query;");
				assertNotNull(stmt);
			} catch (Exception e) {
				assertNotNull(e);
			}

			dbInitializer.closeConnection(conn);
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}
}