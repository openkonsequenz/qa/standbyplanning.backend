/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.exceptions.SpNestedException;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthControllerTest {

	@Mock
	FileHelper fileHelper;

	@InjectMocks
	AuthController authController = new AuthController();

	private String errorToken = "https://169.50.13.155/elogbookFE/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";
	private String accessToken = "http://169.50.13.155/spfe/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";
	private String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";

	private boolean useHttps = true;

	@Test(expected = HttpStatusException.class)
	public void testPerformGetRequest() throws HttpStatusException {
		System.out.println("testPerformGetRequest");

		boolean useHttps = true;
		String baseURL = "https://169.50.13.155/spfe/";
		String restParam = "testParam";

		authController.performGetRequest(useHttps, baseURL, restParam, token);

		System.out.println("testPerformGetRequest - ende");
	}

	@Test(expected = HttpStatusException.class)
	public void testPerformGetRequestException() throws HttpStatusException {
		System.out.println("testPerformGetRequestException");

		boolean useHttps = false;
		String baseURL = "";
		String restParam = "";

		authController.performGetRequest(useHttps, baseURL, restParam, "");

		System.out.println("testPerformGetRequestException - ende");
	}

	@Test(expected = SpNestedException.class)
	public void createJson404() throws SpNestedException {
		System.out.println("createJson404");

		String retJson = null;
		Throwable th = null;
		HttpResponse response = getResponse("http://localhost:8080/test");

		authController.validateResponse(response);
		assertNotNull(th);

		System.out.println("createJson404 - ende");
	}

	@Test(expected = SpNestedException.class)
	public void createJson501() throws SpNestedException {
		System.out.println("createJson501");

		String retJson = null;
		Throwable th = null;
		HttpResponse response = getResponse("http://localhost:8080/manager/html");

		authController.validateResponse(response);
		assertNotNull(th);

		System.out.println("createJson404 - ende");
	}

	@Test
	public void createJson500() {
		System.out.println("createJson500");

		String retJson = null;
		Throwable th = null;
		HttpResponse response = getResponse(errorToken);

		try {
			retJson = authController.responseToJson(response, StandardCharsets.UTF_8);
		} catch (HttpStatusException e) {
			th = e;
		}
		assertNotNull(th);

		System.out.println("createJson500 - ende");
	}

	@Test
	public void createJson502() {
		System.out.println("createJson502");

		HttpResponse response = null;
		Throwable th = null;
		boolean useHttps = true;
		String baseURL = "";
		String restParam = "";

		try {
			response = authController.performGetRequest(useHttps, baseURL, restParam, token);
		} catch (HttpStatusException e) {
			th = e;
		}
		assertNotNull(th);

		System.out.println("createJson502 - ende");
	}

	@Test(expected = SpNestedException.class)
	public void createJsonResponseTest() throws HttpStatusException {
		System.out.println("createJsonResponseTest");

		String retJson;
		HttpResponse response = getResponse(accessToken);

		retJson = authController.responseToJson(response, StandardCharsets.UTF_8);

		assertNotNull(retJson);

		System.out.println("createJsonResponseTest - ende");
	}

	@Test
	public void createHttpsClientTest() {
		System.out.println("createHttpsClientTest");

		boolean useHttps = true;
		SSLContextBuilder builder = null;
		Throwable th = null;
		try {
			CloseableHttpClient client = authController.createHttpsClient(useHttps, builder);
		} catch (HttpStatusException e) {
			th = e;
		}

		assertNotNull(th);

		System.out.println("createHttpsClientTest - ende");
	}

	@Test(expected = SpNestedException.class)
	public void createJsonResponseExceptionTest() throws HttpStatusException {
		System.out.println("createJsonResponseExceptionTest");

		String retJson;
		HttpResponse response = getResponse(accessToken);

		retJson = authController.responseToJson(response, StandardCharsets.UTF_16BE);
		assertNotNull(retJson);

		authController.validateResponse(null);

		System.out.println("createJsonResponseExceptionTest - ende");

	}

	@Test(expected = SpNestedException.class)
	public void loginTest() throws HttpStatusException, IOException {
		System.out.println("createJsonResponseExceptionTest");

		String retJson;
		HttpResponse response = getResponse("http://localhost:8080");

		Mockito.when(fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_BASE_URL))
				.thenReturn("http://localhost:8080");
		Mockito.when(fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_USE_HTTPS)).thenReturn("true");

		Response result = authController.login(accessToken);
		assertNotNull(result);

		authController.validateResponse(null);

		System.out.println("createJsonResponseExceptionTest - ende");

	}

	@Test(expected = SpNestedException.class)
	public void logoutTest() throws HttpStatusException, IOException {
		System.out.println("createJsonResponseExceptionTest");

		String retJson;
		HttpResponse response = getResponse("http://localhost:8080");

		Mockito.when(fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_BASE_URL))
				.thenReturn("http://localhost:8080");
		Mockito.when(fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_USE_HTTPS)).thenReturn("true");

		Response result = authController.logout(accessToken);
		assertNotNull(result);

		authController.validateResponse(null);

		System.out.println("createJsonResponseExceptionTest - ende");

	}

	private HttpResponse getResponse(String strURL) {
		System.out.println("getResponse");

		HttpResponse response = null;
		// Execute request an catch response
		try {
			HttpGet getRequest = new HttpGet(strURL);
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			response = httpClient.execute(getRequest);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("getResponse - ende");
		return response;

	}

}
