/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.sql.SQLException;

import org.eclipse.openk.sp.db.config.DbInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

@RunWith(MockitoJUnitRunner.class)
public class StandbyPlanningApplicationTest {

	@Mock
	private ApplicationContext appContext;

	@Mock
	private DbInitializer dbInitSqlData;

	@InjectMocks
	StandbyPlanningApplication standbyPlanningApplication;

	@Test
	public void StandbyPlanningApplicationConstructorTest() {
		// StandbyPlanningApplication app = new StandbyPlanningApplication();
		// assertNotNull(standbyPlanningApplication);

	}

	@Test
	public void getterSetterTest() {
		String name = "NoFile.properties";
		standbyPlanningApplication.setBaseURLPortal(name);
		assertEquals(name, standbyPlanningApplication.getBaseURLPortal());
	}

	@Test
	public void postConstructTest() {
		Throwable th = null;
		try {
			standbyPlanningApplication.postConstruct();
		} catch (Exception e) {
			th = e;
		}
		assertNull(th);
	}

	@Test
	public void postConstructNeagtivTest() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, SQLException {

			Mockito.when(standbyPlanningApplication.configureApp()).thenThrow(new IOException());
			
			String result = standbyPlanningApplication.postConstruct();
			
			assertNotNull(result);
	}

}
