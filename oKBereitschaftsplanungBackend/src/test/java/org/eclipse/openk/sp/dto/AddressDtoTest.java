/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

public class AddressDtoTest {

	@Test
	public void testGettersAndSetters() throws MalformedURLException {
		AddressDto addr = new AddressDto();
		addr.setCommunity("Community");
		assertEquals("Community", addr.getCommunity());

		addr.setCommunitySuffix("Suffix");
		assertEquals("Suffix", addr.getCommunitySuffix());

		addr.setHousenumber("10a");
		assertEquals("10a", addr.getHousenumber());

		addr.setId((long) 123);
		assertEquals(123l, addr.getId().longValue());

		addr.setLatitude("51.718921");
		assertEquals("51.718921", addr.getLatitude());

		addr.setLongitude("8.757509");
		assertEquals("8.757509", addr.getLongitude());

		addr.setPostcode("33100");
		assertEquals("33100", addr.getPostcode());

		addr.setStreet("Teststraße");
		assertEquals("Teststraße", addr.getStreet());

		addr.setUrlMap(new URL("https://www.mettenmeier.de/").toString());
		assertEquals("https://www.mettenmeier.de/", addr.getUrlMap());

		addr.setWgs84zone("WGS-84");
		assertEquals("WGS-84", addr.getWgs84zone());
	}

}
