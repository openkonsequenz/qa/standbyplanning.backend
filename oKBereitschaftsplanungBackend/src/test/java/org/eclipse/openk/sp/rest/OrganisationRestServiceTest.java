/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.OrganisationController;
import org.eclipse.openk.sp.dto.OrganisationDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class OrganisationRestServiceTest {

	@InjectMocks
	private OrganisationRestService organisationRestService;

	@Mock
	private OrganisationController organisationController;

	public OrganisationRestServiceTest() {

	}

	@Test
	public void getOrganisationsTest() {
		String jwt = "LET-ME-IN";
		Response result = organisationRestService.getOrganisations(jwt);
		assertNotNull(result);
	}
	
	@Test
	public void getOrganisationsSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = organisationRestService.getOrganisationSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getOrganisationTest() {
		String jwt = "LET-ME-IN";
		Response result = organisationRestService.getOrganisation(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void saveOrganisationTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testOrganisation.json");
		OrganisationDto organisationDto = (OrganisationDto) new Gson().fromJson(json, OrganisationDto.class);

		String jwt = "LET-ME-IN";
		Response result = organisationRestService.saveOrganisation(jwt, organisationDto);
		assertNotNull(result);
	}
	

}
