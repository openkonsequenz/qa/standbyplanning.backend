/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.TimeDto;
import org.junit.Test;

public class StandbyScheduleSearchDtoTest {

	@Test
	public void getSetTest() {
		Date d = new Date();
		String text = "test";
		Long id = new Long(2);

		StandbyScheduleSearchDto dto = new StandbyScheduleSearchDto();

		dto.setDateIndex(d);
		assertEquals(d, dto.getDateIndex());

		dto.setCity(text);
		assertEquals(text, dto.getCity());

		dto.setListId(id);
		assertEquals(id.longValue(), dto.getListId().longValue());

		List<BranchDto> list = new ArrayList<BranchDto>();
		dto.setLsBranch(list);
		assertEquals(list, dto.getLsBranch());

		dto.setLsBranchSelected(list);
		assertEquals(list, dto.getLsBranchSelected());

		TimeDto time = new TimeDto();
		dto.setTimeIndex(time);
		assertEquals(time, dto.getTimeIndex());

	}

}
