/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ContactDataDtoTest {

	@Test
	public void testGettersAndSetters() {
		ContactDataDto cd = new ContactDataDto();
		cd.setEmail("info@mettenmeier.de");
		assertEquals("info@mettenmeier.de", cd.getEmail());

		cd.setId((long) 1);
		assertEquals(1l, cd.getId().longValue());

		cd.setIsPrivate(true);
		assertTrue(cd.getIsPrivate());

		cd.setIsPrivate(false);
		assertFalse(cd.getIsPrivate());

		cd.setCellphone("0123 123123123");
		assertEquals("0123 123123123", cd.getCellphone());

		cd.setPager("01234567890");
		assertEquals("01234567890", cd.getPager());

		cd.setPhone("05233 123 456");
		assertEquals("05233 123 456", cd.getPhone());

		cd.setRadiocomm("01234567890");
		assertEquals("01234567890", cd.getRadiocomm());
	}

}
