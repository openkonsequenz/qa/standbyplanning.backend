/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LocationSmallDtoTest {

	@Test
	public void testGettersAndSetters() {
		Long l = new Long(30);
		String t = "Text";
		
		LocationSmallDto locationSmallDto = new LocationSmallDto();

		locationSmallDto.setId(l);
		assertEquals(l, locationSmallDto.getId());
		
		locationSmallDto.setTitle(t);
		assertEquals(t, locationSmallDto.getTitle());
		
	}
}
