/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanningMsgDtoTest {

	@InjectMocks
	PlanningMsgDto planningMsgDto;

	@Test
	public void getSet() {
		String text = "test";

		planningMsgDto.setCssType(PlanningMsgDto.CSS_DANGER);
		assertEquals(PlanningMsgDto.CSS_DANGER, planningMsgDto.getCssType());

		planningMsgDto.setMsg(text);
		assertEquals(text, planningMsgDto.getMsg());

		planningMsgDto.setType(PlanningMsgDto.INFO);
		assertEquals(PlanningMsgDto.INFO, planningMsgDto.getType());

	}

}
