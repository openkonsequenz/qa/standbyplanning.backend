/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserFunctionSelectionDtoTest {

	@Test
	public void testGettersAndSetters() {

		UserFunctionSelectionDto uf = new UserFunctionSelectionDto();

		uf.setId(1l);
		assertEquals(1l, uf.getId().longValue());

		uf.setFunctionName("Name");
		assertEquals("Name", uf.getFunctionName());
		
		uf.setFunctionId(1l);
		assertEquals(1l, uf.getFunctionId().longValue());


	}
}
