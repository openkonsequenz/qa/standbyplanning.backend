/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.mail;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.sp.db.model.ContactData;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleActionDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelper;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.SpMsg;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class MailRequestTest {

	/** Logger. */
	private Logger logger = Logger.getLogger(MailRequestTest.class);

	@Mock
	FileHelper fileHelper;

	@Mock
	MailGenerator mailGenerator;

//	@Spy
	@InjectMocks
	MailRequest mailRequest;

	@Test
	public void generateHTMLTest() throws Exception {
//
//		Mockito.when(fileHelper.loadFileFromFileSystemOrResource(Mockito.anyString(), Mockito.anyString(), false))
//				.thenReturn(null);

		String result = mailRequest.generateHTML("mail/move_message.html", "", "", "", "", "", "");
		assertNull(result);

	}

	@Test
	public void moveMessage1() throws IOException, SpException {

		HtmlEmail email = new HtmlEmail();

		StandbyScheduleActionDto actionDto = new StandbyScheduleActionDto();
		actionDto.setStatusId(SpMsg.STATUS_CLOSED_PLANNING);
		actionDto.setValidFromDate(new Date());
		actionDto.setValidToDate(new Date());

		StandbyGroup targetGroup = new StandbyGroup();
		targetGroup.setTitle("test");

		StandbyScheduleBody standbyBody = new StandbyScheduleBody();
		standbyBody.setStandbyGroup(targetGroup);
		standbyBody.setValidFrom(new Date());
		standbyBody.setValidTo(new Date());
		User user = new User();
		ContactData businessContactData = new ContactData();
		businessContactData.setEmail("test@test.de");
		user.setBusinessContactData(businessContactData);
		standbyBody.setUser(user);

		Properties property = new Properties();
		property.setProperty(Globals.MAIL_TEMPLATE_DIRECTORY, "mail");
		property.setProperty(Globals.SMTP_ACTIVE, "true");
		property.setProperty(Globals.SMTP_MAIL_SENDER_NAME, "test");
		property.setProperty(Globals.SMTP_MAIL_SENDER_EMAIL, "test@test.de");
		property.setProperty(Globals.SMTP_BACKUP_RECIPIENT, "test@test.de");
		property.setProperty(Globals.LINK_DASHBOARD, "http://test");

		FileHelperTest fh = new FileHelperTest();
		// create test data
		InputStream fis = fh.loadFromResource("mail/move_message.html");

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenReturn(property);
		Mockito.when(mailGenerator.generateMail(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(email);
		Mockito.when(fileHelper.loadFileFromResource(Mockito.anyString())).thenReturn(fis);

		HtmlEmail result = mailRequest.moveMessage1(actionDto, standbyBody, targetGroup);
		assertNotNull(result);

		Mockito.when(mailGenerator.generateMail(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any()))
				.thenThrow(new SpException());
		result = mailRequest.moveMessage1(actionDto, standbyBody, targetGroup);
		assertNull(result);

		property.setProperty(Globals.SMTP_ACTIVE, "false");
		result = mailRequest.moveMessage1(actionDto, standbyBody, targetGroup);
		assertNull(result);

		actionDto.setStatusId(SpMsg.STATUS_PLANNING);
		result = mailRequest.moveMessage1(actionDto, standbyBody, targetGroup);
		assertNull(result);

		actionDto.setStatusId(SpMsg.STATUS_PLANNING);
		property.setProperty(Globals.SMTP_ACTIVE, "false");

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenThrow(new IOException());
		result = mailRequest.moveMessage1(actionDto, null, targetGroup);
		assertNull(result);

	}

	@Test
	public void replaceMessage1() throws IOException, SpException {
		HtmlEmail email = new HtmlEmail();
		User oldUser = new User();
		ContactData businessContactData = new ContactData();
		businessContactData.setEmail("test@test.de");
		oldUser.setBusinessContactData(businessContactData);

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		User newUser = new User();
		newUser.setFirstname("test");
		newUser.setLastname("test");

		StandbyScheduleActionDto actionDto = new StandbyScheduleActionDto();
		actionDto.setStatusId(SpMsg.STATUS_CLOSED_PLANNING);
		actionDto.setValidFromDate(new Date());
		actionDto.setValidToDate(new Date());

		Properties property = new Properties();
		property.setProperty(Globals.MAIL_TEMPLATE_DIRECTORY, "mail");
		property.setProperty(Globals.SMTP_ACTIVE, "true");
		property.setProperty(Globals.SMTP_MAIL_SENDER_NAME, "test");
		property.setProperty(Globals.SMTP_MAIL_SENDER_EMAIL, "test@test.de");
		property.setProperty(Globals.SMTP_BACKUP_RECIPIENT, "test@test.de");
		property.setProperty(Globals.LINK_DASHBOARD, "http://test");

		// create test data
		InputStream fis = fh.loadFromResource("mail/replace_message1.html");

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenReturn(property);
		Mockito.when(mailGenerator.generateMail(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(email);
		Mockito.when(fileHelper.loadFileFromResource(Mockito.anyString())).thenReturn(fis);

		HtmlEmail result = mailRequest.replaceMessage1(actionDto, oldUser, newUser, standbyGroup);
		assertNotNull(result);

		Mockito.when(mailGenerator.generateMail(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any()))
				.thenThrow(new SpException());
		result = mailRequest.replaceMessage1(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);

		property.setProperty(Globals.SMTP_ACTIVE, "false");
		result = mailRequest.replaceMessage1(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);

		actionDto.setStatusId(SpMsg.STATUS_PLANNING);
		result = mailRequest.replaceMessage1(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenThrow(new IOException());
		result = mailRequest.replaceMessage1(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);
	}

	@Test
	public void replaceMessage2() throws IOException, SpException {
		HtmlEmail email = new HtmlEmail();
		User newUser = new User();
		ContactData businessContactData = new ContactData();
		businessContactData.setEmail("test@test.de");
		newUser.setBusinessContactData(businessContactData);

		User oldUser = new User();
		oldUser.setFirstname("test");
		oldUser.setLastname("test");

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);

		StandbyScheduleActionDto actionDto = new StandbyScheduleActionDto();
		actionDto.setStatusId(SpMsg.STATUS_CLOSED_PLANNING);
		actionDto.setValidFromDate(new Date());
		actionDto.setValidToDate(new Date());

		Properties property = new Properties();
		property.setProperty(Globals.MAIL_TEMPLATE_DIRECTORY, "mail");
		property.setProperty(Globals.SMTP_ACTIVE, "true");
		property.setProperty(Globals.SMTP_MAIL_SENDER_NAME, "test");
		property.setProperty(Globals.SMTP_MAIL_SENDER_EMAIL, "test@test.de");
		property.setProperty(Globals.SMTP_BACKUP_RECIPIENT, "test@test.de");
		property.setProperty(Globals.LINK_DASHBOARD, "http://test");

		// create test data
		InputStream fis = fh.loadFromResource("mail/replace_message2.html");

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenReturn(property);
		Mockito.when(mailGenerator.generateMail(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(email);
		Mockito.when(fileHelper.loadFileFromResource(Mockito.anyString())).thenReturn(fis);

		HtmlEmail result = mailRequest.replaceMessage2(actionDto, oldUser, newUser, standbyGroup);
		assertNotNull(result);

		Mockito.when(mailGenerator.generateMail(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any()))
				.thenThrow(new SpException());
		result = mailRequest.replaceMessage2(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);

		property.setProperty(Globals.SMTP_ACTIVE, "false");
		result = mailRequest.replaceMessage2(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);

		actionDto.setStatusId(SpMsg.STATUS_PLANNING);
		result = mailRequest.replaceMessage2(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenThrow(new IOException());
		result = mailRequest.replaceMessage2(actionDto, oldUser, newUser, standbyGroup);
		assertNull(result);
	}

}
