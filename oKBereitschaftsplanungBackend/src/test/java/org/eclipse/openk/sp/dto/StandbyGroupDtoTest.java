/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.junit.Test;

public class StandbyGroupDtoTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		Boolean b = new Boolean(true);
		StandbyGroupDto group = new StandbyGroupDto();
		group.setId(1l);
		assertEquals(1l, group.getId().longValue());

		group.setModificationDate(d);
		assertEquals(d, group.getModificationDate());

		group.setTitle("Title");
		assertEquals("Title", group.getTitle());

		group.setNote("Test Note");
		assertEquals("Test Note", group.getNote());

		group.setNextUserInNextCycle(b);
		assertEquals(b, group.getNextUserInNextCycle());

		group.setExtendStandbyTime(b);
		assertEquals(b, group.getExtendStandbyTime());

		ArrayList<StandbyDurationDto> lsDurations = new ArrayList<>();
		StandbyDurationDto dur = new StandbyDurationDto();
		lsDurations.add(dur);
		group.setLsStandbyDurations(lsDurations);
		assertNotNull(group.getLsStandbyDurations());
		assertEquals(dur, group.getLsStandbyDurations().get(0));

		ArrayList<BranchDto> lsBranches = new ArrayList<>();
		BranchDto dtoBranch = new BranchDto();
		lsBranches.add(dtoBranch);

		group.setLsBranches(lsBranches);
		assertNotNull(group.getLsBranches());
		assertEquals(lsBranches, group.getLsBranches());

		ArrayList<UserInStandbyGroupDto> lsUserInStandbyGroups = new ArrayList<>();
		UserInStandbyGroupDto uisg = new UserInStandbyGroupDto();
		lsUserInStandbyGroups.add(uisg);
		group.setLsUserInStandbyGroups(lsUserInStandbyGroups);
		assertNotNull(group.getLsUserInStandbyGroups());
		assertEquals(uisg, group.getLsUserInStandbyGroups().get(0));

		group.setLsUserFunction(new ArrayList<UserFunctionDto>());
		assertNotNull(group.getLsUserFunction());
		
		group.setLsRegions(new ArrayList<RegionSelectionDto>());
		assertNotNull(group.getLsRegions());
		
		group.setLsIgnoredCalendarDays(new ArrayList<CalendarDayDto>());
		assertNotNull(group.getLsIgnoredCalendarDays());
		

	}

	@Test
	public void testCompare() throws MalformedURLException {
		StandbyGroup obj1 = new StandbyGroup();
		obj1.setId(5L);

		StandbyGroup obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyGroup obj3 = new StandbyGroup();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

}
