/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BranchRestServiceTest {
	
	@InjectMocks
	private BranchRestService branchRestService;
	
	@Test
	public void constructorTest() {
		BranchRestService brs = new BranchRestService();
		assertNotNull(brs);
	}
	
	@Test
	public void getLocationsSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = branchRestService.getLocationsSelection(jwt);
		assertNotNull(result);
	}

}
