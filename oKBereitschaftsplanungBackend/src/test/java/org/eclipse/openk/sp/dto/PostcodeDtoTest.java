/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PostcodeDtoTest {

	@Test
	public void testGettersAndSetters() {

		PostcodeDto l = new PostcodeDto("151435");

		l.setId(1l);
		assertEquals(1l, l.getId().longValue());

		l.setPcode("33100");
		assertEquals("33100", l.getPcode());

	}
}
