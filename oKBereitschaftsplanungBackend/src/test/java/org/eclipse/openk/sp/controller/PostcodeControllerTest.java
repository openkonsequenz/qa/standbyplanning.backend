/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.PostcodeRepository;
import org.eclipse.openk.sp.db.model.Postcode;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class PostcodeControllerTest {
	protected static final Logger logger = Logger.getLogger(UserController.class);

	@Mock
	PostcodeRepository postcodeRepository;
	@Mock
	EntityConverter entityConverter;

	@InjectMocks
	PostcodeController postcodeController;

	@Test
	public void saveErrorTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testPostcode.json");
		PostcodeDto postcodeDto = (PostcodeDto) new Gson().fromJson(json, PostcodeDto.class);

		Postcode postcode = (Postcode) new Gson().fromJson(json, Postcode.class);

		Throwable throwableClasses = new Exception("Error on save!");

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) postcode);
		when(postcodeRepository.findOne(postcode.getId())).thenReturn(postcode);

		// run test

		PostcodeDto postcodeResult = postcodeController.savePostcode(postcodeDto);
		assertNull(postcodeResult);
		// assertEquals(1l, postcodeResult.getId().longValue());
	}

	@Test
	public void saveTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testPostcode.json");
		PostcodeDto postcodeDto = (PostcodeDto) new Gson().fromJson(json, PostcodeDto.class);

		Postcode postcode = (Postcode) new Gson().fromJson(json, Postcode.class);

		Throwable throwableClasses = new Exception("Error on save!");

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) postcode);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) postcodeDto);
		when(postcodeRepository.save(postcode)).thenReturn(postcode);

		// run test
		PostcodeDto postcodeResult = null;

		postcodeResult = postcodeController.savePostcode(postcodeDto);
		assertNotNull(postcodeResult);
		assertEquals(30l, postcodeResult.getId().longValue());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAllTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testPostcodeList.json");
		List<PostcodeDto> lsPostcodeDtoMock = (List<PostcodeDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_POSTCODE_DTO_ARRAY_LIST);
		List<Postcode> lsPostcodeMock = (List<Postcode>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_POSTCODE_ARRAY_LIST);

		PostcodeDto postcodeDto = lsPostcodeDtoMock.get(1);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) postcodeDto);
		when(postcodeRepository.findAllByOrderByPcodeAsc()).thenReturn(lsPostcodeMock);

		// run test
		List<PostcodeDto> lsPostcodeDto = postcodeController.getPostcodes();

		assertNotNull(lsPostcodeDto);
		assertTrue(lsPostcodeDto.size() > 0);
		assertTrue(lsPostcodeDto.get(0) instanceof PostcodeDto);
	}

	@Test
	public void getPostcode() {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testPostcode.json");
		PostcodeDto postcodeDto = (PostcodeDto) new Gson().fromJson(json, PostcodeDto.class);

		Postcode postcode = (Postcode) new Gson().fromJson(json, Postcode.class);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) postcode);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) postcodeDto);

		when(postcodeRepository.exists(any())).thenReturn(true);
		when(postcodeRepository.findOne(any())).thenReturn(postcode);

		// run test

		PostcodeDto postcodeResult = postcodeController.getPostcode(id);
		assertNotNull(postcodeResult);
		assertEquals(id, postcodeResult.getId());
	}

	@Test
	public void getSavedPostcodeTest() {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testPostcode.json");
		Postcode postcode = (Postcode) new Gson().fromJson(json, Postcode.class);

		// create when

		when(postcodeRepository.findOne(Mockito.anyLong())).thenReturn(postcode);

		// run test

		Postcode postcodeResult = postcodeController.getSavedPostcode(6L);
		assertNotNull(postcodeResult);
		assertEquals(id, postcodeResult.getId());
	}

}
