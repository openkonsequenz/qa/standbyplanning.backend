/********************************************************************************
 * Copyright (c) 2023 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.pseudonymization;



import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.db.model.ArchiveStandbyScheduleHeader;
import org.eclipse.openk.sp.db.model.ContactData;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.hist.AddressHist;
import org.eclipse.openk.sp.db.model.hist.ContactDataHist;
import org.eclipse.openk.sp.db.model.hist.UserHist;
import org.eclipse.openk.sp.dto.StandbyScheduleArchiveDto;
import org.eclipse.openk.sp.util.FileHelperTest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



/**
 * Assistive class that provides some fundamental functionality used by various pseudonymization tests.
 */
public class PseudonymizationTestSupport
{

	protected static final Logger LOGGER = Logger.getLogger(PseudonymizationTestSupport.class);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static final long USER__ID = 5;
	public static final String USER__FIRST_NAME = "Manfred";
	public static final String USER__LAST_NAME = "Mustermann3";
	public static final String USER__KEY = "mustermann";
	public static final String USER__HR_NUMBER = "a123";

	public static final long USER_2__ID = 15;
	public static final String USER_2__FIRST_NAME = "Michael";
	public static final String USER_2__LAST_NAME = "DA - Schmidt";
	public static final String USER_2__KEY = "N";
	public static final String USER_2__HR_NUMBER = "a123";

	public static final long PRIVATE_ADDR__HIST_ID = 12345L;
	public static final long PRIVATE_ADDR__ID = 12;
	public static final String PRIVATE_ADDR__POSTCODE = "12345";
	public static final String PRIVATE_ADDR__STREET = "Musterstraße";
	public static final String PRIVATE_ADDR__HOUSE_NUMBER = "123a";
	public static final String PRIVATE_ADDR__LATITUDE = "12.345";
	public static final String PRIVATE_ADDR__LONGITUDE = "234b";
	public static final String PRIVATE_ADDR__URL_MAP = "http://foo.bar";

	public static final long PRIVATE_CONTACT__HIST_ID = 12346L;
	public static final long PRIVATE_CONTACT__ID = 6;
	public static final String PRIVATE_CONTACT__PHONE_NUMBER = "0123/456";
	public static final String PRIVATE_CONTACT__CELL_PHONE_NUMBER = "0123/45678";
	public static final String PRIVATE_CONTACT__RADIO_COMM = "foo";
	public static final String PRIVATE_CONTACT__EMAIL = "manfred.mustermann3@example.org";
	public static final String PRIVATE_CONTACT__PAGER = "01234/5678";

	public static final long STANDBY_SCHEDULE_BODY__ID = 12347L;

	public static final long STANDBY_STATUS__ID = 14;
	public static final String STANDBY_STATUS__TITLE = "Ist-Ebene";

	public static final long STANDBY_GROUP__ID = 15;
	public static final String STANDBY_GROUP__TITLE = "Foo bar";

	public static final long ARCHIVE_STANDBY_SCHEDULE_HEADER__ID = 91L;
	public static final String ARCHIVE_STANDBY_SCHEDULE_HEADER__MODIFIED_CAUSE = "104: Bereitschaft (-ten) ersetzt Erika Musterfrau durch Manfred Mustermann3";
	public static final String ARCHIVE_STANDBY_SCHEDULE_HEADER__MODIFIED_PSEUDONYMIZED = "104: Bereitschaft (-ten) ersetzt Erika Musterfrau durch anonymous5 anonymous5";
	public static final String ARCHIVE_STANDBY_SCHEDULE_HEADER__TITLE = "104: Bereitschaft (-ten) ersetzt Manfred Mustermann3 durch Erika Musterfrau";
	public static final String ARCHIVE_STANDBY_SCHEDULE_HEADER__TITLE_PSEUDONYMIZED = "104: Bereitschaft (-ten) ersetzt anonymous5 anonymous5 durch Erika Musterfrau";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor.
	 */
	private PseudonymizationTestSupport()
	{
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static ContactData createPrivateContact()
	{
		ContactData somePrivateContactData = new ContactData();
		somePrivateContactData.setId(PRIVATE_CONTACT__ID);
		somePrivateContactData.setIsPrivate(true);
		somePrivateContactData.setPhone(PRIVATE_CONTACT__PHONE_NUMBER);
		somePrivateContactData.setCellphone(PRIVATE_CONTACT__CELL_PHONE_NUMBER);
		somePrivateContactData.setRadiocomm(PRIVATE_CONTACT__RADIO_COMM);
		somePrivateContactData.setEmail(PRIVATE_CONTACT__EMAIL);
		somePrivateContactData.setPager(PRIVATE_CONTACT__PAGER);
		return somePrivateContactData;
	}

	public static ContactDataHist createPrivateContactHist()
	{
		ContactDataHist ret = new ContactDataHist();
		ret.setHistId(PRIVATE_CONTACT__HIST_ID);
		ret.setOperation("I");
		ret.setId(PRIVATE_CONTACT__ID);
		ret.setIsPrivate(true);
		ret.setPhone(PRIVATE_CONTACT__PHONE_NUMBER);
		ret.setCellphone(PRIVATE_CONTACT__CELL_PHONE_NUMBER);
		ret.setRadiocomm(PRIVATE_CONTACT__RADIO_COMM);
		ret.setEmail(PRIVATE_CONTACT__EMAIL);
		ret.setPager(PRIVATE_CONTACT__PAGER);
		return ret;
	}

	public static User createUser(ContactData privateContactData)
	{
		User someUser = new User();
		someUser.setId(USER__ID);
		someUser.setFirstname(USER__FIRST_NAME);
		someUser.setLastname(USER__LAST_NAME);
		someUser.setIsCompany(false);
		someUser.setPrivateContactData(privateContactData);
		someUser.setUserKey(USER__KEY);
		return someUser;
	}

	public static User createUser2(ContactData privateContactData)
	{
		User someUser = new User();
		someUser.setId(USER_2__ID);
		someUser.setFirstname(USER_2__FIRST_NAME);
		someUser.setLastname(USER_2__LAST_NAME);
		someUser.setIsCompany(false);
		someUser.setPrivateContactData(privateContactData);
		someUser.setUserKey(USER_2__KEY);
		return someUser;
	}

	public static AddressHist createPrivateAddressHist()
	{
		AddressHist ret = new AddressHist();
		ret.setHistId(PRIVATE_ADDR__HIST_ID);
		ret.setOperation("I");
		ret.setStamp(new Date());
		ret.setId(PRIVATE_ADDR__ID);
		ret.setPostcode(PRIVATE_ADDR__POSTCODE);
		ret.setCommunity(null);
		ret.setCommunitySuffix(null);
		ret.setStreet(PRIVATE_ADDR__STREET);
		ret.setHousenumber(PRIVATE_ADDR__HOUSE_NUMBER);
		ret.setWgs84zone(null);
		ret.setLatitude(PRIVATE_ADDR__LATITUDE);
		ret.setLongitude(PRIVATE_ADDR__LONGITUDE);
		ret.setUrlMap(PRIVATE_ADDR__URL_MAP);
		return ret;
	}

	public static UserHist createUserHist(AddressHist somePrivateAddressHist, ContactDataHist somePrivateContactHist)
	{
		UserHist ret = new UserHist();
		ret.setHistId(12345L);
		ret.setOperation("I");
		ret.setStamp(new Date());
		ret.setId(USER__ID);
		ret.setFirstname(USER__FIRST_NAME);
		ret.setIsCompany(false);
		ret.setLastname(USER__LAST_NAME);
		ret.setModificationDate(new Date());
		ret.setNotes("Foo bar baz");
		ret.setValidFrom(new Date());
		ret.setValidTo(new Date());
		ret.setUserKey(USER__KEY);
		ret.setHrNumber(USER__HR_NUMBER);
		ret.setPrivateAddressId(somePrivateAddressHist.getHistId());
		ret.setPrivateContactDataId(somePrivateContactHist.getHistId());

		// String s = new Gson().toJson(someUserHist);
		// LOGGER.info(s);

		return ret;
	}

	public static StandbyStatus createStandbyStatus()
	{
		StandbyStatus ret = new StandbyStatus();
		ret.setId(STANDBY_STATUS__ID);
		ret.setTitle(STANDBY_STATUS__TITLE);
		return ret;
	}

	/*
	public static StandbyGroup createStandbyGroup()
	{
		StandbyGroup ret = new StandbyGroup();
		ret.setId(STANDBY_GROUP__ID);
		ret.setTitle(STANDBY_GROUP__TITLE);
		return ret;
	}
	*/

	public static StandbyScheduleBody createStandbyScheduleBody(User someUser, StandbyStatus someStandbyStatus, StandbyGroup someStandbyGroup)
	{
		StandbyScheduleBody ret = new StandbyScheduleBody();
		ret.setId(STANDBY_SCHEDULE_BODY__ID);
		ret.setValidFrom(new Date());
		ret.setValidTo(new Date());
		ret.setModificationDate(new Date());
		ret.setUser(someUser);
		ret.setModifiedBy(USER__KEY);
		ret.setStatus(someStandbyStatus);
		ret.setStandbyGroup(someStandbyGroup);
		ret.setModifiedCause(null);
		return ret;
	}

	public static ArchiveStandbyScheduleHeader createArchiveStandbyScheduleHeader() throws IOException
	{
		FileHelperTest fh = new FileHelperTest();
		String sJson = fh.loadStringFromResource("pseudonymization/ARCHIVE_STANDBY_SCHEDULE_HEADER__91__org.json");

		ArchiveStandbyScheduleHeader ret = new ArchiveStandbyScheduleHeader();
		ret.setId(ARCHIVE_STANDBY_SCHEDULE_HEADER__ID);
		ret.setComment("  Gruppen: group2, ");
		ret.setJsonPlan(sJson);
		ret.setModificationDate(new Date());
		ret.setModifiedBy(USER__KEY);
		ret.setModifiedCause(ARCHIVE_STANDBY_SCHEDULE_HEADER__MODIFIED_CAUSE);
		ret.setStatusId(2L);
		ret.setStatusName("Ist-Ebene");
		ret.setTitle(ARCHIVE_STANDBY_SCHEDULE_HEADER__TITLE);
		ret.setValidFrom(new Date());
		ret.setValidTo(new Date());
		return ret;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static String objToJsonPretty(Object jsonSerializableObj)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(jsonSerializableObj);
	}

	public static <T> T objFromJson(Class<T> clazz, String jsonData)
	{
		return (T)new Gson().fromJson(jsonData, clazz);
	}

	public static void dump(Object jsonSerializableObj)
	{
		String s = objToJsonPretty(jsonSerializableObj);
		LOGGER.info("\nClass: " + jsonSerializableObj.getClass().getName() + "\n" + s);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void assertIsModified(StandbyScheduleBody someBody)
	{
		assertTrue(someBody.getId() == STANDBY_SCHEDULE_BODY__ID);
		assertNotEquals(someBody.getModifiedBy(), USER__KEY);
	}

	public static void assertIsModified(User someUser)
	{
		assertTrue(someUser.getId() == USER__ID);
		assertNotEquals(someUser.getFirstname(), USER__FIRST_NAME);
		assertNotEquals(someUser.getLastname(), USER__LAST_NAME);
	}

	public static void assertIsModified(UserHist someUserHist)
	{
		assertTrue(someUserHist.getId() == USER__ID);
		assertNotEquals(someUserHist.getFirstname(), USER__FIRST_NAME);
		assertNotEquals(someUserHist.getLastname(), USER__LAST_NAME);
		assertNotEquals(someUserHist.getHrNumber(), USER__HR_NUMBER);
		assertNotEquals(someUserHist.getUserKey(), USER__KEY);
	}

	public static void assertIsModified(AddressHist somePrivateAddressHist)
	{
		assertTrue(somePrivateAddressHist.getHistId() == PRIVATE_ADDR__HIST_ID);
		assertTrue(somePrivateAddressHist.getId() == PRIVATE_ADDR__ID);
		assertNotEquals(somePrivateAddressHist.getPostcode(), PRIVATE_ADDR__POSTCODE);
		assertNotEquals(somePrivateAddressHist.getStreet(), PRIVATE_ADDR__STREET);
		assertNotEquals(somePrivateAddressHist.getHousenumber(), PRIVATE_ADDR__HOUSE_NUMBER);
		assertNotEquals(somePrivateAddressHist.getLatitude(), PRIVATE_ADDR__LATITUDE);
		assertNotEquals(somePrivateAddressHist.getLongitude(), PRIVATE_ADDR__LONGITUDE);
	}

	public static void assertIsModified(ContactData somePrivateContact)
	{
		assertTrue(somePrivateContact.getId() == PRIVATE_CONTACT__ID);
		assertNotEquals(somePrivateContact.getPhone(), PRIVATE_CONTACT__PHONE_NUMBER);
		assertNotEquals(somePrivateContact.getCellphone(), PRIVATE_CONTACT__CELL_PHONE_NUMBER);
		assertNotEquals(somePrivateContact.getRadiocomm(), PRIVATE_CONTACT__RADIO_COMM);
		assertNotEquals(somePrivateContact.getEmail(), PRIVATE_CONTACT__EMAIL);
		assertNotEquals(somePrivateContact.getPager(), PRIVATE_CONTACT__PAGER);
	}

	public static void assertIsModified(ContactDataHist somePrivateContactHist)
	{
		assertTrue(somePrivateContactHist.getHistId() == PRIVATE_CONTACT__HIST_ID);
		assertTrue(somePrivateContactHist.getId() == PRIVATE_CONTACT__ID);
		assertNotEquals(somePrivateContactHist.getPhone(), PRIVATE_CONTACT__PHONE_NUMBER);
		assertNotEquals(somePrivateContactHist.getCellphone(), PRIVATE_CONTACT__CELL_PHONE_NUMBER);
		assertNotEquals(somePrivateContactHist.getRadiocomm(), PRIVATE_CONTACT__RADIO_COMM);
		assertNotEquals(somePrivateContactHist.getEmail(), PRIVATE_CONTACT__EMAIL);
		assertNotEquals(somePrivateContactHist.getPager(), PRIVATE_CONTACT__PAGER);
	}

	public static void assertIsModified_user_5(ArchiveStandbyScheduleHeader someArchiveStandbyScheduleHeader)
	{
		assertTrue(someArchiveStandbyScheduleHeader.getId() == ARCHIVE_STANDBY_SCHEDULE_HEADER__ID);
		assertNotEquals(someArchiveStandbyScheduleHeader.getModifiedCause(), ARCHIVE_STANDBY_SCHEDULE_HEADER__MODIFIED_CAUSE);
		assertEquals(someArchiveStandbyScheduleHeader.getModifiedCause(), ARCHIVE_STANDBY_SCHEDULE_HEADER__MODIFIED_PSEUDONYMIZED);
		assertNotEquals(someArchiveStandbyScheduleHeader.getTitle(), ARCHIVE_STANDBY_SCHEDULE_HEADER__TITLE);
		assertEquals(someArchiveStandbyScheduleHeader.getTitle(), ARCHIVE_STANDBY_SCHEDULE_HEADER__TITLE_PSEUDONYMIZED);

		FileHelperTest fh = new FileHelperTest();
		String sJsonPseudonymized = fh.loadStringFromResource("pseudonymization/ARCHIVE_STANDBY_SCHEDULE_HEADER__91__pseudonymized.json");

		List<String> errMsgs = GsonHelper.compare(
			GsonHelper.parseString(someArchiveStandbyScheduleHeader.getJsonPlan(), StandbyScheduleArchiveDto.class),
			GsonHelper.parseString(sJsonPseudonymized, StandbyScheduleArchiveDto.class)
		);

		if (errMsgs != null) {
			int n = 0;
			for (String s: errMsgs) {
				n += 1;
				if (n > 20) {
					LOGGER.error("JSON COMPARISON ERROR: ...");
					break;
				}
				LOGGER.error("JSON COMPARISON ERROR: " + s);
			}
			fail();
		}
	}

}




