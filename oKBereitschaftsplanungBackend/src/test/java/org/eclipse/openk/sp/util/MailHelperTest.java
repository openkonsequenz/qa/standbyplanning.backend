/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class MailHelperTest {

	@InjectMocks
	private MailHelper mailHelper;

	@Mock
	private FileHelper fileHelper;

	@Before
	public void init() throws IOException {
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.from", "mail@test.tld");
		properties.setProperty("mail.imap.user", "imap-user");
		properties.setProperty("mail.imap.password", "imap-password");
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		File file = new File(classLoader.getResource("mail.properties").getPath());
		Mockito.when(fileHelper.loadFileFromFileSystemOrResource(Mockito.eq("mail.properties"))).thenReturn(file );
		ReflectionTestUtils.setField(mailHelper, "propertiesPath", "mail.properties");
		ReflectionTestUtils.invokeMethod(mailHelper, "init");
	}

	@Test
	public void testNewMultipartMail() throws MessagingException, IOException {
		MimeMessage msg = mailHelper.newMultipartMail();
		assertNotNull(msg);
		assertTrue(msg.getContent() instanceof MimeMultipart);
	}

	@Test
	public void testSetToRecipients() throws MessagingException {
		MimeMessage msg = mailHelper.newMultipartMail();

		List<String> to = new ArrayList<>();
		to.add("test1@test.tld");
		to.add("test2@test.tld");
		MailHelper.setToRecipients(msg, to);
		assertEquals(2, msg.getRecipients(RecipientType.TO).length);
	}

	@Test
	public void testSetSubject() throws MessagingException {
		MimeMessage msg = mailHelper.newMultipartMail();
		String subject = "subject";
		MailHelper.setSubject(msg, subject);
		assertEquals(subject, msg.getSubject());
	}

	@Test
	public void testAddText() throws IOException, MessagingException {
		MimeMessage msg = mailHelper.newMultipartMail();
		String text = "test text";
		MailHelper.addText(msg, text);

		MimeMultipart mult = (MimeMultipart) msg.getContent();
		BodyPart part = mult.getBodyPart(0);
		assertEquals("text/plain", part.getContentType());
		assertEquals(text, part.getContent());
	}
	
	@Test
	public void testAddTextTestNoMultipart() throws IOException, MessagingException {
		MimeMessage msg = Mockito.mock(MimeMessage.class);
		Mockito.when(msg.getContent()).thenReturn("");
		String text = "test text";
		try {
			MailHelper.addText(msg, text);
			fail("Should have thrown MessagingException");
		} catch (MessagingException e) {
			//	pass
		}
	}

	@Test
	public void testAddAttachment() throws MessagingException, IOException {
		MimeMessage msg = mailHelper.newMultipartMail();
		String fileName = "fileName.pdf";
		File reportFile = Mockito.mock(File.class);
		MailHelper.addAttachment(msg, reportFile, fileName);
		MimeMultipart mult = (MimeMultipart) msg.getContent();
		BodyPart part = mult.getBodyPart(0);
		assertNotNull(part);
	}

	@Test
	public void testAddAttachmentTestNoMultipart() throws IOException, MessagingException {
		MimeMessage msg = Mockito.mock(MimeMessage.class);
		Mockito.when(msg.getContent()).thenReturn("");

		String fileName = "fileName.pdf";
		File reportFile = Mockito.mock(File.class);
		try {
			MailHelper.addAttachment(msg, reportFile, fileName);
			fail("Should have thrown MessagingException");
		} catch (MessagingException e) {
			//	pass
		}
	}

	

}
