/********************************************************************************
 * Copyright (c) 2023 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.pseudonymization;



import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.impl.pseudonymization.NewPseudonymizationData;
import org.eclipse.openk.sp.controller.impl.pseudonymization.PseudonymizationCtx;
import org.eclipse.openk.sp.controller.impl.pseudonymization.PseudonymizationHelperController;
import org.eclipse.openk.sp.controller.impl.pseudonymization.PseudonymizeArchiveHelperController;
import org.eclipse.openk.sp.db.dao.ArchiveRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleHistoryRepository;
import org.eclipse.openk.sp.db.dao.hist.AddressHistRepository;
import org.eclipse.openk.sp.db.dao.hist.ContactDataHistRepository;
import org.eclipse.openk.sp.db.dao.hist.StandbyScheduleBodyHistRepository;
import org.eclipse.openk.sp.db.dao.hist.StandbyScheduleHistoryHistRepository;
import org.eclipse.openk.sp.db.dao.hist.UserHistRepository;
import org.eclipse.openk.sp.db.model.ArchiveStandbyScheduleHeader;
import org.eclipse.openk.sp.db.model.ContactData;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.hist.AddressHist;
import org.eclipse.openk.sp.db.model.hist.ContactDataHist;
import org.eclipse.openk.sp.db.model.hist.UserHist;
import org.eclipse.openk.sp.exceptions.SpException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.io.Files;



@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PseudonymizationHelperTest
{

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected static final Logger LOGGER = Logger.getLogger(PseudonymizationHelperTest.class);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// @Mock
	// private UserRepository userRepository;

	// @Mock
	// private ArchiveRepository archiveRepository;

	//@Mock
	//private PseudonymizeArchiveHelperController pseudonymizeArchiveHelper;

	@Mock
	private UserHistRepository userHistRepository;
	@Mock
	private AddressHistRepository addressHistRepository;
	@Mock
	private ContactDataHistRepository contactDataHistRepository;

	@Mock
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Mock
	private StandbyScheduleBodyHistRepository standbyScheduleBodyHistRepository;

	@Mock
	private StandbyScheduleHistoryRepository standbyScheduleHistoryRepository;

	@Mock
	private StandbyScheduleHistoryHistRepository standbyScheduleHistoryHistRepository;

	@Mock
	private ArchiveRepository archiveStandbyScheduleHeaderRepository;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@InjectMocks
	PseudonymizationHelperController pseudonymizationHelper;

	@InjectMocks
	PseudonymizeArchiveHelperController pseudonymizationArchiveHelper;

	// @Mock
	// private PseudonymizationHelperController pseudonymizationHelper;

	// @InjectMocks
	// PseudonymizationController pseudonymizationController;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor.
	 */
	public PseudonymizationHelperTest()
	{
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static void __saveFile(Object createArchiveStandbyScheduleHeader)
	{

		String sNewJson = GsonHelper.toPrettyJson(createArchiveStandbyScheduleHeader);
		try {
			Files.write(sNewJson, new File("D:\\Temp\\Temp4\\out.json"), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Before
	public void init()
	{
		// MockitoAnnotations.initMocks(this);
		/*
		p = new Properties();
		p.setProperty("placeholder.standby.user.ids", "1,2,3,4");
		*/
	}

	@Test
	public void test_1_modify_user() throws SpException
	{
		// prepare data

		ContactData somePrivateContactData = PseudonymizationTestSupport.createPrivateContact();
		User someUser = PseudonymizationTestSupport.createUser(somePrivateContactData);

		// ----------------------------------------------------------------
		// invoke method to be tested

		final NewPseudonymizationData pd = new NewPseudonymizationData(PseudonymizationTestSupport.USER__ID);
		final PseudonymizationCtx ctx = new PseudonymizationCtx(false, someUser, pd);

		pseudonymizationHelper.modifyUser(ctx, someUser);

		// ----------------------------------------------------------------
		// write debug output

		LOGGER.info("@@>> ================================================================================================================================");
		for (String s: ctx.result.getLogMsgs()) {
			LOGGER.info("@@>> " + s);
		}
		LOGGER.info("@@>> ================================================================================================================================");

		PseudonymizationTestSupport.dump(someUser);

		// ----------------------------------------------------------------
		// verify result

		PseudonymizationTestSupport.assertIsModified(someUser);
		PseudonymizationTestSupport.assertIsModified(someUser.getPrivateContactData());
	}

	@Test
	public void test_2_modify_userHist() throws SpException
	{
		// prepare data

		ContactData somePrivateContact = PseudonymizationTestSupport.createPrivateContact();
		User someUser = PseudonymizationTestSupport.createUser(somePrivateContact);
		AddressHist somePrivateAddrHist = PseudonymizationTestSupport.createPrivateAddressHist();
		ContactDataHist somePrivateContactHist = PseudonymizationTestSupport.createPrivateContactHist();
		UserHist someUserHist = PseudonymizationTestSupport.createUserHist(somePrivateAddrHist, somePrivateContactHist);

		// ----------------------------------------------------------------
		// mock DB access

		Mockito.when(userHistRepository.findById(PseudonymizationTestSupport.USER__ID)).thenReturn(ListHelper.of(someUserHist));
		Mockito.when(addressHistRepository.findById(PseudonymizationTestSupport.PRIVATE_ADDR__HIST_ID)).thenReturn(ListHelper.of(somePrivateAddrHist));
		Mockito.when(contactDataHistRepository.findById(PseudonymizationTestSupport.PRIVATE_CONTACT__HIST_ID)).thenReturn(ListHelper.of(somePrivateContactHist));

		// ----------------------------------------------------------------
		// invoke method to be tested

		final NewPseudonymizationData pd = new NewPseudonymizationData(PseudonymizationTestSupport.USER__ID);
		final PseudonymizationCtx ctx = new PseudonymizationCtx(false, someUser, pd);

		pseudonymizationHelper.modifyUserHistByUserId(ctx, PseudonymizationTestSupport.USER__ID);

		// ----------------------------------------------------------------
		// write debug output

		LOGGER.info("@@>> ================================================================================================================================");
		for (String s: ctx.result.getLogMsgs()) {
			LOGGER.info("@@>> " + s);
		}
		LOGGER.info("@@>> ================================================================================================================================");

		PseudonymizationTestSupport.dump(someUserHist);
		PseudonymizationTestSupport.dump(somePrivateAddrHist);
		PseudonymizationTestSupport.dump(somePrivateContactHist);

		// ----------------------------------------------------------------
		// verify result

		PseudonymizationTestSupport.assertIsModified(someUserHist);
		PseudonymizationTestSupport.assertIsModified(somePrivateAddrHist);
		PseudonymizationTestSupport.assertIsModified(somePrivateContactHist);

		Mockito.verify(userHistRepository).findById(PseudonymizationTestSupport.USER__ID);
		Mockito.verify(addressHistRepository).findById(PseudonymizationTestSupport.PRIVATE_ADDR__HIST_ID);
		Mockito.verify(contactDataHistRepository).findById(PseudonymizationTestSupport.PRIVATE_CONTACT__HIST_ID);
		Mockito.verifyNoMoreInteractions(userHistRepository);
		Mockito.verifyNoMoreInteractions(addressHistRepository);
		Mockito.verifyNoMoreInteractions(contactDataHistRepository);
	}

	@Test
	public void test_3_x_replaceFirstnameLastnameInTextN() throws SpException
	{
		// prepare data

		ContactData somePrivateContactData = PseudonymizationTestSupport.createPrivateContact();
		User someUser = PseudonymizationTestSupport.createUser2(somePrivateContactData);
		// StandbyStatus someStandbyStatus = PseudonymizationTestSupport.createStandbyStatus();
		// StandbyScheduleBody someStandbyScheduleBody = PseudonymizationTestSupport.createStandbyScheduleBody(someUser, someStandbyStatus, null);

		// ----------------------------------------------------------------
		// mock DB access

		// Mockito.when(standbyScheduleBodyRepository.findAll()).thenReturn(ListHelper.of(someStandbyScheduleBody));

		// ----------------------------------------------------------------
		// invoke method to be tested

		final NewPseudonymizationData pd = new NewPseudonymizationData(PseudonymizationTestSupport.USER_2__ID);
		final PseudonymizationCtx ctx = new PseudonymizationCtx(false, someUser, pd);

		String inputText = "104: Bereitschaft (-ten) ersetzt Max Mustermann durch Michael DA - Schmidt";
		PseudonymizeArchiveHelperController.ModifiedFlag bModified = new PseudonymizeArchiveHelperController.ModifiedFlag();
		String resultText = PseudonymizeArchiveHelperController.x_replaceFirstnameLastnameInTextN(ctx, inputText, "log-foo-bar", bModified);
		String expectedText = "104: Bereitschaft (-ten) ersetzt Max Mustermann durch anonymous15 anonymous15";

		// ----------------------------------------------------------------
		// write debug output

		LOGGER.info("@@>> ================================================================================================================================");
		for (String s: ctx.result.getLogMsgs()) {
			LOGGER.info("@@>> " + s);
		}
		LOGGER.info("@@>> ================================================================================================================================");

		// PseudonymizationTestSupport.dump(standbyScheduleBodyRepository);

		// ----------------------------------------------------------------
		// verify result

		assertEquals(resultText, expectedText);
	}


	@Test
	public void test_4_modifyAll_standbyScheduleBody() throws SpException
	{
		// prepare data

		ContactData somePrivateContactData = PseudonymizationTestSupport.createPrivateContact();
		User someUser = PseudonymizationTestSupport.createUser(somePrivateContactData);
		StandbyStatus someStandbyStatus = PseudonymizationTestSupport.createStandbyStatus();
		StandbyScheduleBody someStandbyScheduleBody = PseudonymizationTestSupport.createStandbyScheduleBody(someUser, someStandbyStatus, null);

		// ----------------------------------------------------------------
		// mock DB access

		Mockito.when(standbyScheduleBodyRepository.findAll()).thenReturn(ListHelper.of(someStandbyScheduleBody));

		// ----------------------------------------------------------------
		// invoke method to be tested

		final NewPseudonymizationData pd = new NewPseudonymizationData(PseudonymizationTestSupport.USER__ID);
		final PseudonymizationCtx ctx = new PseudonymizationCtx(false, someUser, pd);

		pseudonymizationHelper.modifyAll_StandbyScheduleBody(ctx);

		// ----------------------------------------------------------------
		// write debug output

		LOGGER.info("@@>> ================================================================================================================================");
		for (String s: ctx.result.getLogMsgs()) {
			LOGGER.info("@@>> " + s);
		}
		LOGGER.info("@@>> ================================================================================================================================");

		// PseudonymizationTestSupport.dump(standbyScheduleBodyRepository);

		// ----------------------------------------------------------------
		// verify result

		PseudonymizationTestSupport.assertIsModified(someStandbyScheduleBody);
	}

	@Test
	public void test_5_modifyAll_archiveStandbyScheduleHeader() throws SpException, IOException
	{
		// prepare data

		ContactData somePrivateContactData = PseudonymizationTestSupport.createPrivateContact();
		User someUser = PseudonymizationTestSupport.createUser(somePrivateContactData);
		ArchiveStandbyScheduleHeader createArchiveStandbyScheduleHeader = PseudonymizationTestSupport.createArchiveStandbyScheduleHeader();

		// ----------------------------------------------------------------
		// mock DB access

		Mockito.when(archiveStandbyScheduleHeaderRepository.findAll()).thenReturn(ListHelper.of(createArchiveStandbyScheduleHeader));

		// ----------------------------------------------------------------
		// invoke method to be tested

		final NewPseudonymizationData pd = new NewPseudonymizationData(PseudonymizationTestSupport.USER__ID);
		final PseudonymizationCtx ctx = new PseudonymizationCtx(false, someUser, pd);

		List<PseudonymizeArchiveHelperController.DebuggingInfo_ArchiveStandbyScheduleHeader> debuggingInfos =
			pseudonymizationArchiveHelper.modifyAll_ArchiveStandbyScheduleHeader(ctx);

		// ----------------------------------------------------------------
		// write debug output

		LOGGER.info("@@>> ================================================================================================================================");
		for (String s: ctx.result.getLogMsgs()) {
			LOGGER.info("@@>> " + s);
		}
		LOGGER.info("@@>> ================================================================================================================================");

		// ----------------------------------------------------------------
		// verify result

		assertEquals(debuggingInfos.size(), 1);
		assertEquals(debuggingInfos.get(0).bModified, true);
		// __saveFile(debuggingInfos.get(0).storedPlan);
		PseudonymizationTestSupport.assertIsModified_user_5(createArchiveStandbyScheduleHeader);

		Mockito.verify(archiveStandbyScheduleHeaderRepository).findAll();
		Mockito.verifyNoMoreInteractions(archiveStandbyScheduleHeaderRepository);
	}

	@Test
	public void test_6_modifyAll_archiveStandbyScheduleHeader_with_storing() throws SpException, IOException
	{
		// prepare data

		ContactData somePrivateContactData = PseudonymizationTestSupport.createPrivateContact();
		User someUser = PseudonymizationTestSupport.createUser(somePrivateContactData);
		ArchiveStandbyScheduleHeader createArchiveStandbyScheduleHeader = PseudonymizationTestSupport.createArchiveStandbyScheduleHeader();

		// ----------------------------------------------------------------
		// mock DB access

		Mockito.when(archiveStandbyScheduleHeaderRepository.findAll()).thenReturn(ListHelper.of(createArchiveStandbyScheduleHeader));
		Mockito.when(archiveStandbyScheduleHeaderRepository.save(createArchiveStandbyScheduleHeader)).thenReturn(createArchiveStandbyScheduleHeader);

		// ----------------------------------------------------------------
		// invoke method to be tested

		final NewPseudonymizationData pd = new NewPseudonymizationData(PseudonymizationTestSupport.USER__ID);
		final PseudonymizationCtx ctx = new PseudonymizationCtx(true, someUser, pd);

		List<PseudonymizeArchiveHelperController.DebuggingInfo_ArchiveStandbyScheduleHeader> debuggingInfos =
			pseudonymizationArchiveHelper.modifyAll_ArchiveStandbyScheduleHeader(ctx);

		// ----------------------------------------------------------------
		// write debug output

		LOGGER.info("@@>> ================================================================================================================================");
		for (String s: ctx.result.getLogMsgs()) {
			LOGGER.info("@@>> " + s);
		}
		LOGGER.info("@@>> ================================================================================================================================");

		// ----------------------------------------------------------------
		// verify result

		assertEquals(debuggingInfos.size(), 1);
		assertEquals(debuggingInfos.get(0).bModified, true);
		// __saveFile(debuggingInfos.get(0).storedPlan);
		PseudonymizationTestSupport.assertIsModified_user_5(createArchiveStandbyScheduleHeader);

		Mockito.verify(archiveStandbyScheduleHeaderRepository).findAll();
		Mockito.verify(archiveStandbyScheduleHeaderRepository).save(createArchiveStandbyScheduleHeader);
		Mockito.verifyNoMoreInteractions(archiveStandbyScheduleHeaderRepository);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}




