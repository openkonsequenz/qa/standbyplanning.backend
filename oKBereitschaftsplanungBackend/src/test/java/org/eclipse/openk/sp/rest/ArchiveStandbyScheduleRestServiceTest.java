/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.planning.ArchiveController;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ArchiveStandbyScheduleRestServiceTest {

	@InjectMocks
	private ArchiveStandbyScheduleRestService archiveStandbyScheduleRestService;

	@Mock
	private ArchiveController archiveController;

	public ArchiveStandbyScheduleRestServiceTest() {

	}

	@Test
	public void getAllArchiveHeaderTest() {
		String jwt = "LET-ME-IN";
		Response result = archiveStandbyScheduleRestService.getAllArchiveHeader(jwt);
		assertNotNull(result);
	}

	@Test
	public void getOneArchiveHeaderTest() {
		String jwt = "LET-ME-IN";
		Response result = archiveStandbyScheduleRestService.getOneArchiveHeader(1L, jwt);
		assertNotNull(result);
	}

	@Test
	public void createByListTest() {
		String jwt = "LET-ME-IN";
		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();
		Response result = archiveStandbyScheduleRestService.createByList(1L, jwt, standbyScheduleFilterDto);
		assertNotNull(result);
	}

	@Test
	public void createByGroupTest() {

		String jwt = "LET-ME-IN";
		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();
		Response result = archiveStandbyScheduleRestService.createByGroup(1L, jwt, standbyScheduleFilterDto);
		assertNotNull(result);
	}

	@Test
	public void getOneTest() {

		String jwt = "LET-ME-IN";
		StandbyScheduleSearchDto searchDto = new StandbyScheduleSearchDto();
		Response result = archiveStandbyScheduleRestService.getSearchArchiveHeader(jwt, searchDto);
		assertNotNull(result);
	}

}
