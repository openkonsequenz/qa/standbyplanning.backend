/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Test;

public class StandbyScheduleActionDtoTest {

	@Test
	public void testGettersAndSetters() {
		StandbyScheduleActionDto action = new StandbyScheduleActionDto();

		action.setCurrentUserId(1l);
		assertTrue(action.getCurrentUserId() == 1L);

		action.setNewUserId(1L);
		assertTrue(action.getNewUserId() == 1L);

		action.setScheduleBodyId(1L);
		assertTrue(action.getScheduleBodyId() == 1L);

		action.setStandbyGroupId(1L);
		assertTrue(action.getStandbyGroupId() == 1L);

		action.setStatusId(1L);
		assertTrue(action.getStatusId() == 1L);

		Date date = new Date();
		action.setValidFromDate(date);
		assertTrue(DateHelper.isSameDate(action.getValidFromDate(), date));

		action.setValidToDate(date);
		assertTrue(DateHelper.isSameDate(action.getValidToDate(), date));
	}
}
