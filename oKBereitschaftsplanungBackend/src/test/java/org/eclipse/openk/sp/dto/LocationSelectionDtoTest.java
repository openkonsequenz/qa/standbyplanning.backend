/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LocationSelectionDtoTest {

	@Test
	public void testGettersAndSetters() {

		
		LocationSelectionDto locationSelectionDto = new LocationSelectionDto();

		
		locationSelectionDto.setId(1l);
		assertEquals(1l, locationSelectionDto.getId().longValue());

		locationSelectionDto.setCommunity("community");
		assertEquals("community", locationSelectionDto.getCommunity());

		locationSelectionDto.setDistrict("district");
		assertEquals("district", locationSelectionDto.getDistrict());


		locationSelectionDto.setShorttext("shorttext");
		assertEquals("shorttext", locationSelectionDto.getShorttext());

		locationSelectionDto.setTitle("title");
		assertEquals("title", locationSelectionDto.getTitle());



	}
}
