/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OrganisationSelectionDtoTest {

	@Test
	public void testGettersAndSetters() {
		OrganisationSelectionDto o = new OrganisationSelectionDto();
		
		o.setOrgaName("Mettenmeier");
		assertEquals("Mettenmeier", o.getOrgaName());
		
		o.setId(1l);
		assertEquals(1l, o.getId().longValue());
		
		AddressDto a = new AddressDto();
		o.setAddress(a);
		assertEquals(a, o.getAddress());
		
		o.setAddressId(5L);
		assertEquals(5L, o.getAddressId().longValue());
	}

}
