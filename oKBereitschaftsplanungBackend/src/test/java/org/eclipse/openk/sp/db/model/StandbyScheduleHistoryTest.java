/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class StandbyScheduleHistoryTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		StandbyScheduleHistory history = new StandbyScheduleHistory();
		history.setId(1l);
		assertEquals(1l, history.getId().longValue());

		history.setModificationDate(d);
		assertEquals(d, history.getModificationDate());

		history.setChangedBy("user");
		assertEquals("user", history.getChangedBy());

		byte[] b = new String("test").getBytes();
		history.setFile(b);
		assertEquals(b, history.getFile());

		history.setFileHashValue("1e2e3e");
		assertEquals("1e2e3e", history.getFileHashValue());

		StandbyScheduleHeader header = new StandbyScheduleHeader();
		history.setStandbyScheduleHeader(header);
		assertEquals(header, history.getStandbyScheduleHeader());
	}
	
	
	@Test
	public void testCompare() throws MalformedURLException {
		StandbyScheduleHistory obj1 = new StandbyScheduleHistory();
		obj1.setId(5L);

		StandbyScheduleHistory obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyScheduleHistory obj3 = new StandbyScheduleHistory();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

}
