/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.UserInRegion;
import org.junit.Test;

public class RegionToStringConverterTest {

	@Test
	public void convertTest() {
		Object destination = null;
		Class destClass = null;
		Class sourceClass = null;

		UserInRegion elem1 = new UserInRegion();
		Region region = new Region();
		region.setRegionName("regionName1");
		elem1.setRegion(region);
		

		UserInRegion elem2 = new UserInRegion();
		region = new Region();
		region.setRegionName("regionName2");
		elem2.setRegion(region);

		List<UserInRegion> source = new ArrayList<UserInRegion>();
		source.add(elem1);
		source.add(elem2);

		RegionListToStringConverter conv = new RegionListToStringConverter();
		Object obj = conv.convert(destination, source, destClass, sourceClass);

		assertNotNull(obj);

		String source2 = "Reg1, Reg2, Reg3";
		obj = conv.convert(destination, source2, destClass, sourceClass);

		assertNotNull(obj);

		Long source3 = new Long(2);
		obj = conv.convert(destination, source3, destClass, sourceClass);

		assertNull(obj);
	}

	@Test
	public void UhfListToString() {

		RegionListToStringConverter conv = new RegionListToStringConverter();

		UserInRegion elem1 = new UserInRegion();
		Region region = new Region();
		region.setRegionName("regionName1");
		elem1.setRegion(region);
		

		UserInRegion elem2 = new UserInRegion();
		region = new Region();
		region.setRegionName("regionName2");
		elem2.setRegion(region);

		List<UserInRegion> source = new ArrayList<UserInRegion>();
		source.add(elem1);
		source.add(elem2);

		String str = conv.listUserInRegionToString(source);
		assertNotNull(str);

	}

	@Test
	public void StringToList() {

		RegionListToStringConverter conv = new RegionListToStringConverter();

		String source = "Reg1, Reg2, Reg3";

		List<Region> lsUserHasUserFunctions = conv.stringToRegionList(source);

		assertNotNull(lsUserHasUserFunctions);
		assertEquals(3, lsUserHasUserFunctions.size());

		String source2 = "Reg1";
		lsUserHasUserFunctions = conv.stringToRegionList(source2);

		assertNotNull(lsUserHasUserFunctions);
		assertEquals(1, lsUserHasUserFunctions.size());
	}
}
