/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DateHelperTest {

	private static final String DAY_MITTWOCH = "Mittwoch";

	@Test
	public void testConstructorIsPrivate()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

		Constructor<DateHelper> constructor = DateHelper.class.getDeclaredConstructor();

		assertTrue(Modifier.isPrivate(constructor.getModifiers()));

		constructor.setAccessible(true);
		constructor.newInstance();
	}

	@Test
	public void addDaysToDateTest() {
		// 30. Oct
		DateTime dateTime = new DateTime(2018, 10, 30, 0, 0);
		// 31. Oct
		DateTime dateTimePlusOne = new DateTime(2018, 10, 31, 0, 0);
		// 1. Nov
		DateTime dateTimeOverMonth = new DateTime(2018, 11, 01, 0, 0);

		// add one day
		Date date = DateHelper.addDaysToDate(dateTime.toDate(), 1);
		assertEquals(date, dateTimePlusOne.toDate());

		// add two days to test overlapping month
		date = DateHelper.addDaysToDate(dateTime.toDate(), 2);
		assertEquals(date, dateTimeOverMonth.toDate());
	}

	@Test
	public void calculateDifferenceOfDaysTest() throws IOException {
		Date from = new Date();
		Date to = DateHelper.addDaysToDate(new Date(), 1);

		// success - equals one day
		int result = DateHelper.calculateDifferenceOfDays(5, 5, from, to);
		assertNotNull(result);
		assertEquals(0, result);

		// success - equals 7 days
		result = DateHelper.calculateDifferenceOfDays(5, 5, DateHelper.addDaysToDate(from, 5), to);
		assertNotNull(result);
		assertEquals(7, result);

		// difference smaller left
		result = DateHelper.calculateDifferenceOfDays(1, 6, from, to);
		assertNotNull(result);
		assertEquals(5, result);

		// difference bigger left
		result = DateHelper.calculateDifferenceOfDays(7, 1, from, to);
		assertNotNull(result);
		assertEquals(1, result);
	}

	@Test
	public void convertToStringTest() throws IOException {
		int year = 2045;
		Date date = DateHelper.getDate(year, 12, 31);

		String result = DateHelper.convertToString(date, "YYYY");

		assertNotNull(result);
		assertEquals(year, Integer.parseInt(result));
	}

	@Test
	public void convertToString2Test() throws IOException {

		Date date = DateHelper.getDate(2045, 12, 31);

		String result = DateHelper.convertToString(date, null);

		assertNotNull(result);
		assertEquals("31.12.2045", result);
	}

	@Test
	public void getEndOfDayTest() throws IOException {

		Date date1 = DateHelper.getDate(2045, 12, 30);
		Date date2 = DateHelper.getDate(2045, 12, 31, 0, 0, 0);

		Date result = DateHelper.getEndOfDay(date1);

		assertNotNull(result);
		assertEquals(date2, result);
	}

	@Test
	public void getDateWithTimeTest() {

		Date date = DateHelper.getDate(2018, 10, 1);
		Date timeOfDate = DateHelper.getDate(1700, 1, 1, 10, 10, 10);
		Date expectedResult = DateHelper.getDate(2018, 10, 1, 10, 10, 10);

		Date result = DateHelper.getDateWithTime(date, timeOfDate);
		assertNotNull(result);
		assertEquals(result, expectedResult);
	}

	@Test
	public void getDateWithTimeTest2() {

		Date date = DateHelper.getDate(2018, 10, 1);
		Date expectedResult = DateHelper.getDate(2018, 10, 1, 12, 23, 0);

		Date result = DateHelper.getDateWithTime(date, 12, 23);
		assertNotNull(result);
		assertEquals(result, expectedResult);
	}

	@Test
	public void getDayOfWeek() {
		Date date = DateHelper.getDate(2018, 9, 26);
		// wednesday (3)
		int expectedResult = 3;
		int result = DateHelper.getDayOfWeek(date);
		assertNotNull(result);
		assertEquals(result, expectedResult);
	}

	@Test
	public void getDayOfWeekText() {
		Date date = DateHelper.getDate(2018, 9, 26);
		String result = DateHelper.getDayOfWeekText(date);
		assertNotNull(result);
		assertEquals(DAY_MITTWOCH, result);

	}

	@Test
	public void getStartOfDay() {
		Date date = DateHelper.getDate(2018, 10, 1, 19, 53, 53);
		Date expectedDate = DateHelper.getDate(2018, 10, 1, 0, 0, 0);
		Date result = DateHelper.getStartOfDay(date);
		assertNotNull(result);
		assertEquals(result, expectedDate);
	}

	@Test
	public void isDateAfterTest() {
		Date checkDate = DateHelper.getDate(2018, 10, 1);
		Date earlierDate = DateHelper.getDate(2017, 10, 1);
		Date laterDate = DateHelper.getDate(2018, 10, 2);

		Boolean result = DateHelper.isDateAfter(checkDate, earlierDate);
		assertNotNull(result);
		assertTrue(result == true);

		result = DateHelper.isDateAfter(checkDate, laterDate);
		assertNotNull(result);
		assertTrue(result == false);
	}

	@Test
	public void isDateBeforeTest() {
		Date checkDate = DateHelper.getDate(2018, 10, 1);
		Date earlierDate = DateHelper.getDate(2017, 10, 1);
		Date laterDate = DateHelper.getDate(2018, 10, 2);

		Boolean result = DateHelper.isDateBefore(checkDate, earlierDate);
		assertNotNull(result);
		assertTrue(result == false);

		result = DateHelper.isDateBefore(checkDate, laterDate);
		assertNotNull(result);
		assertTrue(result == true);
	}

	@Test
	public void isSameDateTest() {
		Date checkDate = DateHelper.getDate(2018, 10, 1);
		Date sameDate = DateHelper.getDate(2018, 10, 1);
		Date earlierDate = DateHelper.getDate(2017, 10, 1);
		Date laterDate = DateHelper.getDate(2018, 10, 2);

		Boolean result = DateHelper.isSameDate(checkDate, sameDate);
		assertNotNull(result);
		assertTrue(result == true);

		result = DateHelper.isSameDate(checkDate, earlierDate);
		assertNotNull(result);
		assertTrue(result == false);

		result = DateHelper.isSameDate(checkDate, laterDate);
		assertNotNull(result);
		assertTrue(result == false);
	}

	@Test
	public void isSameDateTime() {
		int result = 0;
		Date checkDate = new Date();
		Date comparativeDate = checkDate;

		result = DateHelper.isSameDateTime(checkDate, comparativeDate);
		assertTrue(result == 0);

		result = DateHelper.isSameDateTime(DateHelper.addDaysToDate(checkDate, 1), comparativeDate);
		assertTrue(result == 1);

		result = DateHelper.isSameDateTime(checkDate, DateHelper.addDaysToDate(comparativeDate, 1));
		assertTrue(result == -1);
	}

	@Ignore
	@Test
	public void getDateFromString() throws ParseException {
		Date date = DateHelper.getDateFromString("2018-10-10", "yyyy-MM-dd");
		assertNotNull(date);
		assertEquals(1539122400000L, date.getTime());
	}

	@Ignore
	@Test
	public void getDateFromString2() throws ParseException {
		Date d = new Date();
		String pattern = "yyyy-MM-dd HH:mm:ss SSS";
		String strDate = DateHelper.convertToString(d, pattern);

		Date date = DateHelper.getDateFromString(strDate, pattern);

		assertNotNull(date);
		assertEquals(d.getTime(), date.getTime());
	}

	/**
	 * Method to check if the coparedInterval is covered by the list of intervals.
	 * 
	 * @param comparedInterval
	 * @param lsIntervals
	 * @return
	 */
	@Test
	public void isCoveredByTest() {
		DateTime slotStart = null;
		DateTime slotEnd = null;

		DateTime compareFrom = new DateTime(DateHelper.getDate(2019, 1, 1, 8, 0, 0));
		DateTime compareTo = new DateTime(DateHelper.getDate(2019, 1, 1, 16, 0, 0));
		Interval comparedInterval = new Interval(compareFrom, compareTo);

		List<Interval> lsIntervals = new ArrayList<>();

		// Test success 1 (08:00-16:00 == 08:00-16:00)
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 8, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 16, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));
		boolean isFullyCovered = DateHelper.isCoveredBy(comparedInterval, lsIntervals);
		assertTrue(isFullyCovered);

		// Test positive 2 (08:00-16:00 == 08:00-12:00 and 12:00-16:00)
		lsIntervals = new ArrayList<>();
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 8, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 12, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 12, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 16, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));
		isFullyCovered = DateHelper.isCoveredBy(comparedInterval, lsIntervals);
		assertTrue(isFullyCovered);

		// Test positive 3 (08:00-16:00 == 06:00-16:00)
		lsIntervals = new ArrayList<>();
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 6, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 16, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));
		isFullyCovered = DateHelper.isCoveredBy(comparedInterval, lsIntervals);
		assertTrue(isFullyCovered);

		// Test positive 4 (08:00-16:00 == 08:00-18:00)
		lsIntervals = new ArrayList<>();
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 8, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 18, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));
		isFullyCovered = DateHelper.isCoveredBy(comparedInterval, lsIntervals);
		assertTrue(isFullyCovered);

		// Test positive 5 (08:00-16:00 == 08:00-2:00 (nextDay))
		lsIntervals = new ArrayList<>();
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 8, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 2, 2, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));
		isFullyCovered = DateHelper.isCoveredBy(comparedInterval, lsIntervals);
		assertTrue(isFullyCovered);

		// Test negative 1 (08:00-16:00 != 09:00-16:00)
		lsIntervals = new ArrayList<>();
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 9, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 16, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));
		isFullyCovered = DateHelper.isCoveredBy(comparedInterval, lsIntervals);
		assertFalse(isFullyCovered);

		// Test negative 2 (08:00-16:00 != 08:00-11:00 + (2h empty) + 13:00-16:00)
		lsIntervals = new ArrayList<>();
		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 8, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 11, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));

		slotStart = new DateTime(DateHelper.getDate(2019, 1, 1, 13, 0, 0));
		slotEnd = new DateTime(DateHelper.getDate(2019, 1, 1, 16, 0, 0));
		lsIntervals.add(new Interval(slotStart, slotEnd));

		isFullyCovered = DateHelper.isCoveredBy(comparedInterval, lsIntervals);
		assertFalse(isFullyCovered);
	}
}
