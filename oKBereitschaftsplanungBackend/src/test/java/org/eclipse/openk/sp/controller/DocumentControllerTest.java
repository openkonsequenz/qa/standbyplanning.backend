/********************************************************************************
 * Copyright (c) 2019 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DocumentControllerTest {

	@InjectMocks
	DocumentController documentController;

	@Mock
	private FileHelper fileHelper;

	@Test(expected = SpException.class)
	public void getUserDocumentationTest() throws SpException {
		File file = null;
		try {
			file = File.createTempFile("test", "test");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Mockito.when(fileHelper.loadFileFromFileSystemOrResource(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyBoolean())).thenReturn(file);

		File result = documentController.getUserDocumentation();
		assertNotNull(result);
		assertEquals(file, result);

		Mockito.when(fileHelper.loadFileFromFileSystemOrResource(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyBoolean())).thenThrow(new MockitoException("test"));

		result = documentController.getUserDocumentation();

	}
}
