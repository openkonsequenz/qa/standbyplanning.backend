/********************************************************************************
 * Copyright (c) 2023 Mettenmeier GmbH
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
package org.eclipse.openk.sp.controller.pseudonymization;



import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;



/**
 * Helper class that assists with JSON serialization/deserialization and equality checks using Gson.
 */
public class GsonHelper
{

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static Gson gson = new Gson();
	private static Gson gsonPretty = new GsonBuilder().setPrettyPrinting().create();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor.
	 */
	private GsonHelper()
	{
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static String ____toStr(JsonElement je)
	{
		if ((je == null) || (je instanceof JsonNull)) {
			return "null";
		}

		if (je instanceof JsonArray) {
			return "JsonArray:...";
		}

		if (je instanceof JsonObject) {
			return "JsonObject:...";
		}

		JsonPrimitive jp = (JsonPrimitive)je;
		if (jp.isBoolean()) {
			return "bool:" + jp.toString();
		}
		if (jp.isNumber()) {
			return "number:" + jp.toString();
		}
		if (jp.isString()) {
			return "number:" + jp.toString();
		}

		throw new RuntimeException("IMPLEMENTATION ERROR");
	}

	private static void __compareAny(String path, JsonElement eA, JsonElement eB, List<String> outErrMsgs)
	{
		if ((eA == null) && (eB == null)) {
			return;
		}

		if (
			(eA == null)
			|| (eA instanceof JsonNull)
			|| (eB == null)
			|| (eB instanceof JsonNull)
			|| (eA.getClass() != eB.getClass())
		) {
			outErrMsgs.add(path + " >>>> " + ____toStr(eA) + " vs. " + ____toStr(eB));
			return;
		}

		if (eA instanceof JsonArray) {
			__compareArr(path, (JsonArray)eA, (JsonArray)eB, outErrMsgs);
			return;
		}

		if (eA instanceof JsonObject) {
			__compareObj(path, (JsonObject)eA, (JsonObject)eB, outErrMsgs);
			return;
		}

		if (eA instanceof JsonPrimitive) {
			__comparePrimitive(path, (JsonPrimitive)eA, (JsonPrimitive)eB, outErrMsgs);
			return;
		}

		throw new RuntimeException("IMPLEMENTATION ERROR");
	}

	private static void __compareObj(String path, JsonObject objA, JsonObject objB, List<String> outErrMsgs)
	{
		SetIntersection<String> si = SetIntersection.intersect(objA.keySet(), objB.keySet());
		if (si.extraA.size() > 0) {
			for (String keyA: si.extraA) {
				outErrMsgs.add(path + "/" + keyA + " >>>> " + ____toStr(objA.get(keyA)) + " vs. n/a");
			}
			for (String keyB: si.extraB) {
				outErrMsgs.add(path + "/" + keyB + " >>>> n/a vs. " + ____toStr(objA.get(keyB)));
			}
		}
		for (String key: si.intersectSet) {
			JsonElement eA = objA.get(key);
			JsonElement eB = objB.get(key);
			__compareAny(path + "/" + key, eA, eB, outErrMsgs);
		}
	}

	private static void __compareArr(String path, JsonArray arrA, JsonArray arrB, List<String> outErrMsgs)
	{
		if (arrA.size() != arrB.size()) {
			outErrMsgs.add(path + " >>>> length " + arrA.size() + " vs. length " + arrB.size());
			return;
		}

		for (int i = 0; i < arrA.size(); i++) {
			JsonElement eA = arrA.get(i);
			JsonElement eB = arrB.get(i);
			__compareAny(path + "[" + i + "]", eA, eB, outErrMsgs);
		}
	}

	private static void __comparePrimitive(String path, JsonPrimitive pA, JsonPrimitive pB, List<String> outErrMsgs)
	{
		if ((pA.getClass() != pB.getClass()) || (!pA.toString().equals(pB.toString()))) {
			outErrMsgs.add(path + " >>>> " + ____toStr(pA) + " vs. " + ____toStr(pB));
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Getters/Setters
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static String toPrettyJson(Object someObj)
	{
		return gsonPretty.toJson(someObj);
	}

	public static <T> JsonObject parseString(String jsonData, Class<T> clazz)
	{
		T someObj = gson.fromJson(jsonData, clazz);
		JsonObject ret = (JsonObject)(gson.toJsonTree(someObj));
		return ret;
	}

	public static <T> JsonObject parseObject(T someObj)
	{
		JsonObject ret = (JsonObject)(gson.toJsonTree(someObj));
		return ret;
	}

	public static String objToJsonPretty(Object jsonSerializableObj)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(jsonSerializableObj);
	}

	/**
	 * Compare two JSON hierarchies.
	 *
	 * @return		Returns <code>null</code> on success. Otherwise the returned list contains error messages.
	 */
	public static ArrayList<String> compare(JsonObject objA, JsonObject objB)
	{
		ArrayList<String> errMsgs = new ArrayList<>();
		__compareObj("", objA, objB, errMsgs);
		if (errMsgs.size() == 0) {
			return null;
		}
		return errMsgs;
	}

}




