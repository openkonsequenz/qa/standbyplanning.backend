/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class ArchiveStandbyScheduleHeaderDtoTest {

	@Test
	public void testGettersAndSetters() throws MalformedURLException {
		Long id = new Long(2);
		String text = "text";
		Date d = new Date();

		ArchiveStandbyScheduleHeaderDto dto = new ArchiveStandbyScheduleHeaderDto();
		dto.setComment(text);
		assertEquals(text, dto.getComment());

		dto.setId(id);
		assertEquals(id, dto.getId());

		dto.setJsonPlan(text);
		assertEquals(text, dto.getJsonPlan());

		dto.setModificationDate(d);
		assertEquals(d, dto.getModificationDate());

		dto.setModifiedBy(text);
		assertEquals(text, dto.getModifiedBy());

		dto.setModifiedCause(text);
		assertEquals(text, dto.getModifiedCause());

		dto.setTitle(text);
		assertEquals(text, dto.getTitle());

		dto.setValidFrom(d);
		assertEquals(d, dto.getValidFrom());

		dto.setValidTo(d);
		assertEquals(d, dto.getValidTo());

		dto.setStatusId(id);
		assertEquals(id, dto.getStatusId());

		dto.setStatusName(text);
		assertEquals(text, dto.getStatusName());

	}

}
