/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.BranchRepository;
import org.eclipse.openk.sp.db.dao.LocationForBranchRepository;
import org.eclipse.openk.sp.db.dao.LocationRepository;
import org.eclipse.openk.sp.db.dao.PostcodeRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.LocationForBranch;
import org.eclipse.openk.sp.db.model.Postcode;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.dto.LabelValueDto;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.LocationForBranchDto;
import org.eclipse.openk.sp.dto.LocationSelectionDto;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.dto.RegionSmallDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle location operations. */
@RunWith(MockitoJUnitRunner.class)
public class LocationControllerTest {
	protected static final Logger logger = Logger.getLogger(LocationController.class);

	@Mock
	LocationRepository locationRepository;

	@Mock
	BranchRepository branchRepository;

	@Mock
	LocationForBranchRepository locForBranchRepository;

	@Mock
	PostcodeRepository postcodeRepository;

	@Mock
	RegionRepository regionRepository;

	@Mock
	EntityConverter entityConverter;

	@InjectMocks
	LocationController locationController = new LocationController();

	@Test
	public void saveLocationTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocation.json");
		LocationDto locationDto = (LocationDto) new Gson().fromJson(json, LocationDto.class);
		locationDto.setId(1l);
		Location location = (Location) new Gson().fromJson(json, Location.class);
		location.setId(1l);

		List list = location.getLsPostcode();
		List listRegion = location.getLsRegions();

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) location);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) locationDto);
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.exists(any())).thenReturn(true);

//		when(postcodeRepository.findOne(Mockito.anyLong())).thenReturn((Postcode) list.get(0));
//		when(regionRepository.findOne(Mockito.anyLong())).thenReturn((Region) listRegion.get(0));
//
//		when(locationRepository.save((Location) any())).thenReturn(location);

		// run test
		LocationDto locationResult = null;

		try {
			locationResult = locationController.saveLocation(locationDto);

			assertNotNull(locationResult);
			assertEquals(1l, locationResult.getId().longValue());
		} catch (Exception e) {
			assertNull(e);
		}

		// run test without id
		try {
			locationDto.setId(null);
			locationResult = locationController.saveLocation(locationDto);
			assertNotNull(locationResult);
		} catch (Exception e) {
			assertNull(e);
		}

		// run exception test
		when(locationRepository.save((Location) any())).thenThrow(new IllegalArgumentException());
		try {
			locationDto.setId(1l);
			locationResult = locationController.saveLocation(locationDto);

		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getLocationsTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocationList.json");
		List<Location> lsLocationMock = (List<Location>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_LOCATION_ARRAY_LIST);

		json = fh.loadStringFromResource("testLocation.json");
		LocationDto locationDtoMock = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) locationDtoMock);
		when(locationRepository.findAllByOrderByTitleAsc()).thenReturn(lsLocationMock);

		// run test
		List<LocationDto> lsOutputUserDto = null;
		try {
			lsOutputUserDto = locationController.getLocations();
		} catch (Exception e) {
			assertNull(e);
		}
		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
		assertTrue(lsOutputUserDto.get(0) instanceof LocationDto);

		// run exception test
		when(locationRepository.findAllByOrderByTitleAsc()).thenThrow(new IllegalArgumentException());
		try {
			lsOutputUserDto = locationController.getLocations();
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getLocationSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocationList.json");
		List<Location> lsLocationMock = (List<Location>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_LOCATION_ARRAY_LIST);

		json = fh.loadStringFromResource("testLocation.json");
		LocationSelectionDto locationDtoMock = (LocationSelectionDto) new Gson().fromJson(json,
				LocationSelectionDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) locationDtoMock);
		when(locationRepository.findAllByOrderByTitleAsc()).thenReturn(lsLocationMock);

		// run test
		List<LocationSelectionDto> lsOutputUserDto = null;
		try {
			lsOutputUserDto = locationController.getLocationsSelection();
			assertNotNull(lsOutputUserDto);
			assertTrue(lsOutputUserDto.size() > 0);
			assertTrue(lsOutputUserDto.get(0) instanceof LocationSelectionDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// run exception test
		when(locationRepository.findAllByOrderByTitleAsc()).thenThrow(new IllegalArgumentException());
		try {
			lsOutputUserDto = locationController.getLocationsSelection();
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test
	public void getLocation() {
		// create test data
		long id = 5;
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testLocation.json");
		Location locationMock = (Location) new Gson().fromJson(json, Location.class);
		locationMock.setId(id);
		LocationDto locationDtoMock = (LocationDto) new Gson().fromJson(json, LocationDto.class);
		locationDtoMock.setId(id);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) locationDtoMock);
		when(locationRepository.findOne(id)).thenReturn(locationMock);

		// run test
		try {
			LocationDto locationDto = locationController.getLocation(id);
			assertNotNull(locationDto);
			assertTrue(locationDto.getId() == id);
		} catch (Exception e) {
			assertNull(e);
		}

		// run exception test
		try {
			when(locationRepository.findOne(id)).thenThrow(new IllegalArgumentException());
			LocationDto locationDto = locationController.getLocation(id);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void validateEntitysTest() {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);
		List list = new ArrayList();
		list.add(location);

		when(locationRepository.findOne(location.getId())).thenReturn(location);

		List resultllist = locationController.validateEntitys(list, locationRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void validateEntitysLogTest() {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);
		List list = new ArrayList();
		list.add(location);

		when(locationRepository.findOne(location.getId())).thenReturn(location);

		List resultllist = locationController.validateEntitys(list, locationRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void validateEntitysLog2Test() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);

		location.setId(null);

		List list = new ArrayList();
		list.add(location);

		when(locationRepository.findOne(location.getId())).thenReturn(location);

		List resultllist = locationController.validateEntitys(list, locationRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void saveBranchesForLocationTest() {
		// prepare data
		Location location = new Location();
		location.setId(1l);
		location.setLsLocationForBranches(new ArrayList<LocationForBranch>());

		List<LocationForBranchDto> lsLocationForBranchDtos = new ArrayList<>();
		LocationForBranchDto dto = new LocationForBranchDto();
		dto.setId(1l);
		dto.setBranchId(1l);
		lsLocationForBranchDtos.add(dto);

		dto = new LocationForBranchDto();
		dto.setId(2l);
		dto.setBranchId(2l);
		lsLocationForBranchDtos.add(dto);

		dto = new LocationForBranchDto();
		dto.setId(null);
		dto.setBranchId(2l);
		lsLocationForBranchDtos.add(dto);

		dto = new LocationForBranchDto();
		dto.setId(0l);
		dto.setBranchId(2l);
		lsLocationForBranchDtos.add(dto);

		// define rules
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.exists(any())).thenReturn(true);
		when(locationRepository.save(any(Location.class))).thenReturn(new Location());
		when(branchRepository.exists(any())).thenReturn(true);
		when(branchRepository.findOne(any())).thenReturn(new Branch());
		when(branchRepository.save(any(Branch.class))).thenReturn(new Branch());
		when(locForBranchRepository.findOne(any())).thenReturn(new LocationForBranch());
		when(locForBranchRepository.save(any(LocationForBranch.class))).thenReturn(new LocationForBranch());
		when(locForBranchRepository.exists(any())).thenReturn(true);
		// run positive test
		try {
			locationController.saveBranchesForLocation(1l, lsLocationForBranchDtos);
		} catch (Exception e) {
			assertNull(e);
		}

		// define rules negative test 1
		when(locationRepository.exists(any())).thenReturn(false);

		// run negative test 1
		try {
			locationController.saveBranchesForLocation(1l, lsLocationForBranchDtos);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// define rules negative test 2
		when(locationRepository.exists(any())).thenReturn(true);
		when(branchRepository.exists(any())).thenReturn(false);

		// run negative test 2
		try {
			locationController.saveBranchesForLocation(1l, lsLocationForBranchDtos);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// run negative test 3
		try {
			locationController.saveBranchesForLocation(1l, null);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test(expected = SpException.class)
	public void deleteBranchesForLocationTest() throws SpException {
		// prepare data
		Location location = new Location();
		location.setId(1l);

		Branch branch = new Branch();

		LocationForBranch locForBranch = new LocationForBranch();
		locForBranch.setLocation(location);
		locForBranch.setBranch(branch);

		List<LocationForBranch> lsLocationForBranch = new ArrayList<>();
		lsLocationForBranch.add(locForBranch);
		location.setLsLocationForBranches(lsLocationForBranch);

		branch.getLsLocationForBranches().add(locForBranch);

		List<LocationForBranchDto> lsLocationForBranchDtos = new ArrayList<>();
		LocationForBranchDto dto = new LocationForBranchDto();
		dto.setId(1l);
		dto.setBranchId(1l);
		lsLocationForBranchDtos.add(dto);

		dto = new LocationForBranchDto();
		dto.setId(2l);
		dto.setBranchId(2l);
		lsLocationForBranchDtos.add(dto);

		// define rules
		when(branchRepository.findOne(any())).thenReturn(branch);
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.save(any(Location.class))).thenReturn(location);
		when(locForBranchRepository.findOne(any())).thenReturn(locForBranch);
		when(locationRepository.exists(any())).thenReturn(true);
		when(locForBranchRepository.exists(any())).thenReturn(true);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationForBranchDtos);

		// LocationForBranchRepository spy = Mockito.spy(locForBranchRepository);
		Mockito.doNothing().when(locForBranchRepository).delete(any(LocationForBranch.class));

		locationController.deleteBranchesForLocation(1l, lsLocationForBranchDtos);

		// define rules for negative test 1
		// run negative test 1

		locationController.deleteBranchesForLocation(1l, null);

		// define rules for negative test 2
		when(locationRepository.exists(any())).thenReturn(false);
		when(locForBranchRepository.exists(any())).thenReturn(false);

		// run negative test 2

		locationController.deleteBranchesForLocation(1l, lsLocationForBranchDtos);

	}

	@Test(expected = SpException.class)
	public void deleteBranchesForLocation2Test() throws SpException {
		// prepare data
		Location location = new Location();
		location.setId(1l);

		Branch branch = new Branch();

		LocationForBranch locForBranch = new LocationForBranch();
		locForBranch.setLocation(location);
		locForBranch.setBranch(branch);

		List<LocationForBranch> lsLocationForBranch = new ArrayList<>();
		lsLocationForBranch.add(locForBranch);
		location.setLsLocationForBranches(lsLocationForBranch);

		branch.getLsLocationForBranches().add(locForBranch);

		List<LocationForBranchDto> lsLocationForBranchDtos = new ArrayList<>();
		LocationForBranchDto dto = new LocationForBranchDto();
		dto.setId(1l);
		dto.setBranchId(1l);
		lsLocationForBranchDtos.add(dto);

		dto = new LocationForBranchDto();
		dto.setId(2l);
		dto.setBranchId(2l);
		lsLocationForBranchDtos.add(dto);

		// define rules
		when(branchRepository.findOne(any())).thenReturn(branch);
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.save(any(Location.class))).thenReturn(location);
		when(locForBranchRepository.findOne(any())).thenReturn(locForBranch);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationForBranchDtos);
		when(locationRepository.exists(any())).thenReturn(false);
		when(locForBranchRepository.exists(any())).thenReturn(false);

		// run negative test 2

		locationController.deleteBranchesForLocation(1l, lsLocationForBranchDtos);

	}

	@Test
	public void saveRegionsForLocationTest() {
		Location location = new Location();

		// prepare data
		Region region = new Region();
		region.setId(1l);
		region.setLsLocations(new ArrayList<Location>());

		List<RegionSmallDto> lsRegionDto = new ArrayList<>();
		RegionSmallDto dto = new RegionSmallDto();
		dto.setId(1l);
		lsRegionDto.add(dto);

		dto = new RegionSmallDto();
		dto.setId(2l);
		lsRegionDto.add(dto);

		dto = new RegionSmallDto();
		dto.setId(null);
		lsRegionDto.add(dto);

		dto = new RegionSmallDto();
		dto.setId(0l);
		lsRegionDto.add(dto);

		// define rules
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.exists(any())).thenReturn(true);
		when(locationRepository.save(any(Location.class))).thenReturn(new Location());
		when(regionRepository.exists(any())).thenReturn(true);
		when(regionRepository.findOne(any())).thenReturn(new Region());
		when(regionRepository.save(any(Region.class))).thenReturn(new Region());
		when(locForBranchRepository.findOne(any())).thenReturn(new LocationForBranch());
		when(locForBranchRepository.save(any(LocationForBranch.class))).thenReturn(new LocationForBranch());

		// run positive test
		try {
			locationController.saveRegionsForLocation(1l, lsRegionDto);
		} catch (Exception e) {
			assertNull(e);
		}

		// run negative test 2
		try {
			locationController.saveRegionsForLocation(1l, null);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// define rules negative test 2
		when(locationRepository.exists(any())).thenReturn(false);

		// run negative test 2
		try {
			locationController.saveRegionsForLocation(1l, lsRegionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// define rules negative test 3
		when(locationRepository.exists(any())).thenReturn(true);
		when(regionRepository.exists(any())).thenReturn(false);

		// run negative test 3
		try {
			locationController.saveRegionsForLocation(1l, lsRegionDto);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test(expected = SpException.class)
	public void deleteRegionsForLocationTest() throws SpException {
		// prepare data
		Location location = new Location();
		location.setId(1l);

		Region region = new Region();
		region.getLsLocations().add(location);

		List<Region> lsRegions = new ArrayList<>();
		lsRegions.add(region);
		location.setLsRegions(lsRegions);

		List<RegionSmallDto> lsRegionDtos = new ArrayList<>();
		RegionSmallDto dto = new RegionSmallDto();
		dto.setId(1l);
		dto.setRegionName("Test");
		lsRegionDtos.add(dto);

		// define rules
		when(regionRepository.exists(any())).thenReturn(true);
		when(locationRepository.exists(any())).thenReturn(true);
		when(regionRepository.findOne(any())).thenReturn(region);
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.save(any(Location.class))).thenReturn(location);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsRegionDtos);

		List<RegionSmallDto> listResult = locationController.deleteRegionsForLocation(1l, lsRegionDtos);
		assertNotNull(listResult);

		locationController.deleteRegionsForLocation(1l, null);

	}

	@Test(expected = SpException.class)
	public void deleteRegionsForLocation2Test() throws SpException {
		// prepare data
		Location location = new Location();
		location.setId(1l);

		Region region = new Region();
		region.getLsLocations().add(location);

		List<Region> lsRegions = new ArrayList<>();
		lsRegions.add(region);
		location.setLsRegions(lsRegions);

		List<RegionSmallDto> lsRegionDtos = new ArrayList<>();
		RegionSmallDto dto = new RegionSmallDto();
		dto.setId(1l);
		dto.setRegionName("Test");
		lsRegionDtos.add(dto);

		// define rules
		when(regionRepository.exists(any())).thenReturn(false);
		when(locationRepository.exists(any())).thenReturn(false);
		when(regionRepository.findOne(any())).thenReturn(region);
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.save(any(Location.class))).thenReturn(location);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsRegionDtos);

		List<RegionSmallDto> listResult = locationController.deleteRegionsForLocation(1l, lsRegionDtos);
		assertNotNull(listResult);

		locationController.deleteRegionsForLocation(1l, null);

	}

	@Test
	public void savePostcodeForLocationTest() {
		// prepare data
		Long locationId = 1l;
		Location location = new Location();
		Postcode postcode = new Postcode();
		PostcodeDto postcodeDto = new PostcodeDto("22345");

		List<PostcodeDto> lsPostcodeDtos = new ArrayList<>();
		lsPostcodeDtos.add(postcodeDto);
		location.setLsPostcode(new ArrayList<>());

		// define rules
		when(locationRepository.exists(any())).thenReturn(true);
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.save(any(Location.class))).thenReturn(location);
		when(postcodeRepository.exists(any())).thenReturn(true);
		when(postcodeRepository.findOne(any())).thenReturn(postcode);
		when(postcodeRepository.save(any(Postcode.class))).thenReturn(postcode);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsPostcodeDtos);

		// run method
		try {
			locationController.savePostcodeForLocation(locationId, lsPostcodeDtos);
		} catch (SpException e) {
			assertNull(e);
		}

		// define rules negative test 1
		when(locationRepository.exists(any())).thenReturn(false);

		// run negative test 1
		try {
			locationController.savePostcodeForLocation(locationId, lsPostcodeDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules negative test 2
		when(locationRepository.exists(any())).thenReturn(true);
		when(postcodeRepository.exists(any())).thenReturn(false);

		// run negative test 2
		try {
			locationController.savePostcodeForLocation(locationId, lsPostcodeDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// run negative test 3
		try {
			locationController.savePostcodeForLocation(locationId, null);
		} catch (SpException e) {
			assertNotNull(e);
		}

	}

	@Test
	public void deletePostcodeForLocationTest() {
		// prepare data
		Long locationId = 1l;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);
		LocationDto locationDto = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		Postcode postcode = location.getLsPostcode().get(0);
		postcode.getLsLocation().add(location);

		List<PostcodeDto> lsPostcodeDtos = locationDto.getLsPostcode();

		// define rules
		when(locationRepository.exists(any())).thenReturn(true);
		when(locationRepository.findOne(any())).thenReturn(location);
		when(locationRepository.save(any(Location.class))).thenReturn(location);
		when(postcodeRepository.exists(any())).thenReturn(true);
		when(postcodeRepository.findOne(any())).thenReturn(postcode);
		when(postcodeRepository.save(any(Postcode.class))).thenReturn(postcode);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsPostcodeDtos);

		// run method
		try {
			locationController.deletePostcodeForLocation(locationId, lsPostcodeDtos);
		} catch (SpException e) {
			assertNull(e);
		}

		// define rules negative test 1
		when(locationRepository.exists(any())).thenReturn(false);

		// run negative test 1
		try {
			locationController.deletePostcodeForLocation(locationId, lsPostcodeDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules negative test 2
		when(locationRepository.exists(any())).thenReturn(false);
		when(postcodeRepository.exists(any())).thenReturn(false);

		// run negative test 2
		try {
			locationController.deletePostcodeForLocation(locationId, lsPostcodeDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// run negative test 3
		try {
			locationController.deletePostcodeForLocation(locationId, null);
		} catch (SpException e) {
			assertNotNull(e);
		}

	}

	@Test
	public void getLocationString() {
		// prepare data
		Long locationId = 1l;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocation.json");
		Location location = (Location) new Gson().fromJson(json, Location.class);
		LocationDto locationDto = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		Postcode postcode = location.getLsPostcode().get(0);
		postcode.getLsLocation().add(location);

		String result = locationController.getLocationString(locationDto);
		assertNotNull(result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getLocationsSearchListTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testLocationList.json");
		List<Location> lsLocationMock = (List<Location>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_LOCATION_ARRAY_LIST);

		json = fh.loadStringFromResource("testLocation.json");
		LocationDto locationDtoMock = (LocationDto) new Gson().fromJson(json, LocationDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) locationDtoMock);
		when(locationRepository.findAll()).thenReturn(lsLocationMock);

		// run test

		List<LabelValueDto> resultList = locationController.getLocationsSearchList();

		assertNotNull(resultList);

	}
}
