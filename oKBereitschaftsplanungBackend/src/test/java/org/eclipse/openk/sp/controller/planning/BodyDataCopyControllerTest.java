/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.CalendarController;
import org.eclipse.openk.sp.controller.validation.ValidationController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.StandbyStatusRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.UserSmallSelectionDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleCopyDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class BodyDataCopyControllerTest {

	protected static final Logger LOGGER = Logger.getLogger(BodyDataCopyControllerTest.class);

	@Mock
	private StandbyStatusRepository standbyStatusRepository;

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Mock
	PlannedDataController plannedDataController;

	@Mock
	private EntityConverter entityConverter;

	@Mock
	CalendarController calendarController;

	@Mock
	ArchiveController archiveController;

	@Mock
	private StandbyGroupRepository standbyGroupRepository;

	@Mock
	private ValidationController validationController;

	@InjectMocks
	BodyDataCopyController bodyDataCopyController;

	@Test
	public void deleteMsgSavingBodyTEST() throws SpException {
		PlanningMsgDto result;
		StandbyScheduleBody stbyBody = new StandbyScheduleBody();
		StandbyGroup sbg = new StandbyGroup();
		sbg.setTitle("title");
		sbg.setId(1L);
		stbyBody.setStandbyGroup(sbg);

		User user = new User();
		user.setId(1L);
		user.setFirstname("firstname");
		user.setLastname("lastname");
		stbyBody.setValidFrom(DateHelper.getDate(2017, 12, 31));
		stbyBody.setValidTo(DateHelper.getDate(2018, 1, 3));
		stbyBody.setUser(user);

		result = bodyDataCopyController.deleteMsgSavingBody(stbyBody);
		assertNotNull(result);

	}

	@Test
	public void deleteExistingBodysInIntervall() throws SpException {

		List<PlanningMsgDto> lsMsg = new ArrayList<>();

		List<StandbyScheduleBody> response = bodyDataCopyController.deleteExistingBodysInIntervall(null, lsMsg);

		assertNull(response);

	}

	@Test
	public void deleteExistingBodysInIntervall2() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBodySearchList.json");

		List<StandbyScheduleBody> lsStandbyScheduleBody = (List<StandbyScheduleBody>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYBODY_ARRAY_LIST);

		List<PlanningMsgDto> lsMsg = new ArrayList<>();

		List<StandbyScheduleBody> response = bodyDataCopyController
				.deleteExistingBodysInIntervall(lsStandbyScheduleBody, lsMsg);

		assertNotNull(response);

	}

	@Test
	public void createMsgSavingBodyTEST() {
		PlanningMsgDto result;
		StandbyScheduleBody stbyBody = new StandbyScheduleBody();
		StandbyGroup sbg = new StandbyGroup();
		sbg.setTitle("title");
		sbg.setId(1L);
		stbyBody.setStandbyGroup(sbg);

		User user = new User();
		user.setId(1L);
		user.setFirstname("firstname");
		user.setLastname("lastname");
		stbyBody.setValidFrom(DateHelper.getDate(2017, 12, 31));
		stbyBody.setValidTo(DateHelper.getDate(2018, 1, 3));
		stbyBody.setUser(user);

		result = bodyDataCopyController.createMsgSavingBody(stbyBody);
		assertNotNull(result);

	}

	@Test
	public void createMsgFailSavingBodyTEST() {
		PlanningMsgDto result;
		StandbyScheduleBodySelectionDto stbyBody = new StandbyScheduleBodySelectionDto();
		StandbyGroupSelectionDto sbg = new StandbyGroupSelectionDto();
		sbg.setTitle("title");
		sbg.setId(1L);
		stbyBody.setStandbyGroup(sbg);

		UserSmallSelectionDto user = new UserSmallSelectionDto();
		user.setId(1L);
		user.setFirstname("firstname");
		user.setLastname("lastname");
		stbyBody.setValidFrom(DateHelper.getDate(2017, 12, 31));
		stbyBody.setValidTo(DateHelper.getDate(2018, 1, 3));
		stbyBody.setUser(user);

		result = bodyDataCopyController.createMsgFailSavingBody(stbyBody);
		assertNotNull(result);

	}

	@Test
	public void getGroupsAndListsTEST() {

		List<TransferGroupDto> result;

		Object[] mx = new Object[4];
		mx[0] = new String("test");
		mx[1] = new Long(2);
		mx[2] = new String("test");
		mx[3] = new Long(2);

		List lsObj = new ArrayList<>();
		lsObj.add(mx);

		when(standbyGroupRepository.findGroupsWithLists()).thenReturn(lsObj);

		result = bodyDataCopyController.getGroupsAndLists();
		assertNotNull(result);

		for (TransferGroupDto dto : result) {
			assertEquals(mx[1], dto.getGroupId());
			assertEquals(mx[3], dto.getListId());
			assertEquals(mx[0], dto.getGroupName());
			assertEquals(mx[2], dto.getListName());
		}

		assertNotNull(new TransferGroupDto());

	}

	@Test
	public void validateTargetBodiesTEST() throws SpException {
		Long groupId = new Long(2);
		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		List<StandbyScheduleBody> result;
		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(true);

		StandbyStatus status = new StandbyStatus();
		status.setId(groupId);
		status.setTitle("Plan-Ebene");
		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();

		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.anyLong())).thenReturn(listBodyTarget);

		result = bodyDataCopyController.deleteTargetBodies(standbyScheduleCopyDto, groupId, status, lsMsg);
		assertNotNull(result);

	}

	@Test
	public void validateTargetBodies3TEST() throws SpException {
		Long groupId = new Long(2);
		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		List<StandbyScheduleBody> result;
		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(true);

		StandbyStatus status = new StandbyStatus();
		status.setId(groupId);
		status.setTitle("Plan-Ebene");
		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();
		StandbyScheduleBody sbsb = new StandbyScheduleBody();
		StandbyGroup sbg = new StandbyGroup();
		sbg.setTitle("title");
		sbg.setId(1L);
		sbsb.setStandbyGroup(sbg);

		listBodyTarget.add(sbsb);
		User user = new User();
		user.setFirstname("firstname");
		user.setLastname("lastname");
		sbsb.setValidFrom(new Date());
		sbsb.setValidTo(new Date());
		sbsb.setUser(user);

		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.anyLong())).thenReturn(listBodyTarget);

		result = bodyDataCopyController.deleteTargetBodies(standbyScheduleCopyDto, groupId, status, lsMsg);
		assertNotNull(result);

	}

	@Test
	public void validateTargetBodies2TEST() throws SpException {
		Long groupId = new Long(2);
		List<PlanningMsgDto> lsMsg = new ArrayList<>();
		List<StandbyScheduleBody> result;
		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(false);

		StandbyStatus status = new StandbyStatus();
		status.setId(groupId);
		status.setTitle("Plan-Ebene");
		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();
		StandbyScheduleBody sbsb = new StandbyScheduleBody();
		listBodyTarget.add(sbsb);

		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.anyLong())).thenReturn(listBodyTarget);

		result = bodyDataCopyController.deleteTargetBodies(standbyScheduleCopyDto, groupId, status, lsMsg);
		assertNotNull(result);

	}

	@Test
	public void validateCopyDto() throws SpException {

		Long id = new Long(2);
		TransferGroupDto dto = new TransferGroupDto();
		List<TransferGroupDto> lsTransferGroup = new ArrayList<TransferGroupDto>();
		lsTransferGroup.add(dto);

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(false);
		standbyScheduleCopyDto.setStatusId(id);
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setLsTransferGroup(lsTransferGroup);

		List<PlanningMsgDto> lsMsg = new ArrayList<>();

		Boolean result = bodyDataCopyController.validateCopyDto(standbyScheduleCopyDto, lsMsg);
		assertNotNull(result);

		standbyScheduleCopyDto.setLsTransferGroup(new ArrayList<>());

		try {
			result = bodyDataCopyController.validateCopyDto(standbyScheduleCopyDto, lsMsg);
		} catch (Exception e) {
			assertNotNull(e);
		}

		standbyScheduleCopyDto.setStatusId(null);

		try {
			result = bodyDataCopyController.validateCopyDto(standbyScheduleCopyDto, lsMsg);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test(expected = SpException.class)
	public void validateDeleteDtoErrorTest() throws SpException {

		Long id = new Long(2);
		TransferGroupDto dto = new TransferGroupDto();
		List<TransferGroupDto> lsTransferGroup = new ArrayList<TransferGroupDto>();
		lsTransferGroup.add(dto);

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(null);
		standbyScheduleCopyDto.setStatusId(null);
		standbyScheduleCopyDto.setValidFrom(null);
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setLsTransferGroup(lsTransferGroup);

		List<PlanningMsgDto> lsMsg = new ArrayList<>();

		standbyScheduleCopyDto.setLsTransferGroup(new ArrayList<>());

		try {
			bodyDataCopyController.validateDeleteDto(standbyScheduleCopyDto, lsMsg);

		} catch (Exception e) {
			assertNotNull(e);
		}

		standbyScheduleCopyDto.setOverwrite(false);
		standbyScheduleCopyDto.setStatusId(id);
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setLsTransferGroup(lsTransferGroup);
		standbyScheduleCopyDto.setStandbyListId(4L);

		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(false);

		Boolean result = bodyDataCopyController.validateDeleteDto(standbyScheduleCopyDto, lsMsg);
		assertNotNull(result);

	}

	@Test
	public void calcTargetStatus() {

		Long sourceStatusId = new Long(2);
		List<PlanningMsgDto> lsMsg = new ArrayList<>();

		StandbyStatus status = new StandbyStatus();
		status.setId(sourceStatusId);
		status.setTitle("title");

		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		StandbyStatus result = bodyDataCopyController.calcTargetStatus(sourceStatusId, lsMsg);
		assertNotNull(result);

	}

	@Test
	public void validateSourceBodies() {

		List<StandbyScheduleBody> result;

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setOverwrite(true);

		Long id = new Long(1);
		List<PlanningMsgDto> lsMsg = new ArrayList<PlanningMsgDto>();

		List<StandbyScheduleBody> listBodySource = new ArrayList<>();

		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(id,
				standbyScheduleCopyDto.getValidFrom(), standbyScheduleCopyDto.getValidTo(), id))
						.thenReturn(listBodySource);

		result = bodyDataCopyController.validateSourceBodies(standbyScheduleCopyDto, id, id, lsMsg);
		assertNotNull(result);

	}

	@Test
	public void copyByGroups() throws SpException {
		PlanningMsgResponseDto result;

		Long id = new Long(2);

		StandbyStatus status = new StandbyStatus();
		status.setId(id);
		status.setTitle("title");

		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();

		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		TransferGroupDto dto = new TransferGroupDto();
		List<TransferGroupDto> lsTransferGroup = new ArrayList<TransferGroupDto>();
		lsTransferGroup.add(dto);
		dto.setGroupId(id);

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(false);
		standbyScheduleCopyDto.setStatusId(id);
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setLsTransferGroup(lsTransferGroup);
		String modUserName = "mx";

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setId(id);
		standbyGroup.setTitle("title");

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.anyLong())).thenReturn(listBodyTarget);

		when(archiveController.createArchiveForIntervallAndGroups(any(), any(), any(), any(), any(), any(), any()))
				.thenReturn(true);

		when(validationController.startValidation(any(), any(), any(), any(), any(), any()))
				.thenReturn(new ArrayList<>());

		result = bodyDataCopyController.copyByGroups(standbyScheduleCopyDto, modUserName);
		assertNotNull(result);

	}

	@Test
	public void copyByGroups2() throws SpException {
		PlanningMsgResponseDto result;

		Long id = new Long(2);

		StandbyStatus status = new StandbyStatus();
		status.setId(id);
		status.setTitle("title");

		List<StandbyScheduleBody> listBodySource = new ArrayList<>();
		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();
		StandbyScheduleBody sbsb = new StandbyScheduleBody();
		listBodySource.add(sbsb);

		User user = new User();
		user.setFirstname("firstname");
		user.setLastname("lastname");
		sbsb.setValidFrom(new Date());
		sbsb.setValidTo(new Date());
		sbsb.setUser(user);

		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		TransferGroupDto dto = new TransferGroupDto();
		List<TransferGroupDto> lsTransferGroup = new ArrayList<TransferGroupDto>();
		lsTransferGroup.add(dto);
		dto.setGroupId(id);

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(false);
		standbyScheduleCopyDto.setStatusId(1L);
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setLsTransferGroup(lsTransferGroup);
		String modUserName = "mx";

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setId(id);
		standbyGroup.setTitle("title");
		sbsb.setStandbyGroup(standbyGroup);

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.eq(2L))).thenReturn(listBodyTarget);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.eq(1L))).thenReturn(listBodySource);

		result = bodyDataCopyController.copyByGroups(standbyScheduleCopyDto, modUserName);
		assertNotNull(result);

	}

	@Test(expected = SpException.class)
	public void copyByGroups3() throws SpException {
		PlanningMsgResponseDto result;

		Long id = new Long(2);

		StandbyStatus status = new StandbyStatus();
		status.setId(id);
		status.setTitle("title");

		List<StandbyScheduleBody> listBodySource = new ArrayList<>();
		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();
		StandbyScheduleBody sbsb = new StandbyScheduleBody();
		listBodySource.add(sbsb);
		User user = new User();
		user.setFirstname("firstname");
		user.setLastname("lastname");
		sbsb.setValidFrom(new Date());
		sbsb.setValidTo(new Date());
		sbsb.setUser(user);

		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		TransferGroupDto dto = new TransferGroupDto();
		List<TransferGroupDto> lsTransferGroup = new ArrayList<TransferGroupDto>();
		lsTransferGroup.add(dto);
		dto.setGroupId(id);

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		// standbyScheduleCopyDto.setOverwrite(false);
		standbyScheduleCopyDto.setStatusId(1L);
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setLsTransferGroup(lsTransferGroup);
		String modUserName = "mx";

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setId(id);
		standbyGroup.setTitle("title");

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.eq(2L))).thenReturn(listBodyTarget);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.eq(1L))).thenReturn(listBodySource);

		result = bodyDataCopyController.copyByGroups(standbyScheduleCopyDto, modUserName);
		assertNotNull(result);

	}

	@Test(expected = SpException.class)
	public void copyByGroups4() throws SpException {
		PlanningMsgResponseDto result;

		Long id = new Long(2);

		StandbyStatus status = new StandbyStatus();
		status.setId(id);
		status.setTitle("title");

		List<StandbyScheduleBody> listBodySource = new ArrayList<>();
		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();
		StandbyScheduleBody sbsb = new StandbyScheduleBody();
		listBodySource.add(sbsb);
		User user = new User();
		user.setFirstname("firstname");
		user.setLastname("lastname");
		sbsb.setValidFrom(new Date());
		sbsb.setValidTo(new Date());
		sbsb.setUser(user);

		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		TransferGroupDto dto = new TransferGroupDto();
		List<TransferGroupDto> lsTransferGroup = new ArrayList<TransferGroupDto>();
		lsTransferGroup.add(dto);
		dto.setGroupId(id);

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		// standbyScheduleCopyDto.setOverwrite(false);
		standbyScheduleCopyDto.setStatusId(1L);
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setLsTransferGroup(lsTransferGroup);
		String modUserName = "mx";

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setId(id);
		standbyGroup.setTitle("title");

		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.eq(2L))).thenReturn(listBodyTarget);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.eq(1L))).thenReturn(listBodySource);

		result = bodyDataCopyController.copyByGroups(null, modUserName);
		assertNotNull(result);

	}

	@Test(expected = SpException.class)
	public void deleteByGroups() throws SpException {

		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		PlanningMsgResponseDto result;

		Long id = new Long(2);

		StandbyStatus status = new StandbyStatus();
		status.setId(id);
		status.setTitle("title");

		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();
		StandbyScheduleBody sbsb = new StandbyScheduleBody();
		StandbyGroup sbg = new StandbyGroup();
		sbg.setTitle("title");
		sbg.setId(1L);
		sbsb.setStandbyGroup(sbg);

		listBodyTarget.add(sbsb);
		User user = new User();
		user.setFirstname("firstname");
		user.setLastname("lastname");
		sbsb.setValidFrom(new Date());
		sbsb.setValidTo(new Date());
		sbsb.setUser(user);

		TransferGroupDto dto = new TransferGroupDto();
		List<TransferGroupDto> lsTransferGroup = new ArrayList<TransferGroupDto>();
		lsTransferGroup.add(dto);
		dto.setGroupId(id);

		StandbyScheduleCopyDto standbyScheduleCopyDto = new StandbyScheduleCopyDto();
		standbyScheduleCopyDto.setOverwrite(false);
		standbyScheduleCopyDto.setStatusId(id);
		standbyScheduleCopyDto.setValidFrom(new Date());
		standbyScheduleCopyDto.setValidTo(new Date());
		standbyScheduleCopyDto.setStandbyListId(1L);
		String modUserName = "mx";

		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setId(id);
		standbyGroup.setTitle("title");

		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);
		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		when(plannedDataController.getPlannedBodysForGroupIdAndIntervallAndStatus(Mockito.anyLong(),
				(Date) Mockito.any(), (Date) Mockito.any(), Mockito.anyLong())).thenReturn(listBodyTarget);

		when(archiveController.createArchiveForIntervallAndGroups(any(), any(), any(), any(), any(), any(), any()))
				.thenReturn(true);

		when(validationController.startValidation(any(), any(), any(), any(), any(), any()))
				.thenReturn(new ArrayList<>());

		result = bodyDataCopyController.deleteByGroups(standbyScheduleCopyDto, modUserName);
		assertNotNull(result);

		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(false);
		result = bodyDataCopyController.deleteByGroups(standbyScheduleCopyDto, modUserName);
		assertNotNull(result);

	}

}
