/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.LocationRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.dto.LocationSelectionDto;
import org.eclipse.openk.sp.dto.RegionDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RegionControllerTest {

	@Mock
	private RegionRepository regionRepository;

	@Mock
	private LocationRepository locationRepository;

	@Mock
	private EntityConverter entityConverter;

	@InjectMocks
	RegionController regionController;

	@Test
	public void getRegionsTest() {
		List<Region> listRegion = new ArrayList<>();
		listRegion.add(new Region());
		RegionDto dto = new RegionDto();

		Mockito.when(regionRepository.findAll()).thenReturn(listRegion);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(dto);

		List<RegionDto> result = regionController.getRegions();
		assertNotNull(result);
	}

	@Test
	public void getRegionsSelectionTest() {
		List<Region> listRegion = new ArrayList<>();
		listRegion.add(new Region());
		RegionSelectionDto dto = new RegionSelectionDto();

		Mockito.when(regionRepository.findAll()).thenReturn(listRegion);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(dto);

		List<RegionSelectionDto> result = regionController.getRegionsSelection();
		assertNotNull(result);
	}

	@Test
	public void getRegionTest() {
		Long regionid = new Long(3);

		Region region = new Region();
		RegionDto dto = new RegionDto();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(dto);

		RegionDto result = regionController.getRegion(regionid);
		assertNotNull(result);
	}

	@Test
	public void saveRegionTest() throws SpException {
		Region region = new Region();
		RegionDto regionDto = new RegionDto();
		regionDto.setId(1L);
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		RegionDto result = regionController.saveRegion(regionDto);
		assertNotNull(result);

	}
	
	@Test
	public void saveRegion2Test() throws SpException {
		Region region = new Region();
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		RegionDto result = regionController.saveRegion(regionDto);
		assertNotNull(result);

	}
	
	@Test
	public void saveRegion3Test() throws SpException {
		Region region = new Region();
		RegionDto regionDto = new RegionDto();
		regionDto.setId(1l);
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		RegionDto result = regionController.saveRegion(regionDto);
		assertNotNull(result);

	}
	
	
	@Test (expected=SpException.class)
	public void saveRegionExceptoinTest() throws SpException {
		Region region = new Region();
		RegionDto regionDto = new RegionDto();
		regionDto.setId(1L);
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
//		 Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
//		 SpException());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		RegionDto result = regionController.saveRegion(null);
		assertNotNull(result);

	}

	@Test
	public void saveLocationsForRegionTest() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.saveLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test(expected=SpException.class)
	public void saveLocationsForRegionTest2() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.saveLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test(expected=SpException.class)
	public void saveLocationsForRegionTest3() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.saveLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test(expected=SpException.class)
	public void saveLocationsForRegionTest4() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.saveLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test(expected=SpException.class)
	public void saveLocationsForRegionTest5() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(null);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.saveLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}

	@Test
	public void deleteLocationsForRegionTest() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		region.getLsLocations().add(location);
		location.getLsRegions().add(region);
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.deleteLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test(expected=SpException.class)
	public void deleteLocationsForRegion2Test() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		region.getLsLocations().add(location);
		location.getLsRegions().add(region);
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.deleteLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	@Test(expected=SpException.class)
	public void deleteLocationsForRegion3Test() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		region.getLsLocations().add(location);
		location.getLsRegions().add(region);
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.deleteLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test(expected=SpException.class)
	public void deleteLocationsForRegion4Test() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
		region.getLsLocations().add(location);
		location.getLsRegions().add(region);
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(false);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.deleteLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test
	public void deleteLocationsForRegion5Test() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
//		region.getLsLocations().add(location);
//		location.getLsRegions().add(region);
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(region);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.deleteLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
	
	@Test(expected=SpException.class)
	public void deleteLocationsForRegion6Test() throws SpException {

		Long regionId = new Long(3);
		List<LocationSelectionDto> lsLocationDtos = new ArrayList<>();
		LocationSelectionDto dto = new LocationSelectionDto();
		dto.setId(4L);
		lsLocationDtos.add(dto);

		Region region = new Region();
		Location location = new Location();
//		region.getLsLocations().add(location);
//		location.getLsRegions().add(region);
		RegionDto regionDto = new RegionDto();
		List<Location> listLocations = new ArrayList<>();

		Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenReturn(null);
		Mockito.when(regionRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(locationRepository.findOne(Mockito.anyLong())).thenReturn(location);
		Mockito.when(regionRepository.save((Region) Mockito.any())).thenReturn(region);

		// Mockito.when(regionRepository.findOne(Mockito.anyLong())).thenThrow(new
		// Exception());
		Mockito.when(entityConverter.convertDtoToEntity((AbstractDto) Mockito.any(), (AbstractEntity) Mockito.any()))
				.thenReturn(region);
		Mockito.when(entityConverter.convertEntityToDto((AbstractEntity) Mockito.any(), (AbstractDto) Mockito.any()))
				.thenReturn(regionDto);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsLocationDtos);

		List<LocationSelectionDto> result = regionController.deleteLocationsForRegion(regionId, lsLocationDtos);
		assertNotNull(result);

	}
}
