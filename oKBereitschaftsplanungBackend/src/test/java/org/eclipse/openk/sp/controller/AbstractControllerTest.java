/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertNull;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AbstractControllerTest {
	
	protected static final Logger LOGGER = Logger.getLogger(AbstractControllerTest.class);
	
	
	@InjectMocks
	AbstractController abstractController;
	
	@Mock
	UserRepository userRepository;

	@Test
	public void validateEntity() {

		AbstractEntity entity = new User();
		entity.setId(1L);
		
		Mockito.when(userRepository.findOne(Mockito.anyLong())).thenReturn(null);

		AbstractEntity result = abstractController.validateEntity(entity, userRepository, LOGGER); 
		
		assertNull(result);

	}
}