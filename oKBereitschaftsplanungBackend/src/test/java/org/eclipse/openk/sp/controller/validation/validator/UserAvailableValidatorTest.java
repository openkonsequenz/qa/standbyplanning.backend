/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation.validator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.UserController;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/** Class to validate if all time slots are set for a group. */
@RunWith(MockitoJUnitRunner.class)
public class UserAvailableValidatorTest {
	protected static final Logger LOGGER = Logger.getLogger(UserAvailableValidatorTest.class);
	Properties p = new Properties();

	@Mock
	private UserController userController;

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	UserAvailableValidator userAvailableValidator;

	@Before
	public void init() {
		p = new Properties();
		p.setProperty("placeholder.standby.user.ids", "1,2,3,4");
	}

	@Test
	public void executeTest() throws SpException {

		Date from = DateHelper.getDate(2018, 9, 9);
		Date till = DateHelper.getDate(2018, 9, 10);
		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		StandbyStatus status = new StandbyStatus();
		status.setId(1L);
		status.setTitle("TestEbene");

		List<StandbyGroup> lsStandbyGroups = new ArrayList<>();
		lsStandbyGroups.add(group);
		lsStandbyGroups.add(group);
		Long statusId = new Long(1);

		User user1 = new User();
		user1.setId(1L);
		user1.setFirstname("First");
		user1.setLastname("LastName");

		// positive test
		try {

			Mockito.when(userController.isUserInGroup(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
					.thenReturn(true);
			Mockito.when(userRepository.findOne(Mockito.any())).thenReturn(user1);
			List<PlanningMsgDto> planningMsgDtos = userAvailableValidator.execute(from, till, group, lsStandbyGroups,
					statusId, null);
			assertNotNull(planningMsgDtos);
		} catch (Exception e) {
			assertNull(e);
		}

		// negative test
		try {
			Mockito.when(userController.isUserInGroup(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
					.thenReturn(false);
			Mockito.when(userRepository.findOne(Mockito.any())).thenReturn(user1);
			List<PlanningMsgDto> planningMsgDtos = userAvailableValidator.execute(from, till, group, lsStandbyGroups,
					statusId, null);
			assertNotNull(planningMsgDtos);
			assertTrue(planningMsgDtos.size() == 1);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}
}
