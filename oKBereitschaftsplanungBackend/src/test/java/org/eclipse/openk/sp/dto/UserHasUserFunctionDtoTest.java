/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class UserHasUserFunctionDtoTest {

	@Test
	public void testGettersAndSetters() {
		
		Long l  = new Long(30);
		String t = "Text";
		Date d = new Date();
		

		UserHasUserFunctionDto uf = new UserHasUserFunctionDto();

		uf.setId(l);
		assertEquals(l, uf.getId());
		
		uf.setFunctionId(l);
		assertEquals(l, uf.getFunctionId());

		uf.setFunctionName(t);
		assertEquals(t, uf.getFunctionName());
	
	}
}
