/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;


public class StandbyScheduleBlueprintDtoTest {


	@Test
	public void getSet() {
		Date d = new Date();
		String text = "test";
		Long id = new Long(3);
		
		StandbyScheduleBlueprintDto standbyScheduleBlueprintDto = new StandbyScheduleBlueprintDto();
		standbyScheduleBlueprintDto.setStandbyGroupId(id);
		assertEquals(id, standbyScheduleBlueprintDto.getStandbyGroupId()); 
		
		standbyScheduleBlueprintDto.setStandbyListId(id);
		assertEquals(id, standbyScheduleBlueprintDto.getStandbyListId()); 
		
		standbyScheduleBlueprintDto.setStartIdOfUser(id);
		assertEquals(id, standbyScheduleBlueprintDto.getStartIdOfUser());
		
		standbyScheduleBlueprintDto.setStatusId(id);
		assertEquals(id, standbyScheduleBlueprintDto.getStatusId());
		
		standbyScheduleBlueprintDto.setValidFrom(d);
		assertEquals(d, standbyScheduleBlueprintDto.getValidFrom());
		
		standbyScheduleBlueprintDto.setValidTo(d);
		assertEquals(d, standbyScheduleBlueprintDto.getValidTo());
		
	}

}