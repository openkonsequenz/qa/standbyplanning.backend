/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.UserFunctionRepository;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.dto.UserFunctionDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle userFunction operations. */
@RunWith(MockitoJUnitRunner.class)
public class UserFunctionControllerTest {
	protected static final Logger logger = Logger.getLogger(UserFunctionController.class);

	@Mock
	UserFunctionRepository userFunctionRepository;
	@Mock
	EntityConverter entityConverter;

	@InjectMocks
	UserFunctionController userFunctionController;

	@Test
	public void saveUserFunctionErrorTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserFunction.json");
		UserFunctionDto userFunctionDto = (UserFunctionDto) new Gson().fromJson(json, UserFunctionDto.class);
		userFunctionDto.setId(1l);
		UserFunction userFunction = (UserFunction) new Gson().fromJson(json, UserFunction.class);
		userFunction.setId(1l);

		// create when
		when(userFunctionRepository.findOne(any())).thenReturn(userFunction);
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) userFunction);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userFunctionDto);

		// run test
		UserFunctionDto userFunctionResult = null;

		userFunctionResult = userFunctionController.saveUserFunction(userFunctionDto);
		assertNotNull(userFunctionResult);
		assertEquals(1l, userFunctionResult.getId().longValue());
	}

	@Test
	public void saveUserFunctionTest() {
		Long id = new Long(20);
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserFunction.json");
		UserFunctionDto userFunctionDto = (UserFunctionDto) new Gson().fromJson(json, UserFunctionDto.class);
		userFunctionDto.setId(id);
		userFunctionDto.setFunctionId(id);
		UserFunction userFunction = (UserFunction) new Gson().fromJson(json, UserFunction.class);
		userFunction.setId(id);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) userFunction);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userFunctionDto);
		when(userFunctionRepository.save(userFunction)).thenReturn(userFunction);
		when(userFunctionRepository.findOne(any())).thenReturn(userFunction);

		// run test
		UserFunctionDto userFunctionResult = null;

		userFunctionResult = userFunctionController.saveUserFunction(userFunctionDto);

		assertNotNull(userFunctionResult);
		assertEquals(id, userFunctionResult.getId());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUserFunctionsTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserFunctionList.json");
		List<UserFunction> lsUserFunctionMock = (List<UserFunction>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USERFUNCTION_ARRAY_LIST);

		json = fh.loadStringFromResource("testUserFunction.json");
		UserFunctionDto userFunctionDtoMock = (UserFunctionDto) new Gson().fromJson(json, UserFunctionDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userFunctionDtoMock);
		when(userFunctionRepository.findAllByOrderByFunctionNameAsc()).thenReturn(lsUserFunctionMock);

		// run test
		List<UserFunctionDto> lsOutputUserDto = userFunctionController.getUserFunctions();

		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
		assertTrue(lsOutputUserDto.get(0) instanceof UserFunctionDto);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUserFunctionsSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserFunctionList.json");
		List<UserFunction> lsUserFunctionMock = (List<UserFunction>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_USERFUNCTION_ARRAY_LIST);

		json = fh.loadStringFromResource("testUserFunction.json");
		UserFunctionSelectionDto userFunctionSelectionDtoMock = (UserFunctionSelectionDto) new Gson().fromJson(json,
				UserFunctionSelectionDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userFunctionSelectionDtoMock);
		when(userFunctionRepository.findAllByOrderByFunctionNameAsc()).thenReturn(lsUserFunctionMock);

		// run test
		List<UserFunctionSelectionDto> lsOutputUserDto = userFunctionController.getUserFunctionsSelection();

		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
		assertTrue(lsOutputUserDto.get(0) instanceof UserFunctionSelectionDto);
	}

	@Test
	public void getUserFunction() {
		// create test data
		long id = 5;
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testUserFunction.json");
		UserFunction userFunctionMock = (UserFunction) new Gson().fromJson(json, UserFunction.class);
		userFunctionMock.setId(id);
		UserFunctionDto userFunctionDtoMock = (UserFunctionDto) new Gson().fromJson(json, UserFunctionDto.class);
		userFunctionDtoMock.setId(id);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userFunctionDtoMock);
		when(userFunctionRepository.findOne(id)).thenReturn(userFunctionMock);

		// run test
		UserFunctionDto userFunctionDto = userFunctionController.getUserFunction(id);
		assertNotNull(userFunctionDto);
		assertTrue(userFunctionDto.getId() == id);
	}
}
