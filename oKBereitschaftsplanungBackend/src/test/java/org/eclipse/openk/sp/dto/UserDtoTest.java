/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class UserDtoTest {

	@Test
	public void testGettersAndSetters() {
		String test = "test";
		Date d = new Date();
		UserDto u = new UserDto();
		ContactDataDto cd = new ContactDataDto();

		u.setId(1l);
		assertEquals(1l, u.getId().longValue());

		u.setBusinessContactData(cd);
		assertEquals(cd, u.getBusinessContactData());

		u.setFirstname("FirstName");
		assertEquals("FirstName", u.getFirstname());

		u.setIsCompany(true);
		assertEquals(true, u.getIsCompany());

		u.setLastname("LastName");
		assertEquals("LastName", u.getLastname());

		u.setHrNumber(test);
		assertEquals(test, u.getHrNumber());

		u.setUserKey(test);
		assertEquals(test, u.getUserKey());

		u.setModificationDate(d);
		assertEquals(d, u.getModificationDate());

		u.setNotes("This is a place to mak e a notice.");
		assertEquals("This is a place to mak e a notice.", u.getNotes());

		OrganisationDto orga = new OrganisationDto();
		u.setOrganisation(orga);
		assertEquals(orga, u.getOrganisation());

		AddressDto add = new AddressDto();
		u.setPrivateAddress(add);
		assertEquals(add, u.getPrivateAddress());

		u.setPrivateContactData(cd);
		assertEquals(cd, u.getPrivateContactData());

		u.setValidFrom(d);
		assertEquals(d, u.getValidFrom());

		u.setValidTo(d);
		assertEquals(d, u.getValidTo());

		UserInRegionDto userInRegionDto = new UserInRegionDto();
		List<UserInRegionDto> lsUserInRegions = new ArrayList<UserInRegionDto>();
		lsUserInRegions.add(userInRegionDto);
		u.setLsUserInRegions(lsUserInRegions);
		assertNotNull(u.getLsUserInRegions());

		List<UserHasUserFunctionDto> lsUserFunctions = new ArrayList<>();
		UserHasUserFunctionDto uf = new UserHasUserFunctionDto();
		lsUserFunctions.add(uf);
		u.setLsUserFunctions(lsUserFunctions);
		assertNotNull(u.getLsUserFunctions());
		assertEquals(uf, u.getLsUserFunctions().get(0));
	}
}
