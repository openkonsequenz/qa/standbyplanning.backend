/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidationHelperTest {

	@Test
	public void isNoNullValueInList() throws SpException {

		List<Object> lsValues = new ArrayList<>();
		lsValues.add("test");
		lsValues.add(new Date());
		lsValues.add(1L);
		lsValues.add(new UserDto());

		Boolean isTrue = false;

		try {
			isTrue = ValidationHelper.isNoNullValueInList(lsValues);
			assertTrue(isTrue);
		} catch (Exception e) {
			assertNull(e);
		}

		try {
			lsValues.add(null);
			isTrue = ValidationHelper.isNoNullValueInList(lsValues);
			assertFalse(isTrue);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}
}
