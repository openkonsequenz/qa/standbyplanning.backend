/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class UserFunctionTest {

	@Test
	public void testGettersAndSetters() {

		UserFunction uf = new UserFunction();

		uf.setId(1l);
		assertEquals(1l, uf.getId().longValue());

		uf.setFunctionName("Name");
		assertEquals("Name", uf.getFunctionName());

		List<UserHasUserFunction> lsUserHasUserFunction = new ArrayList<>();
		UserHasUserFunction userHasFunction = new UserHasUserFunction();
		lsUserHasUserFunction.add(userHasFunction);
		uf.setLsUserHasUserFunction(lsUserHasUserFunction);
		assertNotNull(uf.getLsUserHasUserFunction());
		assertEquals(userHasFunction, uf.getLsUserHasUserFunction().get(0));

		uf.setLsStandbyGroups(new ArrayList<StandbyGroup>());
		assertNotNull(uf.getLsStandbyGroups());
	}

	@Test
	public void testCompare() throws MalformedURLException {
		UserFunction obj1 = new UserFunction();
		obj1.setId(5L);

		UserFunction obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		UserFunction obj3 = new UserFunction();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));
	}

	@Test
	public void testCopy() {
		UserFunction obj1 = new UserFunction();
		obj1.setId(5L);

		UserFunction obj2 = obj1.copy();
		assertEquals(null, obj2.getId());
		assertNotEquals(obj1.hashCode(), obj2.hashCode());
	}
}
