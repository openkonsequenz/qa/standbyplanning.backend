/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.LocationController;
import org.eclipse.openk.sp.controller.RegionController;
import org.eclipse.openk.sp.dto.LocationSelectionDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class RegionRestServiceTest {

	@InjectMocks
	private RegionRestService regionRestService;

	@Mock
	private RegionController regionController;

	@Mock
	private LocationController locationController;

	public RegionRestServiceTest() {

	}

	@Test
	public void getRegionsTest() {
		String jwt = "LET-ME-IN";
		Response result = regionRestService.getRegions(jwt);
		assertNotNull(result);
	}

	@Test
	public void getRegionsSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = regionRestService.getRegionsSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getRegionTest() {
		String jwt = "LET-ME-IN";
		Response result = regionRestService.getRegion(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void saveRegionTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testRegionSelection.json");
		RegionSelectionDto regionDto = (RegionSelectionDto) new Gson().fromJson(json, RegionSelectionDto.class);

		String jwt = "LET-ME-IN";
		Response result = regionRestService.saveRegionDto(jwt, regionDto);
		assertNotNull(result);
	}

	@Test
	public void saveLocationsForRegionTest() {
		List<LocationSelectionDto> lsLocationDto = new ArrayList<LocationSelectionDto>();
		lsLocationDto.add(new LocationSelectionDto());
		String jwt = "LET-ME-IN";

		Response result = regionRestService.saveLocationsForRegion(1l, jwt, lsLocationDto);
		assertNotNull(result);
	}

	@Test
	public void deleteLocationsForRegionTest() {
		List<LocationSelectionDto> lsLocationDto = new ArrayList<LocationSelectionDto>();
		lsLocationDto.add(new LocationSelectionDto());
		String jwt = "LET-ME-IN";

		Response result = regionRestService.deleteLocationsForRegion(1l, jwt, lsLocationDto);
		assertNotNull(result);
	}

}
