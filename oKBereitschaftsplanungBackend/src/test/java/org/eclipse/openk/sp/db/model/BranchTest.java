/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.ArrayList;

import org.junit.Test;

public class BranchTest {

	@Test
	public void testGettersAndSetters() {

		Branch b = new Branch();

		b.setId(1l);
		assertEquals(1l, b.getId().longValue());

		b.setTitle("title");
		assertEquals("title", b.getTitle());

		b.setLsStandbyGroups(new ArrayList<StandbyGroup>());
		assertNotNull(b.getLsStandbyGroups());

		b.setLsLocationForBranches(new ArrayList<LocationForBranch>());
		assertNotNull(b.getLsLocationForBranches());
	}

	@Test
	public void testCompare() throws MalformedURLException {
		Branch obj1 = new Branch();
		obj1.setId(5L);

		Branch obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		Branch obj3 = new Branch();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));
	}

	@Test
	public void testConstructor() {
		Branch obj1 = new Branch();
		obj1.setId(5L);

		assertEquals(new Long(5), obj1.getId());

		Branch obj2 = new Branch(7L);
		assertEquals(new Long(7), obj2.getId());

	}

	@Test
	public void testCopy() {
		Branch obj1 = new Branch();
		obj1.setId(5L);

		Branch obj2 = obj1.copy();
		assertEquals(null, obj2.getId());
		assertNotEquals(obj1.hashCode(), obj2.hashCode());
	}
}
