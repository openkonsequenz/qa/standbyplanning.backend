/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.CalendarController;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class CalendarDayRestServiceTest {

	@InjectMocks
	private CalendarRestService calendarService;

	@Mock
	private CalendarController calendarController;

	public CalendarDayRestServiceTest() {

	}

	@Test
	public void getCalendarDaysTest() {
		String jwt = "LET-ME-IN";
		Response result = calendarService.getCalendarDays(jwt);
		assertNotNull(result);
	}
	
	@Test
	public void getCalendarDaysSelectionsTest() {
		String jwt = "LET-ME-IN";
		Response result = calendarService.getCalendarDaySelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getCalendarDayTest() {
		String jwt = "LET-ME-IN";
		Response result = calendarService.getCalendarDay(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void saveCalendarDayTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testCalendarDay.json");
		CalendarDayDto calendarDayDto = (CalendarDayDto) new Gson().fromJson(json, CalendarDayDto.class);

		String jwt = "LET-ME-IN";
		Response result = calendarService.saveCalendarDay(jwt, calendarDayDto);
		assertNotNull(result);
	}
	
	
	@Test
	public void deleteCalendarDayTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testCalendarDay.json");
		CalendarDayDto calendarDayDto = (CalendarDayDto) new Gson().fromJson(json, CalendarDayDto.class);

		String jwt = "LET-ME-IN";
		Response result = calendarService.deleteCalendarDay(jwt, calendarDayDto);
		assertNotNull(result);
	}

}
