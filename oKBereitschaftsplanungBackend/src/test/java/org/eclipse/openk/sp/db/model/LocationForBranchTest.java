/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class LocationForBranchTest {

	@Test
	public void testGettersAndSetters() {
		LocationForBranch l = new LocationForBranch();

		l.setId(1l);
		assertEquals(1l, l.getId().longValue());
		
		l.setBranch(new Branch());
		assertNotNull(l.getBranch());
		
		l.setLocation(new Location());
		assertNotNull(l.getLocation());
		
		l.setValidFrom(new Date());
		assertNotNull(l.getValidFrom());
		
		l.setValidTo(new Date());
		assertNotNull(l.getValidTo());
	}

	@Test
	public void testCompare() throws MalformedURLException {
		LocationForBranch obj1 = new LocationForBranch();
		obj1.setId(5L);

		LocationForBranch obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		LocationForBranch obj3 = new LocationForBranch();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

	@Test
	public void testConstructor() {
		LocationForBranch obj1 = new LocationForBranch();
		obj1.setId(5L);

		assertEquals(new Long(5), obj1.getId());

		LocationForBranch obj2 = new LocationForBranch(7L);
		assertEquals(new Long(7), obj2.getId());

	}
}
