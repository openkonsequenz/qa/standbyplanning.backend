/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class PostcodeTest {

	@Test
	public void testGettersAndSetters() {

		Postcode l = new Postcode();
		assertNotNull(l);
		
		l = new Postcode("33100");
		assertNotNull(l);

		l.setId(1l);
		assertEquals(1l, l.getId().longValue());

		l.setPcode("33100");
		assertEquals("33100", l.getPcode());
		
		List<Location> lsLocation = new ArrayList<>();
		lsLocation.add(new Location());
		l.setLsLocation(lsLocation);
		
		assertEquals(1, l.getLsLocation().size());
		

	}
	
	@Test
	public void testCompare() throws MalformedURLException {
		Postcode obj1 = new Postcode();
		obj1.setId(5L);

		Postcode obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		Postcode obj3 = new Postcode();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}
	
	@Test
	public void testConstructor() {
		Postcode obj1 = new Postcode();
		obj1.setId(5L);

		assertEquals(new Long(5), obj1.getId());

		Postcode obj2 = new Postcode(7L);
		assertEquals(new Long(7), obj2.getId());

	}
}
