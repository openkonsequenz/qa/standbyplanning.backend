/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.controller.BaseWebService.ModifyingInvokable;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.controller.CalendarController;
import org.eclipse.openk.sp.exceptions.SPExceptionParser;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.TransactionSystemException;

@RunWith(MockitoJUnitRunner.class)
public class BaseWebServiceTest {

	@Mock
	CalendarController calendarController;

	@Mock
	FileHelper fileHelper;

	private String payloadUser_bp_admin = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiOiJmNGIzODJhNy00MDllLTRjYjUtOTI0NC1mNWRmN2Y4YTBlYjAiLCJleHAiOjE1Mjg5ODY1OTgsIm5iZiI6MCwiaWF0IjoxNTI4OTg2Mjk4LCJpc3MiOiJodHRwOi8vMTAuMS4xOC4xNzU6ODM4MC9hdXRoL3JlYWxtcy9PcGVuS1JlYWxtIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjY0MzIwODJiLTdkNTEtNGM3NC1iZGUwLTViM2VlNTg5MGE4YyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJlNjYwNzAxYi0wYTExLTQ1NjAtYjBjMC0zN2UzZjFkNmFhMjgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIkJQX0FkbWluIiwiQlBfU2FjaGJlYXJiZWl0ZXIiLCJCUF9HcnVwcGVubGVpdGVyIiwiQlBfTGVzZWJlcmVjaHRpZ3RlIiwidW1hX2F1dGhvcml6YXRpb24iLCJwbGFubmluZy1hY2Nlc3MiXX0sInJlc291cmNlX2FjY2VzcyI6eyJzdGFuZGJ5cGxhbm5pbmciOnsicm9sZXMiOlsiQlBfQWRtaW4iXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3VtYV9hdXRob3JpemF0aW9uLCBwbGFubmluZy1hY2Nlc3MsIEJQX0dydXBwZW5sZWl0ZXIsIEJQX1NhY2hiZWFyYmVpdGVyLCBvZmZsaW5lX2FjY2VzcywgQlBfQWRtaW4sIEJQX0xlc2ViZXJlY2h0aWd0ZV0iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiJ9.ejcrQJWCyY9rWEnQ-hMW9MzVbzx5h7jCvv3SO8krm03shFHZOprkLAvCb6vKJjGDapX7H58ctS-p6i-v4kW34ocnV7gqMof0dq6m42uKUhsA61urqb8QkvWDd03p1cF6l73qNKhOXe9eJBSgyvYenvzWf9auFeBv2CpmsVojos1iwdhZP0D_vBPqCaoL4BnF974gdqG3Vp49JQDAMpKdxMB2RsXMO01Flxu0mXPp8ejqMnh8LMFyKrNshWdRuh4HlRLs7fUvi5CN-9-DcuhJ9IzIs7WupK1Ot6VZSF6l0VESfOxmHVDBGB7eCsN9sB0P00-elMO-irCGRrzAnTr7OQ";

	private static final org.apache.log4j.Logger EMPTYLOGGER = org.apache.log4j.Logger
			.getLogger(BaseWebServiceTest.class.getName());

	private BaseWebService createBaseWebService() {
		return new BaseWebService(EMPTYLOGGER, fileHelper) {
			@Override
			protected void assertAndRefreshToken(String token, String[] secureType) throws HttpStatusException {

			}
		};
	}

	private BaseWebService createBaseWebService2() {
		return new BaseWebService(EMPTYLOGGER, fileHelper) {
			@Override
			protected void assertAndRefreshToken(String token, String[] secureType) throws HttpStatusException {
				throw new HttpStatusException(Integer.parseInt(token));
			}
		};
	}

	private BaseWebService createBaseWebService3() {
		return new BaseWebService(EMPTYLOGGER, fileHelper) {
			@Override
			protected void assertAndRefreshToken(String token, String[] secureType) throws HttpStatusException {
				throw new TransactionSystemException("test",
						new Exception(token + " ERROR TEST FEHLER TEST Error Code: 9999 Detail: test"));
			}
		};
	}

	private BaseWebService.ModifyingInvokable createInvokable(Object o, Exception e2Return) {
		return (chuser) -> {
			if (e2Return == null) {
				return o;
			} else {
				try {
					throw e2Return;
				} catch (Exception e) {
				}
				return null;
			}
		};
	}

	@Test
	public void testInvokeRunnableOK() {

		String[] arrSecureType = Globals.getAllRolls();
		Response r = createBaseWebService().invokeRunnable(payloadUser_bp_admin, arrSecureType,
				createInvokable("Test Ok", null));

		assertEquals(200, r.getStatus());
	}

	@Test
	public void testInvokeRunnableStreamOK() {
		File f;
		StreamingOutput r = null;
		try {
			f = File.createTempFile("test", "stream");

			String[] arrSecureType = Globals.getAllRolls();
			r = createBaseWebService().invokeRunnableStream(payloadUser_bp_admin, arrSecureType,
					createInvokable(f, null));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(r);
	}

	@Test
	public void testInvokeRunnable_401Exception() {

		String[] arrSecureType = Globals.getAllRolls();

		Response r = createBaseWebService2().invokeRunnable(null, arrSecureType,
				createInvokable(null, new Exception("hallodri")));

		assertEquals(500, r.getStatus());
	}

	@Test(expected = WebApplicationException.class)
	public void testInvokeRunnableStream_ExceptionTest() throws IOException {
		File f = null;
		StreamingOutput r = null;

		f = File.createTempFile("test", "stream");

		String[] arrSecureType = Globals.getAllRolls();
		r = createBaseWebService().invokeRunnableStream(null, arrSecureType, createInvokable(f, null));

		assertNotNull(r);
	}

	@Test
	public void testInvokeRunnable_HTTPException() {
		TestController testController = new TestController();
		ModifyingInvokable<String> invokable = modusr -> testController.getSpException();

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		BaseWebService bws = createBaseWebService();
		Response result = bws.invokeRunnable(payloadUser_bp_admin, securityRoles, invokable);
		assertEquals(400, result.getStatus());
	}

	@Test(expected = WebApplicationException.class)
	public void testInvokeRunnableStream_WebExceptionTest() {
		Exception f = null;
		StreamingOutput r = null;

		f = new Exception("test");

		String[] arrSecureType = Globals.getAllRolls();
		r = createBaseWebService().invokeRunnableStream(payloadUser_bp_admin, arrSecureType, createInvokable(f, null));

		assertNotNull(r);
	}

	@Test
	public void testInvokeRunnable_409Exception() {
		TestController testController = new TestController();
		ModifyingInvokable<String> invokable = modusr -> testController.getSpException1001();

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		BaseWebService bws = createBaseWebService();
		Response result = bws.invokeRunnable(payloadUser_bp_admin, securityRoles, invokable);
		assertEquals(409, result.getStatus());
	}

	@Test
	public void testInvokeRunnable_HttpStatusException401() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getHttpStatusException("401");

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService2();
			Response result = bws.invokeRunnable("401", securityRoles, invokable);
			assertEquals(401, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Test
	public void testInvokeRunnable_HttpStatusException404() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getHttpStatusException("404");

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService2();
			Response result = bws.invokeRunnable("404", securityRoles, invokable);
			assertEquals(401, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Test
	public void testInvokeRunnable_HttpStatusException500() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getHttpStatusException("500");

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService2();
			Response result = bws.invokeRunnable("500", securityRoles, invokable);
			assertEquals(500, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Test
	public void testInvokeRunnable_HttpStatusException501() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getHttpStatusException("501");

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService2();
			Response result = bws.invokeRunnable("501", securityRoles, invokable);
			assertEquals(1000, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Test
	public void testInvokeRunnable_TransactionSystemException1() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getTransactionSystemException1();

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService3();
			Response result = bws.invokeRunnable(SPExceptionParser.MSG_FOREIGN_KEY_CONSTRAINT, securityRoles,
					invokable);
			assertEquals(500, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Test
	public void testInvokeRunnable_TransactionSystemException2() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getTransactionSystemException1();

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService3();
			Response result = bws.invokeRunnable(SPExceptionParser.MSG_NOT_NULL_CONSTRAINT, securityRoles, invokable);
			assertEquals(500, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Test
	public void testInvokeRunnable_TransactionSystemException3() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getTransactionSystemException1();

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService3();
			Response result = bws.invokeRunnable(SPExceptionParser.MSG_NULL_VALUE, securityRoles, invokable);
			assertEquals(500, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Test
	public void testInvokeRunnable_TransactionSystemException4() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getTransactionSystemException1();

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService3();
			Response result = bws.invokeRunnable(SPExceptionParser.MSG_VALUE_TO_LONG, securityRoles, invokable);
			assertEquals(500, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Test
	public void testInvokeRunnable_TransactionSystemException5() {
		try {

			TestController testController = new TestController();
			ModifyingInvokable<String> invokable = modusr -> testController.getTransactionSystemException1();

			String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

			// return response
			BaseWebService bws = createBaseWebService3();
			Response result = bws.invokeRunnable("Keiner der vorgabetexte", securityRoles, invokable);
			assertEquals(500, result.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
