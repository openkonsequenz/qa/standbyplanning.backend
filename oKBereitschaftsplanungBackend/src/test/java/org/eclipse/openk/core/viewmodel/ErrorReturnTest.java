/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.viewmodel;

import static org.eclipse.openk.common.JsonGeneratorBase.getGson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ErrorReturnTest {

	@Test
	public void TestStructureAgainstJson() {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testErrorReturn.json");
		ErrorReturn errRet = getGson().fromJson(json, ErrorReturn.class);

		assertFalse(errRet.getErrorText().isEmpty());
		assertEquals(999, errRet.getErrorCode());
	}

	@Test
	public void TestSetters() {
		ErrorReturn errRet = new ErrorReturn(1, "test");

		errRet.setErrorCode(55);
		assertEquals(55, errRet.getErrorCode());

		errRet.setErrorText("neuerText");
		assertEquals("neuerText", errRet.getErrorText());

	}

}
