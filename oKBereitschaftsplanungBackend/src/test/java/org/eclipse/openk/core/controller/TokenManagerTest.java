/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.auth2.model.JwtPayload;
import org.eclipse.openk.auth2.util.JwtHelper;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TokenManagerTest {

	// @InjectMocks
	// CloseableHttpClient httpClient = HttpClientBuilder.create().build();

	// @InjectMocks
	// RestServiceWrapper restServiceWrapper = new
	// RestServiceWrapper(BackendConfig.getInstance().getPortalBaseUrl(),
	// false);

	private TokenManager tokenManager;

	private String payloadUser_bp_admin = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiOiJmNGIzODJhNy00MDllLTRjYjUtOTI0NC1mNWRmN2Y4YTBlYjAiLCJleHAiOjE1Mjg5ODY1OTgsIm5iZiI6MCwiaWF0IjoxNTI4OTg2Mjk4LCJpc3MiOiJodHRwOi8vMTAuMS4xOC4xNzU6ODM4MC9hdXRoL3JlYWxtcy9PcGVuS1JlYWxtIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjY0MzIwODJiLTdkNTEtNGM3NC1iZGUwLTViM2VlNTg5MGE4YyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJlNjYwNzAxYi0wYTExLTQ1NjAtYjBjMC0zN2UzZjFkNmFhMjgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIkJQX0FkbWluIiwiQlBfU2FjaGJlYXJiZWl0ZXIiLCJCUF9HcnVwcGVubGVpdGVyIiwiQlBfTGVzZWJlcmVjaHRpZ3RlIiwidW1hX2F1dGhvcml6YXRpb24iLCJwbGFubmluZy1hY2Nlc3MiXX0sInJlc291cmNlX2FjY2VzcyI6eyJzdGFuZGJ5cGxhbm5pbmciOnsicm9sZXMiOlsiQlBfQWRtaW4iXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3VtYV9hdXRob3JpemF0aW9uLCBwbGFubmluZy1hY2Nlc3MsIEJQX0dydXBwZW5sZWl0ZXIsIEJQX1NhY2hiZWFyYmVpdGVyLCBvZmZsaW5lX2FjY2VzcywgQlBfQWRtaW4sIEJQX0xlc2ViZXJlY2h0aWd0ZV0iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiJ9.ejcrQJWCyY9rWEnQ-hMW9MzVbzx5h7jCvv3SO8krm03shFHZOprkLAvCb6vKJjGDapX7H58ctS-p6i-v4kW34ocnV7gqMof0dq6m42uKUhsA61urqb8QkvWDd03p1cF6l73qNKhOXe9eJBSgyvYenvzWf9auFeBv2CpmsVojos1iwdhZP0D_vBPqCaoL4BnF974gdqG3Vp49JQDAMpKdxMB2RsXMO01Flxu0mXPp8ejqMnh8LMFyKrNshWdRuh4HlRLs7fUvi5CN-9-DcuhJ9IzIs7WupK1Ot6VZSF6l0VESfOxmHVDBGB7eCsN9sB0P00-elMO-irCGRrzAnTr7OQ";
	private String payloadUser_bp_leiter = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiOiI2OGY5NDc1ZC03ODk3LTRiY2EtOWJkMy0wZGIzNjA2MmI0NWMiLCJleHAiOjE1Mjg5ODc4MzgsIm5iZiI6MCwiaWF0IjoxNTI4OTg3NTM4LCJpc3MiOiJodHRwOi8vMTAuMS4xOC4xNzU6ODM4MC9hdXRoL3JlYWxtcy9PcGVuS1JlYWxtIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImQ2OTY3MTZhLWFkYzEtNDAwNS1iZGRkLTE0N2UxY2NiOWU4MCIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI5M2FmNTQ4Ni05ZjIxLTQ5MjktODRhMy02Y2IwNWJmNDQ5NWIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIkJQX0dydXBwZW5sZWl0ZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInBsYW5uaW5nLWFjY2VzcyJdfSwicmVzb3VyY2VfYWNjZXNzIjp7InN0YW5kYnlwbGFubmluZyI6eyJyb2xlcyI6WyJCUF9HcnVwcGVubGVpdGVyIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJyb2xlcyI6Ilt1bWFfYXV0aG9yaXphdGlvbiwgcGxhbm5pbmctYWNjZXNzLCBCUF9HcnVwcGVubGVpdGVyLCBvZmZsaW5lX2FjY2Vzc10iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9sZWl0ZXIifQ.ZHIXEhQUkrXxLaToPjmFUSOLu1NaoOVe7hCHyiSWIOEmrjOTsgUs0b2GKVG18Kl_2a-6VCiKTSz2fHC8rRhbUD4DkimqOkeZLPq7Ni2cEgRuK-EWSr65BE0Y1jPCY7FIKVzfbwxCTU4W2wkJx9-T6LXfxjNAjYJGhJoIO0V_aPvM_dg6RJGALa8uuN1tJ1OCdZI3Jk37JAq-Hky-DtGSdp-2D7JHDdqDc_iv_6lDBuEsHCYfbacWysN_jRaR4hbYDVNIgOIE9lNevLN9jt6r7SSPqqpkb2AvYCNPFCB7dF8MpvamL5N9dfAkrWCJCdCu2fddg0yHwWPipmgBiuGK8A";
	private String payloadUser_bp_bearbeiter = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiOiIyZmJmYjNhYS1jYjg5LTQ5YmQtOTI5Ny01YzFlYTI1Yjc3NzciLCJleHAiOjE1Mjg5ODY3MDAsIm5iZiI6MCwiaWF0IjoxNTI4OTg2NDAwLCJpc3MiOiJodHRwOi8vMTAuMS4xOC4xNzU6ODM4MC9hdXRoL3JlYWxtcy9PcGVuS1JlYWxtIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImU1M2NlZWM4LWM2YWItNDViMy05ZDBmLWJhYTgyMDA5MzViZiIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJiMmFjNjNmNi1iNGRjLTRmMjMtYTNjYi0zNjljYWIwNDAyZGUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIkJQX1NhY2hiZWFyYmVpdGVyIiwidW1hX2F1dGhvcml6YXRpb24iLCJwbGFubmluZy1hY2Nlc3MiXX0sInJlc291cmNlX2FjY2VzcyI6eyJzdGFuZGJ5cGxhbm5pbmciOnsicm9sZXMiOlsiQlBfU2FjaGJlYXJiZWl0ZXIiXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3VtYV9hdXRob3JpemF0aW9uLCBwbGFubmluZy1hY2Nlc3MsIEJQX1NhY2hiZWFyYmVpdGVyLCBvZmZsaW5lX2FjY2Vzc10iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9iZWFyYmVpdGVyIn0.mhh2WZ9XRwkqBXqfR7f755GoxgZ-mCGrCo4StnhIAjm8HzzkgXX-wJBORe8OUgTgAkpCuE0f6j-YK9JaGD0ZBI4f43NRxp91XkaPGJyZeQhanIt39kipzGyyhKu9gaLb3KW6iSt7-kbr-tUCqnBdwjOT8jEDLB2LFqUEmnLz7xTrkoRPDu4rMgM8rzl2yK_BsUnzRsR4_FE8Q8qlKz2ZOqitOIDutiGDoyndpUaHxUM9xdGtStqP9sJFKiXwLdTpK8z8hPYG9-mp7tz0vE3T7o2Rq5d83PcbgInvkt4SBpBr7VNVPvUILFmDJERi02s6MdpJWeRN3tnGCH1aW2myAg";
	private String payloadUser_bp_leser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiOiJkNTlkY2JlNS04MTg4LTQ2ZjUtOGEzNi01NDA0OTY4YTQ4ZDIiLCJleHAiOjE1Mjg5ODY0NzksIm5iZiI6MCwiaWF0IjoxNTI4OTg2MTc5LCJpc3MiOiJodHRwOi8vMTAuMS4xOC4xNzU6ODM4MC9hdXRoL3JlYWxtcy9PcGVuS1JlYWxtIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImFmNzI4NTI0LWY4NWItNGU5MC05Yzk3LWUyMDVlZDE5MDk0OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI0MDIwZTgzOC1iMjRhLTQ2ODItYjNlYy1hNzE5ZDE3OTA3YmUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsic3RhbmRieXBsYW5uaW5nIjp7InJvbGVzIjpbIkJQX0xlc2ViZXJlY2h0aWd0ZSJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbdW1hX2F1dGhvcml6YXRpb24sIHBsYW5uaW5nLWFjY2Vzcywgb2ZmbGluZV9hY2Nlc3MsIEJQX0xlc2ViZXJlY2h0aWd0ZV0iLCJuYW1lIjoibGUgc2VyIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYnBfbGVzZXIiLCJnaXZlbl9uYW1lIjoibGUiLCJmYW1pbHlfbmFtZSI6InNlciJ9.g2iZGkIZNRKI8ngts0FfVplnjxwqnAzsBTEPV9vyBqCimHOI6iACfAyYgL7XpAlF0O0_GcXPIrDGbOO0sSBZvN46LVvUw-4MoP9x2u13Xz7ZTJtlPRaSOcLch-TfrZC3nZVG6EST6qU8acu6-DZFM6U__FkrhMzqXJbcog0cILqLf_finofJdXzJsPSnsDh-Meby4YaknxSGAr5gTUe3NNLAsuS_uUbAlRiLQ8-BU93nRschXr05of4hOVJ9jDCCZG1I0KVE8xIMhZb94QVUoR51hsbUJ4fYfsaZDx1JbLH0Rh_cyiWHoPjykI4Uh4ZJy0xwiSuv7hZCuvwAY1QZcQ";

	// only elogbook user - jasper - he has not sp access rights
	private String payloadRamboUser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiIwMWVjYjdiOC01MDQ0LTQwZTEtYmI2Ny00ZmZhMjQwYWNkYzkiLCJleHAiOjE1MjE3MTIyNDAsIm5iZiI6MCwiaWF0IjoxNTIxNzExOTQwLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6Ijc5YzQzNWFkLWQ3ZGQtNDRjMC1iMWQ1LTY1OWIwYmMyYzc3ZiIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJmYmU1NzFlOS00ODQ1LTQ0NjYtYWU4MC0yN2JiYmFhYTBlMzIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJyb2xlcyI6IltlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ikphc3BlciBNYWdpY2lhbiIsInByZWZlcnJlZF91c2VybmFtZSI6Imphc3BlciIsImdpdmVuX25hbWUiOiJKYXNwZXIiLCJmYW1pbHlfbmFtZSI6Ik1hZ2ljaWFuIn0.s8M-dVtFWFHEkK5-mN6eUeQbPhqFdEI-Y5ovxUejBYi25-rzR4UGKrxdrOyDXBbAAHsZ9tAtDveh-lBh4xny3b82bdgMiYft3Wpsa_4EhweqM39dz1oROHVlUqFVTH2nmp-FXyvMpdVKj81sOgvGHD9YobgNuJiDWX6ohxS5tR3NU9CbDpUz81jd0gsYH0ZelKQjEinmmCnx2DdYKJPawSUMgTIhR_m_ea60ujwTN2dPTxU09HwtPlLTRSHf9I2uxsdxJrwaIIL-HNVsMBiAhXcRanEikMkW9YBBwgjWny87A1iwg8Y-tH06iFwc5qAYxAnX8eafaT0yOiWELQPXdg";

	JwtPayload jwtPayloadUser_bp_admin = JwtHelper.getJwtPayload(payloadUser_bp_admin);
	JwtPayload jwtPayloadUser_bp_leiter = JwtHelper.getJwtPayload(payloadUser_bp_leiter);
	JwtPayload jwtPayloadUser_bp_bearbeiter = JwtHelper.getJwtPayload(payloadUser_bp_bearbeiter);
	JwtPayload jwtPayloadUser_bp_leser = JwtHelper.getJwtPayload(payloadUser_bp_leser);

	JwtPayload jwtPayloadRamboUser = JwtHelper.getJwtPayload(payloadRamboUser);

	public TokenManagerTest() throws HttpStatusException {
	}

	@Before
	public void init() {
//		BackendConfig.configure("istThePortalBaseURL");
		this.tokenManager = TokenManager.getInstance();
	}

//	@Test(expected = HttpStatusException.class)
//	public void testlogout() throws HttpStatusException {
//		tokenManager.logout("");
//	}
//
//	@Test(expected = HttpStatusException.class)
//	public void testLogout() throws HttpStatusException {
//		tokenManager.logout(payloadUser_bp_admin);
//	}

//	@Test(expected = HttpStatusException.class)
//	public void testCheckAuth_bp_admin() throws HttpStatusException {
//		tokenManager.checkAut(payloadUser_bp_admin);
//	}
//
//	@Test(expected = HttpStatusException.class)
//	public void testCheckAuth() throws HttpStatusException {
//		tokenManager.checkAut("");
//	}

//	// @Test(expected = HttpStatusException.class)
//	@Test
//	public void testlogoutAdmin() throws HttpStatusException {
//
//		when(restServiceWrapper.performGetRequest(anyString(), any())).thenReturn("OK");
//
//		try {
//			tokenManager.logout(payloadUser_bp_admin);
//		} catch (Exception e) {
//			assertNotNull(e);
//		}
//
//	}

	// @Test(expected = HttpStatusException.class)
//	@Test
//	public void test_userroles_bp_leser() throws HttpStatusException {
//		List<String> list = tokenManager.getUserRoles(payloadUser_bp_leser);
//		assertTrue(list.contains(Globals.KEYCLOAK_ROLE_BP_LESEBERECHTIGTE));
//	}

	@Test
	public void test_bp_leser() {
		assertTrue(jwtPayloadUser_bp_leser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_LESEBERECHTIGTE));
	}

	@Test
	public void test_bp_bearbeiter() {
		assertTrue(jwtPayloadUser_bp_bearbeiter.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER));
	}

	@Test
	public void test_bp_leiter() {
		assertTrue(jwtPayloadUser_bp_leiter.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER));
	}

	@Test
	public void test_bp_admin() {
		assertTrue(jwtPayloadUser_bp_admin.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_ADMIN));
	}

	@Test
	public void testRambouser() {
		assertFalse(jwtPayloadRamboUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_ADMIN));
		assertFalse(jwtPayloadRamboUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER));
		assertFalse(jwtPayloadRamboUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER));
		assertFalse(jwtPayloadRamboUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_BP_LESEBERECHTIGTE));
	}

	@Test(expected = HttpStatusException.class)
	public void testPayloadisEmpty() throws HttpStatusException {

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_LESEBERECHTIGTE, Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER,
				Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		tokenManager.checkAutLevel("", securityRoles);
	}

	// @Test(expected = HttpStatusException.class)
	@Test
	public void testPayload_bp_admin() throws HttpStatusException {

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		tokenManager.checkAutLevel(payloadUser_bp_admin, securityRoles);
	}

	// @Test(expected = HttpStatusException.class)
	@Test
	public void testPayload_bp_leiter() throws HttpStatusException {

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		tokenManager.checkAutLevel(payloadUser_bp_leiter, securityRoles);
	}

	// @Test(expected = HttpStatusException.class)
	@Test
	public void testPayload_bp_bearbeiter() throws HttpStatusException {

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER };

		tokenManager.checkAutLevel(payloadUser_bp_bearbeiter, securityRoles);
	}

	// @Test(expected = HttpStatusException.class)
	@Test
	public void testPayload_bp_leser() throws HttpStatusException {

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_LESEBERECHTIGTE };

		tokenManager.checkAutLevel(payloadUser_bp_leser, securityRoles);
	}

	@Test(expected = HttpStatusException.class)
	// @Test
	public void testPayload_bp_leser_to_admin() throws HttpStatusException {

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		tokenManager.checkAutLevel(payloadUser_bp_leser, securityRoles);
	}

	@Test
	public void testSecureTypeNone_bp_admin() throws HttpStatusException {
		String[] securityRoles = Globals.getAllRolls();

		tokenManager.checkAutLevel(payloadUser_bp_admin, securityRoles);
	}

	@Test
	public void testSecureTypeNone_bp_leiter() throws HttpStatusException {
		String[] securityRoles = Globals.getAllRolls();

		tokenManager.checkAutLevel(payloadUser_bp_leiter, securityRoles);
	}

	@Test
	public void testSecureTypeNone_bp_bearbeiter() throws HttpStatusException {
		String[] securityRoles = Globals.getAllRolls();

		tokenManager.checkAutLevel(payloadUser_bp_bearbeiter, securityRoles);
	}

	@Test
	public void testSecureTypeNone_bp_leser() throws HttpStatusException {
		String[] securityRoles = Globals.getAllRolls();

		tokenManager.checkAutLevel(payloadUser_bp_leser, securityRoles);
	}

	@Test(expected = HttpStatusException.class)
	public void testSecureTypeNone_RamboUser() throws HttpStatusException {
		String[] securityRoles = Globals.getAllRolls();

		// Rambo has no BP-Roll

		tokenManager.checkAutLevel(payloadRamboUser, securityRoles);
	}

	// // SecureType = NORMAL
	// @Test(expected = HttpStatusException.class)
	// public void testSecureTypeNormalAndRamboUser() throws HttpStatusException {
	// tokenManager.checkAutLevel(payloadRamboUser,
	// BaseWebService.SecureType.NORMAL);
	// }
	//
	// @Test
	// public void testSecureTypeNormalAndNormalUser() throws HttpStatusException {
	// tokenManager.checkAutLevel(payloadNormalUser,
	// BaseWebService.SecureType.NORMAL);
	// }
	//
	// @Test
	// public void testSecureTypeNormalAndSuperUser() throws HttpStatusException {
	// tokenManager.checkAutLevel(payloadSuperUser,
	// BaseWebService.SecureType.NORMAL);
	// }
	//
	// // SecureType = HIGH
	// @Test(expected = HttpStatusException.class)
	// public void testSecureTypeHighAndNormalUSer() throws HttpStatusException {
	// tokenManager.checkAutLevel(payloadNormalUser,
	// BaseWebService.SecureType.HIGH);
	// }
	//
	// @Test
	// public void testSecureTypeHighAndSuperUSer() throws HttpStatusException {
	// tokenManager.checkAutLevel(payloadSuperUser, BaseWebService.SecureType.HIGH);
	// }

}
