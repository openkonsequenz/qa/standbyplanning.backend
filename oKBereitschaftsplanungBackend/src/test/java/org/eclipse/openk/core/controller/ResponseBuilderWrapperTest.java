/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.controller;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Response;
import org.apache.http.HttpStatus;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;

public class ResponseBuilderWrapperTest {
	@Test
	public void testGetResponseBuilder() throws HttpStatusException {
		String json = "{ 'ret' : 'OK' }";
		Response.ResponseBuilder rb = ResponseBuilderWrapper.INSTANCE.getResponseBuilder(json);
		Response resp = rb.build();
		assertEquals(HttpStatus.SC_OK, resp.getStatus());
	}

	@Test
	public void testBuildOkResponse() throws HttpStatusException {
		String json = "{ 'test' : 'Value' }";
		Response resp = ResponseBuilderWrapper.INSTANCE.buildOKResponse(json);
		assertEquals(HttpStatus.SC_OK, resp.getStatus());

		resp = ResponseBuilderWrapper.INSTANCE.buildOKResponse(json, "ssess");
		assertEquals(HttpStatus.SC_OK, resp.getStatus());

	}

	@Test(expected = HttpStatusException.class)
	public void testBuildOkResponse2() throws HttpStatusException {
		String json = "{ 'test' : 'Value' }";
		byte[] resp = ResponseBuilderWrapper.INSTANCE.jsonStringToBytes(json, "UTF-42");

	}
}
