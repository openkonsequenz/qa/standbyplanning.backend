/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.auth2.model;

import static junit.framework.TestCase.assertEquals;

import org.junit.Test;

public class JwtHeaderTest {

    @Test
    public void testGettersAndSetters(){

        JwtHeader jwtHeader = new JwtHeader();

        jwtHeader.setAlg("algTest");
        assertEquals("algTest", jwtHeader.getAlg());

        jwtHeader.setTyp("typTest");
        assertEquals("typTest", jwtHeader.getTyp());

        jwtHeader.setKid("kidTest");
        assertEquals("kidTest", jwtHeader.getKid());
    }
}
