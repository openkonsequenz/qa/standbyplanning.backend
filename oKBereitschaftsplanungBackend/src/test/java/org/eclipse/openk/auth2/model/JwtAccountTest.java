/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.auth2.model;

import static junit.framework.TestCase.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class JwtAccountTest {

    List<String> rolesList = new ArrayList<String>();

    @Before
    public void createList() {

        rolesList.add("role1");
        rolesList.add("role2");
        rolesList.add("role3");
    }


    @Test
    public void testGettersAndSetters() {

        JwtAccount jwtAccount = new JwtAccount();
        jwtAccount.setRoles(rolesList);
        assertEquals(rolesList, jwtAccount.getRoles());
    }

}
