# Notices for Eclipse openK User Modules

This content is produced and maintained by the Eclipse openK User Modules
project.

* Project home:
   https://projects.eclipse.org/projects/technology.openk-usermodules

## Trademarks

Eclipse openK User Modules is a trademark of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* http://git.eclipse.org/c/openk-usermodules/openk-usermodules.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.mics.centralService.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.mics.homeService.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.backend.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.frontend.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.backend.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.docu.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.frontend.git

## Third-party Content

This project leverages the following third party content.

Apache Commons Lang (2.6)

* License: Apache License, 2.0

Apache HttpClient (4.5.3)

* License: Apache-2.0

cglib (3.1)

* License: Apache License, 2.0, New BSD license

com.google.code.gson : gson : (2.8.5)

* License: Apache-2.0
* Project: https://github.com/google/gson
* Source: https://github.com/google/gson

commons-codec (1.10)

* License: Apache-2.0 AND BSD-3-Clause

commons-codec:commons-codec (1.10)

* License: Apache License, 2.0

commons-io (2.6)

* License: Apache License, 2.0

commons-io:commons-io (2.6)

* License: Apache-2.0

commons-lang (2.6)

* License: Apache License, 2.0

dozer V.5.5.1 (5.5.1)

* License: Apache License, 2.0

easymock (3.3.1)

* License: Apache License, 2.0

eclipselink (2.6.4)

* License: Eclipse Public License
* Project: https://mvnrepository.com/artifact/org.eclipse.persistence/eclipselink/2.6.4

h2 Database (1.3.168)

* License: Eclipse Public License

HikariCP-java7 (2.4.13)

* License: Apache-2.0
* Project: http://brettwooldridge.github.io/HikariCP/
* Source: https://github.com/brettwooldridge/HikariCP

jackson-core (2.8.6)

* License: Apache-2.0

jackson-databind (2.8.6)


jackson-dataformat-yaml (2.8.6)

* License: Apache-2.0

jasperreports (6.7.0)

* License:     GNU Library General Public License (LGPL)
* Project: https://community.jaspersoft.com/project/jasperreports-library
* Source: https://github.com/TIBCOSoftware/jasperreports/tree/release-6.7.0

javax.servlet-api (3.1.0)

* License: Apache-2.0 AND (CDDL-1.1 or GPL-2.0)

jersey-container-servlet-core (2.23.2)

* License: Common Development and Distribution License 1.1

jersey-media-json-jackson (2.23.2)

* License: CDDL-1.1 OR GPL-2.0 With Classpath-exception-2.0

jersey-spring3 (2.23.2)

* License: CDDL-1.1 OR GPL-2.0 With Classpath-exception-2.0
* Project:
   https://mvnrepository.com/artifact/org.glassfish.jersey.ext/jersey-spring3
* Source:
   https://repo1.maven.org/maven2/org/glassfish/jersey/ext/jersey-spring3/2.23.2/

jjwt (0.6.0)

* License: Apache License, 2.0

joda-time (2.9.4)

* License: Apache License, 2.0

JUnit (4.12)

* License: Eclipse Public License

mockito (1.9.5)

* License: Apache License, 2.0, New BSD license, MIT license

powermock-module-junit4-common (1.6.6)

* License: Apache-2.0

slf4j-api (1.7.5)

* License: MIT License + MIT License with no endorsement clause

slf4j-log4j12 (1.7.25)

* License: MIT

spring-data-jpa (1.9.1.RELEASE)

* Licence: Apache-2.0
* Project: https://spring.io/
* Source: https://github.com/spring-projects/spring-data-jpa/tree/1.9.1.RELEASE

spring-jdbc (4.3.17)

* License: Apache-2.0
* Project: https://spring.io/
* Source: 
   http://central.maven.org/maven2/org/springframework/spring-jdbc/4.3.17.RELEASE/

spring-test (4.3.17.RELEASE)

* License: Apache-2.0
* Project: https://spring.io/
* Source: http://central.maven.org/maven2/org/springframework/spring-test/3.2.17.RELEASE/

spring-web (4.3.17)

* License: Apache-2.0
* Project: https://spring.io/
* Source: https://github.com/spring-projects/spring-framework

swagger-jersey2-jaxrs (1.5.12)

* License: Apache-2.0

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.